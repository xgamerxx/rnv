﻿Imports System.Data.OleDb

Module functions_alumnos

    Dim s As New coreRNV.cConexion

    Function autoComplete(palabra As String)

        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cmd As New OleDbCommand("SELECT nombre_producto, desc_producto FROM tikal_productos WHERE nombre_producto LIKE '%" & palabra & "%' OR desc_producto LIKE '%" & palabra & "%' AND deleted = 'N'", cn)
            Dim ds As New DataSet
            Dim da As New OleDbDataAdapter(cmd)
            da.Fill(ds, "tikal_productos")
            Dim col As New AutoCompleteStringCollection
            Dim i As Integer
            Try
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    col.Add(ds.Tables(0).Rows(i)("nombre_producto").ToString() & " - " & ds.Tables(0).Rows(i)("desc_producto").ToString())
                Next

                frmInProductos.txtnproducto.AutoCompleteSource = AutoCompleteSource.CustomSource
                frmInProductos.txtnproducto.AutoCompleteCustomSource = col
                frmInProductos.txtnproducto.AutoCompleteMode = AutoCompleteMode.Suggest
                cn.Close()
            Catch ex As Exception
            End Try
        End Using

        Return False
    End Function
End Module
