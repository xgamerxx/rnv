﻿Imports System.Data.OleDb

Public Module functions_alumnos

    Dim s As New coreRNV.cConexion

    '' guardar / editar alumno
    Function salvar_alumno(codigo_alumno As String, nombres_alumno As String, telefono_1 As String, telefono_2 As String, telefono_3 As String, nombres_padre As String, nombres_madre As String, fecha_nacimiento_alumno As String, fecha_inscripcion As String, t_id As String, txt_direccion As String, txt_correo As String, ciclo As Integer) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = ""
                consulta = "SELECT * FROM rnv_alumnos WHERE t_id = '" & t_id & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' si el codigo existe entonces actualizar
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_alumnos SET codigo_alumno='" & codigo_alumno & "', nombres_alumno ='" & nombres_alumno & "', telefono_1 ='" & telefono_1 & "', telefono_2 ='" & telefono_2 & "', telefono_3 ='" & telefono_3 & "', nombres_padre ='" & nombres_padre & "', nombres_madre ='" & nombres_madre & "', fecha_nacimiento_alumno ='" & fecha_nacimiento_alumno & "', fecha_inscripcion ='" & fecha_inscripcion & "', direccion ='" & txt_direccion & "', correo ='" & txt_correo & "', ciclo=" & ciclo & " WHERE t_id = '" & t_id & "' ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                    '' editar tambien cursos
                    'DO
                    Return True
                Else
                    Dim updateNV = 0, updateVNET = 0
                    If main.ComboBox4.Text = "Academia NV" Then
                        updateNV = main.ComboBox5.SelectedIndex + 1
                    Else
                        updateVNET = main.ComboBox5.SelectedIndex + 1
                    End If
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_alumnos (codigo_alumno, nombres_alumno, telefono_1, telefono_2, telefono_3, nombres_padre, nombres_madre, fecha_nacimiento_alumno, fecha_inscripcion, t_id, fecha_registro, direccion, correo, ciclo, mes_pago_nv, mes_pago_vnet) VALUES ('" & codigo_alumno & "', '" & nombres_alumno & "', '" & telefono_1 & "', '" & telefono_2 & "', '" & telefono_3 & "', '" & nombres_padre & "', '" & nombres_madre & "', '" & fecha_nacimiento_alumno & "', '" & fecha_inscripcion & "', '" & t_id & "', '" & Date.Today & "', '" & txt_direccion & "', '" & txt_correo & "', " & ciclo & ", " & updateNV & ", " & updateVNET & ")")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    consulta = "SELECT * FROM rnv_alumnos WHERE t_id = '" & t_id & "'"
                    Dim cmd2 As New OleDbCommand(consulta, cn)
                    Dim reader2 As OleDbDataReader = cmd2.ExecuteReader()
                    If reader2.Read() Then
                        Try
                            For Each c As Control In main.FlowLayoutPanel1.Controls
                                If TypeOf c Is CheckBox Then
                                    Dim chk As CheckBox
                                    Dim count As Integer
                                    count += 1
                                    chk = CType(c, CheckBox)
                                    If chk.Checked Then
                                        Dim idcurso = chk.Name
                                        Dim idAlum = Convert.ToString(reader2(("id")))
                                        cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_alumnos_cursos (id_alumno, id_curso) VALUES (" & idAlum & ", " & idcurso & ")")
                                        cAdapter.InsertCommand.Connection = cn
                                        cAdapter.InsertCommand.ExecuteNonQuery()
                                    End If
                                End If
                            Next
                            main.verificarTimer()
                        Catch ex As Exception
                        End Try
                        main.verificarTimer()
                        Return True
                    End If
                End If
                cn.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    '' Eliminar el alumno y también eliminar asociacion curso / alumno
    Function borrar_alumno(codigo As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM rnv_alumnos WHERE t_id = '" & codigo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim idAlumno = Convert.ToString(reader(("id")))
                    Dim cmd2 As New OleDbCommand("DELETE * FROM rnv_alumnos WHERE t_id = '" & codigo & "'", cn)
                    cmd2.ExecuteNonQuery()
                    Dim cmd3 As New OleDbCommand("DELETE * FROM rnv_alumnos_cursos WHERE id_alumno = " & idAlumno & "", cn)
                    cmd3.ExecuteNonQuery()
                   main.verificarTimer()
                    Return True
                    cn.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function verificar_alumno(codigo_alumno As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM rnv_alumnos WHERE t_id = '" & codigo_alumno & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                   main.verificarTimer()
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function
End Module
