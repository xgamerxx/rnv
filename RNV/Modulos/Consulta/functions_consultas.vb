﻿Imports System.Data.OleDb
Imports coreRNV

Public Module functions_consultas
    Dim s As New cConexion

    '' para generador de tickets
    Public comentario As String = ""
    Public noRecibo As String = ""

    Sub consultarMegaCortes(idUser As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim consulta As String = "select * 
                                          FROM  rnv_corte_general WHERE codigo_corte = '" & idUser & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                   main.TextBox24.Text = Convert.ToString(reader(("emitido_por")))

                   main.TextBox23.Text = Convert.ToString(reader(("calculado_nv")))
                   main.TextBox18.Text = Convert.ToString(reader(("caja_nv")))
                   main.TextBox16.Text = Convert.ToString(reader(("diferencia_nv")))

                   main.TextBox22.Text = Convert.ToString(reader(("calculado_vnet")))
                   main.TextBox17.Text = Convert.ToString(reader(("caja_vnet")))
                   main.TextBox15.Text = Convert.ToString(reader(("diferencia_vnet")))

                   main.TextBox19.Text = "Q " & Convert.ToString(reader(("calculado_internet")))
                   main.TextBox9.Text = "Q " & Convert.ToString(reader(("caja_internet")))
                   main.TextBox12.Text = Convert.ToString(reader(("diferencia_internet")))

                   main.TextBox21.Text = "Q " & Convert.ToString(reader(("calculado_accesorios")))
                   main.TextBox11.Text = "Q " & Convert.ToString(reader(("caja_accesorios")))
                   main.TextBox8.Text = Convert.ToString(reader(("diferencia_accesorios")))

                   main.TextBox20.Text = "Q " & Convert.ToString(reader(("calculado_pelis")))
                   main.TextBox10.Text = "Q " & Convert.ToString(reader(("caja_pelis")))
                   main.TextBox7.Text = Convert.ToString(reader(("diferencia_pelis")))

                   main.TextBox26.Text = "Q " & Convert.ToString(reader(("calculado")))
                   main.TextBox25.Text = "Q " & Convert.ToString(reader(("caja")))
                   main.TextBox13.Text = "Q " & Convert.ToString(reader(("diferencia")))



                   main.RichTextBox1.Text = Convert.ToString(reader(("comentario")))
                End If
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub cargarProductosVendidosTicket()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT id, id_recibo, comentario 
                                      FROM rnv_cobros 
                                      WHERE id_recibo = '" &main.TextBox45.Text & "'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                comentario = Convert.ToString(reader(("comentario")))
                noRecibo = Convert.ToString(reader(("id_recibo")))
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_recibos_cobros WHERE id_recibo = " & Convert.ToInt32(reader(("id"))) & "", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                   main.gridTicket.Rows.Add(dtr(("descripcion")), dtr(("cantidad")), dtr(("precio")), dtr(("importe")))
                Next
            End If
            cn.Close()
        End Using
    End Sub

    Sub consultarCursos(idUser As Integer)
       main.FlowLayoutPanel2.Controls.Clear()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim objAdapter As New OleDbDataAdapter("
                 select rnv_cursos.id, rnv_cursos.nombre_curso  from ( rnv_alumnos INNER JOIN  rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id) INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso WHERE rnv_alumnos.id = " & idUser & "", cn)
            Dim objDataSet As New DataSet
            Dim tipo As String = "", nombre_beneficio As String = ""
            objAdapter.Fill(objDataSet, "flippy_niveles.nombre_nivel")
            Dim i As Integer
            For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                Dim maquinaCheck As New CheckBox
                With maquinaCheck
                    .Name = objDataSet.Tables(0).Rows(i).Item(0)
                    .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                    .Checked = True
                    .Enabled = False
                    .Text = objDataSet.Tables(0).Rows(i).Item(1)
                End With
               main.FlowLayoutPanel2.Controls.Add(maquinaCheck)
            Next
        End Using
    End Sub


    Sub consultarDatosUsuario(idUser As Integer)

        Try

            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim consulta As String = "select * 
                                          FROM  rnv_alumnos WHERE id = " & idUser & ""

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' mete los datos en variables
                   main.jtid.Text = Convert.ToInt32(reader(("id")))
                   main.TextBox1.Text = Convert.ToString(reader(("nombres_alumno")))
                   main.TextBox4.Text = Convert.ToString(reader(("codigo_alumno")))
                   main.MaskedTextBox1.Text = Convert.ToString(reader(("fecha_nacimiento_alumno")))
                   main.TextBox2.Text = mess(Convert.ToInt32(reader(("mes_pago_nv"))))
                   main.TextBox6.Text = mess(Convert.ToInt32(reader(("mes_pago_vnet"))))
                    If main.TextBox2.Text <> "" Then
                        Dim operarmes1 As Integer = Convert.ToInt32(reader(("mes_pago_nv"))) + (1)
                       main.TextBox3.Text = mess(operarmes1)
                    Else
                       main.TextBox3.Text = ""
                    End If
                    If main.TextBox6.Text <> "" Then
                        Dim operarmes2 As Integer = Convert.ToInt32(reader(("mes_pago_vnet"))) + (1)
                       main.TextBox5.Text = mess(operarmes2)
                    Else
                       main.TextBox6.Text = ""
                    End If
                End If
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Sub consultarCortes(idUser As String)

        Try

            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim consulta As String = "select * 
                                          FROM  rnv_corte WHERE codigo_corte = '" & idUser & "'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                   main.TextBox33.Text = Convert.ToString(reader(("giro")))
                   main.TextBox32.Text = Convert.ToString(reader(("emitido_por")))

                   main.TextBox27.Text = "Q " & Convert.ToString(reader(("caja")))
                   main.TextBox31.Text = Convert.ToString(reader(("calculado")))
                   main.TextBox30.Text = Convert.ToString(reader(("diferencia")))

                   main.TextBox29.Text = Convert.ToString(reader(("retiro")))
                   main.TextBox28.Text = Convert.ToString(reader(("retiro")))

                   main.RichTextBox2.Text = Convert.ToString(reader(("comentario")))
                End If
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


    Sub consultarUsuarios()
        Try
            '' cargar // clientes alumnos
           main.c_user.Items.Clear()
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim objAdapter As New OleDbDataAdapter("SELECT rnv_alumnos.id, rnv_alumnos.nombres_alumno FROM (rnv_alumnos INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id) INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso", cn)
                Dim objDataSet As New DataSet
                Dim nombre_nivel As String = ""
                objAdapter.Fill(objDataSet, "rnv_alumnos.nombres_alumno")
                Dim i As Integer
               main.ComboBox1.Items.Add("-ninguno-")

                For i = 0 To objDataSet.Tables(0).Rows.Count - 1

                    nombre_nivel = objDataSet.Tables(0).Rows(i).Item(1)
                    If nombre_nivel = "-ninguno-" Then
                    Else

                        If main.ComboBox1.FindString(objDataSet.Tables(0).Rows(i).Item(1) & "                                                                                                                                                                                                                                                                                                                                                                                                 " _
                                                                                                                 & "                                                                                           " _
                    & objDataSet.Tables(0).Rows(i).Item(0)) <> "-1" Then
                        Else
                           main.ComboBox1.Items.Add(objDataSet.Tables(0).Rows(i).Item(1) & "                                                                                                                                                                                                                                                                                                                                                                                                 " _
                                                                                     & "                                                                                           " _
& objDataSet.Tables(0).Rows(i).Item(0))
                        End If
                    End If
                Next
                cn.Close()
            End Using
           main.ComboBox1.SelectedIndex = 0
           main.ComboBox1.Enabled = True
           main.ComboBox1.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub consultarProductosInventario(idproducto As String)

        Try

            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim consulta As String = "SELECT * 
                                          FROM  rnv_inventario 
                                          LEFT JOIN  rnv_inventario_stock ON rnv_inventario_stock.id_producto = rnv_inventario.id 
                                          WHERE rnv_inventario.codigo = '" & idproducto & "'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                   main.cod_producto.Text = Convert.ToString(reader(("codigo")))
                   main.nombre_producto.Text = Convert.ToString(reader(("nombre_producto")))
                   main.precio_compra.Text = "Q" & Convert.ToString(reader(("precio_compra")))
                   main.precio_venta.Text = "Q" & Convert.ToString(reader(("precio_venta")))
                   main.precio_minimo.Text = "Q" & Convert.ToString(reader(("precio_minimo")))
                   main.descripcion_producto.Text = Convert.ToString(reader(("descripcion")))
                   main.tipo_de_producto.Text = Convert.ToString(reader(("tipo")))
                   main.stock_actual.Text = Convert.ToString(reader(("stock")))
                   main.Panel8.Visible = False
                End If
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub consultarVentas(idventa As String)

        Try

            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim consulta As String = "select * 
                                          FROM  rnv_cobros WHERE id_recibo = '" & idventa & "'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                   main.TextBox34.Text = Convert.ToString(reader(("id_recibo")))
                   main.TextBox40.Text = Convert.ToString(reader(("fecha")))
                   main.TextBox35.Text = Convert.ToString(reader(("total_venta")))
                   main.RichTextBox4.Text = Convert.ToString(reader(("comentario")))
                   main.Panel6.Visible = False
                End If
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub consultarStockProducto(codigo As String)
        ''sacar fechas buenas
        Dim str6 As String =main.DateTimePicker1.Value.ToString("dd/MM/yyyy")
        Dim str5 As String
        Dim convertidor3 As DateTime
        Dim hoy3 As String
        str6 = System.Text.RegularExpressions.Regex.Replace(str6, "[a-z]{2}(?=,)", String.Empty)
        Try
            str5 = DateTime.Parse(str6).ToString("yyyy/MM/dd")
            convertidor3 = str5
            hoy3 = DateTime.ParseExact(convertidor3, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        Catch ex As Exception
            str5 = DateTime.Parse(str6).ToString("MM/dd/yyyy")
            hoy3 = DateTime.ParseExact(str5, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        End Try

        Dim str3 As String =main.DateTimePicker2.Value.ToString("dd/MM/yyyy")
        Dim str4 As String
        Dim convertidor2 As DateTime
        Dim hoy2 As String
        str3 = System.Text.RegularExpressions.Regex.Replace(str3, "[a-z]{2}(?=,)", String.Empty)
        Try
            str4 = DateTime.Parse(str3).ToString("yyyy/MM/dd")
            convertidor2 = str4
            hoy2 = DateTime.ParseExact(convertidor2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        Catch ex As Exception
            str4 = DateTime.Parse(str3).ToString("MM/dd/yyyy")
            hoy2 = DateTime.ParseExact(str4, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        End Try

        If codigo <> "" Then
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                ''Si trae un codigo de producto
                Dim cmd As New OleDbCommand("SELECT rnv_inventario.nombre_producto, rnv_inventario_movimientos.movimiento, rnv_inventario_movimientos.comentario, rnv_inventario_movimientos.fecha   
                FROM rnv_inventario 
                INNER JOIN rnv_inventario_movimientos ON rnv_inventario_movimientos.id_producto = rnv_inventario.id
                WHERE (rnv_inventario_movimientos.fecha BETWEEN #" & hoy2 & "# AND #" & hoy3 & "#) AND  rnv_inventario.codigo = '" & codigo & "'", cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
               main.gridProductos.DataSource = dv
               main.gridProductos.Columns(0).HeaderText = "Producto del producto"
               main.gridProductos.Columns(1).HeaderText = "Movimiento"
               main.gridProductos.Columns(2).HeaderText = "Comentario"
               main.gridProductos.Columns(3).HeaderText = "Fecha"
                cn.Close()
            End Using
        Else
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                ''Si trae un codigo de producto
                Dim cmd As New OleDbCommand("SELECT rnv_inventario.nombre_producto, rnv_inventario_movimientos.movimiento, rnv_inventario_movimientos.comentario, rnv_inventario_movimientos.fecha   
                FROM rnv_inventario 
                INNER JOIN rnv_inventario_movimientos ON rnv_inventario_movimientos.id_producto = rnv_inventario.id
                WHERE (rnv_inventario_movimientos.fecha BETWEEN #" & hoy2 & "# AND #" & hoy3 & "#)", cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
               main.gridProductos.DataSource = dv
               main.gridProductos.Columns(0).HeaderText = "Producto del producto"
               main.gridProductos.Columns(1).HeaderText = "Movimiento"
               main.gridProductos.Columns(2).HeaderText = "Comentario"
               main.gridProductos.Columns(3).HeaderText = "Fecha"
                cn.Close()
            End Using
        End If

    End Sub
End Module
