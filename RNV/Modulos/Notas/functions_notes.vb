﻿Imports coreRNV
Imports System.Data.OleDb


Module functions_notes
    Dim s As New coreRNV.cConexion

    Function editarNota(idNota As Integer, contenido As String)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cAdapter As New OleDbDataAdapter
            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET contenido='" & contenido & "' WHERE id=" & idNota & "", cn)
            cAdapter.InsertCommand.Connection = cn
            cAdapter.InsertCommand.ExecuteNonQuery()
            Return True
        End Using
        Return False
    End Function

    Function borrar_nota(id As String)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cAdapter As New OleDbDataAdapter
            cAdapter.InsertCommand = New OleDbCommand("DELETE * FROM rnv_notas WHERE id=" & id & "", cn)
            cAdapter.InsertCommand.Connection = cn
            cAdapter.InsertCommand.ExecuteNonQuery()
            Return True
        End Using
        Return False
    End Function

    Function salvarNota(titulo As String, contenido As String, show As Boolean, expira As String, id As String)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cAdapter As New OleDbDataAdapter
            If show = True Then
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET show=0", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                If id <> "" Then
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET titulo = '" & titulo & "', contenido = '" & contenido & "', show=1 WHERE id = " & id & "", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                Else
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_notas (titulo, contenido, expira, show) VALUES ('" & titulo & "', '" & contenido & "', '" & expira & "', 1)")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                End If
            Else
                If id <> "" Then
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET titulo = '" & titulo & "', contenido = '" & contenido & "', expira = '" & expira & "', show=0 WHERE id = " & id & "", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                Else
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_notas (titulo, contenido, expira, show) VALUES ('" & titulo & "', '" & contenido & "', '" & expira & "', 0)")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                End If
            End If
                Return True
                cn.Close()
        End Using

        Return False
    End Function

    ''Metodo para verificar si la nota ya expiro.
    Sub verificarNotas()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cAdapter As New OleDbDataAdapter
            Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_notas", cn)
            Dim ds As New DataSet
            dt.Fill(ds)
            For Each dtr As DataRow In ds.Tables(0).Rows
                Dim id = Convert.ToInt32(dtr("id"))
                Dim expira = Convert.ToString(dtr(("expira")))
                Dim convertir As Date = expira
                Dim hoy As Date = Today.ToString("dd/MM/yyyy")
                If hoy >= convertir Then
                    '' Si alguna nota ya expiró. Le cambia el estado
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET show = 0 WHERE id = " & id & "", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                End If
            Next
        End Using
    End Sub

End Module
