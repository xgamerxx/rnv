﻿Imports System.Data.OleDb

Public Module functions_cursos

    Dim s As New coreRNV.cConexion

    Function salvar_curso(curso As String, desc As String, precio As String, codigo As String, servi As String, ciclo As Integer) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM rnv_cursos WHERE codigo_curso = '" & codigo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' si el codigo existe entonces actualizar
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_cursos SET nombre_curso='" & curso & "', desc_curso ='" & desc & "', cobro_para = '" & servi & "', mensualidad_curso ='" & precio & "' ciclo = " & ciclo & " WHERE codigo_curso = '" & codigo & "' ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                    Return True
                Else
                    '' sino entonces insertar
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_cursos (nombre_curso, desc_curso, mensualidad_curso, fecha_registro, codigo_curso, cobro_para, ciclo) VALUES ('" & curso & "', '" & desc & "', '" & precio & "', '" & Date.Today & "', '" & codigo & "', '" & servi & "', " & ciclo & ")")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                    Return True
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
            cn.Close()
        End Using
        Return False
    End Function

    '' Eliminar el curso y también eliminar asociacion curso / alumno
    Function borrar_curso(codigo As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM rnv_cursos WHERE codigo_curso = '" & codigo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim idCurso = Convert.ToString(reader(("id")))
                    Dim cmd2 As New OleDbCommand("DELETE * FROM rnv_cursos WHERE codigo_curso = '" & codigo & "'", cn)
                    cmd2.ExecuteNonQuery()
                    Dim cmd3 As New OleDbCommand("DELETE * FROM rnv_alumnos_cursos WHERE id_curso = " & idCurso & "", cn)
                    cmd3.ExecuteNonQuery()
                   main.verificarTimer()
                    Return True
                    cn.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function verificar_curso(codigo As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM rnv_cursos WHERE codigo_curso = '" & codigo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                   main.verificarTimer()
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Public Sub recargarCursos()
       main.FlowLayoutPanel1.Controls.Clear()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim objAdapter As New OleDbDataAdapter("
                  select rnv_cursos.id, rnv_cursos.nombre_curso  from ( rnv_alumnos INNER JOIN  rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id) INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso WHERE rnv_alumnos.id = " &main.txt_realID.Text & "", cn)

            Dim objDataSet As New DataSet
            Dim tipo As String = "", nombre_beneficio As String = ""
            objAdapter.Fill(objDataSet, "rnv_cursos.nombre_curso")
            Dim i As Integer
            For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                Dim maquinaCheck As New CheckBox
                With maquinaCheck
                    .Name = objDataSet.Tables(0).Rows(i).Item(0)
                    .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                    .Checked = True
                    .Enabled = False
                    .Text = objDataSet.Tables(0).Rows(i).Item(1)
                End With
               main.FlowLayoutPanel1.Controls.Add(maquinaCheck)
            Next
        End Using
    End Sub

    Sub carga_cursos_manageNV(giro As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT id, nombre_curso from rnv_cursos WHERE ciclo = " & main.TextBox42.Text & " AND cobro_para = '" & giro & "'", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                Dim i As Integer
                i = 0
                For Each dtr As DataRow In ds.Tables(0).Rows
                    Dim maquinaCheck As New CheckBox
                    With maquinaCheck
                        .Name = CType(dtr("id"), Integer)
                        .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                        .Text = CType(dtr("nombre_curso"), String)
                    End With
                    main.FlowLayoutPanel1.Controls.Add(maquinaCheck)
                    i = i + 1
                Next
                cn.Close()
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Sub carga_cursos_manageVNET(giro As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT id, nombre_curso from rnv_cursos WHERE cobro_para = '" & giro & "'", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                Dim i As Integer
                i = 0
                For Each dtr As DataRow In ds.Tables(0).Rows
                    Dim maquinaCheck As New CheckBox
                    With maquinaCheck
                        .Name = CType(dtr("id"), Integer)
                        .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                        .Text = CType(dtr("nombre_curso"), String)
                    End With
                    main.FlowLayoutPanel1.Controls.Add(maquinaCheck)
                    i = i + 1
                Next
                cn.Close()
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Sub cargarCursoData(id As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT * FROM rnv_cursos WHERE id = " & id & ""
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
               main.cobro_precio.Text = Convert.ToString(reader(("mensualidad_curso")))
               main.txt_codigo.Text = Convert.ToString(reader(("codigo_curso")))
               main.txt_descripcion.Text = Convert.ToString(reader(("nombre_curso")))
            End If
            cn.Close()
        End Using
    End Sub
End Module
