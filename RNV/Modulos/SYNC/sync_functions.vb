﻿Imports System.Data.OleDb
Imports coreRNV

Public Module sync_functions
    ' instanciar la clase de conexión
    Private s As New cConexion

    Function getQueryNuevo() As String
        If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
            Return s.rutaSistema & "ESME_rnv_bdd.mdb"
        End If

        If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
            Return s.rutaSistema & "JEREZ_rnv_bdd.mdb"
        End If
        Return ""
    End Function

    ' Validar si existe la bdd nueva
    Function getQueryViejo() As String
        Return s.rutaSistema & "rnv_bdd.mdb"
    End Function

    '' ******************** EMPEZAR A MIGRAR **********************

    '/*** Tomar todos los datos ***/'

    Function getAlumnos() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_alumnos", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    Dim codigo_alumno = Convert.ToString(dtr(("codigo_alumno")))
                    Dim nombres_alumno = Convert.ToString(dtr(("nombres_alumno")))
                    Dim telefono_1 = Convert.ToString(dtr(("telefono_1")))
                    Dim telefono_2 = Convert.ToString(dtr(("telefono_2")))
                    Dim telefono_3 = Convert.ToString(dtr(("telefono_3")))
                    Dim nombres_padre = Convert.ToString(dtr(("nombres_padre")))
                    Dim nombres_madre = Convert.ToString(dtr(("nombres_padre")))
                    Dim fecha_nacimiento_alumno = Convert.ToString(dtr(("fecha_nacimiento_alumno")))
                    Dim fecha_inscripcion = Convert.ToString(dtr(("fecha_inscripcion")))
                    Dim t_id = Convert.ToString(dtr(("t_id")))
                    Dim fecha_registro = Convert.ToString(dtr(("fecha_registro")))
                    Dim direccion = Convert.ToString(dtr(("direccion")))
                    Dim mes_pago_nv = Convert.ToInt32(dtr(("mes_pago_nv")))
                    Dim mes_pago_vnet = Convert.ToInt32(dtr(("mes_pago_vnet")))
                    Dim correo = Convert.ToString(dtr(("correo")))
                    Dim ciclo = Convert.ToInt32(dtr(("ciclo")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_alumnos WHERE t_id = '" & t_id & "'"
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_alumnos SET codigo_alumno = '" & codigo_alumno & "', nombres_alumno = '" & nombres_alumno & "', telefono_1 = '" & telefono_1 & "', telefono_2 = '" & telefono_2 & "', telefono_3 = '" & telefono_3 & "', nombres_padre = '" & nombres_padre & "', nombres_madre = '" & nombres_madre & "', fecha_nacimiento_alumno = '" & fecha_nacimiento_alumno & "', fecha_inscripcion = '" & fecha_inscripcion & "', direccion = '" & direccion & "', mes_pago_nv = " & mes_pago_nv & ", mes_pago_vnet = " & mes_pago_vnet & ", correo = '" & correo & "', ciclo = " & ciclo & " WHERE t_id = '" & t_id & "' ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_alumnos (codigo_alumno, nombres_alumno, telefono_1,
                                                               telefono_2, telefono_3, nombres_padre, nombres_madre, fecha_nacimiento_alumno,
                                                                fecha_inscripcion, t_id, fecha_registro, direccion, mes_pago_nv, mes_pago_vnet, correo, ciclo) 
                                                       values ('" & codigo_alumno & "', '" & nombres_alumno & "',
                                                       '" & telefono_1 & "', 
                                                       '" & telefono_2 & "', '" & telefono_3 & "', 
                                                       '" & nombres_padre & "', '" & nombres_madre & "', 
                                                       '" & fecha_nacimiento_alumno & "', '" & fecha_inscripcion & "',
                                                       '" & t_id & "',
                                                       '" & fecha_registro & "',
                                                       '" & direccion & "',
                                                       " & mes_pago_nv & ",
                                                       '" & correo & "',
                                                       " & mes_pago_vnet & ",
                                                       " & ciclo & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                main.Timer1.Stop()
                Return getAlumnosCursos()
                cn.Close()
            End Using
        Catch ex As Exception
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            MsgBox(ex.Message, vbInformation, "1")
            Return False
        End Try
        Return False
    End Function

    Private Sub vaciarTablaCursos()
        Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
            cnx.Open()
            Dim cAdapter As New OleDbDataAdapter
            cAdapter.InsertCommand = New OleDbCommand("DELETE * FROM  rnv_alumnos_cursos", cnx)
            cAdapter.InsertCommand.Connection = cnx
            cAdapter.InsertCommand.ExecuteNonQuery()
            cnx.Close()
        End Using
    End Sub

    Function getAlumnosCursos() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                vaciarTablaCursos()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_alumnos_cursos", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim id_alumno = Convert.ToInt32(dtr(("id_alumno")))
                    Dim id_curso = Convert.ToInt32(dtr(("id_curso")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim cAdapter As New OleDbDataAdapter
                        cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_alumnos_cursos (id_alumno, id_curso) 
                                                       values (
                                                       " & id_alumno & ",
                                                       " & id_curso & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()

                            cnx.Close()
                    End Using
                Next
                Return getCobros()
                cn.Close()
            End Using
        Catch ex As Exception
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            MsgBox(ex.Message, vbInformation, "2")
            Return False
        End Try
        Return False
    End Function

    Function getCobros() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_cobros", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim id_recibo = Convert.ToString(dtr(("id_recibo")))
                    Dim por = Convert.ToString(dtr(("por")))
                    Dim total_venta = Convert.ToString(dtr(("total_venta")))
                    Dim comentario = Convert.ToString(dtr(("comentario")))
                    Dim str As String = dtr("fecha")
                    Dim ciclo = Convert.ToInt32(dtr(("ciclo")))
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    Threading.Thread.Sleep(350)
                    Dim mes_pago = Convert.ToInt32(dtr(("mes_pago")))
                    Dim id_cliente = Convert.ToInt32(dtr(("id_cliente")))
                    Dim cut = Convert.ToInt32(dtr(("cut")))
                    Dim giro = Convert.ToString(dtr(("giro")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_cobros WHERE id_recibo = '" & id_recibo & "'"
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_cobros SET por = '" & por & "', total_venta = '" & total_venta & "', fecha = #" & hoy & "#, mes_pago = " & mes_pago & ", id_cliente = " & id_cliente & ", comentario = '" & comentario & "', cut = " & cut & ", giro = '" & giro & "', ciclo = " & ciclo & " WHERE id_recibo = '" & id_recibo & "' ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_cobros (id_recibo, por, total_venta, fecha, mes_pago, id_cliente, comentario, cut, giro, ciclo) 
                                                       values (
                                                       '" & id_recibo & "',
                                                        '" & por & "',
                                                        '" & total_venta & "',
                                                        #" & hoy & "#,
                                                       " & mes_pago & ",
                                                       " & id_cliente & ",
                                                       '" & comentario & "',
                                                       " & cut & ",
                                                       '" & giro & "',
                                                       " & ciclo & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True

                        End If
                        cnx.Close()
                    End Using


                Next
                Return getConfig()
                cn.Close()
            End Using
        Catch ex As Exception
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            MsgBox(ex.Message, vbInformation, "3")
            Return False
        End Try

        Return False
    End Function

    Function getConfig() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim consulta As String = "SELECT * FROM rnv_config"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' mete los datos en variables
                    Dim pass_admin = Convert.ToString(reader(("pass_admin")))
                    Dim pass_protected = Convert.ToString(reader(("pass_protected")))
                    Dim titulo_nv = Convert.ToString(reader(("titulo_nv")))
                    Dim titulo_vnet = Convert.ToString(reader(("titulo_vnet")))
                    Dim slogan_nv = Convert.ToString(reader(("slogan_nv")))
                    Dim slogan_vnet = Convert.ToString(reader(("slogan_vnet")))
                    Dim fb_nv = Convert.ToString(reader(("fb_nv")))
                    Dim fb_vnet = Convert.ToString(reader(("fb_vnet")))
                    Dim backup = Convert.ToString(reader(("backup")))
                    Dim mostrar_notas = Convert.ToInt32(reader(("mostrar_notas")))
                    Dim expira_notas = Convert.ToInt32(reader(("expira_notas")))
                    Dim texto_email = Convert.ToString(reader(("texto_email")))
                    Dim dia_minimo = Convert.ToInt32(reader(("dia_minimo")))
                    Dim dia_maximo = Convert.ToInt32(reader(("dia_maximo")))
                    Dim tipo_exedicion = Convert.ToString(reader(("tipo_exedicion")))
                    Dim cobro = Convert.ToString(reader(("cobro")))
                    Dim mora = Convert.ToInt32(reader(("mora")))
                    Dim ciclo = Convert.ToInt32(reader(("ciclo_actual")))
                    'migra la data obtenida solo lo que no exista
                    Return insertConfig(pass_admin, titulo_nv, titulo_vnet, slogan_nv, slogan_vnet, fb_nv, fb_vnet, backup, pass_protected, mostrar_notas, expira_notas, texto_email, dia_minimo, dia_maximo, tipo_exedicion, cobro, mora, ciclo)
                Else
                    Return True
                End If
                cn.Close()
            End Using
        Catch ex As Exception
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            MsgBox(ex.Message)
            Return False
        End Try

        Return False
    End Function

    Function insertConfig(pass_admin As String, titulo_nv As String, titulo_vnet As String, slogan_nv As String, slogan_vnet As String, fb_nv As String, fb_vnet As String, backup As String, pass_protected As String, mostrar_notas As Integer, expira_notas As Integer, texto_email As String, dia_minimo As Integer, dia_maximo As Integer, tipo_exedicion As String, cobro As String, mora As Integer, ciclo As Integer)
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                cn.Open()
                Dim cAdapter As New OleDbDataAdapter
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET titulo_vnet='" & titulo_vnet & "', slogan_vnet ='" & slogan_vnet & "', fb_vnet ='" & fb_vnet & "', pass_protected = '" & pass_protected & "', titulo_nv='" & titulo_nv & "', slogan_nv ='" & slogan_nv & "', backup='" & backup & "', fb_nv ='" & fb_nv & "', pass_admin='" & pass_admin & "', mostrar_notas=" & mostrar_notas & ", expira_notas=" & expira_notas & ", texto_email='" & texto_email & "', dia_minimo = " & dia_minimo & ", dia_maximo = " & dia_maximo & ", tipo_exedicion = '" & tipo_exedicion & "', cobro = '" & cobro & "', mora = " & mora & ", ciclo_actual=" & ciclo & " ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                ''si la operación se realizó con con exito manda un True
                Return getCursos()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "4")
            Return False
        End Try
        '' si la operación falla manda un false
        Return False
    End Function

    Function getCursos() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()

                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_cursos", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim nombre_curso = Convert.ToString(dtr(("nombre_curso")))
                    Dim desc_curso = Convert.ToString(dtr(("desc_curso")))
                    Dim mensualidad_curso = Convert.ToString(dtr(("mensualidad_curso")))
                    Dim fecha_registro = Convert.ToString(dtr(("fecha_registro")))
                    Dim codigo_curso = Convert.ToString(dtr(("codigo_curso")))
                    Dim cobro_para = Convert.ToString(dtr(("cobro_para")))
                    Dim ciclo = Convert.ToInt32(dtr(("ciclo")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_cursos WHERE codigo_curso = '" & codigo_curso & "'"
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_cursos SET nombre_curso =  '" & nombre_curso & "', desc_curso = '" & desc_curso & "', mensualidad_curso = '" & mensualidad_curso & "', fecha_registro = '" & fecha_registro & "', cobro_para = '" & cobro_para & "', ciclo = " & ciclo & " WHERE codigo_curso = '" & codigo_curso & "' ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_cursos (nombre_curso, desc_curso, mensualidad_curso, fecha_registro, codigo_curso, cobro_para, ciclo) 
                                                       values (
                                                       '" & nombre_curso & "',
                                                        '" & desc_curso & "',
                                                        '" & mensualidad_curso & "',
                                                        '" & fecha_registro & "',
                                                       '" & codigo_curso & "',
                                                       '" & cobro_para & "',
                                                       " & ciclo & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True

                        End If
                        cnx.Close()
                    End Using

                Next
                Return getRecibosCobros()
                cn.Close()
            End Using
        Catch ex As Exception
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            MsgBox(ex.Message, vbInformation, "5")
            Return False
        End Try

        Return False
    End Function

    Function getRecibosCobros() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_recibos_cobros", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim id_recibo = Convert.ToInt32(dtr(("id_recibo")))
                    Dim id_alumno = Convert.ToInt32(dtr(("id_alumno")))
                    Dim descripcion = Convert.ToString(dtr(("descripcion")))
                    Dim cantidad = Convert.ToString(dtr(("cantidad")))
                    Dim precio = Convert.ToString(dtr(("precio")))
                    Dim importe = Convert.ToString(dtr(("importe")))
                    Dim ciclo = Convert.ToInt32(dtr("ciclo"))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_recibos_cobros WHERE id_recibo = " & id_recibo & " AND id_alumno = " & id_alumno & ""
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_recibos_cobros SET descripcion = '" & descripcion & "', cantidad = '" & cantidad & "', precio = '" & precio & "', importe = '" & importe & "', ciclo = " & ciclo & " WHERE id_recibo = " & id_recibo & " AND id_alumno = " & id_alumno & " ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_recibos_cobros (id_recibo, id_alumno, descripcion, cantidad, precio, importe, ciclo) 
                                                       values (
                                                       " & id_recibo & ",
                                                        " & id_alumno & ",
                                                        '" & descripcion & "',
                                                        '" & cantidad & "',
                                                       '" & precio & "',
                                                       '" & importe & "',
                                                       " & ciclo & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True

                        End If
                        cnx.Close()
                    End Using

                Next
                Return getReportes()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "7")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try

        Return False
    End Function


    Function getReportes() As Boolean
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_reportes", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim codigo_reporte = Convert.ToString(dtr(("codigo_reporte")))
                    Dim emitido_por = Convert.ToString(dtr(("emitido_por")))
                    Dim para = Convert.ToString(dtr(("para")))
                    Dim fecha = Convert.ToString(dtr(("fecha")))
                    Dim sucur = Convert.ToString(dtr(("sucur")))
                    Dim ciclo = Convert.ToInt32(dtr(("ciclo")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_reportes WHERE codigo_reporte = '" & codigo_reporte & "'"
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_reportes SET emitido_por = '" & emitido_por & "', para = '" & para & "', fecha = '" & fecha & "', sucur = '" & sucur & "', ciclo = " & ciclo & " WHERE codigo_reporte = '" & codigo_reporte & "' ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_reportes (codigo_reporte, emitido_por, para, fecha, sucur, ciclo) 
                                                       values (
                                                       '" & codigo_reporte & "',
                                                        '" & emitido_por & "',
                                                        '" & para & "',
                                                       '" & fecha & "',
                                                       '" & sucur & "',
                                                       " & ciclo & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return getCorte()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "8")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try
        Return False
    End Function

    Function getCorte()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_corte", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim codigo_corte = Convert.ToString(dtr(("codigo_corte")))
                    Dim emitido_por = Convert.ToString(dtr(("emitido_por")))
                    Dim calculado = Convert.ToString(dtr(("calculado")))
                    Dim diferencia = Convert.ToString(dtr(("diferencia")))
                    Dim caja = Convert.ToString(dtr(("caja")))
                    Dim retiro = Convert.ToString(dtr(("retiro")))
                    Dim fecha = Convert.ToString(dtr(("fecha")))
                    Dim fecha_2 = Convert.ToString(dtr(("fecha_2")))
                    Dim giro = Convert.ToString(dtr(("giro")))
                    Dim comentario = Convert.ToString(dtr(("comentario")))
                    Dim cut = Convert.ToInt32(dtr(("cut")))
                    Dim ciclo = Convert.ToInt32(dtr(("ciclo")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_corte WHERE codigo_corte = '" & codigo_corte & "'"
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_corte SET  emitido_por = '" & emitido_por & "', calculado = '" & calculado & "', diferencia = '" & diferencia & "', caja = '" & caja & "', retiro = '" & retiro & "', fecha = '" & fecha & "', fecha_2 = '" & fecha_2 & "', giro = '" & giro & "', comentario = '" & comentario & "', cut = " & cut & ", ciclo = " & ciclo & " WHERE codigo_corte = '" & codigo_corte & "' ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_corte (codigo_corte, emitido_por, calculado, diferencia, caja, retiro, fecha, giro, comentario, fecha_2, cut, ciclo) 
                                                       values (
                                                       '" & codigo_corte & "',
                                                        '" & emitido_por & "',
                                                        '" & calculado & "',
                                                       '" & diferencia & "',
                                                       '" & caja & "',
                                                       '" & retiro & "',
                                                       '" & fecha & "',
                                                       '" & giro & "',
                                                       '" & comentario & "',
                                                       '" & fecha_2 & "',
                                                       " & cut & ",
                                                       " & ciclo & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return get_corte_general()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "10")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try
        Return False
    End Function

    Function get_corte_general()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_corte_general", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim codigo_corte = Convert.ToString(dtr(("codigo_corte")))
                    Dim emitido_por = Convert.ToString(dtr(("emitido_por")))
                    Dim calculado = Convert.ToString(dtr(("calculado")))
                    Dim diferencia = Convert.ToString(dtr(("diferencia")))
                    Dim caja = Convert.ToString(dtr(("caja")))
                    Dim fecha = Convert.ToString(dtr(("fecha")))
                    Dim fecha_2 = Convert.ToString(dtr(("fecha_2")))
                    Dim comentario = Convert.ToString(dtr(("comentario")))

                    Dim calculado_vnet = Convert.ToString(dtr(("calculado_vnet")))
                    Dim caja_vnet = Convert.ToString(dtr(("caja_vnet")))
                    Dim diferencia_vnet = Convert.ToString(dtr(("diferencia_vnet")))

                    Dim calculado_nv = Convert.ToString(dtr(("calculado_nv")))
                    Dim caja_nv = Convert.ToString(dtr(("caja_nv")))
                    Dim diferencia_nv = Convert.ToString(dtr(("diferencia_nv")))

                    Dim calculado_internet = Convert.ToString(dtr(("calculado_internet")))
                    Dim caja_internet = Convert.ToString(dtr(("caja_internet")))
                    Dim diferencia_internet = Convert.ToString(dtr(("diferencia_internet")))

                    Dim calculado_pelis = Convert.ToString(dtr(("calculado_pelis")))
                    Dim caja_pelis = Convert.ToString(dtr(("caja_pelis")))
                    Dim diferencia_pelis = Convert.ToString(dtr(("diferencia_pelis")))

                    Dim calculado_accesorios = Convert.ToString(dtr(("calculado_accesorios")))
                    Dim caja_accesorios = Convert.ToString(dtr(("caja_accesorios")))
                    Dim diferencia_accesorios = Convert.ToString(dtr(("diferencia_accesorios")))
                    Dim ciclo = Convert.ToInt32(dtr(("ciclo")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_corte_general WHERE codigo_corte = '" & codigo_corte & "'"
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_corte_general SET calculado_vnet = '" & calculado_vnet & "', caja_vnet = '" & caja_vnet & "', diferencia_vnet = '" & diferencia_vnet & "', calculado_nv = '" & calculado_nv & "', caja_nv = '" & caja_nv & "', diferencia_nv = '" & diferencia_nv & "', calculado_internet = '" & calculado_internet & "', caja_internet = '" & caja_internet & "', diferencia_internet = '" & diferencia_internet & "', calculado_pelis = '" & calculado_pelis & "', caja_pelis = '" & caja_pelis & "', diferencia_pelis = '" & diferencia_pelis & "', calculado_accesorios = '" & calculado_accesorios & "', caja_accesorios = '" & caja_accesorios & "', diferencia_accesorios = '" & diferencia_accesorios & "', emitido_por = '" & emitido_por & "', calculado = '" & calculado & "', diferencia = '" & diferencia & "', caja = '" & caja & "', fecha = '" & fecha & "', fecha_2 = '" & fecha_2 & "', comentario = '" & comentario & "', ciclo = " & ciclo & " WHERE codigo_corte = '" & codigo_corte & "' ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_corte_general (codigo_corte, emitido_por, calculado_vnet, caja_vnet, diferencia_vnet, calculado_nv, caja_nv, diferencia_nv, calculado_internet, caja_internet, diferencia_internet, calculado_pelis, caja_pelis, diferencia_pelis, calculado_accesorios, caja_accesorios, diferencia_accesorios, fecha, fecha_2, comentario, calculado, caja, diferencia, ciclo)
                                                       values (
                                                       '" & codigo_corte & "',
                                                       '" & emitido_por & "',
                                                       '" & calculado_vnet & "',
                                                       '" & caja_vnet & "',
                                                       '" & diferencia_vnet & "',
                                                       '" & calculado_nv & "',
                                                       '" & caja_nv & "',
                                                       '" & diferencia_nv & "',
                                                       '" & calculado_internet & "',
                                                       '" & caja_internet & "',
                                                       '" & diferencia_internet & "',
                                                       '" & calculado_pelis & "',
                                                       '" & caja_pelis & "',
                                                       '" & diferencia_pelis & "',
                                                       '" & calculado_accesorios & "',
                                                       '" & caja_accesorios & "',
                                                       '" & diferencia_accesorios & "',
                                                       '" & fecha & "',
                                                       '" & fecha_2 & "',
                                                       '" & comentario & "',
                                                       '" & calculado & "',
                                                       '" & caja & "',
                                                       '" & diferencia & "',
                                                       " & ciclo & "
                                                       )")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return getNotas()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "9")
            Return False
        End Try
        Return False
    End Function

    Function getNotas()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_notas", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim titulo = Convert.ToString(dtr(("titulo")))
                    Dim contenido = Convert.ToString(dtr(("contenido")))
                    Dim expira = Convert.ToString(dtr(("expira")))
                    Dim show = Convert.ToInt32(dtr(("show")))
                    Dim id = Convert.ToInt32(dtr(("id")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_notas WHERE id = " & id & ""
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET titulo = '" & titulo & "', contenido = '" & contenido & "', expira = '" & expira & "', show = " & show & " WHERE id = " & id & " ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_notas (titulo, contenido, expira, show) 
                                                       values (
                                                       '" & titulo & "',
                                                        '" & contenido & "',
                                                        '" & expira & "',
                                                       " & show & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return getInventario()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "12")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try
        Return False
    End Function

    Function getInventario()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_inventario", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim id = Convert.ToInt32(dtr(("id")))
                    Dim codigo = Convert.ToString(dtr(("codigo")))
                    Dim nombre_producto = Convert.ToString(dtr(("nombre_producto")))
                    Dim precio_compra = Convert.ToString(dtr(("precio_compra")))
                    Dim precio_venta = Convert.ToString(dtr(("precio_venta")))
                    Dim precio_minimo = Convert.ToString(dtr(("precio_minimo")))
                    Dim descripcion = Convert.ToString(dtr(("descripcion")))
                    Dim tipo = Convert.ToString(dtr(("tipo")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_inventario WHERE id = " & id & ""
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_inventario SET codigo = '" & codigo & "', nombre_producto = '" & nombre_producto & "', precio_compra = '" & precio_compra & "', precio_venta = '" & precio_venta & "', precio_minimo = '" & precio_minimo & "', descripcion = '" & descripcion & "', tipo = '" & tipo & "' WHERE id = " & id & " ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario (codigo, nombre_producto, precio_compra, precio_venta, precio_minimo, descripcion, tipo) 
                                                       values (
                                                       '" & codigo & "',
                                                        '" & nombre_producto & "',
                                                        '" & precio_compra & "',
                                                       '" & precio_venta & "',
                                                        '" & precio_minimo & "',
                                                       '" & descripcion & "',
                                                        '" & tipo & "')")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return getInventarioMovimientos()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "13")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try
        Return False
    End Function

    Function getInventarioMovimientos()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_inventario_movimientos", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim id = Convert.ToInt32(dtr(("id")))
                    Dim movimiento = Convert.ToString(dtr(("movimiento")))
                    Dim id_producto = Convert.ToInt32(dtr(("id_producto")))
                    Dim comentario = Convert.ToString(dtr(("comentario")))
                    Dim tipo = Convert.ToString(dtr(("tipo")))
                    Dim str As String = dtr("fecha")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    Threading.Thread.Sleep(350)
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_inventario_movimientos WHERE id = " & id & ""
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_inventario_movimientos SET movimiento = '" & movimiento & "', id_producto = " & id_producto & ", comentario = '" & comentario & "', tipo = '" & tipo & "', fecha = #" & hoy & "# WHERE id = " & id & " ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario_movimientos (movimiento, id_producto, fecha, comentario, tipo) 
                                                       values (
                                                       '" & movimiento & "',
                                                        " & id_producto & ",
                                                        #" & hoy & "#,
                                                       '" & comentario & "',
                                                        '" & tipo & "')")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return getInventarioStock()
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "14")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try
        Return False
    End Function

    Function getInventarioStock()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getQueryViejo))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_inventario_stock", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    '' mete los datos en variables
                    Dim id = Convert.ToInt32(dtr(("id")))
                    Dim id_producto = Convert.ToInt32(dtr(("id_producto")))
                    Dim stock = Convert.ToInt32(dtr(("stock")))
                    Using cnx As New OleDbConnection(s.constructorBDD(getQueryNuevo))
                        cnx.Open()
                        Dim consulta As String = "SELECT * FROM rnv_inventario_stock WHERE id = " & id & ""
                        Dim cmd As New OleDbCommand(consulta, cnx)
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If reader.Read() Then
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_inventario_stock SET id_producto = " & id_producto & ", stock = " & stock & " WHERE id = " & id & " ", cnx)
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario_stock (id_producto, stock) 
                                                       values (
                                                       " & id_producto & ",
                                                       " & stock & ")")
                            cAdapter.InsertCommand.Connection = cnx
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''si la operación se realizó con con exito manda un True
                        End If
                        cnx.Close()
                    End Using
                Next
                Return True
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "15")
            Dim trace = New System.Diagnostics.StackTrace(ex, True)
            Return False
        End Try
        Return False
    End Function

    ''''''''============================================================

    ''Verificar si el SYNC se encuentra ocupado.
    ''Motivo: No se puede estar Sincronizando una misma base de datos en el mismo momento,
    ''debido a que podría generar duplicidad de datos o bien no podrían guardarse todos los datos
    ''recogidos de las bases de datos locales.
    Function verificarSiEstaOcupado() As Boolean
        Try
            Dim request As System.Net.HttpWebRequest = System.Net.WebRequest.Create("http://www.flippyescolar.com/app/api/RNV/isbusy.rnv")
            Dim response As System.Net.HttpWebResponse = request.GetResponse()
            Dim sr As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())
            Dim newestversion As String = sr.ReadToEnd()
            If newestversion.Contains("true") Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return True
        End Try
        Return True
    End Function
End Module