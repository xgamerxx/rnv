﻿Public Class Eencabezado_cobro
    'Clase propiedades de encabezado de la factura
    Public Property codigo_reporte() As String
    Public Property tipo_reporte() As String
    Public Property encabezado_reporte() As String
    Public Property total() As String
    Public Property logo() As String
    Public Property fecha() As String
    'Creamos una lista con una nueva Instancia de la clase personas cobro
    'esta lista contendra el detalle del reporte de cobro
    Public Detalle As New List(Of EPersonas_cobro)()
End Class
