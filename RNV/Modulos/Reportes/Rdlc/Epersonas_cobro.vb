﻿Public Class Epersonas_cobro
    'Clase de los personas en reporte cobro
    Public Property codigo_recibo() As String
    Public Property descripcion() As String
    Public Property nombre_completo() As String
    Public Property codigo_alumno() As String
    Public Property mes_pago() As String
    Public Property articulos() As String
    Public Property fecha() As String
    Public Property total_cobro As Decimal

End Class
