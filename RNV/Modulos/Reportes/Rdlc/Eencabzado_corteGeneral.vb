﻿Public Class Eencabezado_corteGeneral
    'Clase propiedades de encabezado de la factura
    Property fecha() As String
    Property emitido_por() As String
    Property codigo_corte() As String
    Property total_venta_nv() As String
    Property total_venta_vnet() As String
    Property total_venta_internet() As String
    Property total_venta_extras() As String
    Property total_venta_pelis() As String
    Property total_venta_accesorios() As String
    Property total_calculado_nv() As String
    Property total_calculado_extras() As String
    Property total_calculado_vnet() As String
    Property total_calculado_internet() As String
    Property total_calculado_pelis() As String
    Property total_calculado_accesorios() As String
    Property total_caja_nv() As String
    Property total_caja_extras() As String
    Property total_caja_vnet() As String
    Property total_caja_internet() As String
    Property total_caja_pelis() As String
    Property total_caja_accesorios() As String
    Property total_venta() As String
    Property total_calculado() As String
    Property total_caja() As String
    Property comentario() As String

    'Creamos una lista con una nueva Instancia de la clase personas cobro
    'esta lista contendra el detalle del reporte de cobro
    Public Detalle As New List(Of Eencabezado_corteGeneral)()
End Class
