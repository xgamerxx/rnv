﻿Imports System.Data.OleDb
Imports coreRNV

Public Module functions_reportes
    Dim s As New cConexion
    Function todos_los_clientes(para As String) As DataTable
        Dim dt As New DataTable()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT * from rnv_alumnos"
            Dim comando As New OleDbCommand(consulta, cn)
            Dim adap As New OleDbDataAdapter(comando)
            adap.Fill(dt)
            Return dt
        End Using
    End Function

    Function convertiFecha(fecha As Date) As String
        Return fecha.Year & fecha.Month.ToString.PadLeft(2, "0") & fecha.Day.ToString.PadLeft(2, "0")
    End Function

    Sub actualizar()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cAdapter As New OleDbDataAdapter
            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_cobros SET cut=1 WHERE giro = '" & view_corte.giro.Text & "' ", cn)
            cAdapter.InsertCommand.Connection = cn
            cAdapter.InsertCommand.ExecuteNonQuery()
            cn.Close()
        End Using
    End Sub

    Sub actualizar2()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim cAdapter As New OleDbDataAdapter
            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_corte SET cut=1 ", cn)
            cAdapter.InsertCommand.Connection = cn
            cAdapter.InsertCommand.ExecuteNonQuery()
            cn.Close()
        End Using
    End Sub

    Sub consulta_corte_inventario()
        Dim str As String = Date.Today.ToString("dd/MM/yyyy")
        Dim str2 As String
        Dim convertidor As DateTime
        Dim hoy As String
        str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
        Try
            str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
            convertidor = str2
            hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        Catch ex As Exception
            str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
            hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        End Try
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            ''Si trae un codigo de producto
            Dim cmd As New OleDbCommand("SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE fecha = #" & hoy & "# AND giro = '" & view_corte.giro.Text & "' AND rnv_cobros.cut = 0 ORDER BY id_recibo DESC", cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Dim dt As New DataTable
            adaptador.Fill(dt)
            Dim dv As DataView = dt.DefaultView
            grid_datos_cobros.gridinforme.DataSource = dv
            grid_datos_cobros.gridinforme.Columns(0).HeaderText = "ID Recibo"
            grid_datos_cobros.gridinforme.Columns(1).HeaderText = "Fecha"
            grid_datos_cobros.gridinforme.Columns(2).HeaderText = "Total Venta"
            cn.Close()
        End Using
    End Sub

    Sub consulta_corte()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim str As String = Date.Today.ToString("dd/MM/yyyy")
            Dim str2 As String
            Dim convertidor As DateTime
            Dim hoy As String
            str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
            Try
                str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                convertidor = str2
                hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
            Catch ex As Exception
                str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
            End Try
            Dim whereconsulta As String
            whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                             FROM ((rnv_cobros
                             INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                             INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id)
                             WHERE rnv_cobros.fecha = #" & hoy & "# AND rnv_cobros.giro = '" & view_corte.giro.Text & "' AND rnv_cobros.cut = 0 ORDER BY rnv_cobros.id_recibo DESC"
            Dim cmd As New OleDbCommand(whereconsulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Dim dt As New DataTable
            adaptador.Fill(dt)
            Dim dv As DataView = dt.DefaultView
            grid_datos_cobros.gridinforme.DataSource = dv
            grid_datos_cobros.gridinforme.Columns(0).HeaderText = "# Recibo"
            grid_datos_cobros.gridinforme.Columns(1).HeaderText = "mes pago"
            grid_datos_cobros.gridinforme.Columns(2).HeaderText = "Total venta"
            grid_datos_cobros.gridinforme.Columns(3).HeaderText = "nombres alumno"
            grid_datos_cobros.gridinforme.Columns(4).HeaderText = "curso / servicio"
            grid_datos_cobros.gridinforme.Columns(5).HeaderText = "fecha"
            grid_datos_cobros.gridinforme.Columns(6).HeaderText = "Codigo estudiante"
            grid_datos_cobros.gridinforme.Columns(7).HeaderText = "cantidad"
            cn.Close()
        End Using
    End Sub

    '///Función para crear reporte de ventas de inventario
    Sub informe_ventas_inventario(ByVal consulta As Integer, ByVal mes_integer As Integer, ByVal mes_integer_mes_año As Integer)
        Dim consulta_tipo, texto_consulta, whereconsulta As String
        Dim m As Integer = mes_integer
        Dim m2 As Integer = mes_integer_mes_año
        Dim y As Integer = Year(Now)
        Dim y2 As Integer = main.cañoe.Text
        Dim dtini As New DateTime(y, m, 1)
        Dim dtfin As New DateTime(y, m, DateTime.DaysInMonth(y, m))
        Dim dtini2 As New DateTime(y2, m2, 1)
        Dim dtfin2 As New DateTime(y2, m2, DateTime.DaysInMonth(y2, m2))
        consulta_tipo = Nothing
        texto_consulta = Nothing
        whereconsulta = Nothing
        Dim str9 As String = Date.Today.AddDays(-1).ToString("dd/MM/yyyy")
        Dim str5 As String
        Dim convertidor3 As DateTime
        Dim last_fecha As String
        str9 = System.Text.RegularExpressions.Regex.Replace(str9, "[a-z]{2}(?=,)", String.Empty)
        Try
            str5 = DateTime.Parse(str9).ToString("yyyy/MM/dd")
            convertidor3 = str5
            last_fecha = DateTime.ParseExact(convertidor3, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        Catch ex As Exception
            str5 = DateTime.Parse(str9).ToString("MM/dd/yyyy")
            last_fecha = DateTime.ParseExact(str5, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
        End Try
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Select Case consulta
                'Reporte del dia
                Case 0
                    Dim str As String = Date.Today.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE fecha = #" & hoy & "# AND giro = 'Inventario NV' ORDER BY id_recibo DESC"
                Case 1
                    Dim str As String = Date.Today.AddDays(-1).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE fecha = #" & hoy & "# AND giro = 'Inventario NV' ORDER BY id_recibo DESC"
                Case 2
                    'Reporte de la última semana
                    Dim str As String = Date.Today.AddDays(-7).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE (fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND giro = 'Inventario NV' AND ORDER BY id_recibo DESC"
                Case 3
                    'Reporte de la último mes
                    Dim str As String = Date.Today.AddDays(-30).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE (fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND giro = 'Inventario NV' ORDER BY id_recibo DESC"
                Case 4
                    'Reporte de la ultimo año
                    Dim str As String = Date.Today.AddDays(-365).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE (fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND giro = 'Inventario NV' ORDER BY id_recibo DESC"
                Case 5
                    'Reporte fecha especifica
                    Dim str As String = main.datafecha.Value.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE fecha = #" & hoy & "# AND giro = 'Inventario NV' ORDER BY id_recibo DESC"
                Case 6
                    Dim str As String = main.datafecha1.Value.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try

                    Dim str3 As String = main.datafecha2.Value.ToString("dd/MM/yyyy")
                    Dim str4 As String
                    Dim convertidor2 As DateTime
                    Dim hoy2 As String
                    str3 = System.Text.RegularExpressions.Regex.Replace(str3, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str4 = DateTime.Parse(str3).ToString("yyyy/MM/dd")
                        convertidor2 = str4
                        hoy2 = DateTime.ParseExact(convertidor2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str4 = DateTime.Parse(str3).ToString("MM/dd/yyyy")
                        hoy2 = DateTime.ParseExact(str4, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE (fecha BETWEEN #" & hoy & "# AND #" & hoy2 & "#) AND giro = 'Inventario NV' ORDER BY id_recibo DESC"
                Case 7
                    whereconsulta = "SELECT id_recibo, fecha, total_venta 
                       FROM rnv_cobros
                       WHERE giro = 'Inventario NV' AND ORDER BY id_recibo DESC"
            End Select
            ''Si trae un codigo de producto

            Dim cmd As New OleDbCommand(whereconsulta, cn)
            cmd.Parameters.AddWithValue("@fecha1", dtini.ToString("dd/MM/yyy"))
            cmd.Parameters.AddWithValue("@fecha2", dtfin.ToString("dd/MM/yyy"))
            cmd.Parameters.AddWithValue("@fecha3", "01/01/" & main.c_año.Text.ToString)
            cmd.Parameters.AddWithValue("@fecha4", "31/12/" & main.c_año.Text.ToString)
            cmd.Parameters.AddWithValue("@fecha5", dtini2.ToString("dd/MM/yyy"))
            cmd.Parameters.AddWithValue("@fecha6", dtfin2.ToString("dd/MM/yyy"))
            Dim adaptador As New OleDbDataAdapter(cmd)
            Dim dt As New DataTable
            adaptador.Fill(dt)
            Dim dv As DataView = dt.DefaultView
            grid_datos_cobros.gridinforme.DataSource = dv
            grid_datos_cobros.gridinforme.Columns(0).HeaderText = "ID Recibo"
            grid_datos_cobros.gridinforme.Columns(1).HeaderText = "Fecha"
            grid_datos_cobros.gridinforme.Columns(2).HeaderText = "Total Venta"
            cn.Close()
        End Using
    End Sub

    '///Función para crear reporte de ventas
    Sub informe_rapido(ByVal consulta As Integer, ByVal mes_integer As Integer, ByVal mes_integer_mes_año As Integer, id As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta_tipo, texto_consulta, whereconsulta As String
            Dim m As Integer = mes_integer
            Dim m2 As Integer = mes_integer_mes_año
            Dim y As Integer = Year(Now)
            Dim y2 As Integer =main.cañoe.Text
            Dim dtini As New DateTime(y, m, 1)
            Dim dtfin As New DateTime(y, m, DateTime.DaysInMonth(y, m))
            Dim dtini2 As New DateTime(y2, m2, 1)
            Dim dtfin2 As New DateTime(y2, m2, DateTime.DaysInMonth(y2, m2))
            consulta_tipo = Nothing
            texto_consulta = Nothing
            whereconsulta = Nothing
            Dim str9 As String = Date.Today.AddDays(-1).ToString("dd/MM/yyyy")
            Dim str5 As String
            Dim convertidor3 As DateTime
            Dim last_fecha As String
            str9 = System.Text.RegularExpressions.Regex.Replace(str9, "[a-z]{2}(?=,)", String.Empty)
            Try
                str5 = DateTime.Parse(str9).ToString("yyyy/MM/dd")
                convertidor3 = str5
                last_fecha = DateTime.ParseExact(convertidor3, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
            Catch ex As Exception
                str5 = DateTime.Parse(str9).ToString("MM/dd/yyyy")
                last_fecha = DateTime.ParseExact(str5, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
            End Try
            Select Case consulta
                'Reporte del dia
                Case 0
                    Dim str As String = Date.Today.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE rnv_cobros.fecha = #" & hoy & "# AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "(Hoy) " & Date.Today.ToString("dd/MM/yyy") & " " & "(Todos)"

                    ElseIf main.CheckBox2.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (((rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id)
                         INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id)
                         INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso
                         WHERE rnv_cobros.fecha = #" & hoy & "# AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = "(Hoy) " & Date.Today.ToString("dd/MM/yyy") & " " & "(" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"

                    End If
                    'ayer
                Case 1
                    Dim str As String = Date.Today.AddDays(-1).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & hoy & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "(Ayer) " & Date.Today.AddDays(-1).ToString("dd/MM/yyy") & " " & "(Todos)"

                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id)
                         INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & hoy & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = "(Semana pasada) " & Date.Today.AddDays(-1).ToString("dd/MM/yyy") & " " & "(" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"
                    End If
                Case 2
                    'Reporte de la última semana
                    Dim str As String = Date.Today.AddDays(-7).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "(Última semana) " & Date.Today.AddDays(-7).ToString("dd/MM/yyy") & " " & "(Todos)"

                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = "(Última semana) " & Date.Today.AddDays(-7).ToString("dd/MM/yyy") & " " & "(" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"
                    End If
                Case 3
                    'Reporte de la último mes
                    Dim str As String = Date.Today.AddDays(-30).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "(Último mes) " & Date.Today.AddDays(-30).ToString("dd/MM/yyy") & " " & "(Todos)"

                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = "(Últimos mes) " & Date.Today.AddDays(-30).ToString("dd/MM/yyy") & " " & "(" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"
                    End If
                Case 4
                    'Reporte de la último año
                    Dim str As String = Date.Today.AddDays(-365).ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "(Último mes) " & Date.Today.AddDays(-365).ToString("dd/MM/yyy") & " " & "(Todos)"

                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & last_fecha & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = "(Últimos mes) " & Date.Today.AddDays(-365).ToString("dd/MM/yyy") & " " & "(" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"
                    End If

                Case 5
                    'Reporte fecha especifica
                    Dim str As String = main.datafecha.Value.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE rnv_cobros.fecha = #" & hoy & "# AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = " " & main.datafecha.Value.ToString("dd/MM/yyy") & " (Todos)"

                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE rnv_cobros.fecha = #" & hoy & "# AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = " " & main.datafecha.Value.ToString("dd/MM/yyy") & " (" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"

                    End If
                Case 6
                    'Reporte de fecha a fecha especifica

                    Dim str As String = main.datafecha1.Value.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try

                    Dim str3 As String = main.datafecha2.Value.ToString("dd/MM/yyyy")
                    Dim str4 As String
                    Dim convertidor2 As DateTime
                    Dim hoy2 As String
                    str3 = System.Text.RegularExpressions.Regex.Replace(str3, "[a-z]{2}(?=,)", String.Empty)
                    Try
                        str4 = DateTime.Parse(str3).ToString("yyyy/MM/dd")
                        convertidor2 = str4
                        hoy2 = DateTime.ParseExact(convertidor2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str4 = DateTime.Parse(str3).ToString("MM/dd/yyyy")
                        hoy2 = DateTime.ParseExact(str4, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & hoy2 & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "" & main.datafecha1.Value.ToString("dd/MM/yyy") & " al " & main.datafecha2.Value.ToString("dd/MM/yyy") & " (Todos)"

                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE (rnv_cobros.fecha BETWEEN #" & hoy & "# AND #" & hoy2 & "#) AND rnv_cobros.giro = '" & main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String = main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                        main.texto_consulta.Text = "" & main.datafecha1.Value.ToString("dd/MM/yyy") & " al " & main.datafecha2.Value.ToString("dd/MM/yyy") & " (" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"

                    End If
                Case 7
                    'Reporte de la último año
                    consulta_tipo = "01-01-1990"
                    If main.CheckBox1.Checked = True Then
                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE rnv_cobros.giro = '" & main.reporte_para.Text & "' ORDER BY rnv_cobros.id_recibo DESC"

                        main.texto_consulta.Text = "(Reporte completo) " & " " & "(Todos)"
                    ElseIf main.CheckBox2.Checked = True Then

                        whereconsulta = "SELECT rnv_cobros.id_recibo, rnv_cobros.mes_pago, rnv_recibos_cobros.precio, rnv_alumnos.nombres_alumno, rnv_recibos_cobros.descripcion, rnv_cobros.fecha, rnv_alumnos.codigo_alumno, rnv_recibos_cobros.cantidad
                         FROM (rnv_cobros
                         INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente)
                         INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                         WHERE rnv_cobros.giro = '" &main.reporte_para.Text & "' AND rnv_alumnos.id = " & id & " ORDER BY rnv_cobros.id_recibo DESC"
                        Dim ff As String =main.c_user.Text
                        Dim id_ben As String
                        id_ben = Microsoft.VisualBasic.Right(ff, 9)
                       main.texto_consulta.Text = "(Reporte completo) " & " " & "(" & Convert.ToString(Microsoft.VisualBasic.Left(ff, 15)) & ")"
                    End If
            End Select
            Dim cmd As New OleDbCommand(whereconsulta, cn)
            cmd.Parameters.AddWithValue("@fecha1", dtini.ToString("dd/MM/yyy"))
            cmd.Parameters.AddWithValue("@fecha2", dtfin.ToString("dd/MM/yyy"))
            cmd.Parameters.AddWithValue("@fecha3", "01/01/" &main.c_año.Text.ToString)
            cmd.Parameters.AddWithValue("@fecha4", "31/12/" &main.c_año.Text.ToString)
            cmd.Parameters.AddWithValue("@fecha5", dtini2.ToString("dd/MM/yyy"))
            cmd.Parameters.AddWithValue("@fecha6", dtfin2.ToString("dd/MM/yyy"))
            Dim adaptador As New OleDbDataAdapter(cmd)
            Dim dt As New DataTable
            adaptador.Fill(dt)
            Dim dv As DataView = dt.DefaultView
            grid_datos_cobros.gridinforme.DataSource = dv
            grid_datos_cobros.gridinforme.Columns(0).HeaderText = "# Recibo"
            grid_datos_cobros.gridinforme.Columns(1).HeaderText = "mes pago"
            grid_datos_cobros.gridinforme.Columns(2).HeaderText = "Total venta"
            grid_datos_cobros.gridinforme.Columns(3).HeaderText = "nombres alumno"
            grid_datos_cobros.gridinforme.Columns(4).HeaderText = "curso / servicio"
            grid_datos_cobros.gridinforme.Columns(5).HeaderText = "fecha"
            grid_datos_cobros.gridinforme.Columns(6).HeaderText = "Codigo estudiante"
            grid_datos_cobros.gridinforme.Columns(7).HeaderText = "cantidad"
            cn.Close()
        End Using
    End Sub

    Function salvar_reporte(codigo As String, de As String, para As String, fecha As String, sucur As String)

        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                '' sino entonces insertar
                Dim cAdapter As New OleDbDataAdapter
                cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_reportes (codigo_reporte, emitido_por, para, fecha, sucur) VALUES ('" & codigo & "', '" & de & "', '" & para & "', '" & Date.Today & "', '" & sucur & "')")
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
            cn.Close()
        End Using

        Return False
    End Function
End Module