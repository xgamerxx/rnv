﻿Imports coreRNV
Imports System.Data.OleDb

Module inventario_functions
    Dim s As New cConexion

    Function cargarStock(codigo As String) As Integer
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta = ""
            Try
                consulta = "SELECT * FROM rnv_inventario WHERE codigo = '" & codigo & "'"
                Dim cmd2 As New OleDbCommand(consulta, cn)
                Dim reader2 As OleDbDataReader = cmd2.ExecuteReader()
                If reader2.Read() Then
                    Dim id = Convert.ToInt32(reader2(("id")))
                    consulta = "SELECT * FROM rnv_inventario_stock WHERE id_producto = " & id & ""
                    Dim cmd As New OleDbCommand(consulta, cn)
                    Dim reader As OleDbDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        Return Convert.ToInt32(reader(("stock")))
                    End If
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Using
        Return 0
    End Function

    Function actualizar_stock(stock As String, comentario As String, tipo As String, pure As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim str As String = Date.Today.ToString("yyyy/MM/dd")
            Dim str2 As String
            Dim convertidor As DateTime
            Dim hoy As String
            str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
            Try
                str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                convertidor = str2
                hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
            Catch ex As Exception
                Dim hoySinConvert As String = Date.Today.ToString("MM/dd/yyyy")
                hoy = DateTime.ParseExact(hoySinConvert, "M/d/yyyy",
                                 Globalization.CultureInfo.InstalledUICulture)
            End Try
            Dim consulta = "SELECT * FROM rnv_inventario WHERE codigo = '" &main.cod_inventario.Text & "'"
            Dim cmd2 As New OleDbCommand(consulta, cn)
            Dim reader2 As OleDbDataReader = cmd2.ExecuteReader()
            If reader2.Read() Then
                Dim id_producto = Convert.ToInt32(reader2(("id")))
                Dim cAdapter As New OleDbDataAdapter
                cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario_movimientos (movimiento, id_producto, fecha, comentario, tipo) VALUES ('" & stock & "', " & id_producto & ", #" & hoy & "#, '" & comentario & "', '" & tipo & "')")
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                Dim datos
                If tipo = "positivo" Then
                    datos = "stock + " & pure & ""
                Else
                    datos = "stock - " & pure & ""
                End If
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_inventario_stock SET stock = (" & datos & ") WHERE id_producto = " & id_producto & " ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
               main.verificarTimer()
                Return True
            End If
            cn.Close()
        End Using
        Return False
    End Function

    Function verificarStockProductos(id As String, cantidad As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta = ""
            Try
                consulta = "SELECT * FROM rnv_inventario_stock WHERE id_producto = " & id & ""
                Dim cmd2 As New OleDbCommand(consulta, cn)
                Dim reader2 As OleDbDataReader = cmd2.ExecuteReader()
                If reader2.Read() Then
                    Dim stock As Integer
                    stock = Convert.ToInt32(reader2(("stock")))
                    If stock >= cantidad Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
            End Try
        End Using
        Return False
    End Function

    Function salvarInventario(product As String, precio As String, venta As String, minimo As String, comentario As String, t_id As String, tipo As String, stock As Integer, codigo As String)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = ""
                Dim id_producto = 0
                consulta = "SELECT * FROM rnv_inventario WHERE codigo = '" & codigo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' si el codigo existe entonces actualizar
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_inventario SET nombre_producto = '" & product & "', precio_compra = '" & precio & "', precio_venta = '" & venta & "', precio_minimo = '" & minimo & "', descripcion = '" & comentario & "', tipo = '" & tipo & "' WHERE codigo = '" & codigo & "' ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                Else
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario (nombre_producto, precio_compra, precio_venta, precio_minimo, descripcion, tipo, codigo) VALUES ('" & product & "', '" & precio & "', '" & venta & "', '" & minimo & "', '" & comentario & "', '" & tipo & "', '" & codigo & "')")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                End If
                If tipo = "producto" Then
                    consulta = "SELECT * FROM rnv_inventario WHERE codigo = '" & codigo & "'"
                    Dim cmd2 As New OleDbCommand(consulta, cn)
                    Dim reader2 As OleDbDataReader = cmd2.ExecuteReader()
                    If reader2.Read() Then
                        If main.Button36.Visible = False Then
                            ''guardar stock
                            id_producto = Convert.ToInt32(reader2(("id")))
                            Dim cAdapter As New OleDbDataAdapter
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario_stock (id_producto, stock) VALUES (" & id_producto & ", " & stock & ")")
                            cAdapter.InsertCommand.Connection = cn
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        End If
                    Else
                        ''guardar stock
                        id_producto = Convert.ToInt32(reader2(("id")))
                        Dim cAdapter As New OleDbDataAdapter
                        cAdapter.InsertCommand = New OleDbCommand("DELETE FROM rnv_inventario_stock WHERE id_producto = " & id_producto & "")
                        cAdapter.InsertCommand.Connection = cn
                        cAdapter.InsertCommand.ExecuteNonQuery()
                    End If
                End If
                main.verificarTimer()
                cn.Close()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Using
        Return False
    End Function

    Function borrar_inventario(codigo As String)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM rnv_inventario WHERE codigo = '" & codigo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim id_producto = Convert.ToInt32(reader(("id")))
                    Dim cmd2 As New OleDbCommand("DELETE * FROM rnv_inventario WHERE codigo = '" & codigo & "'", cn)
                    cmd2.ExecuteNonQuery()
                    Dim cmd3 As New OleDbCommand("DELETE * FROM rnv_inventario_stock WHERE id_producto = " & id_producto & "", cn)
                    cmd3.ExecuteNonQuery()
                   main.verificarTimer()
                    Return True
                    cn.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

End Module
