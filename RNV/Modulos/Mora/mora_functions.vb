﻿Imports System.Data.OleDb
Imports coreRNV

Public Module mora_functions
    'instancia de conexión
    Dim s As New cConexion

    'variables
    Dim mes_actual = Today.Month
    Dim dia_actual = Today.Day
    Dim maximo As Integer

    'Publicas
    Public dias_atraso = 0
    Public mcobro = 0.00
    Public cantidadT As Integer
    Public cantidadJ As String

    Function validarPago(idAlumno As Integer, giro As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT mes_pago_nv, mes_pago_vnet FROM rnv_alumnos WHERE id = " & idAlumno & ""
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                Dim mesNV As Integer = Convert.ToInt32(reader(("mes_pago_nv")))
                Dim mesVNET As Integer = Convert.ToInt32(reader(("mes_pago_vnet")))
                Dim operarVNET As Integer = mesVNET + 2
                Dim operarNV As Integer = mesNV + 2
                Dim hoy As Date = Date.Now.ToString("dd/MM/yyyy")
                Dim diasAtrasoNormal As Integer = 0
                Dim diasAtrasoIncrementado As Integer = 0
                Dim spanIncrementado
                Dim spanNormal
                If (giro = "Academia NV") Then
                    ''solo para dias del mes
                    Dim dateIncrementado As Date = maximo & "/" & operarNV & "/" & Today.Year()
                    spanIncrementado = hoy - dateIncrementado
                    diasAtrasoIncrementado = spanIncrementado.TotalDays
                    ''para varios meses
                    Dim dateNormal As Date = maximo & "/" & mesNV & "/" & Today.Year()
                    spanNormal = hoy - dateIncrementado
                    diasAtrasoNormal = spanNormal.TotalDays
                    dias_atraso = diasAtrasoIncrementado
                    If (mesNV = mes_actual) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    ''solo para dias del mes
                    Dim dateIncrementado As Date = maximo & "/" & operarVNET & "/" & Today.Year()
                    spanIncrementado = hoy - dateIncrementado
                    diasAtrasoIncrementado = spanIncrementado.TotalDays
                    ''para varios meses
                    Dim dateNormal As Date = maximo & "/" & mesVNET & "/" & Today.Year()
                    spanNormal = hoy - dateIncrementado
                    diasAtrasoNormal = spanNormal.TotalDays
                    If diasAtrasoIncrementado >= 30 Then
                        dias_atraso = diasAtrasoIncrementado
                    Else
                        dias_atraso = diasAtrasoNormal
                    End If
                    If (mesNV = mes_actual) Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
        End Using
        Return False
    End Function

    Sub detectarMora(idAlumno As Integer, giro As String, dia_minimo As Integer, dia_maximo As Integer, cobro As String, tipo_exedicion As String)
        maximo = dia_maximo
        If (validarPago(idAlumno, giro) = False) Then
            If (dia_actual >= dia_minimo And dia_actual <= dia_maximo) Then
               main.Label147.Visible = False
            Else
                ''validar que no sea un pago anticipado
                If (validarPago(idAlumno, giro) = False) Then
                    Dim partirMeses As Integer
                    Dim recalcular As Double
                    If tipo_exedicion = "fijo" Then
                        If dias_atraso >= 30 Then
                            partirMeses = dias_atraso / 30
                            recalcular = cobro * partirMeses
                            cantidadT = partirMeses
                            cantidadJ = "(x" & partirMeses & " mes/es)"
                        Else
                            recalcular = cobro
                            cantidadJ = "(x" & dias_atraso & " día/s)"
                            cantidadT = 1
                        End If
                       main.Label147.Text = "Cobro por mora: Q" & recalcular & ". Retraso: " & cantidadJ
                    Else
                        recalcular = dias_atraso * cobro
                       main.Label147.Text = "Cobro por mora: Q" & recalcular & ". Retraso: " & dias_atraso & " días."
                    End If

                    mcobro = cobro
                   main.Label147.Visible = True
                Else
                   main.Label147.Visible = False
                End If
            End If
        Else
           main.Label147.Visible = False
        End If
    End Sub

End Module
