﻿Imports System.Data.OleDb
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Text.Encoding
Imports coreRNV
Public Module ciber_functions
    Dim ADMINISTRADOR As New UdpClient()
    Public ORDENADOR As IPAddress

    ' instanciar la clase de conexión
    Private s As New cConexion

    ' Validar si existe la bdd nueva
    Function getCiberBDD() As String
        Return "Q:\Projects\RNVGIT\RNV\bin\Debug\sys\rnv_ciber.mdb"
    End Function

    Sub refrescarIndividualIP(ip As String)
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            Try
                cn.Open()
                Dim consulta As String = "SELECT * FROM ciber_maquinas WHERE ip_fija = '" & ip & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then

                    If reader("status") <> "SIN USAR" And reader("status") <> "DESCONECTADO" Then
                        view_opciones_terminal.Label36.Text = reader("status")
                        view_opciones_terminal.Label3.Text = reader("user_conectado")
                        view_opciones_terminal.Label7.Text = reader("hora_inicio")
                        If reader("user_conectado") = "LIBRE" Then
                            view_opciones_terminal.Label5.Text = "No aplica"
                        End If
                    Else
                        view_opciones_terminal.Label36.Text = "SIN USAR"
                        view_opciones_terminal.Label3.Text = "N/A"
                        view_opciones_terminal.Label7.Text = "00:00:00"
                        view_opciones_terminal.Label5.Text = "00:00:00"
                    End If
                End If
                cn.Close()
            Catch ex As Exception
                cn.Close()
            End Try

        End Using
    End Sub


    Sub cargarPlanes()
        Try
            '' cargar // clientes alumnos
            view_asignar_tiempo.ComboBox1.Items.Clear()
            Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
                cn.Open()
                Dim objAdapter As New OleDbDataAdapter("SELECT nombre_plan, precio FROM ciber_planes", cn)
                Dim objDataSet As New DataSet
                Dim nombre_nivel As String = ""
                objAdapter.Fill(objDataSet, "nombre_plan")
                Dim i As Integer
                For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                    nombre_nivel = objDataSet.Tables(0).Rows(i).Item(0)
                    If nombre_nivel = "-ninguno-" Then
                    Else
                        view_asignar_tiempo.ComboBox1.Items.Add(objDataSet.Tables(0).Rows(i).Item(0) & " (Q." & FormatNumber(objDataSet.Tables(0).Rows(i).Item(1), 2) & ")")
                    End If
                Next
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub verificarRefresh()
        Try
            main.pc_rnv.Rows.Clear()
            Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM ciber_maquinas", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    Dim ip = dtr(("ip_fija"))
                    Dim numero = dtr(("numero"))
                    Dim status = dtr(("status"))
                    If verificarIP(ip) = True Then
                        main.pc_rnv.Rows.Add(numero, "00:00:00", ip, "N/A", status)
                    Else

                        main.pc_rnv.Rows.Add(numero, "00:00:00", ip, "N/A", "DESCONECTADO")
                    End If
                Next
                cn.Close()
            End Using
        Catch ex As Exception
        End Try
    End Sub

    ''Función para convertir segundos a formato hora:minuto:segundo
    Function segundosAhoras(segundos As Integer)
        Dim num As Integer
        Dim hor As Integer
        Dim min As Integer
        Dim seg As Integer
        num = segundos
        hor = Math.Floor(num / 3600)
        min = Math.Floor((num - hor * 3600) / 60)
        seg = num - (hor * 3600 + min * 60)
        Return Trim(hor) + " h " + Trim(min) +
  " m " + Trim(seg) + " s"
    End Function

    Sub verificarProcess()
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT * FROM ciber_maquinas", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    Dim ip = dtr(("ip_fija"))
                    Dim numero = dtr(("numero"))
                    Dim status = dtr(("status"))
                    If verificarIP(ip) = True Then
                        main.pc_rnv.Rows.Add(numero, "00:00:00", ip, "N/A", status)
                    Else
                        main.pc_rnv.Rows.Add(numero, "00:00:00", ip, "N/A", "DESCONECTADO")
                    End If
                Next
                main.Label152.Visible = False
                main.Label153.Text = "ACTUALIZADO: " & DateTime.Now.ToString("dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString()
                cn.Close()
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Function verificarIP(ip As String)
        Dim reachable As Boolean = False
        Try
            reachable = My.Computer.Network.IsAvailable AndAlso
                        My.Computer.Network.Ping(ip, 5000)
        Catch pingException As System.Net.NetworkInformation.PingException
        Catch genericNetworkException As System.Net.NetworkInformation.NetworkInformationException
            ' Fail silently and return false
        End Try
        Return reachable
    End Function


    Sub refrescarOrdenadores()
        main.pc_rnv.Rows.Clear()
        Dim t As New Thread(New ThreadStart(AddressOf verificarRefresh))
        t.Priority = Threading.ThreadPriority.Normal
        t.Start()
    End Sub

    Sub verificarOrdenadores()
        main.pc_rnv.Rows.Clear()
        Dim t As New Thread(New ThreadStart(AddressOf verificarProcess))
        t.Priority = Threading.ThreadPriority.Normal
        t.Start()
    End Sub

    Sub detenerPC()
        Try
            Dim myIP As IPEndPoint = New IPEndPoint(ORDENADOR, "5555")
            Dim FRASE As String = "BLOQUEO:0:0"
            Dim MENSAJE As Byte() = UTF7.GetBytes(FRASE)
            ADMINISTRADOR.Connect(myIP)
            ADMINISTRADOR.Send(MENSAJE, MENSAJE.Length)
            MsgBox("Orden de bloqueo enviada a PC: " & view_opciones_terminal.Text, vbInformation)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub iniciarPC()
        Try
            Dim myIP As IPEndPoint = New IPEndPoint(ORDENADOR, "5555")
            Dim FRASE As String = "INICIAR: 5" & ": 5"
            Dim MENSAJE As Byte() = UTF7.GetBytes(FRASE)
            ADMINISTRADOR.Connect(myIP)
            ADMINISTRADOR.Send(MENSAJE, MENSAJE.Length)
            MsgBox("Orden de inicio enviada a PC: " & view_opciones_terminal.Text, vbInformation)
        Catch ex As Exception
        End Try
    End Sub

    Sub filtrar(palabra As String)
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
                Dim consulta As String = "select numero, ip_fija, id FROM ciber_maquinas WHERE ip_fija LIKE '%" & palabra & "%'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)

                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                view_buscar_terminal.gridProductos.DataSource = dv
                view_buscar_terminal.gridProductos.Columns(0).HeaderText = "Número de terminal"
                view_buscar_terminal.gridProductos.Columns(1).HeaderText = "Dirección IP"
                view_buscar_terminal.gridProductos.Columns(2).HeaderText = "Identificador"
            End Using
        Catch ex As Exception
        End Try
    End Sub


    Sub filtrar_usuarios(palabra As String)
        Try
            Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
                Dim consulta As String = "select id, nombre_completo, usuario, pass FROM ciber_usuarios WHERE nombre_completo LIKE '%" & palabra & "%' OR usuario LIKE '%" & palabra & "%'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)

                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                view_buscar_usuarios.gridProductos.DataSource = dv
                view_buscar_usuarios.gridProductos.Columns(0).HeaderText = "ID"
                view_buscar_usuarios.gridProductos.Columns(1).HeaderText = "Nombre completo"
                view_buscar_usuarios.gridProductos.Columns(2).HeaderText = "Nombre de Usuario"
                view_buscar_usuarios.gridProductos.Columns(3).HeaderText = "Contraseña"
            End Using
        Catch ex As Exception
        End Try
    End Sub

    '********* CRUD TERMINALES **********

    '' guardar / editar alumno
    Function salvar_maquina(ip As String, id As String, nomaquina As String) As Boolean
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            cn.Open()
            Try
                Dim consulta As String = ""
                consulta = "SELECT * FROM ciber_maquinas WHERE id = " & id & ""
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' si el codigo existe entonces actualizar
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE ciber_maquinas SET numero =" & nomaquina & ", ip_fija ='" & ip & "' WHERE id = " & id & " ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    Return True
                Else
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO ciber_maquinas (numero, ip_fija, status) VALUES (" & nomaquina & ", '" & ip & "', 'SIN USAR')")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    Return True
                End If
                cn.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    '' Eliminar el alumno y también eliminar asociacion curso / alumno
    Function borrar_maquina(ip As String) As Boolean
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM ciber_maquinas WHERE ip_fija = '" & ip & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim idIP = (reader(("id")))
                    Dim cmd2 As New OleDbCommand("DELETE * FROM ciber_maquinas WHERE id = " & idIP & "", cn)
                    cmd2.ExecuteNonQuery()
                    Return True
                    cn.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function verificar_maquina(ip As String) As Boolean
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM ciber_maquinas WHERE ip_fija = '" & ip & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then

                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    '********* CRUD USUARIOS **********

    'Guardar / Editar
    Function salvarUsuario(id As Integer, nombre_completo As String, usuario As String, pass As String)
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            cn.Open()
            Try
                Dim strStr As String = pass
                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(strStr)
                Dim password = System.Convert.ToBase64String(byt)
                Dim consulta As String = ""
                consulta = "SELECT * FROM ciber_usuarios WHERE id = " & id & ""
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    '' si el codigo existe entonces actualizar
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE ciber_usuarios SET nombre_completo ='" & nombre_completo & "', usuario ='" & usuario & "', pass='" & password & "' WHERE id = " & id & " ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    Return True
                Else
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("INSERT INTO ciber_usuarios (nombre_completo, usuario, pass) VALUES ('" & nombre_completo & "', '" & usuario & "', '" & password & "')")
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    Return True
                End If
                cn.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function verificar_usuario(usuario As String) As Boolean
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM ciber_usuarios WHERE usuario = '" & usuario & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then

                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function borrar_usuario(usuario As String) As Boolean
        Using cn As New OleDbConnection(s.constructorBDD(getCiberBDD))
            cn.Open()
            Try
                Dim consulta As String = "SELECT * FROM ciber_usuarios WHERE usuario = '" & usuario & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim idIP = (reader(("id")))
                    Dim cmd2 As New OleDbCommand("DELETE * FROM ciber_usuarios WHERE id = " & idIP & "", cn)
                    cmd2.ExecuteNonQuery()
                    Return True
                    cn.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function
End Module
