﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Net.Mail
Module functions_cobros
    '' instancia a clase de conexión
    Dim s As New coreRNV.cConexion

    '' Almacenamos los datos que usaremos para variables de mail
    Dim tcliente, tmespago As String

    '' Estructura para las variables del email
    Private Structure VARIABLES_EMAIL
        Dim cliente As String
        Dim mes_pago As String
    End Structure

    '-- Control de comprobantes emitidos
    Function numero_de_comprobante()
        Try
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim consulta As String = "Select count(*) from rnv_cobros"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim numero_filas
                numero_filas = CStr(cmd.ExecuteScalar())
                Dim valor As Integer = numero_filas
                Dim retornar = String.Format("R" & "{0:000000}", valor + 1)
                Return retornar
                cn.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical, "Excepción")
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Función para obtener y setear las variables
    ''' </summary>
    ''' <param name="contains"></param>
    ''' <returns>Contains</returns>
    Function getMessageWithVariables(contains As String)
        Dim variables As New VARIABLES_EMAIL
        If contains.Contains("{CLIENTE}") Then
            variables.cliente = tcliente
            contains = contains.Replace("{CLIENTE}", tcliente)
        End If
        If contains.Contains("{MES_PAGO}") Then
            variables.mes_pago = tmespago
            contains = contains.Replace("{MES_PAGO}", tmespago)
        End If
        Return contains
    End Function

    ''Enviar recibo por correo
    Function enviarPorCorreo(correo As String, recibo As String, mespago As String, giro As String, cliente As String)
        Try
            tcliente = cliente
            tmespago = mespago
            '' envia el correo
            Dim smail As New System.Net.Mail.MailMessage()
            Dim SMTP As New System.Net.Mail.SmtpClient
            SMTP.Credentials = New System.Net.NetworkCredential("no-reply@flippyescolar.com", "martes12")
            SMTP.Host = "mx1.hostmania.es"
            SMTP.Port = 2525
            SMTP.EnableSsl = False
            smail.[To].Add(correo)
            smail.From = New System.Net.Mail.MailAddress("no-reply@flippyescolar.com", "ProyectosNV", System.Text.Encoding.UTF8)
            smail.Subject = giro & " | Recibo de pago " & mespago
            smail.BodyEncoding = System.Text.Encoding.UTF8
            smail.Priority = System.Net.Mail.MailPriority.Normal
            smail.IsBodyHtml = True
            smail.SubjectEncoding = System.Text.Encoding.UTF8
            smail.Attachments.Add(New Attachment(s.rutaArchivos & "/recibos/recibo_" & recibo & ".pdf", "text/pdf"))
            smail.Body = "<body bgcolor='#E1E1E1' leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'> <center style='background-color:#E1E1E1;'> <table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable' style='table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;'> <tr> <td align='center' valign='top' id='bodyCell'> <table bgcolor='#FFFFFF' border='0' cellpadding='0' cellspacing='0' width='60%' id='emailBody'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' style='color:#FFFFFF;' bgcolor='#3498db'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='60%' class='flexibleContainer'> <tr> <td align='center' valign='top' width='60%' class='flexibleContainerCell'> <table border='0' cellpadding='30' cellspacing='0' width='100%'> <tr> <td align='center' valign='top' class='textContent'> <h1 style='color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;'>ProyectosNV</h1></td></tr></table> </td></tr></table> </td></tr></table> </td></tr><tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#F8F8F8'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' class='flexibleContainer'> <tr> <td align='center' valign='top' width='60%' class='flexibleContainerCell'> <table border='0' cellpadding='30' cellspacing='0' width='100%'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%'> <tr> <td valign='top' class='textContent'> <h3 mc:edit='header' style='color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:normal;margin-top:0;margin-bottom:12px;text-align:center;'>Gracias!</h3> <div mc:edit='body' style='font-family: Helvetica,Arial,sans-serif; font-size: 16px; margin-bottom: 0; color: rgba(0, 0, 0, 0.8); line-height: 135%; padding: 3px 0 0 0;text-align: center;'>" & getMessageWithVariables(main.txt_cuerpo_email.Text) & "</div></td></tr></table> </td></tr></table> </td></tr></table> </td></tr></table> </td></tr></table> <table bgcolor='#E1E1E1' border='0' cellpadding='0' cellspacing='0' width='60%' id='emailFooter'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='60%' class='flexibleContainer'> <tr> <td align='center' valign='top' width='60%' class='flexibleContainerCell'> <table border='0' cellpadding='30' cellspacing='0' width='100%'> <tr> <td valign='top' bgcolor='#E1E1E1'> <div style='font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;'> <div>Copyright 2017 <a href='https://www.facebook.com/nvproyectos/' target='_blank' style='text-decoration:none;color:#828282;'><span style='color:#828282;'>ProyectosNV</span></a> Todos los derechos reservados </div><div>Correo electronico generado desde SistemaNV Desktop.</div>"
            SMTP.Send(smail)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Function guardarVenta(recibo As String, id As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim Total = 0
                For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                    Total += Val(row.Cells(4).Value)
                Next
                Dim consulta As String = ""
                Dim cAdapter As New OleDbDataAdapter

                Dim str As String = Date.Today.ToString("yyyy/MM/dd")
                Dim str2 As String
                Dim convertidor As DateTime
                Dim hoy As String
                str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)
                Try
                    str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                    convertidor = str2
                    hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                Catch ex As Exception
                    Dim hoySinConvert As String = Date.Today.ToString("MM/dd/yyyy")
                    hoy = DateTime.ParseExact(hoySinConvert, "M/d/yyyy",
                                 Globalization.CultureInfo.InstalledUICulture)
                End Try
                cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_cobros (id_recibo, por, total_venta, fecha, id_cliente, comentario, giro, ciclo) VALUES ('" & recibo & "', 'admin', '" & FormatNumber(Total) & "', #" & hoy & "#, " & main.idMaster.Text & ", '" & main.comentario.Text & "', '" & main.cbox_select.Text & "', " & main.TextBox42.Text & ")")
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                consulta = "SELECT * FROM rnv_cobros WHERE id_recibo = '" & recibo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim idRecibo = Convert.ToString(reader(("id")))
                    For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                        If (main.cbox_select.Text <> "Inventario NV") Then
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_recibos_cobros (id_recibo, id_alumno, descripcion, cantidad, precio, importe, ciclo) VALUES (" & idRecibo & ", " & id & ", '" & main.txt_client.Text & "', '" & row.Cells(2).Value & "', '" & row.Cells(3).Value & "', '" & row.Cells(4).Value & "', " & main.TextBox42.Text & ")")
                            cAdapter.InsertCommand.Connection = cn
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        Else
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_recibos_cobros (id_recibo, id_alumno, descripcion, cantidad, precio, importe, ciclo) VALUES (" & idRecibo & ", " & id & ", '" & row.Cells(1).Value & "', '" & row.Cells(2).Value & "', '" & row.Cells(3).Value & "', '" & row.Cells(4).Value & "', " & main.TextBox42.Text & ")")
                            cAdapter.InsertCommand.Connection = cn
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            'Insertar movimiento debito
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_inventario_movimientos (movimiento, id_producto, fecha, comentario, tipo) VALUES ('- " & row.Cells(2).Value & "', " & row.Cells(0).Value & ", #" & hoy & "#, 'Por concepto de venta', 'negativo')")
                            cAdapter.InsertCommand.Connection = cn
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            ''debitar de inventario
                            cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_inventario_stock SET stock = (stock - " & row.Cells(2).Value & ") WHERE id_producto = " & row.Cells(0).Value & " ", cn)
                            cAdapter.InsertCommand.Connection = cn
                            cAdapter.InsertCommand.ExecuteNonQuery()
                        End If
                        main.verificarTimer()
                    Next
                    main.verificarTimer()
                    Return True
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
            cn.Close()
        End Using
        Return False
    End Function

    ''  INSIDE FUNCTIONS
    Function guardarRecibo(recibo As String, id As String) As Boolean
        '' validar mes
        Dim mes As Integer = 0
        If main.cbox_mes.Text = "ENERO" Then
            mes = 1
        ElseIf main.cbox_mes.Text = "FEBRERO" Then
            mes = 2
        ElseIf main.cbox_mes.Text = "MARZO" Then
            mes = 3
        ElseIf main.cbox_mes.Text = "ABRIL" Then
            mes = 4
        ElseIf main.cbox_mes.Text = "MAYO" Then
            mes = 5
        ElseIf main.cbox_mes.Text = "JUNIO" Then
            mes = 6
        ElseIf main.cbox_mes.Text = "JULIO" Then
            mes = 7
        ElseIf main.cbox_mes.Text = "AGOSTO" Then
            mes = 8
        ElseIf main.cbox_mes.Text = "SEPTIEMBRE" Then
            mes = 9
        ElseIf main.cbox_mes.Text = "OCTUBRE" Then
            mes = 10
        ElseIf main.cbox_mes.Text = "NOVIEMBRE" Then
            mes = 11
        ElseIf main.cbox_mes.Text = "DICIEMBRE" Then
            mes = 12
        End If
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Try
                Dim Total = 0
                For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                    Total += Val(row.Cells(4).Value)
                Next
                Dim consulta As String = ""
                Dim cAdapter As New OleDbDataAdapter

                Dim str As String = Date.Today.ToString("yyyy/MM/dd")
                Dim str2 As String
                Dim convertidor As DateTime
                Dim hoy As String
                str = System.Text.RegularExpressions.Regex.Replace(str, "[a-z]{2}(?=,)", String.Empty)

                Try
                    str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                    convertidor = str2
                    hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                Catch ex As Exception
                    Dim hoySinConvert As String = Date.Today.ToString("MM/dd/yyyy")
                    hoy = DateTime.ParseExact(hoySinConvert, "M/d/yyyy",
                                 Globalization.CultureInfo.InstalledUICulture)
                End Try
                cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_cobros (id_recibo, por, total_venta, fecha, mes_pago, id_cliente, comentario, giro) VALUES ('" & recibo & "', 'admin', '" & FormatNumber(Total) & "', #" & hoy & "#, " & mes & ", " & main.idMaster.Text & ", '" & main.comentario.Text & "', '" & main.cbox_select.Text & "')")
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                consulta = "SELECT * FROM rnv_cobros WHERE id_recibo = '" & recibo & "'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim reader As OleDbDataReader = cmd.ExecuteReader()
                Threading.Thread.Sleep(490)
                If main.cbox_select.Text = "Academia NV" Then
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_alumnos SET mes_pago_nv=" & mes & " WHERE id = " & main.idMaster.Text & " ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                Else
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_alumnos SET mes_pago_vnet=" & mes & " WHERE id = " & main.idMaster.Text & " ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                    main.verificarTimer()
                End If
                If reader.Read() Then
                    Dim idRecibo = Convert.ToString(reader(("id")))
                    consulta = "SELECT * FROM rnv_alumnos WHERE t_id = '" & id & "'"
                    Dim cmdx As New OleDbCommand(consulta, cn)
                    Dim readerx As OleDbDataReader = cmdx.ExecuteReader()
                    If readerx.Read() Then
                        Dim idAlumno = Convert.ToString(readerx(("id")))
                        For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_recibos_cobros (id_recibo, id_alumno, descripcion, cantidad, precio, importe) VALUES (" & idRecibo & ", " & idAlumno & ", '" & row.Cells(1).Value & "', '" & row.Cells(2).Value & "', '" & row.Cells(3).Value & "', '" & row.Cells(4).Value & "')")
                            cAdapter.InsertCommand.Connection = cn
                            cAdapter.InsertCommand.ExecuteNonQuery()
                            main.verificarTimer()
                        Next
                        main.verificarTimer()
                        Return True
                    End If

                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
            cn.Close()
        End Using
        Return False
    End Function
End Module
