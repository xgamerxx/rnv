﻿Imports System.Collections.Generic
Public Class EFactura
    'Clase propiedades de encabezado de la factura
    Public Property recibo() As String
    Public Property Nombre() As String
    Public Property Direccion() As String
    Public Property facebook() As String
    Public Property datos_empresa() As String
    Public Property Total() As Decimal
    Public Property mes() As String
    Public Property comentario() As String
    Public Property labelestu() As String
    Public Property codigo_alumno() As String
    Public Property logo() As String
    Public Property fecha() As String
    'Creamos una lista con una nueva Instancia de la clase Articulo
    'esta lista contendra el detalle de la factura
    Public Detail As New List(Of EArticulo)()
End Class
