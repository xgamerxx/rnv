﻿Public Class EArticulo
    'Clase de los articulos en factura
    Public Property codigo() As String
    Public Property descripcion() As String
    Public Property cantidad() As Integer
    Public Property precio() As Decimal
    Public Property importe() As Decimal
    Public Property total() As Decimal
End Class
