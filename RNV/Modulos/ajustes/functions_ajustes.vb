﻿Imports RNV
Imports coreRNV
Imports System.Data.OleDb
Public Module functions_ajustes
    Dim s As New cConexion

    ''Instanciar al Formulario principal
    Public main As New view_principal

    Public Sub cargarAjustes()

        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT * FROM rnv_config"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                main.txt_titulo_nv.Text = Convert.ToString(reader(("titulo_nv")))
                main.txt_slogan_nv.Text = Convert.ToString(reader(("slogan_nv")))
                main.txt_facebook_nv.Text = Convert.ToString(reader(("fb_nv")))
                main.txt_logo_nv.Text = My.Application.Info.DirectoryPath & "\images\" & Convert.ToString(reader(("logo_nv")))

                main.txt_titulo_vnet.Text = Convert.ToString(reader(("titulo_vnet")))
                main.txt_slogan_vnet.Text = Convert.ToString(reader(("slogan_vnet")))
                main.txt_facebook_vnet.Text = Convert.ToString(reader(("fb_vnet")))
                main.NumericUpDown1.Value = Convert.ToInt32(reader(("backup")))

                main.maxdias.Value = Convert.ToInt32(reader(("expira_notas")))
                main.txt_cuerpo_email.Text = Convert.ToString(reader("texto_email"))
                main.NumericUpDown2.Value = Convert.ToInt32(reader("dia_minimo"))
                main.NumericUpDown3.Value = Convert.ToInt32(reader("dia_maximo"))
                main.TextBox41.Text = Convert.ToString(reader("cobro"))
                Dim cobranza As String = Convert.ToString(reader("tipo_exedicion"))
                If cobranza = "fijo" Then
                    main.CheckBox7.Checked = True
                Else
                    main.CheckBox8.Checked = True
                End If

                Dim moraActiva As Integer = Convert.ToInt32(reader("mora"))

                If moraActiva = 1 Then
                    main.CheckBox6.Checked = True
                Else
                    main.CheckBox6.Checked = False
                End If

                Dim bool As Integer = Convert.ToInt32(reader(("mostrar_notas")))
                If bool = 1 Then
                    main.frontal.Checked = True
                Else
                    main.frontal.Checked = False
                End If
                main.txt_logo_vnet.Text = My.Application.Info.DirectoryPath & "\images\" & Convert.ToString(reader(("logo_vnet")))
                main.TextBox42.Text = Convert.ToInt32(reader(("ciclo_actual")))
            End If
            cn.Close()
        End Using
    End Sub

    Function actualizarEmail(cuerpo As String)
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET texto_email = '" & cuerpo & "' ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarCiclo(ciclo As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET ciclo_actual = " & ciclo & " ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarNotas(maxdias As Integer, bool As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET mostrar_notas = " & bool & ", expira_notas = " & maxdias & " ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarNV(txt_titulo_nv As String, txt_slogan_nv As String, txt_facebook_nv As String, txt_logo_nv As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET titulo_nv='" & CStr(txt_titulo_nv) & "', slogan_nv ='" & txt_slogan_nv & "', fb_nv ='" & txt_facebook_nv & "', logo_nv ='" & txt_logo_nv & "' ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarVNET(txt_titulo_nv As String, txt_slogan_nv As String, txt_facebook_nv As String, txt_logo_nv As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET titulo_vnet='" & txt_titulo_nv & "', slogan_vnet ='" & txt_slogan_nv & "', fb_vnet ='" & txt_facebook_nv & "', logo_vnet ='" & txt_logo_nv & "' ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarPROTECTED(passw As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET pass_protected='" & passw & "' ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarPASS(passw As String) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET pass_admin='" & passw & "' ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarbackup(value As Integer) As Boolean
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET backup=" & value & " ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

    Function actualizarMora(minimo As Integer, maximo As Integer, cobro As String, tipo As String, inicio As Integer)
        Using cn As New OleDbConnection(s.sQuery)
            Try
                Dim cAdapter As New OleDbDataAdapter
                cn.Open()
                cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_config SET dia_minimo=" & minimo & ", dia_maximo = " & maximo & ", cobro = '" & cobro & "', tipo_exedicion = '" & tipo & "', mora = " & inicio & " ", cn)
                cAdapter.InsertCommand.Connection = cn
                cAdapter.InsertCommand.ExecuteNonQuery()
                main.verificarTimer()
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Using
        Return False
    End Function

End Module
