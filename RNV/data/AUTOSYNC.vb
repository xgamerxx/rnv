﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports coreRNV
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class AUTOSYNC
    Dim s As New cConexion
    Function leerRNV()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "current.rnv")
        Return fileReader
    End Function

    Function leerSync()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "ifsync.rnv")
        Return fileReader
    End Function

    Function leerupdate()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "lastupdate.rnv")
        Return fileReader
    End Function
    Private Sub AUTOSYNC_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        If verificarSiEstaOcupado() = True Then
        Else
            ocupar()
            DownloadFile(leerRNV, leerRNV)
            If getAlumnos() = True Then

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                    UploadFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb", "ESME_rnv_bdd.mdb")
                End If

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                    UploadFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb", "JEREZ_rnv_bdd.mdb")
                End If

                MsgBox("La sincronización se realizó con exito.", vbInformation)
                'verificarTimer()
                File.WriteAllText(s.rutaSistema & "ifsync.rnv", "false")
                File.WriteAllText(s.rutaSistema & "lastupdate.rnv", DateTime.Now.ToString("dd/MM/yyy") & " " & DateTime.Now.ToShortTimeString())
                If leerupdate() = "" Then
                   main.lupdate.Text = "n/a"
                Else
                   main.lupdate.Text = leerupdate()
                End If
                liberar()
                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                    My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb")
                End If

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                    My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb")
                End If
            Else

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                    My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb")
                End If

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                    My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb")
                End If
                liberar()
            End If
        End If
    End Sub
End Class