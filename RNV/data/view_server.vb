﻿Imports System.ComponentModel
Imports System.IO
Imports coreRNV

Public Class view_server
    Dim s As New cConexion

    Public fichero As String = "helper_ticket_NV"
    Public nombreTxt As String = "helper_ticket_NV.txt"

    Private Sub view_server_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            My.Computer.FileSystem.DeleteFile(s.rutaArchivos & "\helper_ticket_NV")
        Catch ex As Exception
        End Try
        ComboBox1.SelectedIndex = 0
    End Sub

    Function download() As Boolean
        Try
            If DownloadFile(ComboBox1.Text, ComboBox1.Text) = "true" Then
                Label2.Text = "Asignando base de datos..."

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\rnv_bdd.mdb") Then
                    My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\rnv_bdd.mdb")
                End If

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                    My.Computer.FileSystem.RenameFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb", "rnv_bdd.mdb")

                End If

                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                    My.Computer.FileSystem.RenameFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb", "rnv_bdd.mdb")
                End If
                File.WriteAllText(s.rutaSistema & "current.rnv", ComboBox1.Text)
                frmLoginNODB.Label3.Text = leerRNV()
               main.Text = "RNV | " & ComboBox1.Text
                core_dataLoad.Show()
                Panel1.Visible = False
                Me.Close()
            Else
                Panel1.Visible = False
                Application.Restart()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Application.Restart()
        End Try

        Return True
    End Function
    Dim counter As Integer = 1

    Function leerRNV()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "current.rnv")
        Return fileReader
    End Function

    Function leerSync()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "ifsync.rnv")
        Return fileReader
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ComboBox1.Text = "SEGUIR USANDO EL SERVIDOR ACTUAL" Then
            Panel1.Visible = True
            Me.Refresh()
            Me.Update()
           main.Text = "RNV | " & leerRNV() & " (continuidad)"
            frmLoginNODB.Label3.Text = leerRNV()
            core_dataLoad.Show()
            Me.Close()
        Else
            If leerSync() = "true" Then
                Dim result As Integer = MessageBox.Show("ATENCIÓN: La base de datos aún no ha sido sincronizada. " & vbCrLf & " " & vbCrLf & " ¿Estás seguro que deseas cargar este servidor?", "RNV", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                If result = DialogResult.Yes Then
                    Panel1.Visible = True
                    Timer1.Start()
                End If
            Else
                Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas cargar este servidor?", "RNV", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then
                    Panel1.Visible = True
                    Timer1.Start()
                End If
            End If

        End If
    End Sub


    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        counter = counter - 1
        If counter = 0 Then
            Dim worker As New BackgroundWorker
            worker.WorkerSupportsCancellation = True
            worker.WorkerReportsProgress = True
            If worker.IsBusy Then
                worker.CancelAsync()
            Else
                worker.RunWorkerAsync(download())

            End If
            Timer1.Stop()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Try
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas restaurar la base de datos al último backup guardado?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                If func_restoreBDD() = True Then
                    MsgBox("La base de datos fue restaurada al último backup guardado. Para continuar haz click en CONECTAR.", vbInformation)
                    ComboBox1.Items.Clear()
                    ComboBox1.Items.Add("SEGUIR USANDO EL SERVIDOR ACTUAL")
                    ComboBox1.SelectedIndex = 0
                Else
                    MsgBox("No se encontró ningún punto de restauración.", vbExclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) 
        Call Button1_Click(sender, e)
    End Sub
End Class