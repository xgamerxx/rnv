﻿
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.IO
Imports coreRNV
Imports iTextSharp.text
Imports iTextSharp.text.pdf

''' <summary>
''' Simple inicio de sesión sin uso de Base de Datos.
''' </summary>
Public Class frmLoginNODB
    Dim user, password As String
    Dim s As New cConexion
    Private Sub cargarAjustes()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT * FROM rnv_config"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                main.txt_titulo_nv.Text = Convert.ToString(reader(("titulo_nv")))
                main.txt_slogan_nv.Text = Convert.ToString(reader(("slogan_nv")))
                main.txt_facebook_nv.Text = Convert.ToString(reader(("fb_nv")))
                main.txt_logo_nv.Text = My.Application.Info.DirectoryPath & "\images\" & Convert.ToString(reader(("logo_nv")))

                main.txt_titulo_vnet.Text = Convert.ToString(reader(("titulo_vnet")))
                main.txt_slogan_vnet.Text = Convert.ToString(reader(("slogan_vnet")))
                main.txt_facebook_vnet.Text = Convert.ToString(reader(("fb_vnet")))
                main.txt_logo_vnet.Text = My.Application.Info.DirectoryPath & "\images\" & Convert.ToString(reader(("logo_vnet")))
            End If
            cn.Close()
        End Using
    End Sub

    Private Function getPass() As String
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT * FROM rnv_config"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                Return Convert.ToString(reader(("pass_admin")))
            End If
            cn.Close()
        End Using
        Return ""
    End Function
    Function leerRNV()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "current.rnv")
        Return fileReader
    End Function
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        user = txtuser.Text
        password = txtpass.Text
        Dim pass = getPass()
        If user = "admin" And pass = txtpass.Text Then
            main.Text = "RNV | " & leerRNV()
            txtpass.Text = ""
            main.Show()
            main.BringToFront()
            cargarAjustes()
            Me.Hide()
        Else
            MsgBox("Datos incorrectos.", vbExclamation)
        End If
    End Sub

    Private Sub view_principal_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        End
    End Sub

    Private Sub txtuser_KeyDown(sender As Object, e As KeyEventArgs) Handles txtuser.KeyDown
        If e.KeyCode = Keys.Enter Then
            Call Button4_Click(sender, e)
        End If
    End Sub

    Private Sub txtpass_KeyDown(sender As Object, e As KeyEventArgs) Handles txtpass.KeyDown
        If e.KeyCode = Keys.Enter Then
            Call Button4_Click(sender, e)
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Application.Restart()
    End Sub

    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) 
        Call Button4_Click(sender, e)
    End Sub

    Private Sub frmLoginNODB_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.BringToFront()
    End Sub
End Class