﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Public Class view_mov_small_stock
    Dim s As New cConexion

    '' Inside functions
    Private Sub cargarMovimientos()
        Label2.Text =main.nombre_producto.Text
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_inventario INNER JOIN rnv_inventario_movimientos ON rnv_inventario_movimientos.id_producto = rnv_inventario.id WHERE rnv_inventario.codigo = '" &main.cod_producto.Text & "'", cn)
            Dim ds As New DataSet
                dt.Fill(ds)
            For Each dtr As DataRow In ds.Tables(0).Rows
                gridProductos.Rows.Add(dtr(("nombre_producto")), dtr(("movimiento")), dtr(("comentario")), dtr(("fecha")))
            Next
            cn.Close()
        End Using
       main.Enabled = False
    End Sub

    Private Sub view_pagos_realizados_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub view_pagos_realizados_Load(sender As Object, e As EventArgs) Handles Me.Load
        cargarMovimientos()
    End Sub

    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
End Class