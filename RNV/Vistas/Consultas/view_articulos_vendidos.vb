﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Public Class view_articulos_vendidos
    Dim s As New cConexion

    '' para generador
    Public comentario As String = ""
    Public noRecibo As String = ""
    Public para As String = ""
    Public direccion As String = ""
    Public giro As String = ""
    Public codigoestudiante As String = ""
    Public estemes As String = ""

    '' Inside functions

    '' Volver a generar recibo
    Public Sub reRegenerarRecibo()
        reGenerarRecibo2.Show()
    End Sub

    Private Sub cargarProductosVendidos()
        Label2.Text =main.TextBox1.Text
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT id, id_recibo, comentario 
                                      FROM rnv_cobros 
                                      WHERE id_recibo = '" &main.TextBox45.Text & "'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                comentario = Convert.ToString(reader(("comentario")))
                noRecibo = Convert.ToString(reader(("id_recibo")))
                Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_recibos_cobros WHERE id_recibo = " & Convert.ToInt32(reader(("id"))) & "", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    gridProductos.Rows.Add(dtr(("descripcion")), dtr(("cantidad")), dtr(("precio")), dtr(("importe")))
                Next
            End If
            cn.Close()
        End Using
       main.Enabled = False
    End Sub

    Private Sub view_pagos_realizados_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub view_pagos_realizados_Load(sender As Object, e As EventArgs) Handles Me.Load
        cargarProductosVendidos()
    End Sub

    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
End Class