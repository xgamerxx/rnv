﻿Imports Microsoft.Reporting.WinForms
Imports System.Drawing.Printing
Imports System.IO
Imports coreRNV
Imports System.ComponentModel

Public Class view_reportes_inventario
    'Encabezado factura y detalle (articulos)
    Public Invoice As New List(Of Eencabezado_ventasInventario)()
    Public Detail As New List(Of Articulos_Inventario)()
    'Cree las propiedades publicas Titulo y Empresa
    Public Property Titulo() As String
    Public Property Empresa() As String

    Private Sub InvoiceGenerate()
        Dim invoice As New Eencabezado_ventasInventario()
        Dim Total As Double = 0.00
        For Each row As DataGridViewRow In grid_datos_cobros.gridinforme.Rows
            Total += FormatNumber(Val(row.Cells(2).Value), 2)
        Next
        invoice.codigo_reporte = main.Label44.Text

        invoice.total = "Q" & FormatNumber(Total, 2)
        invoice.fecha = Format(DateTime.Now, "dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString()
        For Each row As DataGridViewRow In grid_datos_cobros.gridinforme.Rows
            Dim article As New Articulos_Inventario()
            article.codigo = row.Cells(0).Value
            article.producto = "Venta de mercadería"
            article.fecha = row.Cells(1).Value
            article.total_cobro = FormatNumber(Val(row.Cells(2).Value), 2)
            invoice.Detalle.Add(article)
        Next
        Me.Invoice.Add(invoice)
        Me.Detail = invoice.Detalle
    End Sub

    Private Function guardar_pdf()
        Try
            Dim ps As PageSettings = rv_factura.GetPageSettings
            Dim ruta_exacta As String = System.Environment.CurrentDirectory & "\recibos\corte_" & main.Label44.Text & ".pdf"
            Dim byteViewer As Byte() = rv_factura.LocalReport.Render("PDF")
            Dim pdf_dialogo As New SaveFileDialog()
            pdf_dialogo.Filter = "*PDF files (*.pdf)|*.pdf"
            pdf_dialogo.FilterIndex = 2
            pdf_dialogo.RestoreDirectory = True
            Dim factura_ As New FileStream(ruta_exacta, FileMode.Create)
            factura_.Write(byteViewer, 0, byteViewer.Length)
            factura_.Close()
        Catch ex As Exception
        End Try
        Return True
    End Function

    Private Sub view_recibo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            InvoiceGenerate()
            rv_factura.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
            rv_factura.LocalReport.EnableExternalImages = True
            rv_factura.DocumentMapCollapsed = True
            rv_factura.LocalReport.ReportPath = System.Environment.CurrentDirectory & "\reporte_inventario.rdlc"
            rv_factura.LocalReport.DataSources.Clear()
            'Establecemos los margenes de la factura
            Dim instance As New PageSettings()
            Dim value As New Margins(31, 0, 15, 5)
            instance.Margins = value
            rv_factura.SetPageSettings(instance)
            'Limpiemos el DataSource del informe
            rv_factura.LocalReport.DataSources.Clear()
            'Establezcamos la lista como Datasource del informe
            rv_factura.LocalReport.DataSources.Add(New ReportDataSource("Encabezado", Invoice))
            rv_factura.LocalReport.DataSources.Add(New ReportDataSource("Detalle", Detail))
            'Hagamos un refresh al reportViewer
            rv_factura.RefreshReport()
            main.Label43.Visible = False
            Dim worker As New BackgroundWorker
            worker.WorkerSupportsCancellation = True
            worker.WorkerReportsProgress = True
            If worker.IsBusy Then
                worker.CancelAsync()
            Else
                worker.RunWorkerAsync(guardar_pdf())
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.rv_factura.RefreshReport()
    End Sub

    Private Sub view_recibo_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Me.Hide()
        main.Enabled = True
    End Sub
End Class