﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class grid_datos_cobros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.gridinforme = New System.Windows.Forms.DataGridView()
        CType(Me.gridinforme, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridinforme
        '
        Me.gridinforme.AllowUserToAddRows = False
        Me.gridinforme.AllowUserToDeleteRows = False
        Me.gridinforme.AllowUserToOrderColumns = True
        Me.gridinforme.AllowUserToResizeColumns = False
        Me.gridinforme.AllowUserToResizeRows = False
        Me.gridinforme.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridinforme.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridinforme.Location = New System.Drawing.Point(0, 0)
        Me.gridinforme.Name = "gridinforme"
        Me.gridinforme.Size = New System.Drawing.Size(10, 10)
        Me.gridinforme.TabIndex = 0
        '
        'grid_datos_cobros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(10, 10)
        Me.Controls.Add(Me.gridinforme)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "grid_datos_cobros"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Grid de datos. Reporte de cobros"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        CType(Me.gridinforme, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gridinforme As DataGridView
End Class
