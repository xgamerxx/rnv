﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV

Public Class view_protected

    Dim s As New cConexion
    Public ventana As String = ""
    Private Function getPass() As String
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT * FROM rnv_config"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                Return Convert.ToString(reader(("pass_protected")))
            End If
            cn.Close()
        End Using
        Return ""
    End Function
    Private Sub view_protected_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
        view_corte.Enabled = True
        view_corte_general.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim pass = getPass()
        If TextBox1.Text <> "" Then
            If pass = TextBox1.Text Then
                If ventana = "ajustes" Then
                   main.TabControl1.Visible = True
                   main.TabControl1.SelectedIndex = 3
                    view_carrito.Close()
                   main.viewcar.Text = "Ver carrito (0)"
                    '' cargar todos los datos
                    cargarAjustes()
                    Me.Close()
                ElseIf ventana = "reportes" Then
                   main.lcode.Text = cadena_aleatoria(9)
                   main.TabControl1.Visible = True
                   main.TabControl1.SelectedIndex = 4
                   main.TabControl2.SelectedIndex = 0
                   main.cbox_fast.SelectedIndex = 0
                   main.cmese.SelectedIndex = 0
                   main.reporte_para.SelectedIndex = 0
                   main.c_mes.SelectedIndex = 0
                   main.cañoe.SelectedIndex = 0
                   main.c_año.SelectedIndex = 0
                   main.CheckBox1.Enabled = False
                   main.CheckBox1.Checked = False
                   main.CheckBox2.Enabled = False
                   main.CheckBox2.Checked = False
                    Me.Close()
                ElseIf ventana = "consultas" Then
                   main.TabControl1.Visible = True
                   main.TabControl1.SelectedIndex = 6
                    Me.Close()
                ElseIf ventana = "recorte" Then
                   main.TabControl1.Visible = True
                   main.TabControl1.SelectedIndex = 8
                    Me.Close()
                ElseIf ventana = "recortegeneral" Then
                   main.TabControl1.Visible = True
                   main.TabControl1.SelectedIndex = 9
                    Me.Close()
                ElseIf ventana = "pagos_realizados" Then
                   main.Enabled = False
                    view_pagos_realizados.Show()
                    Me.Close()
                ElseIf ventana = "viewcortegeneral" Then
                   main.Enabled = False
                    view_buscar_corte_general.Show()
                    Me.Close()
                ElseIf ventana = "viewcorte" Then
                   main.Enabled = False
                    view_buscar_corte.Show()
                    Me.Close()
                ElseIf ventana = "escritorio" Then
                   main.GetPagos()
                   main.total_ventas()
                   main.total_alumnos()
                   main.total_cursos()
                   main.TabControl1.Visible = True
                   main.TabControl1.SelectedIndex = 10
                    Me.Close()
                End If
            Else
                MsgBox("Contraseña incorrecta.", vbExclamation)
            End If
        Else
            MsgBox("El campo no puede quedar vacío.", vbExclamation)
        End If

    End Sub

    Private Sub txtuser_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Call Button1_Click(sender, e)
        End If
    End Sub

End Class