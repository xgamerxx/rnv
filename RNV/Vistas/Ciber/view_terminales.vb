﻿Imports System.ComponentModel

Public Class view_terminales_
    Private Sub view_terminales__Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        main.Enabled = True
    End Sub

    Private Sub all_string_fields_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "0123456789." 'Permite unicamente esta cadena de caracteres
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text <> "" And txt_client.Text <> "" Then
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Dim boolVerificarMaquina = False
                If Label1.Text = "0" Then
                    boolVerificarMaquina = verificar_maquina(idtxt.Text)
                Else
                    boolVerificarMaquina = False
                End If
                If boolVerificarMaquina = True Then
                    MsgBox("La IP ingresada ya fue registrada anteriormente.", vbExclamation)
                Else
                    If salvar_maquina(TextBox1.Text, idtxt.Text, txt_client.Text) = True Then
                        MsgBox("Registro completado.", vbInformation)
                        main.Enabled = True
                        main.Label152.Visible = True
                        main.Refresh()
                        verificarOrdenadores()
                        Me.Close()
                    Else
                        MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                    End If
                End If
            End If
        Else
            MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
        End If
    End Sub

    Private Sub Button39_Click(sender As Object, e As EventArgs) Handles Button39.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas eliminar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If borrar_maquina(TextBox1.Text) = True Then
                MsgBox("Terminal eliminada.", vbInformation)
                main.Enabled = True
                main.Label152.Visible = True
                main.Refresh()
                verificarOrdenadores()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        view_buscar_terminal.Show()
        Me.Enabled = False
    End Sub
End Class