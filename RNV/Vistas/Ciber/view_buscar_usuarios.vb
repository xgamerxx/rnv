﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class view_buscar_usuarios
    Dim s As New cConexion
    Public isAsignar As Boolean = False
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        filtrar_usuarios(txtbus.Text)
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick
        If IsDataGridViewEmpty(gridProductos) = False Then
            Dim row As DataGridViewRow = gridProductos.CurrentRow
            If isAsignar = False Then
                view_clientes_ciber.Label1.Text = CStr(row.Cells(0).Value)
                view_clientes_ciber.txt_nombres.Text = CStr(row.Cells(1).Value)
                view_clientes_ciber.txt_cod.Text = CStr(row.Cells(2).Value)
                Dim ePw = CStr(row.Cells(3).Value)
                Dim dPW As String
                Dim data() As Byte
                data = System.Convert.FromBase64String(ePw)
                dPW = System.Text.ASCIIEncoding.ASCII.GetString(data)
                view_clientes_ciber.txt_padres.Text = dPW
                view_clientes_ciber.txt_madres.Text = dPW
                view_clientes_ciber.Enabled = True
                view_clientes_ciber.Button39.Visible = True
            Else
                view_asignar_tiempo.txt_nombres.Text = CStr(row.Cells(0).Value)
                view_asignar_tiempo.ComboBox1.Enabled = True
            End If

            Me.Close()
        End If
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        view_clientes_ciber.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar_usuarios(txtbus.Text)
    End Sub
End Class