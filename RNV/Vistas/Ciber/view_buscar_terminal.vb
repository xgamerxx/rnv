﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class view_buscar_terminal
    Dim s As New cConexion

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        filtrar(txtbus.Text)
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick

        If IsDataGridViewEmpty(gridProductos) = False Then
            Dim row As DataGridViewRow = gridProductos.CurrentRow
            view_terminales_.txt_client.Text = CStr(row.Cells(0).Value)
            view_terminales_.TextBox1.Text = CStr(row.Cells(1).Value)
            view_terminales_.idtxt.Text = CStr(row.Cells(2).Value)
            view_terminales_.Enabled = True
            view_terminales_.Button39.Visible = True
            Me.Close()
        End If
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        view_terminales_.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub
End Class