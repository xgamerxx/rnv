﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class view_terminales_
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(view_terminales_))
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_client = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.idtxt = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label36
        '
        Me.Label36.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(12, 62)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 74
        Me.Label36.Text = "Dirección IP *"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 13)
        Me.Label1.TabIndex = 67
        Me.Label1.Text = "Número de terminal *"
        '
        'txt_client
        '
        Me.txt_client.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_client.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_client.Location = New System.Drawing.Point(15, 32)
        Me.txt_client.Name = "txt_client"
        Me.txt_client.Size = New System.Drawing.Size(438, 20)
        Me.txt_client.TabIndex = 66
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(183, 115)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(114, 33)
        Me.Button1.TabIndex = 80
        Me.Button1.Text = "GUARDAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(8, 170)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(292, 13)
        Me.Label2.TabIndex = 81
        Me.Label2.Text = "* La IP la obtienes desde el software instalado en la terminal."
        '
        'Button39
        '
        Me.Button39.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button39.Image = Global.RNV.My.Resources.Resources._1_04_20
        Me.Button39.Location = New System.Drawing.Point(476, 44)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(34, 34)
        Me.Button39.TabIndex = 617
        Me.Button39.TabStop = False
        Me.Button39.UseVisualStyleBackColor = True
        Me.Button39.Visible = False
        '
        'Button37
        '
        Me.Button37.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button37.Image = Global.RNV.My.Resources.Resources.if_pencil_173067
        Me.Button37.Location = New System.Drawing.Point(476, 7)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(34, 34)
        Me.Button37.TabIndex = 616
        Me.Button37.TabStop = False
        Me.Button37.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox1.Location = New System.Drawing.Point(15, 78)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(438, 20)
        Me.TextBox1.TabIndex = 618
        '
        'idtxt
        '
        Me.idtxt.AutoSize = True
        Me.idtxt.Location = New System.Drawing.Point(426, 237)
        Me.idtxt.Name = "idtxt"
        Me.idtxt.Size = New System.Drawing.Size(13, 13)
        Me.idtxt.TabIndex = 619
        Me.idtxt.Text = "0"
        '
        'view_terminales_
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(517, 189)
        Me.Controls.Add(Me.idtxt)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button39)
        Me.Controls.Add(Me.Button37)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_client)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "view_terminales_"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Terminales"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label36 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_client As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Button39 As Button
    Friend WithEvents Button37 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents idtxt As Label
End Class
