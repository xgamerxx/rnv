﻿Imports System.ComponentModel

Public Class view_clientes_ciber
    Private Sub view_clientes_ciber_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        main.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txt_cod.Text <> "" And txt_madres.Text <> "" And txt_padres.Text <> "" And txt_nombres.Text <> "" Then
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                If txt_padres.Text = txt_madres.Text Then
                    Dim boolVerificarMaquina = False
                    If Label1.Text = "0" Then
                        boolVerificarMaquina = verificar_usuario(txt_cod.Text)
                    Else
                        boolVerificarMaquina = False
                    End If
                    If boolVerificarMaquina = True Then
                        MsgBox("El nombre de usuario ya existe. Por favor elige otro.", vbExclamation, "RNV")
                    Else
                        If salvarUsuario(Label1.Text, txt_nombres.Text, txt_cod.Text, txt_padres.Text) = True Then
                            MsgBox("Guardado correctamente!", vbInformation)
                            limpiarCampos()
                        Else
                            MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                        End If
                    End If
                Else
                    MsgBox("Las contraseñas no coinciden.", vbExclamation, "RNV")
                End If
            End If
        Else
            MsgBox("Todos los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
        End If
    End Sub

    Private Sub limpiarCampos()
        txt_cod.Text = ""
        txt_nombres.Text = ""
        txt_padres.Text = ""
        txt_madres.Text = ""
        Label1.Text = "0"
        Button39.Visible = False
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas limpiar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            limpiarCampos()
        End If

    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        Me.Enabled = False
        view_buscar_usuarios.Show()
    End Sub

    Private Sub Button39_Click(sender As Object, e As EventArgs) Handles Button39.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas eliminar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If borrar_usuario(txt_cod.Text) = True Then
                MsgBox("Usuario eliminado.", vbInformation)
                limpiarCampos()
            End If
        End If
    End Sub
End Class