﻿Imports System.ComponentModel

Public Class view_opciones_terminal
    Public status
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ORDENADOR = Net.IPAddress.Parse(Me.Text)
        iniciarPC()
    End Sub

    Private Sub view_opciones_terminal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Label36.Text <> "DESCONECTADO" Then
            refresh_data.Start()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ORDENADOR = Net.IPAddress.Parse(Me.Text)
        detenerPC()
    End Sub

    Private Sub refresh_data_Tick(sender As Object, e As EventArgs) Handles refresh_data.Tick
        refrescarIndividualIP(Me.Text)
    End Sub

    Private Sub view_opciones_terminal_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        main.Enabled = True
        main.Label152.Visible = True
        main.Refresh()
        verificarOrdenadores()
    End Sub
End Class