﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class view_clientes_ciber
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(view_clientes_ciber))
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_cod = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_madres = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_padres = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_nombres = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(12, 51)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(105, 13)
        Me.Label14.TabIndex = 90
        Me.Label14.Text = "Nombre de Usuario *"
        '
        'txt_cod
        '
        Me.txt_cod.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_cod.Location = New System.Drawing.Point(15, 67)
        Me.txt_cod.Name = "txt_cod"
        Me.txt_cod.Size = New System.Drawing.Size(250, 20)
        Me.txt_cod.TabIndex = 79
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 138)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 13)
        Me.Label9.TabIndex = 86
        Me.Label9.Text = "Repetir contraseña *"
        '
        'txt_madres
        '
        Me.txt_madres.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_madres.Location = New System.Drawing.Point(15, 154)
        Me.txt_madres.Name = "txt_madres"
        Me.txt_madres.Size = New System.Drawing.Size(250, 20)
        Me.txt_madres.TabIndex = 81
        Me.txt_madres.UseSystemPasswordChar = True
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 94)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 13)
        Me.Label8.TabIndex = 85
        Me.Label8.Text = "Contraseña *"
        '
        'txt_padres
        '
        Me.txt_padres.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_padres.Location = New System.Drawing.Point(15, 110)
        Me.txt_padres.Name = "txt_padres"
        Me.txt_padres.Size = New System.Drawing.Size(250, 20)
        Me.txt_padres.TabIndex = 80
        Me.txt_padres.UseSystemPasswordChar = True
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(109, 13)
        Me.Label7.TabIndex = 82
        Me.Label7.Text = "Nombres y Apellidos *"
        '
        'txt_nombres
        '
        Me.txt_nombres.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_nombres.Location = New System.Drawing.Point(15, 25)
        Me.txt_nombres.Name = "txt_nombres"
        Me.txt_nombres.Size = New System.Drawing.Size(250, 20)
        Me.txt_nombres.TabIndex = 78
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(41, 190)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 26)
        Me.Button1.TabIndex = 620
        Me.Button1.Text = "GUARDAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(139, 190)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(92, 26)
        Me.Button2.TabIndex = 621
        Me.Button2.Text = "LIMPIAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(400, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(13, 13)
        Me.Label1.TabIndex = 622
        Me.Label1.Text = "0"
        '
        'Button39
        '
        Me.Button39.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button39.Image = Global.RNV.My.Resources.Resources._1_04_20
        Me.Button39.Location = New System.Drawing.Point(281, 44)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(34, 34)
        Me.Button39.TabIndex = 619
        Me.Button39.TabStop = False
        Me.Button39.UseVisualStyleBackColor = True
        Me.Button39.Visible = False
        '
        'Button37
        '
        Me.Button37.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button37.Image = Global.RNV.My.Resources.Resources.if_pencil_173067
        Me.Button37.Location = New System.Drawing.Point(281, 7)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(34, 34)
        Me.Button37.TabIndex = 618
        Me.Button37.TabStop = False
        Me.Button37.UseVisualStyleBackColor = True
        '
        'view_clientes_ciber
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(323, 239)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button39)
        Me.Controls.Add(Me.Button37)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txt_cod)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txt_madres)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txt_padres)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_nombres)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "view_clientes_ciber"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Usuarios"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label14 As Label
    Friend WithEvents txt_cod As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_madres As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_padres As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_nombres As TextBox
    Friend WithEvents Button39 As Button
    Friend WithEvents Button37 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label1 As Label
End Class
