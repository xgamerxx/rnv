﻿Imports System.ComponentModel

Public Class view_carrito
    Private Sub view_carrito_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        e.Cancel = True
        Me.Hide()
       main.Enabled = True
    End Sub

    Private Sub dt_productos_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dt_productos.CellEndEdit
        Try
            If e.ColumnIndex = 2 Then
                Dim cantidad As Integer = CInt(dt_productos.Rows(e.RowIndex).Cells(2).Value)
                Dim precio As Decimal = FormatNumber(dt_productos.Rows(e.RowIndex).Cells(3).Value)
                Dim importe As Decimal = FormatNumber(dt_productos.Rows(e.RowIndex).Cells(4).Value)
                Dim nuevoTotal = (cantidad * precio)
                dt_productos.Rows(e.RowIndex).Cells(4).Value = nuevoTotal
            ElseIf e.ColumnIndex = 3 Then
                Dim cantidad As Integer = CInt(dt_productos.Rows(e.RowIndex).Cells(2).Value)
                Dim precio As Decimal = FormatNumber(dt_productos.Rows(e.RowIndex).Cells(3).Value)
                Dim importe As Decimal = FormatNumber(dt_productos.Rows(e.RowIndex).Cells(4).Value)
                Dim nuevoTotal = (cantidad * precio)
                dt_productos.Rows(e.RowIndex).Cells(4).Value = nuevoTotal
            End If
            Dim tx = 0
            For Each row As DataGridViewRow In dt_productos.Rows
                tx += Val(row.Cells(4).Value)
            Next
            total.Text = "Q" & FormatNumber(tx, 2)
        Catch ex As Exception
        End Try

    End Sub


    Private Sub dt_productos_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dt_productos.UserDeletingRow
       main.viewcar.Text = "Ver carrito (" & dt_productos.Rows.Count - 1 & ")"
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas vaciar el carrito?", "caption", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                dt_productos.Rows.Clear()
                main.viewcar.Text = "Ver carrito (0)"
                MsgBox("Carrito limpio.")
            End If
        Catch ex As Exception
        End Try
        Dim tx As Double
        For Each row As DataGridViewRow In dt_productos.Rows
            tx += Val(row.Cells(4).Value)
        Next
        total.Text = "Q" & FormatNumber(tx, 2)
    End Sub
End Class