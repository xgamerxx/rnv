﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class view_buscar_recibos
    Dim s As New cConexion

    '' para generador
    Public comentario As String = ""
    Public noRecibo As String = ""
    Public para As String = ""
    Public direccion As String = ""
    Public giro As String = ""
    Public codigoestudiante As String = ""
    Public estemes As String = ""

    '' Inside functions

    '' Volver a generar recibo
    Public Sub reRegenerarRecibo()
        reGenerarRecibo.Show()
    End Sub

    Public Sub cargarProductos(id_recibo As String)
        dt_productos.Rows.Clear()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "SELECT id, id_recibo, comentario FROM rnv_cobros WHERE id_recibo = '" & id_recibo & "'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            If reader.Read() Then
                comentario = Convert.ToString(reader(("comentario")))
                noRecibo = Convert.ToString(reader(("id_recibo")))
                Dim dt As New OleDbDataAdapter("SELECT * 
                                                 FROM (rnv_recibos_cobros
                                                 INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_recibos_cobros.id_alumno)
                                                 INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso 
                                                WHERE id_recibo = '" & Convert.ToInt32(reader(("id_recibo"))) & "'", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                For Each dtr As DataRow In ds.Tables(0).Rows
                    dt_productos.Rows.Add(dtr(("codigo_curso")), dtr(("descripcion")), dtr(("cantidad")), dtr(("precio")), dtr(("importe")))
                Next
            End If
            cn.Close()
        End Using
    End Sub

    Sub filtrar(palabra As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "SELECT id_recibo, por, rnv_alumnos.nombres_alumno, total_venta, fecha, comentario, rnv_alumnos.direccion, rnv_cobros.giro, rnv_alumnos.codigo_alumno, rnv_cobros.mes_pago
                                          FROM rnv_cobros
                                          INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente
                                          WHERE mes_pago_nv > 0 AND rnv_cobros.ciclo = " & main.TextBox42.Text & " AND id_recibo LIKE '%" & palabra & "%' OR rnv_alumnos.nombres_alumno LIKE '%" & palabra & "%' OR rnv_cobros.comentario LIKE '%" & palabra & "%'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "No. Recibo"
                gridProductos.Columns(1).HeaderText = "Emitido por"
                gridProductos.Columns(1).Width = 200
                gridProductos.Columns(2).HeaderText = "Para"
                gridProductos.Columns(3).HeaderText = "Total del recibo"
                gridProductos.Columns(4).HeaderText = "Fecha"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Comentario"
                gridProductos.Columns(5).Width = 170
                gridProductos.Columns(6).HeaderText = "Dirección"
                gridProductos.Columns(6).Width = 170
                gridProductos.Columns(7).HeaderText = "Giro"
                gridProductos.Columns(7).Width = 170
                gridProductos.Columns(8).HeaderText = "Codigo estudiante"
                gridProductos.Columns(8).Width = 170
                gridProductos.Columns(9).HeaderText = "Mes pagó"
                gridProductos.Columns(9).Width = 170
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Sub filtrar_vnet(palabra As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "SELECT id_recibo, por, rnv_alumnos.nombres_alumno, total_venta, fecha, comentario, rnv_alumnos.direccion, rnv_cobros.giro, rnv_alumnos.codigo_alumno, rnv_cobros.mes_pago
                                          FROM rnv_cobros
                                          INNER JOIN rnv_alumnos ON rnv_alumnos.id = rnv_cobros.id_cliente
                                          WHERE mes_pago_vnet > 0 AND id_recibo LIKE '%" & palabra & "%' OR rnv_alumnos.nombres_alumno LIKE '%" & palabra & "%' OR rnv_cobros.comentario LIKE '%" & palabra & "%'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "No. Recibo"
                gridProductos.Columns(1).HeaderText = "Emitido por"
                gridProductos.Columns(1).Width = 200
                gridProductos.Columns(2).HeaderText = "Para"
                gridProductos.Columns(3).HeaderText = "Total del recibo"
                gridProductos.Columns(4).HeaderText = "Fecha"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Comentario"
                gridProductos.Columns(5).Width = 170
                gridProductos.Columns(6).HeaderText = "Dirección"
                gridProductos.Columns(6).Width = 170
                gridProductos.Columns(7).HeaderText = "Giro"
                gridProductos.Columns(7).Width = 170
                gridProductos.Columns(8).HeaderText = "Codigo estudiante"
                gridProductos.Columns(8).Width = 170
                gridProductos.Columns(9).HeaderText = "Mes pagó"
                gridProductos.Columns(9).Width = 170
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
       main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        If txtbus.Text <> "" Then
            If ComboBox1.SelectedIndex = 0 Then
                filtrar(txtbus.Text)
            Else
                MsgBox("buscar en vnet")
                filtrar_vnet(txtbus.Text)
            End If
        End If
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick
        Try
            If IsDataGridViewEmpty(gridProductos) = False Then
                Dim row As DataGridViewRow = gridProductos.CurrentRow
                If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\recibos\recibo_" & CStr(row.Cells(0).Value) & ".pdf") Then
                    System.Diagnostics.Process.Start(My.Application.Info.DirectoryPath & "\recibos\recibo_" & CStr(row.Cells(0).Value) & ".pdf")

                Else
                    Label25.Visible = True
                    Me.Refresh()
                    Me.Update()
                    cargarProductos(CStr(row.Cells(0).Value))
                    para = CStr(row.Cells(2).Value)
                    direccion = CStr(row.Cells(6).Value)
                    giro = CStr(row.Cells(7).Value)
                    codigoestudiante = CStr(row.Cells(8).Value)
                    estemes = mess(row.Cells(9).Value)
                    reRegenerarRecibo()
                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\recibos\recibo_" & CStr(row.Cells(0).Value) & ".pdf") Then
                        System.Diagnostics.Process.Start(My.Application.Info.DirectoryPath & "\recibos\recibo_" & CStr(row.Cells(0).Value) & ".pdf")
                    End If
                    reGenerarRecibo.Close()
                    Label25.Visible = False
                End If
                End If
        Catch ex As Exception
            '' es poco probable que tire error
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub

    Private Sub view_buscar_recibos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.SelectedIndex = 0
    End Sub
End Class