﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class view_recibo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(view_recibo))
        Me.dt_ventasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.articulos = New RNV.articulos()
        Me.rv_factura = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.EFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EArticuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label25 = New System.Windows.Forms.Label()
        CType(Me.dt_ventasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.articulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EArticuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dt_ventasBindingSource
        '
        Me.dt_ventasBindingSource.DataMember = "dt_ventas"
        Me.dt_ventasBindingSource.DataSource = Me.articulos
        '
        'articulos
        '
        Me.articulos.DataSetName = "articulos"
        Me.articulos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'rv_factura
        '
        Me.rv_factura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rv_factura.Location = New System.Drawing.Point(0, 0)
        Me.rv_factura.Name = "rv_factura"
        Me.rv_factura.ShowExportButton = False
        Me.rv_factura.ShowStopButton = False
        Me.rv_factura.Size = New System.Drawing.Size(776, 400)
        Me.rv_factura.TabIndex = 0
        '
        'EFacturaBindingSource
        '
        Me.EFacturaBindingSource.DataSource = GetType(RNV.EFactura)
        '
        'EArticuloBindingSource
        '
        Me.EArticuloBindingSource.DataSource = GetType(RNV.EArticulo)
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(6, 168)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(764, 65)
        Me.Label25.TabIndex = 23
        Me.Label25.Text = "ENVIANDO CORREO, POR FAVOR ESPERA..."
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label25.Visible = False
        '
        'view_recibo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 400)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.rv_factura)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "view_recibo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recibo | PNV"
        CType(Me.dt_ventasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.articulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EArticuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dt_ventasBindingSource As BindingSource
    Friend WithEvents articulos As articulos
    Friend WithEvents EFacturaBindingSource As BindingSource
    Friend WithEvents EArticuloBindingSource As BindingSource
    Friend WithEvents rv_factura As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents Label25 As Label
End Class
