﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reGenerarRecibo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.rv_factura = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.dt_ventasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.articulos = New RNV.articulos()
        Me.EFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EArticuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dt_ventasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.articulos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EArticuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rv_factura
        '
        Me.rv_factura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rv_factura.Location = New System.Drawing.Point(0, 0)
        Me.rv_factura.Name = "rv_factura"
        Me.rv_factura.ShowExportButton = False
        Me.rv_factura.ShowStopButton = False
        Me.rv_factura.Size = New System.Drawing.Size(733, 385)
        Me.rv_factura.TabIndex = 1
        '
        'dt_ventasBindingSource
        '
        Me.dt_ventasBindingSource.DataMember = "dt_ventas"
        Me.dt_ventasBindingSource.DataSource = Me.articulos
        '
        'articulos
        '
        Me.articulos.DataSetName = "articulos"
        Me.articulos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EFacturaBindingSource
        '
        Me.EFacturaBindingSource.DataSource = GetType(RNV.EFactura)
        '
        'EArticuloBindingSource
        '
        Me.EArticuloBindingSource.DataSource = GetType(RNV.EArticulo)
        '
        'reGenerarRecibo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(733, 385)
        Me.Controls.Add(Me.rv_factura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "reGenerarRecibo"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "reGenerarRecibo"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        CType(Me.dt_ventasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.articulos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EArticuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents rv_factura As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents dt_ventasBindingSource As BindingSource
    Friend WithEvents articulos As articulos
    Friend WithEvents EFacturaBindingSource As BindingSource
    Friend WithEvents EArticuloBindingSource As BindingSource
End Class
