﻿Imports Microsoft.Reporting.WinForms
Imports System.Drawing.Printing
Imports System.IO
Imports coreRNV
Imports System.ComponentModel

Public Class reGenerarRecibo
    'Encabezado factura y detalle (articulos)
    Public Invoice As New List(Of EFactura)()
    Public Detail As New List(Of EArticulo)()
    'Cree las propiedades publicas Titulo y Empresa
    Public Property Titulo() As String
    Public Property Empresa() As String

    Private Sub InvoiceGenerate()
        Dim invoice As New EFactura()
        Dim Total = 0

        For Each row As DataGridViewRow In view_buscar_recibos.dt_productos.Rows
            Total += Val(row.Cells(4).Value)
        Next
        invoice.recibo = view_buscar_recibos.noRecibo
        invoice.Nombre = view_buscar_recibos.para
        invoice.Direccion = view_buscar_recibos.direccion
        If view_buscar_recibos.comentario = "" Then
            invoice.comentario = ""
        Else
            invoice.comentario = "****> " & view_buscar_recibos.comentario & " <****"
        End If
        If view_buscar_recibos.giro = "Vision NET" Then
            invoice.facebook =main.txt_facebook_vnet.Text
            invoice.datos_empresa =main.txt_slogan_vnet.Text
            invoice.logo = "logornv"
        Else
            invoice.facebook =main.txt_facebook_nv.Text
            invoice.datos_empresa =main.txt_slogan_nv.Text
            invoice.labelestu = "Código de estudiante"
            invoice.codigo_alumno = view_buscar_recibos.codigoestudiante
            invoice.logo = "logopnvflat"
        End If

        invoice.Total = Total
        invoice.mes = view_buscar_recibos.estemes
        invoice.fecha = Format(DateTime.Now, "dd/MM/yyyy")
        For Each row As DataGridViewRow In view_buscar_recibos.dt_productos.Rows
            Dim article As New EArticulo()
            article.codigo = row.Cells(0).Value
            article.descripcion = row.Cells(1).Value
            article.cantidad = row.Cells(2).Value
            article.precio = row.Cells(3).Value
            article.importe = row.Cells(4).Value
            article.total = Total
            invoice.Detail.Add(article)
        Next
        If view_buscar_recibos.giro = "Vision NET" Then
            Titulo =main.txt_titulo_vnet.Text
        Else
            Titulo =main.txt_titulo_nv.Text
        End If

        Dim dec() As String
        dec = Split(FormatNumber(Total), ".")
        Dim ema = Num2Text(Int(Total)) & " QUETZALES " & " CON " & dec(1) & "/100 CENTAVOS"
        Me.Empresa = ema
        Me.Invoice.Add(invoice)
        Me.Detail = invoice.Detail
    End Sub

    Private Function guardar_pdf()
        Try
            Dim ps As PageSettings = rv_factura.GetPageSettings
            Dim ruta_exacta As String = System.Environment.CurrentDirectory & "\recibos\recibo_" & view_buscar_recibos.noRecibo & ".pdf"
            Dim byteViewer As Byte() = rv_factura.LocalReport.Render("PDF")
            Dim pdf_dialogo As New SaveFileDialog()
            pdf_dialogo.Filter = "*PDF files (*.pdf)|*.pdf"
            pdf_dialogo.FilterIndex = 2
            pdf_dialogo.RestoreDirectory = True
            Dim factura_ As New FileStream(ruta_exacta, FileMode.Create)
            factura_.Write(byteViewer, 0, byteViewer.Length)
            factura_.Close()
        Catch ex As Exception
        End Try
        Return True
    End Function

    Private Sub view_recibo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            InvoiceGenerate()
            rv_factura.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
            rv_factura.LocalReport.EnableExternalImages = True
            rv_factura.DocumentMapCollapsed = True
            rv_factura.LocalReport.ReportPath = System.Environment.CurrentDirectory & "\recibo.rdlc"
            rv_factura.LocalReport.DataSources.Clear()
            'Establecemos los margenes de la factura
            Dim instance As New PageSettings()
            Dim value As New Margins(31, 0, 15, 5)
            instance.Margins = value
            rv_factura.SetPageSettings(instance)
            'Limpiemos el DataSource del informe
            rv_factura.LocalReport.DataSources.Clear()
            'Establezcamos los parametros que enviaremos al reporte
            Dim parameters As ReportParameter() = New ReportParameter(1) {}
            parameters(0) = New ReportParameter("parameterTitulo", Titulo)
            parameters(1) = New ReportParameter("parameterEmpresa", Empresa)
            'Establezcamos la lista como Datasource del informe
            rv_factura.LocalReport.DataSources.Add(New ReportDataSource("Encabezado", Invoice))
            rv_factura.LocalReport.DataSources.Add(New ReportDataSource("Detalle", Detail))
            'Enviemos la lista de parametros
            rv_factura.LocalReport.SetParameters(parameters)
            'Hagamos un refresh al reportViewer
            rv_factura.RefreshReport()
           main.Label25.Visible = False
            Dim worker As New BackgroundWorker
            worker.WorkerSupportsCancellation = True
            worker.WorkerReportsProgress = True
            If worker.IsBusy Then
                worker.CancelAsync()
            Else
                worker.RunWorkerAsync(guardar_pdf())
            End If
            Me.Hide()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.rv_factura.RefreshReport()
    End Sub

    Private Sub view_recibo_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Me.Hide()
       main.Enabled = True
    End Sub
End Class