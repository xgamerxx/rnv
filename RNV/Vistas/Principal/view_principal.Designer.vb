﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class view_principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(view_principal))
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label151 = New System.Windows.Forms.Label()
        Me.Button52 = New System.Windows.Forms.Button()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BunifuSeparator8 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.comentario = New System.Windows.Forms.RichTextBox()
        Me.micorreo = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.tipo_producto = New System.Windows.Forms.Label()
        Me.prenormal = New System.Windows.Forms.Label()
        Me.preminimo = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.cbox_select = New System.Windows.Forms.ComboBox()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.codiestu = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.idMaster = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.idx = New System.Windows.Forms.Label()
        Me.txtdireccion = New System.Windows.Forms.Label()
        Me.txt_descripcion = New System.Windows.Forms.Label()
        Me.txt_codigo = New System.Windows.Forms.Label()
        Me.viewcar = New System.Windows.Forms.LinkLabel()
        Me.groupCobro = New System.Windows.Forms.GroupBox()
        Me.Label147 = New System.Windows.Forms.Label()
        Me.LinkLabel6 = New System.Windows.Forms.LinkLabel()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cantidad = New System.Windows.Forms.NumericUpDown()
        Me.txtpagar = New System.Windows.Forms.TextBox()
        Me.cobro_precio = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbox_mes = New System.Windows.Forms.ComboBox()
        Me.cbox_cobro_curso = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_client = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.BunifuSeparator2 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.Label159 = New System.Windows.Forms.Label()
        Me.txt_ciclo_alumno = New System.Windows.Forms.TextBox()
        Me.LinkLabel11 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel12 = New System.Windows.Forms.LinkLabel()
        Me.ciclo_alumnos = New System.Windows.Forms.Label()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.txt_correo = New System.Windows.Forms.TextBox()
        Me.txt_insc = New System.Windows.Forms.MaskedTextBox()
        Me.txt_nac = New System.Windows.Forms.MaskedTextBox()
        Me.txt_realID = New System.Windows.Forms.Label()
        Me.reasig = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_id = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_tel3 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_cod = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_direccion = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_tel2 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_tel1 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_madres = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_padres = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_nombres = New System.Windows.Forms.TextBox()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.btnDelete_al = New System.Windows.Forms.Button()
        Me.btn_editar_alum = New System.Windows.Forms.Button()
        Me.tabpage2 = New System.Windows.Forms.TabPage()
        Me.Label157 = New System.Windows.Forms.Label()
        Me.txt_ciclo_curso = New System.Windows.Forms.TextBox()
        Me.LinkLabel13 = New System.Windows.Forms.LinkLabel()
        Me.ciclo_cursos = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.cbox_servi = New System.Windows.Forms.ComboBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.txt_desc = New System.Windows.Forms.RichTextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_precio = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_cod_curso = New System.Windows.Forms.TextBox()
        Me.btnGuardar_curso = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txt_curso = New System.Windows.Forms.TextBox()
        Me.btn_delete_curso = New System.Windows.Forms.Button()
        Me.btnEdit_curso = New System.Windows.Forms.Button()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox24 = New System.Windows.Forms.GroupBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.TextBox42 = New System.Windows.Forms.TextBox()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Button56 = New System.Windows.Forms.Button()
        Me.GroupBox23 = New System.Windows.Forms.GroupBox()
        Me.Label146 = New System.Windows.Forms.Label()
        Me.TextBox41 = New System.Windows.Forms.TextBox()
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown()
        Me.Label144 = New System.Windows.Forms.Label()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.Label143 = New System.Windows.Forms.Label()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.Label141 = New System.Windows.Forms.Label()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.Label140 = New System.Windows.Forms.Label()
        Me.Button47 = New System.Windows.Forms.Button()
        Me.txtsafe2 = New System.Windows.Forms.Label()
        Me.txtsafe1 = New System.Windows.Forms.Label()
        Me.GroupBox22 = New System.Windows.Forms.GroupBox()
        Me.txt_cuerpo_email = New System.Windows.Forms.RichTextBox()
        Me.Label138 = New System.Windows.Forms.Label()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.Button44 = New System.Windows.Forms.Button()
        Me.GroupBox21 = New System.Windows.Forms.GroupBox()
        Me.frontal = New System.Windows.Forms.CheckBox()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.maxdias = New System.Windows.Forms.NumericUpDown()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Button32 = New System.Windows.Forms.Button()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.btnGuardarProtected = New System.Windows.Forms.Button()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txt_protected2 = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txt_protected = New System.Windows.Forms.TextBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnGuardar_pass = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtpass2 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtpass = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnGuardar_anv = New System.Windows.Forms.Button()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txt_facebook_vnet = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txt_logo_vnet = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txt_slogan_vnet = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txt_titulo_vnet = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_Guardar_vnet = New System.Windows.Forms.Button()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txt_facebook_nv = New System.Windows.Forms.TextBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txt_logo_nv = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txt_slogan_nv = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txt_titulo_nv = New System.Windows.Forms.TextBox()
        Me.Label142 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.lcode = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.texto_consulta = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.reporte_para = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.lblfechafecha = New System.Windows.Forms.Label()
        Me.lblfecha = New System.Windows.Forms.Label()
        Me.datafecha2 = New System.Windows.Forms.DateTimePicker()
        Me.datafecha1 = New System.Windows.Forms.DateTimePicker()
        Me.datafecha = New System.Windows.Forms.DateTimePicker()
        Me.c_user = New System.Windows.Forms.ComboBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.cbox_fast = New System.Windows.Forms.ComboBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.lblmes = New System.Windows.Forms.Label()
        Me.c_mes = New System.Windows.Forms.ComboBox()
        Me.lblaño = New System.Windows.Forms.Label()
        Me.c_año = New System.Windows.Forms.ComboBox()
        Me.lblañomes = New System.Windows.Forms.Label()
        Me.cañoe = New System.Windows.Forms.ComboBox()
        Me.cmese = New System.Windows.Forms.ComboBox()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.lupdate = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.BunifuSeparator1 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.BunifuImageButton1 = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.Button48 = New System.Windows.Forms.Button()
        Me.Button43 = New System.Windows.Forms.Button()
        Me.Button41 = New System.Windows.Forms.Button()
        Me.Button40 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.jtid = New System.Windows.Forms.TextBox()
        Me.mostrarpagos = New System.Windows.Forms.LinkLabel()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.MaskedTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator3 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.BunifuSeparator5 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator6 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.codigorecorte = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator4 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TextBox33 = New System.Windows.Forms.TextBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.TextBox32 = New System.Windows.Forms.TextBox()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.TextBox46 = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator9 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.Label167 = New System.Windows.Forms.Label()
        Me.GroupBox26 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel14 = New System.Windows.Forms.LinkLabel()
        Me.GroupBox25 = New System.Windows.Forms.GroupBox()
        Me.Label165 = New System.Windows.Forms.Label()
        Me.tprod = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.GroupBox20 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel()
        Me.BunifuSeparator7 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.GroupBox18 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.GroupBox19 = New System.Windows.Forms.GroupBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.TabPage16 = New System.Windows.Forms.TabPage()
        Me.LinkLabel10 = New System.Windows.Forms.LinkLabel()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.id_inventario = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.TextBox39 = New System.Windows.Forms.TextBox()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.TextBox38 = New System.Windows.Forms.TextBox()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.TextBox37 = New System.Windows.Forms.TextBox()
        Me.Button34 = New System.Windows.Forms.Button()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.cod_inventario = New System.Windows.Forms.TextBox()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.TextBox36 = New System.Windows.Forms.TextBox()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button38 = New System.Windows.Forms.Button()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.gridTicket = New System.Windows.Forms.DataGridView()
        Me.descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_venta = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.LinkLabel8 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel7 = New System.Windows.Forms.LinkLabel()
        Me.TextBox34 = New System.Windows.Forms.TextBox()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.RichTextBox4 = New System.Windows.Forms.RichTextBox()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.TextBox35 = New System.Windows.Forms.TextBox()
        Me.TextBox40 = New System.Windows.Forms.TextBox()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.TextBox45 = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator10 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label126 = New System.Windows.Forms.Label()
        Me.Button42 = New System.Windows.Forms.Button()
        Me.TabPage18 = New System.Windows.Forms.TabPage()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.stock_actual = New System.Windows.Forms.TextBox()
        Me.Label137 = New System.Windows.Forms.Label()
        Me.tipo_de_producto = New System.Windows.Forms.TextBox()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.precio_minimo = New System.Windows.Forms.TextBox()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.precio_venta = New System.Windows.Forms.TextBox()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.LinkLabel9 = New System.Windows.Forms.LinkLabel()
        Me.cod_producto = New System.Windows.Forms.TextBox()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.descripcion_producto = New System.Windows.Forms.RichTextBox()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.precio_compra = New System.Windows.Forms.TextBox()
        Me.nombre_producto = New System.Windows.Forms.TextBox()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.TextBox44 = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator11 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.Button45 = New System.Windows.Forms.Button()
        Me.TabPage19 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox51 = New System.Windows.Forms.TextBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.Button46 = New System.Windows.Forms.Button()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label139 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.gridProductos = New System.Windows.Forms.DataGridView()
        Me.Label145 = New System.Windows.Forms.Label()
        Me.BunifuSeparator12 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.TabPage20 = New System.Windows.Forms.TabPage()
        Me.Button57 = New System.Windows.Forms.Button()
        Me.Button54 = New System.Windows.Forms.Button()
        Me.Label153 = New System.Windows.Forms.Label()
        Me.Label152 = New System.Windows.Forms.Label()
        Me.Label150 = New System.Windows.Forms.Label()
        Me.Button51 = New System.Windows.Forms.Button()
        Me.pc_rnv = New System.Windows.Forms.DataGridView()
        Me.codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button49 = New System.Windows.Forms.Button()
        Me.Button50 = New System.Windows.Forms.Button()
        Me.Label148 = New System.Windows.Forms.Label()
        Me.Label149 = New System.Windows.Forms.Label()
        Me.Button53 = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.BunifuSeparator13 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.TabPage21 = New System.Windows.Forms.TabPage()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label154 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label155 = New System.Windows.Forms.Label()
        Me.Label156 = New System.Windows.Forms.Label()
        Me.TextBox48 = New System.Windows.Forms.TextBox()
        Me.Label158 = New System.Windows.Forms.Label()
        Me.TextBox49 = New System.Windows.Forms.TextBox()
        Me.Label161 = New System.Windows.Forms.Label()
        Me.TextBox50 = New System.Windows.Forms.TextBox()
        Me.TextBox52 = New System.Windows.Forms.TextBox()
        Me.Label162 = New System.Windows.Forms.Label()
        Me.Label163 = New System.Windows.Forms.Label()
        Me.BunifuSeparator15 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label160 = New System.Windows.Forms.Label()
        Me.TextBox53 = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator14 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.Label164 = New System.Windows.Forms.Label()
        Me.Button55 = New System.Windows.Forms.Button()
        Me.TabPage22 = New System.Windows.Forms.TabPage()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.openFD = New System.Windows.Forms.OpenFileDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.refreshPC = New System.Windows.Forms.Timer(Me.components)
        Me.Articulos1 = New RNV.articulos()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AbrirRNVToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SincronizarYCerrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.Label166 = New System.Windows.Forms.Label()
        Me.EpersonascobroBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.groupCobro.SuspendLayout()
        CType(Me.cantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.tabpage2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox24.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox22.SuspendLayout()
        Me.GroupBox21.SuspendLayout()
        CType(Me.maxdias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage9.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage12.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TabPage13.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TabPage14.SuspendLayout()
        Me.GroupBox26.SuspendLayout()
        Me.GroupBox25.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage15.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        Me.TabPage16.SuspendLayout()
        Me.TabPage17.SuspendLayout()
        CType(Me.gridTicket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.TabPage18.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.TabPage19.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.gridProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage20.SuspendLayout()
        CType(Me.pc_rnv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage21.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel13.SuspendLayout()
        CType(Me.Articulos1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EpersonascobroBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label151)
        Me.GroupBox1.Controls.Add(Me.Button52)
        Me.GroupBox1.Controls.Add(Me.Button33)
        Me.GroupBox1.Controls.Add(Me.Button29)
        Me.GroupBox1.Controls.Add(Me.Button22)
        Me.GroupBox1.Controls.Add(Me.Label35)
        Me.GroupBox1.Controls.Add(Me.Button15)
        Me.GroupBox1.Controls.Add(Me.Button13)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.BunifuSeparator8)
        Me.GroupBox1.Location = New System.Drawing.Point(-8, -13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(185, 492)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'Label151
        '
        Me.Label151.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label151.AutoSize = True
        Me.Label151.ForeColor = System.Drawing.Color.Red
        Me.Label151.Location = New System.Drawing.Point(101, 20)
        Me.Label151.Name = "Label151"
        Me.Label151.Size = New System.Drawing.Size(79, 13)
        Me.Label151.TabIndex = 17
        Me.Label151.Text = "¡Próximamente!"
        '
        'Button52
        '
        Me.Button52.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button52.Enabled = False
        Me.Button52.Image = Global.RNV.My.Resources.Resources.laptop
        Me.Button52.Location = New System.Drawing.Point(122, 36)
        Me.Button52.Name = "Button52"
        Me.Button52.Size = New System.Drawing.Size(36, 34)
        Me.Button52.TabIndex = 16
        Me.Button52.TabStop = False
        Me.Button52.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button52.UseVisualStyleBackColor = True
        '
        'Button33
        '
        Me.Button33.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button33.Image = Global.RNV.My.Resources.Resources.dropbox_24
        Me.Button33.Location = New System.Drawing.Point(80, 36)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(36, 34)
        Me.Button33.TabIndex = 13
        Me.Button33.TabStop = False
        Me.Button33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button33.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button29.Image = Global.RNV.My.Resources.Resources.dashboard_24
        Me.Button29.Location = New System.Drawing.Point(38, 36)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(36, 34)
        Me.Button29.TabIndex = 12
        Me.Button29.TabStop = False
        Me.Button29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button29.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button22.Image = Global.RNV.My.Resources.Resources.consulta
        Me.Button22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button22.Location = New System.Drawing.Point(28, 323)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(138, 34)
        Me.Button22.TabIndex = 11
        Me.Button22.TabStop = False
        Me.Button22.Text = "Consultas"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label35.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label35.Location = New System.Drawing.Point(20, 439)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(146, 13)
        Me.Label35.TabIndex = 10
        Me.Label35.Text = "RNV | v1.7.0"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Button15
        '
        Me.Button15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button15.Image = Global.RNV.My.Resources.Resources.chat_24
        Me.Button15.Location = New System.Drawing.Point(-39, 429)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(34, 30)
        Me.Button15.TabIndex = 9
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button13.Image = Global.RNV.My.Resources.Resources._519840_52_Cloud_Sync_20
        Me.Button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button13.Location = New System.Drawing.Point(28, 226)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(138, 34)
        Me.Button13.TabIndex = 8
        Me.Button13.TabStop = False
        Me.Button13.Text = "RNV SYNC"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6.Image = Global.RNV.My.Resources.Resources._25_icons_20
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.Location = New System.Drawing.Point(28, 369)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(138, 34)
        Me.Button6.TabIndex = 6
        Me.Button6.TabStop = False
        Me.Button6.Text = "Ajustes"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.Image = Global.RNV.My.Resources.Resources.Clipboard_20
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(28, 275)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(138, 34)
        Me.Button5.TabIndex = 5
        Me.Button5.TabStop = False
        Me.Button5.Text = "Reportes"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.Image = Global.RNV.My.Resources.Resources.settings_201
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(28, 178)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(138, 34)
        Me.Button3.TabIndex = 4
        Me.Button3.TabStop = False
        Me.Button3.Text = "Cur / Serv"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.Image = Global.RNV.My.Resources.Resources.group_20
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(28, 129)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(138, 34)
        Me.Button2.TabIndex = 3
        Me.Button2.TabStop = False
        Me.Button2.Text = "Alumn / Clien"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Image = Global.RNV.My.Resources.Resources._1_12_20
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(28, 80)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(138, 34)
        Me.Button1.TabIndex = 2
        Me.Button1.TabStop = False
        Me.Button1.Text = "Cobro"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'BunifuSeparator8
        '
        Me.BunifuSeparator8.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator8.LineThickness = 1
        Me.BunifuSeparator8.Location = New System.Drawing.Point(41, 405)
        Me.BunifuSeparator8.Name = "BunifuSeparator8"
        Me.BunifuSeparator8.Size = New System.Drawing.Size(110, 35)
        Me.BunifuSeparator8.TabIndex = 15
        Me.BunifuSeparator8.TabStop = False
        Me.BunifuSeparator8.Transparency = 255
        Me.BunifuSeparator8.Vertical = False
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.tabpage2)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage11)
        Me.TabControl1.Controls.Add(Me.TabPage12)
        Me.TabControl1.Controls.Add(Me.TabPage13)
        Me.TabControl1.Controls.Add(Me.TabPage14)
        Me.TabControl1.Controls.Add(Me.TabPage15)
        Me.TabControl1.Controls.Add(Me.TabPage16)
        Me.TabControl1.Controls.Add(Me.TabPage17)
        Me.TabControl1.Controls.Add(Me.TabPage18)
        Me.TabControl1.Controls.Add(Me.TabPage19)
        Me.TabControl1.Controls.Add(Me.TabPage20)
        Me.TabControl1.Controls.Add(Me.TabPage21)
        Me.TabControl1.Controls.Add(Me.TabPage22)
        Me.TabControl1.Location = New System.Drawing.Point(172, -30)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(645, 486)
        Me.TabControl1.TabIndex = 1
        Me.TabControl1.Visible = False
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.comentario)
        Me.TabPage3.Controls.Add(Me.micorreo)
        Me.TabPage3.Controls.Add(Me.ListBox1)
        Me.TabPage3.Controls.Add(Me.tipo_producto)
        Me.TabPage3.Controls.Add(Me.prenormal)
        Me.TabPage3.Controls.Add(Me.preminimo)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Controls.Add(Me.cbox_select)
        Me.TabPage3.Controls.Add(Me.Button21)
        Me.TabPage3.Controls.Add(Me.LinkLabel1)
        Me.TabPage3.Controls.Add(Me.codiestu)
        Me.TabPage3.Controls.Add(Me.Button11)
        Me.TabPage3.Controls.Add(Me.idMaster)
        Me.TabPage3.Controls.Add(Me.Label25)
        Me.TabPage3.Controls.Add(Me.idx)
        Me.TabPage3.Controls.Add(Me.txtdireccion)
        Me.TabPage3.Controls.Add(Me.txt_descripcion)
        Me.TabPage3.Controls.Add(Me.txt_codigo)
        Me.TabPage3.Controls.Add(Me.viewcar)
        Me.TabPage3.Controls.Add(Me.groupCobro)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.cbox_mes)
        Me.TabPage3.Controls.Add(Me.cbox_cobro_curso)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.Label1)
        Me.TabPage3.Controls.Add(Me.txt_client)
        Me.TabPage3.Controls.Add(Me.Button4)
        Me.TabPage3.Controls.Add(Me.BunifuSeparator2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(637, 460)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "Cobros"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'comentario
        '
        Me.comentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.comentario.Location = New System.Drawing.Point(272, 250)
        Me.comentario.Name = "comentario"
        Me.comentario.Size = New System.Drawing.Size(335, 96)
        Me.comentario.TabIndex = 61
        Me.comentario.Text = ""
        Me.comentario.Visible = False
        '
        'micorreo
        '
        Me.micorreo.AutoSize = True
        Me.micorreo.Location = New System.Drawing.Point(664, 254)
        Me.micorreo.Name = "micorreo"
        Me.micorreo.Size = New System.Drawing.Size(0, 13)
        Me.micorreo.TabIndex = 70
        '
        'ListBox1
        '
        Me.ListBox1.ColumnWidth = 15
        Me.ListBox1.Font = New System.Drawing.Font("Lucida Console", 9.0!)
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 12
        Me.ListBox1.Location = New System.Drawing.Point(746, -22)
        Me.ListBox1.MultiColumn = True
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(68, 256)
        Me.ListBox1.TabIndex = 69
        '
        'tipo_producto
        '
        Me.tipo_producto.AutoSize = True
        Me.tipo_producto.Location = New System.Drawing.Point(743, 126)
        Me.tipo_producto.Name = "tipo_producto"
        Me.tipo_producto.Size = New System.Drawing.Size(0, 13)
        Me.tipo_producto.TabIndex = 68
        '
        'prenormal
        '
        Me.prenormal.AutoSize = True
        Me.prenormal.Location = New System.Drawing.Point(60, -16)
        Me.prenormal.Name = "prenormal"
        Me.prenormal.Size = New System.Drawing.Size(13, 13)
        Me.prenormal.TabIndex = 67
        Me.prenormal.Text = "0"
        '
        'preminimo
        '
        Me.preminimo.AutoSize = True
        Me.preminimo.Location = New System.Drawing.Point(36, -24)
        Me.preminimo.Name = "preminimo"
        Me.preminimo.Size = New System.Drawing.Size(13, 13)
        Me.preminimo.TabIndex = 66
        Me.preminimo.Text = "0"
        '
        'Label36
        '
        Me.Label36.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(95, 68)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(66, 13)
        Me.Label36.TabIndex = 65
        Me.Label36.Text = "Cobro para *"
        '
        'cbox_select
        '
        Me.cbox_select.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_select.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbox_select.FormattingEnabled = True
        Me.cbox_select.Items.AddRange(New Object() {"-Selecciona cobro para-", "Academia NV", "Vision NET", "Inventario NV"})
        Me.cbox_select.Location = New System.Drawing.Point(98, 84)
        Me.cbox_select.Name = "cbox_select"
        Me.cbox_select.Size = New System.Drawing.Size(439, 21)
        Me.cbox_select.TabIndex = 64
        '
        'Button21
        '
        Me.Button21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button21.Image = Global.RNV.My.Resources.Resources.Cut_20
        Me.Button21.Location = New System.Drawing.Point(595, 63)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(34, 34)
        Me.Button21.TabIndex = 62
        Me.Button21.TabStop = False
        Me.Button21.Tag = "Corte de caja"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel1.Location = New System.Drawing.Point(394, 361)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(142, 13)
        Me.LinkLabel1.TabIndex = 60
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Agregar comentario al recibo"
        Me.LinkLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'codiestu
        '
        Me.codiestu.Location = New System.Drawing.Point(-144, 95)
        Me.codiestu.Name = "codiestu"
        Me.codiestu.Size = New System.Drawing.Size(100, 23)
        Me.codiestu.TabIndex = 59
        Me.codiestu.Text = "Label54"
        '
        'Button11
        '
        Me.Button11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button11.Image = Global.RNV.My.Resources.Resources.Search_20
        Me.Button11.Location = New System.Drawing.Point(595, 15)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(34, 34)
        Me.Button11.TabIndex = 58
        Me.Button11.TabStop = False
        Me.Button11.Tag = "Buscar recibos anteriores"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'idMaster
        '
        Me.idMaster.AutoSize = True
        Me.idMaster.Location = New System.Drawing.Point(-993, 72)
        Me.idMaster.Name = "idMaster"
        Me.idMaster.Size = New System.Drawing.Size(45, 13)
        Me.idMaster.TabIndex = 23
        Me.idMaster.Text = "Label36"
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(3, 164)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(631, 65)
        Me.Label25.TabIndex = 22
        Me.Label25.Text = "POR FAVOR, ESPERA..."
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label25.Visible = False
        '
        'idx
        '
        Me.idx.AutoSize = True
        Me.idx.Location = New System.Drawing.Point(135, -63)
        Me.idx.Name = "idx"
        Me.idx.Size = New System.Drawing.Size(45, 13)
        Me.idx.TabIndex = 21
        Me.idx.Text = "Label25"
        '
        'txtdireccion
        '
        Me.txtdireccion.AutoSize = True
        Me.txtdireccion.Location = New System.Drawing.Point(134, -41)
        Me.txtdireccion.Name = "txtdireccion"
        Me.txtdireccion.Size = New System.Drawing.Size(67, 13)
        Me.txtdireccion.TabIndex = 20
        Me.txtdireccion.Text = "txt_direccion"
        '
        'txt_descripcion
        '
        Me.txt_descripcion.AutoSize = True
        Me.txt_descripcion.Location = New System.Drawing.Point(142, -41)
        Me.txt_descripcion.Name = "txt_descripcion"
        Me.txt_descripcion.Size = New System.Drawing.Size(13, 13)
        Me.txt_descripcion.TabIndex = 19
        Me.txt_descripcion.Text = "0"
        '
        'txt_codigo
        '
        Me.txt_codigo.AutoSize = True
        Me.txt_codigo.Location = New System.Drawing.Point(142, -60)
        Me.txt_codigo.Name = "txt_codigo"
        Me.txt_codigo.Size = New System.Drawing.Size(13, 13)
        Me.txt_codigo.TabIndex = 18
        Me.txt_codigo.Text = "0"
        '
        'viewcar
        '
        Me.viewcar.AutoSize = True
        Me.viewcar.Location = New System.Drawing.Point(106, 335)
        Me.viewcar.Name = "viewcar"
        Me.viewcar.Size = New System.Drawing.Size(70, 13)
        Me.viewcar.TabIndex = 17
        Me.viewcar.TabStop = True
        Me.viewcar.Text = "Ver carrito (0)"
        '
        'groupCobro
        '
        Me.groupCobro.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupCobro.Controls.Add(Me.Label147)
        Me.groupCobro.Controls.Add(Me.LinkLabel6)
        Me.groupCobro.Controls.Add(Me.btnAdd)
        Me.groupCobro.Controls.Add(Me.Label23)
        Me.groupCobro.Controls.Add(Me.Label22)
        Me.groupCobro.Controls.Add(Me.Label21)
        Me.groupCobro.Controls.Add(Me.cantidad)
        Me.groupCobro.Controls.Add(Me.txtpagar)
        Me.groupCobro.Controls.Add(Me.cobro_precio)
        Me.groupCobro.Location = New System.Drawing.Point(100, 163)
        Me.groupCobro.Name = "groupCobro"
        Me.groupCobro.Size = New System.Drawing.Size(433, 193)
        Me.groupCobro.TabIndex = 13
        Me.groupCobro.TabStop = False
        Me.groupCobro.Text = "Detalles"
        '
        'Label147
        '
        Me.Label147.AutoSize = True
        Me.Label147.ForeColor = System.Drawing.Color.Red
        Me.Label147.Location = New System.Drawing.Point(81, 109)
        Me.Label147.Name = "Label147"
        Me.Label147.Size = New System.Drawing.Size(41, 13)
        Me.Label147.TabIndex = 67
        Me.Label147.Text = "@mora"
        Me.Label147.Visible = False
        '
        'LinkLabel6
        '
        Me.LinkLabel6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel6.Location = New System.Drawing.Point(233, 21)
        Me.LinkLabel6.Name = "LinkLabel6"
        Me.LinkLabel6.Size = New System.Drawing.Size(142, 13)
        Me.LinkLabel6.TabIndex = 66
        Me.LinkLabel6.TabStop = True
        Me.LinkLabel6.Text = "Usar precio mínimo"
        Me.LinkLabel6.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LinkLabel6.Visible = False
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(161, 153)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(95, 30)
        Me.btnAdd.TabIndex = 14
        Me.btnAdd.Text = "Agregar al carrito"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(49, 109)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(31, 13)
        Me.Label23.TabIndex = 16
        Me.Label23.Text = "Total"
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(49, 66)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(49, 13)
        Me.Label22.TabIndex = 15
        Me.Label22.Text = "Cantidad"
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(49, 22)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(44, 13)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Precio *"
        '
        'cantidad
        '
        Me.cantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cantidad.Location = New System.Drawing.Point(52, 82)
        Me.cantidad.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cantidad.Name = "cantidad"
        Me.cantidad.Size = New System.Drawing.Size(323, 20)
        Me.cantidad.TabIndex = 2
        Me.cantidad.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtpagar
        '
        Me.txtpagar.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtpagar.Location = New System.Drawing.Point(52, 125)
        Me.txtpagar.Name = "txtpagar"
        Me.txtpagar.ReadOnly = True
        Me.txtpagar.Size = New System.Drawing.Size(323, 20)
        Me.txtpagar.TabIndex = 1
        '
        'cobro_precio
        '
        Me.cobro_precio.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cobro_precio.Location = New System.Drawing.Point(52, 38)
        Me.cobro_precio.Name = "cobro_precio"
        Me.cobro_precio.Size = New System.Drawing.Size(323, 20)
        Me.cobro_precio.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(98, 436)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "XXXXX"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 435)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Número de recibo:"
        '
        'cbox_mes
        '
        Me.cbox_mes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_mes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbox_mes.FormattingEnabled = True
        Me.cbox_mes.Location = New System.Drawing.Point(98, 380)
        Me.cbox_mes.Name = "cbox_mes"
        Me.cbox_mes.Size = New System.Drawing.Size(438, 21)
        Me.cbox_mes.TabIndex = 8
        '
        'cbox_cobro_curso
        '
        Me.cbox_cobro_curso.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_cobro_curso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbox_cobro_curso.Enabled = False
        Me.cbox_cobro_curso.FormattingEnabled = True
        Me.cbox_cobro_curso.Location = New System.Drawing.Point(98, 131)
        Me.cbox_cobro_curso.Name = "cbox_cobro_curso"
        Me.cbox_cobro_curso.Size = New System.Drawing.Size(438, 21)
        Me.cbox_cobro_curso.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(98, 361)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(158, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Mes (Solo AcademiaNV y Vnet):"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(95, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Curso / Servicio"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(95, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Cliente / Alumno / Producto *"
        '
        'txt_client
        '
        Me.txt_client.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_client.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txt_client.Enabled = False
        Me.txt_client.Location = New System.Drawing.Point(98, 38)
        Me.txt_client.Name = "txt_client"
        Me.txt_client.ReadOnly = True
        Me.txt_client.Size = New System.Drawing.Size(438, 20)
        Me.txt_client.TabIndex = 1
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.Location = New System.Drawing.Point(528, 420)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(95, 30)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "Guardar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'BunifuSeparator2
        '
        Me.BunifuSeparator2.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator2.LineThickness = 1
        Me.BunifuSeparator2.Location = New System.Drawing.Point(601, 39)
        Me.BunifuSeparator2.Name = "BunifuSeparator2"
        Me.BunifuSeparator2.Size = New System.Drawing.Size(24, 35)
        Me.BunifuSeparator2.TabIndex = 63
        Me.BunifuSeparator2.TabStop = False
        Me.BunifuSeparator2.Transparency = 255
        Me.BunifuSeparator2.Vertical = False
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label166)
        Me.TabPage1.Controls.Add(Me.ComboBox5)
        Me.TabPage1.Controls.Add(Me.ComboBox4)
        Me.TabPage1.Controls.Add(Me.Label159)
        Me.TabPage1.Controls.Add(Me.txt_ciclo_alumno)
        Me.TabPage1.Controls.Add(Me.LinkLabel11)
        Me.TabPage1.Controls.Add(Me.LinkLabel12)
        Me.TabPage1.Controls.Add(Me.ciclo_alumnos)
        Me.TabPage1.Controls.Add(Me.Label123)
        Me.TabPage1.Controls.Add(Me.txt_correo)
        Me.TabPage1.Controls.Add(Me.txt_insc)
        Me.TabPage1.Controls.Add(Me.txt_nac)
        Me.TabPage1.Controls.Add(Me.txt_realID)
        Me.TabPage1.Controls.Add(Me.reasig)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel1)
        Me.TabPage1.Controls.Add(Me.Button10)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.txt_id)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.btnGuardar)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.txt_tel3)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.txt_cod)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.txt_direccion)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.txt_tel2)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.txt_tel1)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.txt_madres)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.txt_padres)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.txt_nombres)
        Me.TabPage1.Controls.Add(Me.Label125)
        Me.TabPage1.Controls.Add(Me.btnDelete_al)
        Me.TabPage1.Controls.Add(Me.btn_editar_alum)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(637, 460)
        Me.TabPage1.TabIndex = 1
        Me.TabPage1.Text = "Alumnos_clientes"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'ComboBox4
        '
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Items.AddRange(New Object() {"Academia NV", "Vision NET"})
        Me.ComboBox4.Location = New System.Drawing.Point(336, 216)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(237, 21)
        Me.ComboBox4.TabIndex = 12
        '
        'Label159
        '
        Me.Label159.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label159.AutoSize = True
        Me.Label159.Location = New System.Drawing.Point(13, 333)
        Me.Label159.Name = "Label159"
        Me.Label159.Size = New System.Drawing.Size(160, 13)
        Me.Label159.TabIndex = 77
        Me.Label159.Text = "Ciclo (Aplica para Academia NV)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txt_ciclo_alumno
        '
        Me.txt_ciclo_alumno.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_ciclo_alumno.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_ciclo_alumno.Location = New System.Drawing.Point(16, 349)
        Me.txt_ciclo_alumno.Name = "txt_ciclo_alumno"
        Me.txt_ciclo_alumno.Size = New System.Drawing.Size(309, 20)
        Me.txt_ciclo_alumno.TabIndex = 7
        '
        'LinkLabel11
        '
        Me.LinkLabel11.AutoSize = True
        Me.LinkLabel11.Location = New System.Drawing.Point(639, 255)
        Me.LinkLabel11.Name = "LinkLabel11"
        Me.LinkLabel11.Size = New System.Drawing.Size(45, 13)
        Me.LinkLabel11.TabIndex = 75
        Me.LinkLabel11.TabStop = True
        Me.LinkLabel11.Text = "Cambiar"
        Me.LinkLabel11.Visible = False
        '
        'LinkLabel12
        '
        Me.LinkLabel12.AutoSize = True
        Me.LinkLabel12.Location = New System.Drawing.Point(-47, 376)
        Me.LinkLabel12.Name = "LinkLabel12"
        Me.LinkLabel12.Size = New System.Drawing.Size(45, 13)
        Me.LinkLabel12.TabIndex = 70
        Me.LinkLabel12.TabStop = True
        Me.LinkLabel12.Text = "Cambiar"
        '
        'ciclo_alumnos
        '
        Me.ciclo_alumnos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ciclo_alumnos.ForeColor = System.Drawing.Color.Red
        Me.ciclo_alumnos.Location = New System.Drawing.Point(-104, 376)
        Me.ciclo_alumnos.Name = "ciclo_alumnos"
        Me.ciclo_alumnos.Size = New System.Drawing.Size(57, 13)
        Me.ciclo_alumnos.TabIndex = 69
        Me.ciclo_alumnos.Text = "@ciclo"
        Me.ciclo_alumnos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label123
        '
        Me.Label123.AutoSize = True
        Me.Label123.Location = New System.Drawing.Point(13, 284)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(93, 13)
        Me.Label123.TabIndex = 68
        Me.Label123.Text = "Correo electrónico"
        '
        'txt_correo
        '
        Me.txt_correo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_correo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_correo.Location = New System.Drawing.Point(16, 300)
        Me.txt_correo.Name = "txt_correo"
        Me.txt_correo.Size = New System.Drawing.Size(309, 20)
        Me.txt_correo.TabIndex = 6
        '
        'txt_insc
        '
        Me.txt_insc.Location = New System.Drawing.Point(336, 166)
        Me.txt_insc.Mask = "00/00/0000"
        Me.txt_insc.Name = "txt_insc"
        Me.txt_insc.Size = New System.Drawing.Size(237, 20)
        Me.txt_insc.TabIndex = 11
        Me.txt_insc.ValidatingType = GetType(Date)
        '
        'txt_nac
        '
        Me.txt_nac.Location = New System.Drawing.Point(335, 122)
        Me.txt_nac.Mask = "00/00/0000"
        Me.txt_nac.Name = "txt_nac"
        Me.txt_nac.Size = New System.Drawing.Size(238, 20)
        Me.txt_nac.TabIndex = 10
        Me.txt_nac.ValidatingType = GetType(Date)
        '
        'txt_realID
        '
        Me.txt_realID.AutoSize = True
        Me.txt_realID.Location = New System.Drawing.Point(-1119, 13)
        Me.txt_realID.Name = "txt_realID"
        Me.txt_realID.Size = New System.Drawing.Size(45, 13)
        Me.txt_realID.TabIndex = 66
        Me.txt_realID.Text = "Label39"
        '
        'reasig
        '
        Me.reasig.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.reasig.AutoSize = True
        Me.reasig.Location = New System.Drawing.Point(514, 195)
        Me.reasig.Name = "reasig"
        Me.reasig.Size = New System.Drawing.Size(58, 13)
        Me.reasig.TabIndex = 65
        Me.reasig.TabStop = True
        Me.reasig.Text = "Re asignar"
        Me.reasig.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel1.AutoScroll = True
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(336, 243)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(237, 146)
        Me.FlowLayoutPanel1.TabIndex = 10
        '
        'Button10
        '
        Me.Button10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button10.Location = New System.Drawing.Point(423, 418)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(95, 30)
        Me.Button10.TabIndex = 11
        Me.Button10.Text = "Limpiar"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(279, 409)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(93, 13)
        Me.Label18.TabIndex = 29
        Me.Label18.Text = "Código en sistema"
        '
        'txt_id
        '
        Me.txt_id.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_id.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_id.Enabled = False
        Me.txt_id.Location = New System.Drawing.Point(279, 426)
        Me.txt_id.Name = "txt_id"
        Me.txt_id.ReadOnly = True
        Me.txt_id.Size = New System.Drawing.Size(132, 20)
        Me.txt_id.TabIndex = 28
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(333, 195)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(90, 13)
        Me.Label17.TabIndex = 27
        Me.Label17.Text = "Curso / Servicio *"
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(333, 150)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(151, 13)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "Fecha inscripción / instalación"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(333, 106)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(106, 13)
        Me.Label15.TabIndex = 23
        Me.Label15.Text = "Fecha de nacimiento"
        '
        'btnGuardar
        '
        Me.btnGuardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGuardar.Location = New System.Drawing.Point(524, 418)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(95, 30)
        Me.btnGuardar.TabIndex = 12
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(333, 21)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 13)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "Telefono #3"
        '
        'txt_tel3
        '
        Me.txt_tel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_tel3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_tel3.Location = New System.Drawing.Point(335, 37)
        Me.txt_tel3.Name = "txt_tel3"
        Me.txt_tel3.Size = New System.Drawing.Size(238, 20)
        Me.txt_tel3.TabIndex = 8
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(13, 63)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(83, 13)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Código personal"
        '
        'txt_cod
        '
        Me.txt_cod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_cod.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_cod.Location = New System.Drawing.Point(16, 79)
        Me.txt_cod.Name = "txt_cod"
        Me.txt_cod.Size = New System.Drawing.Size(309, 20)
        Me.txt_cod.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(333, 63)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 13)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Dirección"
        '
        'txt_direccion
        '
        Me.txt_direccion.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_direccion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_direccion.Location = New System.Drawing.Point(335, 79)
        Me.txt_direccion.Name = "txt_direccion"
        Me.txt_direccion.Size = New System.Drawing.Size(238, 20)
        Me.txt_direccion.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 239)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Telefono #2"
        '
        'txt_tel2
        '
        Me.txt_tel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_tel2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_tel2.Location = New System.Drawing.Point(16, 255)
        Me.txt_tel2.Name = "txt_tel2"
        Me.txt_tel2.Size = New System.Drawing.Size(309, 20)
        Me.txt_tel2.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 195)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Telefono #1 *"
        '
        'txt_tel1
        '
        Me.txt_tel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_tel1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_tel1.Location = New System.Drawing.Point(16, 211)
        Me.txt_tel1.Name = "txt_tel1"
        Me.txt_tel1.Size = New System.Drawing.Size(309, 20)
        Me.txt_tel1.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 150)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(150, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Nombres y Apellidos (MADRE)"
        '
        'txt_madres
        '
        Me.txt_madres.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_madres.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_madres.Location = New System.Drawing.Point(16, 166)
        Me.txt_madres.Name = "txt_madres"
        Me.txt_madres.Size = New System.Drawing.Size(309, 20)
        Me.txt_madres.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(13, 106)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(148, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Nombres y Apellidos (PADRE)"
        '
        'txt_padres
        '
        Me.txt_padres.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_padres.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_padres.Location = New System.Drawing.Point(16, 122)
        Me.txt_padres.Name = "txt_padres"
        Me.txt_padres.Size = New System.Drawing.Size(309, 20)
        Me.txt_padres.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(109, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Nombres y Apellidos *"
        '
        'txt_nombres
        '
        Me.txt_nombres.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_nombres.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_nombres.Location = New System.Drawing.Point(16, 37)
        Me.txt_nombres.Name = "txt_nombres"
        Me.txt_nombres.Size = New System.Drawing.Size(309, 20)
        Me.txt_nombres.TabIndex = 0
        '
        'Label125
        '
        Me.Label125.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label125.AutoSize = True
        Me.Label125.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label125.ForeColor = System.Drawing.Color.Red
        Me.Label125.Location = New System.Drawing.Point(104, 283)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(13, 13)
        Me.Label125.TabIndex = 16
        Me.Label125.Text = "&?"
        '
        'btnDelete_al
        '
        Me.btnDelete_al.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete_al.Image = Global.RNV.My.Resources.Resources._1_04_20
        Me.btnDelete_al.Location = New System.Drawing.Point(589, 53)
        Me.btnDelete_al.Name = "btnDelete_al"
        Me.btnDelete_al.Size = New System.Drawing.Size(34, 34)
        Me.btnDelete_al.TabIndex = 60
        Me.btnDelete_al.TabStop = False
        Me.btnDelete_al.UseVisualStyleBackColor = True
        Me.btnDelete_al.Visible = False
        '
        'btn_editar_alum
        '
        Me.btn_editar_alum.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_editar_alum.Image = Global.RNV.My.Resources.Resources.if_pencil_173067
        Me.btn_editar_alum.Location = New System.Drawing.Point(589, 16)
        Me.btn_editar_alum.Name = "btn_editar_alum"
        Me.btn_editar_alum.Size = New System.Drawing.Size(34, 34)
        Me.btn_editar_alum.TabIndex = 59
        Me.btn_editar_alum.TabStop = False
        Me.btn_editar_alum.UseVisualStyleBackColor = True
        '
        'tabpage2
        '
        Me.tabpage2.Controls.Add(Me.Label157)
        Me.tabpage2.Controls.Add(Me.txt_ciclo_curso)
        Me.tabpage2.Controls.Add(Me.LinkLabel13)
        Me.tabpage2.Controls.Add(Me.ciclo_cursos)
        Me.tabpage2.Controls.Add(Me.Label39)
        Me.tabpage2.Controls.Add(Me.cbox_servi)
        Me.tabpage2.Controls.Add(Me.Button9)
        Me.tabpage2.Controls.Add(Me.txt_desc)
        Me.tabpage2.Controls.Add(Me.Label20)
        Me.tabpage2.Controls.Add(Me.txt_precio)
        Me.tabpage2.Controls.Add(Me.Label19)
        Me.tabpage2.Controls.Add(Me.txt_cod_curso)
        Me.tabpage2.Controls.Add(Me.btnGuardar_curso)
        Me.tabpage2.Controls.Add(Me.Label24)
        Me.tabpage2.Controls.Add(Me.Label30)
        Me.tabpage2.Controls.Add(Me.txt_curso)
        Me.tabpage2.Controls.Add(Me.btn_delete_curso)
        Me.tabpage2.Controls.Add(Me.btnEdit_curso)
        Me.tabpage2.Location = New System.Drawing.Point(4, 22)
        Me.tabpage2.Name = "tabpage2"
        Me.tabpage2.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpage2.Size = New System.Drawing.Size(637, 460)
        Me.tabpage2.TabIndex = 2
        Me.tabpage2.Text = "TabPage2"
        Me.tabpage2.UseVisualStyleBackColor = True
        '
        'Label157
        '
        Me.Label157.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label157.AutoSize = True
        Me.Label157.Location = New System.Drawing.Point(151, 199)
        Me.Label157.Name = "Label157"
        Me.Label157.Size = New System.Drawing.Size(167, 13)
        Me.Label157.TabIndex = 74
        Me.Label157.Text = "Ciclo (Aplica para Academia NV) *"
        '
        'txt_ciclo_curso
        '
        Me.txt_ciclo_curso.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_ciclo_curso.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_ciclo_curso.Location = New System.Drawing.Point(154, 215)
        Me.txt_ciclo_curso.Name = "txt_ciclo_curso"
        Me.txt_ciclo_curso.Size = New System.Drawing.Size(298, 20)
        Me.txt_ciclo_curso.TabIndex = 73
        '
        'LinkLabel13
        '
        Me.LinkLabel13.AutoSize = True
        Me.LinkLabel13.Location = New System.Drawing.Point(457, 218)
        Me.LinkLabel13.Name = "LinkLabel13"
        Me.LinkLabel13.Size = New System.Drawing.Size(45, 13)
        Me.LinkLabel13.TabIndex = 72
        Me.LinkLabel13.TabStop = True
        Me.LinkLabel13.Text = "Cambiar"
        Me.LinkLabel13.Visible = False
        '
        'ciclo_cursos
        '
        Me.ciclo_cursos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ciclo_cursos.ForeColor = System.Drawing.Color.Red
        Me.ciclo_cursos.Location = New System.Drawing.Point(-21, 259)
        Me.ciclo_cursos.Name = "ciclo_cursos"
        Me.ciclo_cursos.Size = New System.Drawing.Size(11, 13)
        Me.ciclo_cursos.TabIndex = 71
        Me.ciclo_cursos.Text = "@ciclo"
        Me.ciclo_cursos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label39
        '
        Me.Label39.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(151, 244)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(66, 13)
        Me.Label39.TabIndex = 61
        Me.Label39.Text = "Cobro para *"
        '
        'cbox_servi
        '
        Me.cbox_servi.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_servi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbox_servi.FormattingEnabled = True
        Me.cbox_servi.Items.AddRange(New Object() {"-Selecciona cobro para-", "Academia NV", "Vision NET"})
        Me.cbox_servi.Location = New System.Drawing.Point(154, 260)
        Me.cbox_servi.Name = "cbox_servi"
        Me.cbox_servi.Size = New System.Drawing.Size(298, 21)
        Me.cbox_servi.TabIndex = 14
        '
        'Button9
        '
        Me.Button9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button9.Location = New System.Drawing.Point(427, 417)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(92, 30)
        Me.Button9.TabIndex = 15
        Me.Button9.Text = "limpiar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'txt_desc
        '
        Me.txt_desc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_desc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_desc.Location = New System.Drawing.Point(154, 93)
        Me.txt_desc.Name = "txt_desc"
        Me.txt_desc.Size = New System.Drawing.Size(298, 58)
        Me.txt_desc.TabIndex = 12
        Me.txt_desc.Text = ""
        '
        'Label20
        '
        Me.Label20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(151, 156)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 13)
        Me.Label20.TabIndex = 56
        Me.Label20.Text = "Precio *"
        '
        'txt_precio
        '
        Me.txt_precio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_precio.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_precio.Location = New System.Drawing.Point(154, 172)
        Me.txt_precio.Name = "txt_precio"
        Me.txt_precio.Size = New System.Drawing.Size(298, 20)
        Me.txt_precio.TabIndex = 13
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(15, 411)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(93, 13)
        Me.Label19.TabIndex = 54
        Me.Label19.Text = "Código en sistema"
        '
        'txt_cod_curso
        '
        Me.txt_cod_curso.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_cod_curso.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_cod_curso.Enabled = False
        Me.txt_cod_curso.Location = New System.Drawing.Point(18, 427)
        Me.txt_cod_curso.Name = "txt_cod_curso"
        Me.txt_cod_curso.ReadOnly = True
        Me.txt_cod_curso.Size = New System.Drawing.Size(132, 20)
        Me.txt_cod_curso.TabIndex = 53
        '
        'btnGuardar_curso
        '
        Me.btnGuardar_curso.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGuardar_curso.Location = New System.Drawing.Point(525, 417)
        Me.btnGuardar_curso.Name = "btnGuardar_curso"
        Me.btnGuardar_curso.Size = New System.Drawing.Size(95, 30)
        Me.btnGuardar_curso.TabIndex = 16
        Me.btnGuardar_curso.Text = "Guardar"
        Me.btnGuardar_curso.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(151, 77)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(63, 13)
        Me.Label24.TabIndex = 45
        Me.Label24.Text = "Descripción"
        '
        'Label30
        '
        Me.Label30.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(151, 35)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(144, 13)
        Me.Label30.TabIndex = 35
        Me.Label30.Text = "Nombre del curso / servicio *"
        '
        'txt_curso
        '
        Me.txt_curso.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_curso.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txt_curso.Location = New System.Drawing.Point(154, 51)
        Me.txt_curso.Name = "txt_curso"
        Me.txt_curso.Size = New System.Drawing.Size(298, 20)
        Me.txt_curso.TabIndex = 11
        '
        'btn_delete_curso
        '
        Me.btn_delete_curso.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_delete_curso.Image = Global.RNV.My.Resources.Resources._1_04_20
        Me.btn_delete_curso.Location = New System.Drawing.Point(587, 51)
        Me.btn_delete_curso.Name = "btn_delete_curso"
        Me.btn_delete_curso.Size = New System.Drawing.Size(34, 34)
        Me.btn_delete_curso.TabIndex = 58
        Me.btn_delete_curso.TabStop = False
        Me.btn_delete_curso.UseVisualStyleBackColor = True
        Me.btn_delete_curso.Visible = False
        '
        'btnEdit_curso
        '
        Me.btnEdit_curso.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit_curso.Image = Global.RNV.My.Resources.Resources.if_pencil_173067
        Me.btnEdit_curso.Location = New System.Drawing.Point(587, 14)
        Me.btnEdit_curso.Name = "btnEdit_curso"
        Me.btnEdit_curso.Size = New System.Drawing.Size(34, 34)
        Me.btnEdit_curso.TabIndex = 57
        Me.btnEdit_curso.TabStop = False
        Me.btnEdit_curso.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.AutoScroll = True
        Me.TabPage4.Controls.Add(Me.GroupBox24)
        Me.TabPage4.Controls.Add(Me.GroupBox23)
        Me.TabPage4.Controls.Add(Me.txtsafe2)
        Me.TabPage4.Controls.Add(Me.txtsafe1)
        Me.TabPage4.Controls.Add(Me.GroupBox22)
        Me.TabPage4.Controls.Add(Me.GroupBox21)
        Me.TabPage4.Controls.Add(Me.GroupBox9)
        Me.TabPage4.Controls.Add(Me.GroupBox10)
        Me.TabPage4.Controls.Add(Me.GroupBox4)
        Me.TabPage4.Controls.Add(Me.GroupBox3)
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Controls.Add(Me.Label142)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(637, 460)
        Me.TabPage4.TabIndex = 15
        Me.TabPage4.Text = "TabPage4"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox24
        '
        Me.GroupBox24.Controls.Add(Me.Label107)
        Me.GroupBox24.Controls.Add(Me.TextBox42)
        Me.GroupBox24.Controls.Add(Me.Label105)
        Me.GroupBox24.Controls.Add(Me.Button56)
        Me.GroupBox24.Location = New System.Drawing.Point(69, 1584)
        Me.GroupBox24.Name = "GroupBox24"
        Me.GroupBox24.Size = New System.Drawing.Size(482, 120)
        Me.GroupBox24.TabIndex = 129
        Me.GroupBox24.TabStop = False
        Me.GroupBox24.Text = "Configurar ciclo por defecto"
        '
        'Label107
        '
        Me.Label107.Location = New System.Drawing.Point(414, 100)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(100, 23)
        Me.Label107.TabIndex = 130
        '
        'TextBox42
        '
        Me.TextBox42.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox42.Location = New System.Drawing.Point(25, 50)
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.Size = New System.Drawing.Size(432, 20)
        Me.TextBox42.TabIndex = 129
        '
        'Label105
        '
        Me.Label105.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(23, 32)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(69, 13)
        Me.Label105.TabIndex = 127
        Me.Label105.Text = "Ciclo actual *"
        '
        'Button56
        '
        Me.Button56.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button56.Location = New System.Drawing.Point(195, 81)
        Me.Button56.Name = "Button56"
        Me.Button56.Size = New System.Drawing.Size(95, 30)
        Me.Button56.TabIndex = 113
        Me.Button56.Text = "Actualizar"
        Me.Button56.UseVisualStyleBackColor = True
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.Label146)
        Me.GroupBox23.Controls.Add(Me.TextBox41)
        Me.GroupBox23.Controls.Add(Me.NumericUpDown3)
        Me.GroupBox23.Controls.Add(Me.Label144)
        Me.GroupBox23.Controls.Add(Me.NumericUpDown2)
        Me.GroupBox23.Controls.Add(Me.Label143)
        Me.GroupBox23.Controls.Add(Me.CheckBox8)
        Me.GroupBox23.Controls.Add(Me.CheckBox7)
        Me.GroupBox23.Controls.Add(Me.Label141)
        Me.GroupBox23.Controls.Add(Me.CheckBox6)
        Me.GroupBox23.Controls.Add(Me.Label140)
        Me.GroupBox23.Controls.Add(Me.Button47)
        Me.GroupBox23.Location = New System.Drawing.Point(69, 1326)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(482, 252)
        Me.GroupBox23.TabIndex = 127
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "Configurar sistema de Mora y atraso de pagos"
        '
        'Label146
        '
        Me.Label146.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label146.AutoSize = True
        Me.Label146.Location = New System.Drawing.Point(23, 131)
        Me.Label146.Name = "Label146"
        Me.Label146.Size = New System.Drawing.Size(240, 13)
        Me.Label146.TabIndex = 127
        Me.Label146.Text = "Taza de cobro (Ej: 5Q si es diario O Q25 si es fijo)"
        '
        'TextBox41
        '
        Me.TextBox41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox41.Location = New System.Drawing.Point(26, 147)
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.Size = New System.Drawing.Size(432, 20)
        Me.TextBox41.TabIndex = 128
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.Location = New System.Drawing.Point(26, 100)
        Me.NumericUpDown3.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me.NumericUpDown3.Minimum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(430, 20)
        Me.NumericUpDown3.TabIndex = 126
        Me.NumericUpDown3.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'Label144
        '
        Me.Label144.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label144.AutoSize = True
        Me.Label144.Location = New System.Drawing.Point(23, 81)
        Me.Label144.Name = "Label144"
        Me.Label144.Size = New System.Drawing.Size(315, 13)
        Me.Label144.TabIndex = 125
        Me.Label144.Text = "Dia máximo de pago de cada mes vencido (Mínimo: 4 Máximo: 7)"
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Location = New System.Drawing.Point(25, 48)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.NumericUpDown2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(430, 20)
        Me.NumericUpDown2.TabIndex = 124
        Me.NumericUpDown2.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'Label143
        '
        Me.Label143.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label143.AutoSize = True
        Me.Label143.Location = New System.Drawing.Point(22, 29)
        Me.Label143.Name = "Label143"
        Me.Label143.Size = New System.Drawing.Size(375, 13)
        Me.Label143.TabIndex = 123
        Me.Label143.Text = "Dia minimo de comienzo de pago de cada mes vencido (Mínimo: 1 Máximo: 3)"
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.Location = New System.Drawing.Point(385, 186)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(53, 17)
        Me.CheckBox8.TabIndex = 122
        Me.CheckBox8.Text = "Diario"
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.Checked = True
        Me.CheckBox7.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox7.Location = New System.Drawing.Point(345, 186)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(42, 17)
        Me.CheckBox7.TabIndex = 121
        Me.CheckBox7.Text = "Fijo"
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'Label141
        '
        Me.Label141.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label141.AutoSize = True
        Me.Label141.Location = New System.Drawing.Point(221, 187)
        Me.Label141.Name = "Label141"
        Me.Label141.Size = New System.Drawing.Size(120, 13)
        Me.Label141.TabIndex = 120
        Me.Label141.Text = "Tipo de cobro de mora?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Checked = True
        Me.CheckBox6.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox6.Location = New System.Drawing.Point(177, 186)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(36, 17)
        Me.CheckBox6.TabIndex = 119
        Me.CheckBox6.Text = "SI"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'Label140
        '
        Me.Label140.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label140.AutoSize = True
        Me.Label140.Location = New System.Drawing.Point(44, 187)
        Me.Label140.Name = "Label140"
        Me.Label140.Size = New System.Drawing.Size(130, 13)
        Me.Label140.TabIndex = 118
        Me.Label140.Text = "Habilitar sistema de mora?"
        '
        'Button47
        '
        Me.Button47.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button47.Location = New System.Drawing.Point(195, 213)
        Me.Button47.Name = "Button47"
        Me.Button47.Size = New System.Drawing.Size(95, 30)
        Me.Button47.TabIndex = 113
        Me.Button47.Text = "Actualizar"
        Me.Button47.UseVisualStyleBackColor = True
        '
        'txtsafe2
        '
        Me.txtsafe2.AutoSize = True
        Me.txtsafe2.Location = New System.Drawing.Point(423, -12)
        Me.txtsafe2.Name = "txtsafe2"
        Me.txtsafe2.Size = New System.Drawing.Size(51, 13)
        Me.txtsafe2.TabIndex = 128
        Me.txtsafe2.Text = "Label138"
        '
        'txtsafe1
        '
        Me.txtsafe1.AutoSize = True
        Me.txtsafe1.Location = New System.Drawing.Point(548, -16)
        Me.txtsafe1.Name = "txtsafe1"
        Me.txtsafe1.Size = New System.Drawing.Size(51, 13)
        Me.txtsafe1.TabIndex = 127
        Me.txtsafe1.Text = "Label138"
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.txt_cuerpo_email)
        Me.GroupBox22.Controls.Add(Me.Label138)
        Me.GroupBox22.Controls.Add(Me.Label124)
        Me.GroupBox22.Controls.Add(Me.Button44)
        Me.GroupBox22.Location = New System.Drawing.Point(69, 1149)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Size = New System.Drawing.Size(482, 170)
        Me.GroupBox22.TabIndex = 125
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "Configurar envio de recibo por correo *"
        '
        'txt_cuerpo_email
        '
        Me.txt_cuerpo_email.Location = New System.Drawing.Point(24, 47)
        Me.txt_cuerpo_email.Name = "txt_cuerpo_email"
        Me.txt_cuerpo_email.Size = New System.Drawing.Size(434, 73)
        Me.txt_cuerpo_email.TabIndex = 120
        Me.txt_cuerpo_email.Text = ""
        '
        'Label138
        '
        Me.Label138.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label138.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label138.ForeColor = System.Drawing.Color.Red
        Me.Label138.Location = New System.Drawing.Point(460, 13)
        Me.Label138.Name = "Label138"
        Me.Label138.Size = New System.Drawing.Size(19, 13)
        Me.Label138.TabIndex = 16
        Me.Label138.Text = "&?"
        Me.Label138.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label124
        '
        Me.Label124.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label124.AutoSize = True
        Me.Label124.Location = New System.Drawing.Point(21, 29)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(153, 13)
        Me.Label124.TabIndex = 118
        Me.Label124.Text = "Cuerpo del mensaje a mostrar *"
        '
        'Button44
        '
        Me.Button44.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button44.Location = New System.Drawing.Point(195, 129)
        Me.Button44.Name = "Button44"
        Me.Button44.Size = New System.Drawing.Size(95, 30)
        Me.Button44.TabIndex = 113
        Me.Button44.Text = "Actualizar"
        Me.Button44.UseVisualStyleBackColor = True
        '
        'GroupBox21
        '
        Me.GroupBox21.Controls.Add(Me.frontal)
        Me.GroupBox21.Controls.Add(Me.Label104)
        Me.GroupBox21.Controls.Add(Me.maxdias)
        Me.GroupBox21.Controls.Add(Me.Label103)
        Me.GroupBox21.Controls.Add(Me.Button32)
        Me.GroupBox21.Location = New System.Drawing.Point(69, 987)
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.Size = New System.Drawing.Size(482, 156)
        Me.GroupBox21.TabIndex = 124
        Me.GroupBox21.TabStop = False
        Me.GroupBox21.Text = "Configurar notas"
        '
        'frontal
        '
        Me.frontal.AutoSize = True
        Me.frontal.Location = New System.Drawing.Point(164, 86)
        Me.frontal.Name = "frontal"
        Me.frontal.Size = New System.Drawing.Size(36, 17)
        Me.frontal.TabIndex = 117
        Me.frontal.Text = "SI"
        Me.frontal.UseVisualStyleBackColor = True
        '
        'Label104
        '
        Me.Label104.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(23, 87)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(136, 13)
        Me.Label104.TabIndex = 116
        Me.Label104.Text = "Mostrar al inciar el sistema?"
        '
        'maxdias
        '
        Me.maxdias.Location = New System.Drawing.Point(26, 54)
        Me.maxdias.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.maxdias.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.maxdias.Name = "maxdias"
        Me.maxdias.Size = New System.Drawing.Size(430, 20)
        Me.maxdias.TabIndex = 115
        Me.maxdias.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'Label103
        '
        Me.Label103.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label103.AutoSize = True
        Me.Label103.Location = New System.Drawing.Point(23, 35)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(360, 13)
        Me.Label103.TabIndex = 114
        Me.Label103.Text = "Máximo de dias en que expira una nota (Min: 2 = dos días Max: 5 = 5 días)"
        '
        'Button32
        '
        Me.Button32.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button32.Location = New System.Drawing.Point(195, 117)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(95, 30)
        Me.Button32.TabIndex = 113
        Me.Button32.Text = "Actualizar"
        Me.Button32.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.btnGuardarProtected)
        Me.GroupBox9.Controls.Add(Me.Label52)
        Me.GroupBox9.Controls.Add(Me.txt_protected2)
        Me.GroupBox9.Controls.Add(Me.Label53)
        Me.GroupBox9.Controls.Add(Me.txt_protected)
        Me.GroupBox9.Location = New System.Drawing.Point(69, 27)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(482, 155)
        Me.GroupBox9.TabIndex = 121
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Cambiar contraseña para vistas protegidas"
        '
        'btnGuardarProtected
        '
        Me.btnGuardarProtected.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGuardarProtected.Location = New System.Drawing.Point(201, 115)
        Me.btnGuardarProtected.Name = "btnGuardarProtected"
        Me.btnGuardarProtected.Size = New System.Drawing.Size(95, 30)
        Me.btnGuardarProtected.TabIndex = 103
        Me.btnGuardarProtected.Text = "Actualizar"
        Me.btnGuardarProtected.UseVisualStyleBackColor = True
        '
        'Label52
        '
        Me.Label52.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(23, 71)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(104, 13)
        Me.Label52.TabIndex = 3
        Me.Label52.Text = "Repetir contraseña *"
        '
        'txt_protected2
        '
        Me.txt_protected2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_protected2.Location = New System.Drawing.Point(26, 87)
        Me.txt_protected2.Name = "txt_protected2"
        Me.txt_protected2.Size = New System.Drawing.Size(432, 20)
        Me.txt_protected2.TabIndex = 102
        Me.txt_protected2.UseSystemPasswordChar = True
        '
        'Label53
        '
        Me.Label53.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(23, 26)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(102, 13)
        Me.Label53.TabIndex = 1
        Me.Label53.Text = "Nueva contraseña *"
        '
        'txt_protected
        '
        Me.txt_protected.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_protected.Location = New System.Drawing.Point(26, 42)
        Me.txt_protected.Name = "txt_protected"
        Me.txt_protected.Size = New System.Drawing.Size(432, 20)
        Me.txt_protected.TabIndex = 101
        Me.txt_protected.UseSystemPasswordChar = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.Button20)
        Me.GroupBox10.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox10.Controls.Add(Me.Label54)
        Me.GroupBox10.Location = New System.Drawing.Point(69, 852)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(482, 129)
        Me.GroupBox10.TabIndex = 123
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Backup automatico"
        '
        'Button20
        '
        Me.Button20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button20.Location = New System.Drawing.Point(174, 85)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(123, 30)
        Me.Button20.TabIndex = 107
        Me.Button20.Text = "ACTUALIZAR"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(25, 55)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {90, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(430, 20)
        Me.NumericUpDown1.TabIndex = 107
        Me.NumericUpDown1.Value = New Decimal(New Integer() {90, 0, 0, 0})
        '
        'Label54
        '
        Me.Label54.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(22, 36)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(250, 13)
        Me.Label54.TabIndex = 105
        Me.Label54.Text = "Hacer backup cada (90 = 1.5min MAX: 300 = 5min)"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnGuardar_pass)
        Me.GroupBox4.Controls.Add(Me.Label37)
        Me.GroupBox4.Controls.Add(Me.txtpass2)
        Me.GroupBox4.Controls.Add(Me.Label38)
        Me.GroupBox4.Controls.Add(Me.txtpass)
        Me.GroupBox4.Location = New System.Drawing.Point(69, 691)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(482, 155)
        Me.GroupBox4.TabIndex = 120
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Cambiar contraseña"
        '
        'btnGuardar_pass
        '
        Me.btnGuardar_pass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGuardar_pass.Location = New System.Drawing.Point(201, 115)
        Me.btnGuardar_pass.Name = "btnGuardar_pass"
        Me.btnGuardar_pass.Size = New System.Drawing.Size(95, 30)
        Me.btnGuardar_pass.TabIndex = 103
        Me.btnGuardar_pass.Text = "Actualizar"
        Me.btnGuardar_pass.UseVisualStyleBackColor = True
        '
        'Label37
        '
        Me.Label37.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(23, 71)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(104, 13)
        Me.Label37.TabIndex = 3
        Me.Label37.Text = "Repetir contraseña *"
        '
        'txtpass2
        '
        Me.txtpass2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtpass2.Location = New System.Drawing.Point(26, 87)
        Me.txtpass2.Name = "txtpass2"
        Me.txtpass2.Size = New System.Drawing.Size(432, 20)
        Me.txtpass2.TabIndex = 102
        Me.txtpass2.UseSystemPasswordChar = True
        '
        'Label38
        '
        Me.Label38.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(23, 26)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(102, 13)
        Me.Label38.TabIndex = 1
        Me.Label38.Text = "Nueva contraseña *"
        '
        'txtpass
        '
        Me.txtpass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtpass.Location = New System.Drawing.Point(26, 42)
        Me.txtpass.Name = "txtpass"
        Me.txtpass.Size = New System.Drawing.Size(432, 20)
        Me.txtpass.TabIndex = 101
        Me.txtpass.UseSystemPasswordChar = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnGuardar_anv)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.txt_facebook_vnet)
        Me.GroupBox3.Controls.Add(Me.Button8)
        Me.GroupBox3.Controls.Add(Me.Label32)
        Me.GroupBox3.Controls.Add(Me.txt_logo_vnet)
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.txt_slogan_vnet)
        Me.GroupBox3.Controls.Add(Me.Label34)
        Me.GroupBox3.Controls.Add(Me.txt_titulo_vnet)
        Me.GroupBox3.Location = New System.Drawing.Point(69, 444)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(482, 241)
        Me.GroupBox3.TabIndex = 122
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos recibo VNET"
        '
        'btnGuardar_anv
        '
        Me.btnGuardar_anv.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGuardar_anv.Location = New System.Drawing.Point(198, 203)
        Me.btnGuardar_anv.Name = "btnGuardar_anv"
        Me.btnGuardar_anv.Size = New System.Drawing.Size(95, 30)
        Me.btnGuardar_anv.TabIndex = 113
        Me.btnGuardar_anv.Text = "Actualizar"
        Me.btnGuardar_anv.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(23, 115)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(62, 13)
        Me.Label31.TabIndex = 8
        Me.Label31.Text = "Facebook *"
        '
        'txt_facebook_vnet
        '
        Me.txt_facebook_vnet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_facebook_vnet.Location = New System.Drawing.Point(26, 131)
        Me.txt_facebook_vnet.Name = "txt_facebook_vnet"
        Me.txt_facebook_vnet.Size = New System.Drawing.Size(432, 20)
        Me.txt_facebook_vnet.TabIndex = 111
        '
        'Button8
        '
        Me.Button8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button8.Enabled = False
        Me.Button8.Location = New System.Drawing.Point(400, 174)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(58, 20)
        Me.Button8.TabIndex = 112
        Me.Button8.Text = "buscar"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(23, 158)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(38, 13)
        Me.Label32.TabIndex = 5
        Me.Label32.Text = "Logo *"
        '
        'txt_logo_vnet
        '
        Me.txt_logo_vnet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_logo_vnet.Enabled = False
        Me.txt_logo_vnet.Location = New System.Drawing.Point(26, 174)
        Me.txt_logo_vnet.Name = "txt_logo_vnet"
        Me.txt_logo_vnet.ReadOnly = True
        Me.txt_logo_vnet.Size = New System.Drawing.Size(368, 20)
        Me.txt_logo_vnet.TabIndex = 4
        Me.txt_logo_vnet.TabStop = False
        '
        'Label33
        '
        Me.Label33.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(23, 73)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(96, 13)
        Me.Label33.TabIndex = 3
        Me.Label33.Text = "Slogan del recibo *"
        '
        'txt_slogan_vnet
        '
        Me.txt_slogan_vnet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_slogan_vnet.Location = New System.Drawing.Point(26, 89)
        Me.txt_slogan_vnet.Name = "txt_slogan_vnet"
        Me.txt_slogan_vnet.Size = New System.Drawing.Size(432, 20)
        Me.txt_slogan_vnet.TabIndex = 110
        '
        'Label34
        '
        Me.Label34.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(23, 28)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(89, 13)
        Me.Label34.TabIndex = 1
        Me.Label34.Text = "Titulo del recibo *"
        '
        'txt_titulo_vnet
        '
        Me.txt_titulo_vnet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_titulo_vnet.Location = New System.Drawing.Point(26, 44)
        Me.txt_titulo_vnet.Name = "txt_titulo_vnet"
        Me.txt_titulo_vnet.Size = New System.Drawing.Size(432, 20)
        Me.txt_titulo_vnet.TabIndex = 109
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_Guardar_vnet)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.txt_facebook_nv)
        Me.GroupBox2.Controls.Add(Me.Button7)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.txt_logo_nv)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.txt_slogan_nv)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.txt_titulo_nv)
        Me.GroupBox2.Location = New System.Drawing.Point(69, 189)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(482, 246)
        Me.GroupBox2.TabIndex = 119
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos recibo ANV"
        '
        'btn_Guardar_vnet
        '
        Me.btn_Guardar_vnet.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Guardar_vnet.Location = New System.Drawing.Point(198, 204)
        Me.btn_Guardar_vnet.Name = "btn_Guardar_vnet"
        Me.btn_Guardar_vnet.Size = New System.Drawing.Size(95, 30)
        Me.btn_Guardar_vnet.TabIndex = 108
        Me.btn_Guardar_vnet.Text = "Actualizar"
        Me.btn_Guardar_vnet.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(23, 113)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(62, 13)
        Me.Label29.TabIndex = 8
        Me.Label29.Text = "Facebook *"
        '
        'txt_facebook_nv
        '
        Me.txt_facebook_nv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_facebook_nv.Location = New System.Drawing.Point(26, 129)
        Me.txt_facebook_nv.Name = "txt_facebook_nv"
        Me.txt_facebook_nv.Size = New System.Drawing.Size(432, 20)
        Me.txt_facebook_nv.TabIndex = 106
        '
        'Button7
        '
        Me.Button7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button7.Enabled = False
        Me.Button7.Location = New System.Drawing.Point(400, 172)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(58, 20)
        Me.Button7.TabIndex = 107
        Me.Button7.Text = "buscar"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(23, 156)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(38, 13)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Logo *"
        '
        'txt_logo_nv
        '
        Me.txt_logo_nv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_logo_nv.Enabled = False
        Me.txt_logo_nv.Location = New System.Drawing.Point(26, 172)
        Me.txt_logo_nv.Name = "txt_logo_nv"
        Me.txt_logo_nv.ReadOnly = True
        Me.txt_logo_nv.Size = New System.Drawing.Size(368, 20)
        Me.txt_logo_nv.TabIndex = 4
        Me.txt_logo_nv.TabStop = False
        '
        'Label27
        '
        Me.Label27.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(23, 71)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(96, 13)
        Me.Label27.TabIndex = 3
        Me.Label27.Text = "Slogan del recibo *"
        '
        'txt_slogan_nv
        '
        Me.txt_slogan_nv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_slogan_nv.Location = New System.Drawing.Point(26, 87)
        Me.txt_slogan_nv.Name = "txt_slogan_nv"
        Me.txt_slogan_nv.Size = New System.Drawing.Size(432, 20)
        Me.txt_slogan_nv.TabIndex = 105
        '
        'Label26
        '
        Me.Label26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(23, 26)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(89, 13)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Titulo del recibo *"
        '
        'txt_titulo_nv
        '
        Me.txt_titulo_nv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_titulo_nv.Location = New System.Drawing.Point(26, 42)
        Me.txt_titulo_nv.Name = "txt_titulo_nv"
        Me.txt_titulo_nv.Size = New System.Drawing.Size(432, 20)
        Me.txt_titulo_nv.TabIndex = 104
        '
        'Label142
        '
        Me.Label142.Location = New System.Drawing.Point(487, 1701)
        Me.Label142.Name = "Label142"
        Me.Label142.Size = New System.Drawing.Size(100, 23)
        Me.Label142.TabIndex = 130
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.TabControl2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(637, 460)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "TabPage5"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl2.Controls.Add(Me.TabPage6)
        Me.TabControl2.Controls.Add(Me.TabPage7)
        Me.TabControl2.Controls.Add(Me.TabPage10)
        Me.TabControl2.Location = New System.Drawing.Point(-4, -46)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(654, 525)
        Me.TabControl2.TabIndex = 0
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.lcode)
        Me.TabPage6.Controls.Add(Me.Button19)
        Me.TabPage6.Controls.Add(Me.GroupBox6)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(646, 499)
        Me.TabPage6.TabIndex = 0
        Me.TabPage6.Text = "TabPage6"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'lcode
        '
        Me.lcode.AutoSize = True
        Me.lcode.Location = New System.Drawing.Point(409, 12)
        Me.lcode.Name = "lcode"
        Me.lcode.Size = New System.Drawing.Size(0, 13)
        Me.lcode.TabIndex = 60
        '
        'Button19
        '
        Me.Button19.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button19.Image = Global.RNV.My.Resources.Resources.Search_20
        Me.Button19.Location = New System.Drawing.Point(585, 45)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(34, 34)
        Me.Button19.TabIndex = 59
        Me.Button19.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.Button30)
        Me.GroupBox6.Controls.Add(Me.Button12)
        Me.GroupBox6.Location = New System.Drawing.Point(20, 90)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(599, 370)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Reportes disponibles"
        '
        'Button30
        '
        Me.Button30.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button30.Image = Global.RNV.My.Resources.Resources.listado
        Me.Button30.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button30.Location = New System.Drawing.Point(218, 44)
        Me.Button30.Name = "Button30"
        Me.Button30.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button30.Size = New System.Drawing.Size(119, 94)
        Me.Button30.TabIndex = 1
        Me.Button30.Text = "Listado A/C"
        Me.Button30.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button30.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Image = Global.RNV.My.Resources.Resources.Clipboard_48
        Me.Button12.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button12.Location = New System.Drawing.Point(85, 44)
        Me.Button12.Name = "Button12"
        Me.Button12.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button12.Size = New System.Drawing.Size(119, 94)
        Me.Button12.TabIndex = 0
        Me.Button12.Text = "Cobros"
        Me.Button12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button12.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.Label43)
        Me.TabPage7.Controls.Add(Me.GroupBox7)
        Me.TabPage7.Controls.Add(Me.lblmes)
        Me.TabPage7.Controls.Add(Me.c_mes)
        Me.TabPage7.Controls.Add(Me.lblaño)
        Me.TabPage7.Controls.Add(Me.c_año)
        Me.TabPage7.Controls.Add(Me.lblañomes)
        Me.TabPage7.Controls.Add(Me.cañoe)
        Me.TabPage7.Controls.Add(Me.cmese)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(646, 499)
        Me.TabPage7.TabIndex = 1
        Me.TabPage7.Text = "TabPage7"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'Label43
        '
        Me.Label43.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(6, 163)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(625, 65)
        Me.Label43.TabIndex = 23
        Me.Label43.Text = "POR FAVOR, ESPERA..."
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label43.Visible = False
        '
        'GroupBox7
        '
        Me.GroupBox7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox7.Controls.Add(Me.texto_consulta)
        Me.GroupBox7.Controls.Add(Me.Label44)
        Me.GroupBox7.Controls.Add(Me.reporte_para)
        Me.GroupBox7.Controls.Add(Me.Label40)
        Me.GroupBox7.Controls.Add(Me.Button14)
        Me.GroupBox7.Controls.Add(Me.CheckBox2)
        Me.GroupBox7.Controls.Add(Me.CheckBox1)
        Me.GroupBox7.Controls.Add(Me.lblfechafecha)
        Me.GroupBox7.Controls.Add(Me.lblfecha)
        Me.GroupBox7.Controls.Add(Me.datafecha2)
        Me.GroupBox7.Controls.Add(Me.datafecha1)
        Me.GroupBox7.Controls.Add(Me.datafecha)
        Me.GroupBox7.Controls.Add(Me.c_user)
        Me.GroupBox7.Controls.Add(Me.Label41)
        Me.GroupBox7.Controls.Add(Me.cbox_fast)
        Me.GroupBox7.Controls.Add(Me.Label42)
        Me.GroupBox7.Location = New System.Drawing.Point(34, 45)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(572, 418)
        Me.GroupBox7.TabIndex = 10
        Me.GroupBox7.TabStop = False
        '
        'texto_consulta
        '
        Me.texto_consulta.AutoSize = True
        Me.texto_consulta.Location = New System.Drawing.Point(31, 438)
        Me.texto_consulta.Name = "texto_consulta"
        Me.texto_consulta.Size = New System.Drawing.Size(45, 13)
        Me.texto_consulta.TabIndex = 30
        Me.texto_consulta.Text = "Label45"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(-82, 435)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(45, 13)
        Me.Label44.TabIndex = 29
        Me.Label44.Text = "Label44"
        '
        'reporte_para
        '
        Me.reporte_para.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.reporte_para.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.reporte_para.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.reporte_para.FormattingEnabled = True
        Me.reporte_para.Items.AddRange(New Object() {"-Selecciona reporte para-", "Academia NV", "Inventario NV", "Vision NET"})
        Me.reporte_para.Location = New System.Drawing.Point(16, 109)
        Me.reporte_para.Name = "reporte_para"
        Me.reporte_para.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.reporte_para.Size = New System.Drawing.Size(539, 23)
        Me.reporte_para.TabIndex = 28
        '
        'Label40
        '
        Me.Label40.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label40.Location = New System.Drawing.Point(199, 86)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(190, 18)
        Me.Label40.TabIndex = 27
        Me.Label40.Text = "Selecciona el reporte para *"
        '
        'Button14
        '
        Me.Button14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button14.Location = New System.Drawing.Point(437, 371)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(120, 36)
        Me.Button14.TabIndex = 26
        Me.Button14.Text = "Crear reporte"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Enabled = False
        Me.CheckBox2.Location = New System.Drawing.Point(266, 176)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(111, 17)
        Me.CheckBox2.TabIndex = 24
        Me.CheckBox2.Text = "Alumno / Cliente *"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Location = New System.Drawing.Point(204, 176)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(56, 17)
        Me.CheckBox1.TabIndex = 23
        Me.CheckBox1.Text = "Todos"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'lblfechafecha
        '
        Me.lblfechafecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblfechafecha.AutoSize = True
        Me.lblfechafecha.Enabled = False
        Me.lblfechafecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.lblfechafecha.Location = New System.Drawing.Point(238, 301)
        Me.lblfechafecha.Name = "lblfechafecha"
        Me.lblfechafecha.Size = New System.Drawing.Size(101, 18)
        Me.lblfechafecha.TabIndex = 15
        Me.lblfechafecha.Text = "Fecha a fecha"
        '
        'lblfecha
        '
        Me.lblfecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblfecha.AutoSize = True
        Me.lblfecha.Enabled = False
        Me.lblfecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.lblfecha.Location = New System.Drawing.Point(225, 241)
        Me.lblfecha.Name = "lblfecha"
        Me.lblfecha.Size = New System.Drawing.Size(119, 18)
        Me.lblfecha.TabIndex = 14
        Me.lblfecha.Text = "Fecha especifica"
        '
        'datafecha2
        '
        Me.datafecha2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.datafecha2.Enabled = False
        Me.datafecha2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.datafecha2.Location = New System.Drawing.Point(295, 326)
        Me.datafecha2.MinDate = New Date(2015, 1, 1, 0, 0, 0, 0)
        Me.datafecha2.Name = "datafecha2"
        Me.datafecha2.Size = New System.Drawing.Size(129, 20)
        Me.datafecha2.TabIndex = 13
        '
        'datafecha1
        '
        Me.datafecha1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.datafecha1.Enabled = False
        Me.datafecha1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.datafecha1.Location = New System.Drawing.Point(137, 326)
        Me.datafecha1.MinDate = New Date(2015, 1, 1, 0, 0, 0, 0)
        Me.datafecha1.Name = "datafecha1"
        Me.datafecha1.Size = New System.Drawing.Size(125, 20)
        Me.datafecha1.TabIndex = 12
        '
        'datafecha
        '
        Me.datafecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.datafecha.CustomFormat = ""
        Me.datafecha.Enabled = False
        Me.datafecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.datafecha.Location = New System.Drawing.Point(16, 265)
        Me.datafecha.MinDate = New Date(2015, 1, 1, 0, 0, 0, 0)
        Me.datafecha.Name = "datafecha"
        Me.datafecha.Size = New System.Drawing.Size(539, 20)
        Me.datafecha.TabIndex = 11
        '
        'c_user
        '
        Me.c_user.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.c_user.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.c_user.Enabled = False
        Me.c_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.c_user.FormattingEnabled = True
        Me.c_user.Location = New System.Drawing.Point(16, 198)
        Me.c_user.Name = "c_user"
        Me.c_user.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.c_user.Size = New System.Drawing.Size(539, 23)
        Me.c_user.TabIndex = 5
        '
        'Label41
        '
        Me.Label41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label41.Location = New System.Drawing.Point(246, 145)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(81, 18)
        Me.Label41.TabIndex = 4
        Me.Label41.Text = "Selecciona"
        '
        'cbox_fast
        '
        Me.cbox_fast.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbox_fast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbox_fast.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbox_fast.FormattingEnabled = True
        Me.cbox_fast.Items.AddRange(New Object() {"Hoy", "Ayer", "Última semana", "Último mes", "Último año", "Fecha especifica", "Fecha a fecha", "Todo"})
        Me.cbox_fast.Location = New System.Drawing.Point(19, 52)
        Me.cbox_fast.Name = "cbox_fast"
        Me.cbox_fast.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbox_fast.Size = New System.Drawing.Size(536, 23)
        Me.cbox_fast.TabIndex = 3
        '
        'Label42
        '
        Me.Label42.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label42.Location = New System.Drawing.Point(187, 28)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(214, 18)
        Me.Label42.TabIndex = 2
        Me.Label42.Text = "Selecciona el tipo de consulta *"
        '
        'lblmes
        '
        Me.lblmes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblmes.AutoSize = True
        Me.lblmes.Enabled = False
        Me.lblmes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.lblmes.Location = New System.Drawing.Point(658, 70)
        Me.lblmes.Name = "lblmes"
        Me.lblmes.Size = New System.Drawing.Size(108, 18)
        Me.lblmes.TabIndex = 16
        Me.lblmes.Text = "Mes especifico"
        Me.lblmes.Visible = False
        '
        'c_mes
        '
        Me.c_mes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.c_mes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.c_mes.Enabled = False
        Me.c_mes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.c_mes.FormattingEnabled = True
        Me.c_mes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.c_mes.Location = New System.Drawing.Point(800, 96)
        Me.c_mes.Name = "c_mes"
        Me.c_mes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.c_mes.Size = New System.Drawing.Size(10, 23)
        Me.c_mes.TabIndex = 17
        Me.c_mes.Visible = False
        '
        'lblaño
        '
        Me.lblaño.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblaño.AutoSize = True
        Me.lblaño.Enabled = False
        Me.lblaño.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.lblaño.Location = New System.Drawing.Point(658, 133)
        Me.lblaño.Name = "lblaño"
        Me.lblaño.Size = New System.Drawing.Size(105, 18)
        Me.lblaño.TabIndex = 18
        Me.lblaño.Text = "Año especifico"
        Me.lblaño.Visible = False
        '
        'c_año
        '
        Me.c_año.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.c_año.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.c_año.Enabled = False
        Me.c_año.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.c_año.FormattingEnabled = True
        Me.c_año.Items.AddRange(New Object() {"2017", "2018", "2019", "2020", "2021", "2022", "2023"})
        Me.c_año.Location = New System.Drawing.Point(800, 159)
        Me.c_año.Name = "c_año"
        Me.c_año.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.c_año.Size = New System.Drawing.Size(10, 23)
        Me.c_año.TabIndex = 19
        Me.c_año.Visible = False
        '
        'lblañomes
        '
        Me.lblañomes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblañomes.AutoSize = True
        Me.lblañomes.Enabled = False
        Me.lblañomes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.lblañomes.Location = New System.Drawing.Point(642, 195)
        Me.lblañomes.Name = "lblañomes"
        Me.lblañomes.Size = New System.Drawing.Size(149, 18)
        Me.lblañomes.TabIndex = 20
        Me.lblañomes.Text = "Año y mes especifico"
        Me.lblañomes.Visible = False
        '
        'cañoe
        '
        Me.cañoe.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cañoe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cañoe.Enabled = False
        Me.cañoe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cañoe.FormattingEnabled = True
        Me.cañoe.Items.AddRange(New Object() {"2017", "2018", "2019", "2020", "2021", "2022", "2023"})
        Me.cañoe.Location = New System.Drawing.Point(800, 221)
        Me.cañoe.Name = "cañoe"
        Me.cañoe.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cañoe.Size = New System.Drawing.Size(10, 23)
        Me.cañoe.TabIndex = 22
        Me.cañoe.Visible = False
        '
        'cmese
        '
        Me.cmese.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmese.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmese.Enabled = False
        Me.cmese.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmese.FormattingEnabled = True
        Me.cmese.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Noviembre", "Diciembre"})
        Me.cmese.Location = New System.Drawing.Point(695, 221)
        Me.cmese.Name = "cmese"
        Me.cmese.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmese.Size = New System.Drawing.Size(10, 23)
        Me.cmese.TabIndex = 21
        Me.cmese.Visible = False
        '
        'TabPage10
        '
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(646, 499)
        Me.TabPage10.TabIndex = 2
        Me.TabPage10.Text = "TabPage10"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.Label51)
        Me.TabPage8.Controls.Add(Me.Button18)
        Me.TabPage8.Controls.Add(Me.Button17)
        Me.TabPage8.Controls.Add(Me.lupdate)
        Me.TabPage8.Controls.Add(Me.Label55)
        Me.TabPage8.Controls.Add(Me.GroupBox8)
        Me.TabPage8.Controls.Add(Me.BunifuSeparator1)
        Me.TabPage8.Controls.Add(Me.Label47)
        Me.TabPage8.Controls.Add(Me.Label46)
        Me.TabPage8.Controls.Add(Me.Label45)
        Me.TabPage8.Controls.Add(Me.BunifuImageButton1)
        Me.TabPage8.Controls.Add(Me.Button16)
        Me.TabPage8.Controls.Add(Me.PictureBox1)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(637, 460)
        Me.TabPage8.TabIndex = 5
        Me.TabPage8.Text = "TabPage8"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'Label51
        '
        Me.Label51.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(3, 190)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(631, 65)
        Me.Label51.TabIndex = 23
        Me.Label51.Text = "SINCRONIZANDO EL SISTEMA, POR FAVOR ESPERA..."
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label51.Visible = False
        '
        'Button18
        '
        Me.Button18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button18.Location = New System.Drawing.Point(309, 84)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(122, 27)
        Me.Button18.TabIndex = 109
        Me.Button18.Text = "Cambiar de Servidor"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button17.Location = New System.Drawing.Point(208, 84)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(94, 27)
        Me.Button17.TabIndex = 107
        Me.Button17.Text = "Liberar SYNC"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'lupdate
        '
        Me.lupdate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lupdate.AutoSize = True
        Me.lupdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lupdate.Location = New System.Drawing.Point(174, 439)
        Me.lupdate.Name = "lupdate"
        Me.lupdate.Size = New System.Drawing.Size(0, 13)
        Me.lupdate.TabIndex = 3
        '
        'Label55
        '
        Me.Label55.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(5, 439)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(171, 13)
        Me.Label55.TabIndex = 3
        Me.Label55.Text = "Último SYNC realizado en esta PC:"
        '
        'GroupBox8
        '
        Me.GroupBox8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox8.Controls.Add(Me.Label50)
        Me.GroupBox8.Controls.Add(Me.Label49)
        Me.GroupBox8.Controls.Add(Me.Label48)
        Me.GroupBox8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupBox8.Location = New System.Drawing.Point(72, 282)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(494, 128)
        Me.GroupBox8.TabIndex = 6
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Información"
        '
        'Label50
        '
        Me.Label50.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label50.ForeColor = System.Drawing.Color.Red
        Me.Label50.Location = New System.Drawing.Point(17, 60)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(463, 79)
        Me.Label50.TabIndex = 2
        Me.Label50.Text = resources.GetString("Label50.Text")
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label49
        '
        Me.Label49.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(250, 27)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(57, 13)
        Me.Label49.TabIndex = 1
        Me.Label49.Text = "SERVER"
        '
        'Label48
        '
        Me.Label48.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(117, 27)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(136, 13)
        Me.Label48.TabIndex = 0
        Me.Label48.Text = "SERVIDOR CONECTADO:"
        '
        'BunifuSeparator1
        '
        Me.BunifuSeparator1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BunifuSeparator1.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator1.LineThickness = 1
        Me.BunifuSeparator1.Location = New System.Drawing.Point(41, 105)
        Me.BunifuSeparator1.Name = "BunifuSeparator1"
        Me.BunifuSeparator1.Size = New System.Drawing.Size(563, 35)
        Me.BunifuSeparator1.TabIndex = 5
        Me.BunifuSeparator1.Transparency = 255
        Me.BunifuSeparator1.Vertical = False
        '
        'Label47
        '
        Me.Label47.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label47.Location = New System.Drawing.Point(241, 147)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(166, 20)
        Me.Label47.TabIndex = 4
        Me.Label47.Text = "Sincronizar el Sistema"
        '
        'Label46
        '
        Me.Label46.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(408, 30)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(31, 13)
        Me.Label46.TabIndex = 2
        Me.Label46.Text = "1.6.5"
        '
        'Label45
        '
        Me.Label45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(264, 38)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(155, 31)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "RNV SYNC"
        '
        'BunifuImageButton1
        '
        Me.BunifuImageButton1.BackColor = System.Drawing.Color.White
        Me.BunifuImageButton1.Image = Global.RNV.My.Resources.Resources.cloud_arrow_up_64
        Me.BunifuImageButton1.ImageActive = Nothing
        Me.BunifuImageButton1.Location = New System.Drawing.Point(284, 185)
        Me.BunifuImageButton1.Name = "BunifuImageButton1"
        Me.BunifuImageButton1.Size = New System.Drawing.Size(83, 76)
        Me.BunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuImageButton1.TabIndex = 110
        Me.BunifuImageButton1.TabStop = False
        Me.BunifuImageButton1.Zoom = 10
        '
        'Button16
        '
        Me.Button16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button16.Image = Global.RNV.My.Resources.Resources.cloud_arrow_up_64
        Me.Button16.Location = New System.Drawing.Point(274, 185)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(103, 76)
        Me.Button16.TabIndex = 3
        Me.Button16.UseVisualStyleBackColor = True
        Me.Button16.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.RNV.My.Resources.Resources._519840_52_Cloud_Sync_48
        Me.PictureBox1.Location = New System.Drawing.Point(214, 27)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.Label66)
        Me.TabPage9.Controls.Add(Me.GroupBox11)
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(637, 460)
        Me.TabPage9.TabIndex = 6
        Me.TabPage9.Text = "TabPage9"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.ForeColor = System.Drawing.Color.Red
        Me.Label66.Location = New System.Drawing.Point(16, 433)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(307, 13)
        Me.Label66.TabIndex = 2
        Me.Label66.Text = "** ATENCIÓN: Estas consultas no generan reportes imprimibles."
        '
        'GroupBox11
        '
        Me.GroupBox11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox11.Controls.Add(Me.Button48)
        Me.GroupBox11.Controls.Add(Me.Button43)
        Me.GroupBox11.Controls.Add(Me.Button41)
        Me.GroupBox11.Controls.Add(Me.Button40)
        Me.GroupBox11.Controls.Add(Me.Button26)
        Me.GroupBox11.Controls.Add(Me.Button25)
        Me.GroupBox11.Controls.Add(Me.Button23)
        Me.GroupBox11.Location = New System.Drawing.Point(19, 42)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(599, 370)
        Me.GroupBox11.TabIndex = 1
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Consultas disponibles"
        '
        'Button48
        '
        Me.Button48.Enabled = False
        Me.Button48.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button48.Image = Global.RNV.My.Resources.Resources.clock_64
        Me.Button48.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button48.Location = New System.Drawing.Point(105, 259)
        Me.Button48.Name = "Button48"
        Me.Button48.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button48.Size = New System.Drawing.Size(119, 95)
        Me.Button48.TabIndex = 6
        Me.Button48.Text = "Moras"
        Me.Button48.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button48.UseVisualStyleBackColor = True
        '
        'Button43
        '
        Me.Button43.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button43.Image = Global.RNV.My.Resources.Resources.Flat1_18_64
        Me.Button43.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button43.Location = New System.Drawing.Point(378, 146)
        Me.Button43.Name = "Button43"
        Me.Button43.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button43.Size = New System.Drawing.Size(119, 105)
        Me.Button43.TabIndex = 5
        Me.Button43.Text = "Mov. Stock"
        Me.Button43.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button43.UseVisualStyleBackColor = True
        '
        'Button41
        '
        Me.Button41.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button41.Image = Global.RNV.My.Resources.Resources.Product_release_64
        Me.Button41.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button41.Location = New System.Drawing.Point(241, 146)
        Me.Button41.Name = "Button41"
        Me.Button41.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button41.Size = New System.Drawing.Size(119, 105)
        Me.Button41.TabIndex = 4
        Me.Button41.Text = "Inventario"
        Me.Button41.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button41.UseVisualStyleBackColor = True
        '
        'Button40
        '
        Me.Button40.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button40.Image = Global.RNV.My.Resources.Resources._678131_money_48
        Me.Button40.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button40.Location = New System.Drawing.Point(105, 146)
        Me.Button40.Name = "Button40"
        Me.Button40.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button40.Size = New System.Drawing.Size(119, 105)
        Me.Button40.TabIndex = 3
        Me.Button40.Text = "Ventas Inventario"
        Me.Button40.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button40.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.Image = Global.RNV.My.Resources.Resources.Portofolio_64
        Me.Button26.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button26.Location = New System.Drawing.Point(378, 31)
        Me.Button26.Name = "Button26"
        Me.Button26.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button26.Size = New System.Drawing.Size(119, 105)
        Me.Button26.TabIndex = 2
        Me.Button26.Text = "Corte general"
        Me.Button26.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button25.Image = Global.RNV.My.Resources.Resources.note_48
        Me.Button25.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button25.Location = New System.Drawing.Point(241, 31)
        Me.Button25.Name = "Button25"
        Me.Button25.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button25.Size = New System.Drawing.Size(119, 105)
        Me.Button25.TabIndex = 1
        Me.Button25.Text = "Corte individual"
        Me.Button25.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button23.Image = Global.RNV.My.Resources.Resources.system_users_641
        Me.Button23.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button23.Location = New System.Drawing.Point(105, 31)
        Me.Button23.Name = "Button23"
        Me.Button23.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.Button23.Size = New System.Drawing.Size(119, 105)
        Me.Button23.TabIndex = 0
        Me.Button23.Text = "Alumn / Clien"
        Me.Button23.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button23.UseVisualStyleBackColor = True
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(Me.TextBox14)
        Me.TabPage11.Controls.Add(Me.jtid)
        Me.TabPage11.Controls.Add(Me.mostrarpagos)
        Me.TabPage11.Controls.Add(Me.GroupBox12)
        Me.TabPage11.Controls.Add(Me.BunifuSeparator3)
        Me.TabPage11.Controls.Add(Me.ComboBox1)
        Me.TabPage11.Controls.Add(Me.Label56)
        Me.TabPage11.Controls.Add(Me.Button24)
        Me.TabPage11.Location = New System.Drawing.Point(4, 22)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(637, 460)
        Me.TabPage11.TabIndex = 7
        Me.TabPage11.Text = "TabPage11"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'TextBox14
        '
        Me.TextBox14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TextBox14.Location = New System.Drawing.Point(111, 55)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(412, 20)
        Me.TextBox14.TabIndex = 41
        '
        'jtid
        '
        Me.jtid.Location = New System.Drawing.Point(29, -22)
        Me.jtid.Name = "jtid"
        Me.jtid.Size = New System.Drawing.Size(100, 20)
        Me.jtid.TabIndex = 40
        '
        'mostrarpagos
        '
        Me.mostrarpagos.AutoSize = True
        Me.mostrarpagos.Location = New System.Drawing.Point(483, 150)
        Me.mostrarpagos.Name = "mostrarpagos"
        Me.mostrarpagos.Size = New System.Drawing.Size(105, 13)
        Me.mostrarpagos.TabIndex = 39
        Me.mostrarpagos.TabStop = True
        Me.mostrarpagos.Text = "Ver pagos realizados"
        Me.mostrarpagos.Visible = False
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.Panel1)
        Me.GroupBox12.Controls.Add(Me.MaskedTextBox1)
        Me.GroupBox12.Controls.Add(Me.Label65)
        Me.GroupBox12.Controls.Add(Me.Label63)
        Me.GroupBox12.Controls.Add(Me.TextBox5)
        Me.GroupBox12.Controls.Add(Me.Label64)
        Me.GroupBox12.Controls.Add(Me.TextBox6)
        Me.GroupBox12.Controls.Add(Me.Label62)
        Me.GroupBox12.Controls.Add(Me.TextBox4)
        Me.GroupBox12.Controls.Add(Me.FlowLayoutPanel2)
        Me.GroupBox12.Controls.Add(Me.Label59)
        Me.GroupBox12.Controls.Add(Me.Label61)
        Me.GroupBox12.Controls.Add(Me.TextBox3)
        Me.GroupBox12.Controls.Add(Me.Label60)
        Me.GroupBox12.Controls.Add(Me.TextBox2)
        Me.GroupBox12.Controls.Add(Me.Label58)
        Me.GroupBox12.Controls.Add(Me.TextBox1)
        Me.GroupBox12.Location = New System.Drawing.Point(49, 164)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(539, 272)
        Me.GroupBox12.TabIndex = 38
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Resultado"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label57)
        Me.Panel1.Location = New System.Drawing.Point(19, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(505, 237)
        Me.Panel1.TabIndex = 73
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.ForeColor = System.Drawing.Color.Red
        Me.Label57.Location = New System.Drawing.Point(200, 103)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(114, 13)
        Me.Label57.TabIndex = 0
        Me.Label57.Text = "Aún nada que mostrar."
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.Location = New System.Drawing.Point(21, 131)
        Me.MaskedTextBox1.Mask = "00/00/0000"
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.ReadOnly = True
        Me.MaskedTextBox1.Size = New System.Drawing.Size(251, 20)
        Me.MaskedTextBox1.TabIndex = 75
        Me.MaskedTextBox1.ValidatingType = GetType(Date)
        '
        'Label65
        '
        Me.Label65.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(19, 115)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(106, 13)
        Me.Label65.TabIndex = 74
        Me.Label65.Text = "Fecha de nacimiento"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.ForeColor = System.Drawing.Color.Black
        Me.Label63.Location = New System.Drawing.Point(284, 220)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(104, 13)
        Me.Label63.TabIndex = 72
        Me.Label63.Text = "Mes siguiente VNET"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(287, 236)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(237, 20)
        Me.TextBox5.TabIndex = 71
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.ForeColor = System.Drawing.Color.Black
        Me.Label64.Location = New System.Drawing.Point(18, 220)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(130, 13)
        Me.Label64.TabIndex = 70
        Me.Label64.Text = "Mes actual pagado VNET"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(21, 236)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(251, 20)
        Me.TextBox6.TabIndex = 69
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.ForeColor = System.Drawing.Color.Black
        Me.Label62.Location = New System.Drawing.Point(18, 68)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(40, 13)
        Me.Label62.TabIndex = 68
        Me.Label62.Text = "Código"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(21, 84)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(251, 20)
        Me.TextBox4.TabIndex = 67
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel2.AutoScroll = True
        Me.FlowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel2.Enabled = False
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(287, 38)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(237, 119)
        Me.FlowLayoutPanel2.TabIndex = 66
        '
        'Label59
        '
        Me.Label59.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(284, 19)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(134, 13)
        Me.Label59.TabIndex = 65
        Me.Label59.Text = "Curso / Servicio asignados"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.ForeColor = System.Drawing.Color.Black
        Me.Label61.Location = New System.Drawing.Point(284, 174)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(90, 13)
        Me.Label61.TabIndex = 39
        Me.Label61.Text = "Mes siguiente NV"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(287, 190)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(237, 20)
        Me.TextBox3.TabIndex = 38
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.ForeColor = System.Drawing.Color.Black
        Me.Label60.Location = New System.Drawing.Point(18, 174)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(116, 13)
        Me.Label60.TabIndex = 37
        Me.Label60.Text = "Mes actual pagado NV"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(21, 190)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(251, 20)
        Me.TextBox2.TabIndex = 36
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.ForeColor = System.Drawing.Color.Black
        Me.Label58.Location = New System.Drawing.Point(18, 23)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(90, 13)
        Me.Label58.TabIndex = 34
        Me.Label58.Text = "Nombre completo"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(21, 39)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(251, 20)
        Me.TextBox1.TabIndex = 1
        '
        'BunifuSeparator3
        '
        Me.BunifuSeparator3.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator3.LineThickness = 1
        Me.BunifuSeparator3.Location = New System.Drawing.Point(49, 123)
        Me.BunifuSeparator3.Name = "BunifuSeparator3"
        Me.BunifuSeparator3.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator3.TabIndex = 37
        Me.BunifuSeparator3.Transparency = 255
        Me.BunifuSeparator3.Vertical = False
        '
        'ComboBox1
        '
        Me.ComboBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(654, 53)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComboBox1.Size = New System.Drawing.Size(21, 23)
        Me.ComboBox1.TabIndex = 36
        '
        'Label56
        '
        Me.Label56.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label56.Location = New System.Drawing.Point(232, 26)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(181, 18)
        Me.Label56.TabIndex = 35
        Me.Label56.Text = "Selecciona Alumn / Clien *"
        '
        'Button24
        '
        Me.Button24.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button24.Location = New System.Drawing.Point(264, 84)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(120, 36)
        Me.Button24.TabIndex = 34
        Me.Button24.Text = "Consultar datos"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'TabPage12
        '
        Me.TabPage12.Controls.Add(Me.Panel3)
        Me.TabPage12.Controls.Add(Me.Panel2)
        Me.TabPage12.Controls.Add(Me.codigorecorte)
        Me.TabPage12.Controls.Add(Me.BunifuSeparator4)
        Me.TabPage12.Controls.Add(Me.Label76)
        Me.TabPage12.Controls.Add(Me.Button27)
        Me.TabPage12.Location = New System.Drawing.Point(4, 22)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage12.Size = New System.Drawing.Size(637, 460)
        Me.TabPage12.TabIndex = 8
        Me.TabPage12.Text = "TabPage12"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label80)
        Me.Panel3.Location = New System.Drawing.Point(3, 150)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(628, 303)
        Me.Panel3.TabIndex = 48
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.ForeColor = System.Drawing.Color.Red
        Me.Label80.Location = New System.Drawing.Point(266, 126)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(114, 13)
        Me.Label80.TabIndex = 0
        Me.Label80.Text = "Aún nada que mostrar."
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.TextBox13)
        Me.Panel2.Controls.Add(Me.TextBox25)
        Me.Panel2.Controls.Add(Me.TextBox26)
        Me.Panel2.Controls.Add(Me.Label81)
        Me.Panel2.Controls.Add(Me.Label67)
        Me.Panel2.Controls.Add(Me.Label68)
        Me.Panel2.Controls.Add(Me.RichTextBox1)
        Me.Panel2.Controls.Add(Me.TextBox12)
        Me.Panel2.Controls.Add(Me.TextBox7)
        Me.Panel2.Controls.Add(Me.TextBox8)
        Me.Panel2.Controls.Add(Me.TextBox15)
        Me.Panel2.Controls.Add(Me.TextBox16)
        Me.Panel2.Controls.Add(Me.Label79)
        Me.Panel2.Controls.Add(Me.Label69)
        Me.Panel2.Controls.Add(Me.BunifuSeparator5)
        Me.Panel2.Controls.Add(Me.TextBox9)
        Me.Panel2.Controls.Add(Me.TextBox24)
        Me.Panel2.Controls.Add(Me.TextBox10)
        Me.Panel2.Controls.Add(Me.Label78)
        Me.Panel2.Controls.Add(Me.TextBox11)
        Me.Panel2.Controls.Add(Me.Label77)
        Me.Panel2.Controls.Add(Me.TextBox17)
        Me.Panel2.Controls.Add(Me.Label75)
        Me.Panel2.Controls.Add(Me.TextBox18)
        Me.Panel2.Controls.Add(Me.Label74)
        Me.Panel2.Controls.Add(Me.Label70)
        Me.Panel2.Controls.Add(Me.Label73)
        Me.Panel2.Controls.Add(Me.TextBox19)
        Me.Panel2.Controls.Add(Me.Label72)
        Me.Panel2.Controls.Add(Me.Label71)
        Me.Panel2.Controls.Add(Me.TextBox23)
        Me.Panel2.Controls.Add(Me.TextBox20)
        Me.Panel2.Controls.Add(Me.TextBox22)
        Me.Panel2.Controls.Add(Me.TextBox21)
        Me.Panel2.Controls.Add(Me.BunifuSeparator6)
        Me.Panel2.Location = New System.Drawing.Point(3, 153)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(628, 300)
        Me.Panel2.TabIndex = 47
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(444, 295)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.Size = New System.Drawing.Size(147, 20)
        Me.TextBox13.TabIndex = 123
        Me.TextBox13.TabStop = False
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(284, 295)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.ReadOnly = True
        Me.TextBox25.Size = New System.Drawing.Size(147, 20)
        Me.TextBox25.TabIndex = 122
        '
        'TextBox26
        '
        Me.TextBox26.Location = New System.Drawing.Point(127, 295)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.ReadOnly = True
        Me.TextBox26.Size = New System.Drawing.Size(147, 20)
        Me.TextBox26.TabIndex = 121
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label81.Location = New System.Drawing.Point(37, 298)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(75, 13)
        Me.Label81.TabIndex = 124
        Me.Label81.Text = "** TOTAL **"
        '
        'Label67
        '
        Me.Label67.Location = New System.Drawing.Point(579, 433)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(29, 13)
        Me.Label67.TabIndex = 120
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(37, 336)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(111, 13)
        Me.Label68.TabIndex = 119
        Me.Label68.Text = "Comentario (Opcional)"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(40, 354)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(551, 60)
        Me.RichTextBox1.TabIndex = 118
        Me.RichTextBox1.Text = ""
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(444, 238)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.Size = New System.Drawing.Size(147, 20)
        Me.TextBox12.TabIndex = 110
        Me.TextBox12.TabStop = False
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(444, 207)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(147, 20)
        Me.TextBox7.TabIndex = 106
        Me.TextBox7.TabStop = False
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(444, 177)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(147, 20)
        Me.TextBox8.TabIndex = 100
        Me.TextBox8.TabStop = False
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(444, 148)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(147, 20)
        Me.TextBox15.TabIndex = 117
        Me.TextBox15.TabStop = False
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(444, 116)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(147, 20)
        Me.TextBox16.TabIndex = 116
        Me.TextBox16.TabStop = False
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(37, 16)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(109, 13)
        Me.Label79.TabIndex = 92
        Me.Label79.Text = "Nombre del operando"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(441, 96)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(55, 13)
        Me.Label69.TabIndex = 115
        Me.Label69.Text = "Diferencia"
        '
        'BunifuSeparator5
        '
        Me.BunifuSeparator5.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator5.LineThickness = 1
        Me.BunifuSeparator5.Location = New System.Drawing.Point(35, 58)
        Me.BunifuSeparator5.Name = "BunifuSeparator5"
        Me.BunifuSeparator5.Size = New System.Drawing.Size(563, 35)
        Me.BunifuSeparator5.TabIndex = 93
        Me.BunifuSeparator5.Transparency = 255
        Me.BunifuSeparator5.Vertical = False
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(284, 238)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(147, 20)
        Me.TextBox9.TabIndex = 109
        '
        'TextBox24
        '
        Me.TextBox24.Location = New System.Drawing.Point(38, 33)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.ReadOnly = True
        Me.TextBox24.Size = New System.Drawing.Size(342, 20)
        Me.TextBox24.TabIndex = 91
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(284, 207)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(147, 20)
        Me.TextBox10.TabIndex = 104
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(37, 96)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(29, 13)
        Me.Label78.TabIndex = 94
        Me.Label78.Text = "Para"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(284, 177)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.Size = New System.Drawing.Size(147, 20)
        Me.TextBox11.TabIndex = 99
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.Location = New System.Drawing.Point(37, 116)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(32, 13)
        Me.Label77.TabIndex = 95
        Me.Label77.Text = "ANV"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(284, 148)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(147, 20)
        Me.TextBox17.TabIndex = 114
        Me.TextBox17.TabStop = False
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.Location = New System.Drawing.Point(37, 148)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(40, 13)
        Me.Label75.TabIndex = 96
        Me.Label75.Text = "VNET"
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(284, 116)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(147, 20)
        Me.TextBox18.TabIndex = 113
        Me.TextBox18.TabStop = False
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label74.Location = New System.Drawing.Point(37, 180)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(47, 13)
        Me.Label74.TabIndex = 98
        Me.Label74.Text = "ACCES"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(281, 96)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(76, 13)
        Me.Label70.TabIndex = 112
        Me.Label70.Text = "Dinero en caja"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.Location = New System.Drawing.Point(37, 210)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(42, 13)
        Me.Label73.TabIndex = 101
        Me.Label73.Text = "PELIS"
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(127, 238)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.Size = New System.Drawing.Size(147, 20)
        Me.TextBox19.TabIndex = 108
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(124, 96)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(60, 13)
        Me.Label72.TabIndex = 103
        Me.Label72.Text = "Calculando"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.Location = New System.Drawing.Point(37, 241)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(37, 13)
        Me.Label71.TabIndex = 111
        Me.Label71.Text = "INV *"
        '
        'TextBox23
        '
        Me.TextBox23.Location = New System.Drawing.Point(127, 116)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.ReadOnly = True
        Me.TextBox23.Size = New System.Drawing.Size(147, 20)
        Me.TextBox23.TabIndex = 105
        Me.TextBox23.TabStop = False
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(127, 207)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(147, 20)
        Me.TextBox20.TabIndex = 102
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(127, 148)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.ReadOnly = True
        Me.TextBox22.Size = New System.Drawing.Size(147, 20)
        Me.TextBox22.TabIndex = 107
        Me.TextBox22.TabStop = False
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(127, 177)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.ReadOnly = True
        Me.TextBox21.Size = New System.Drawing.Size(147, 20)
        Me.TextBox21.TabIndex = 97
        '
        'BunifuSeparator6
        '
        Me.BunifuSeparator6.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator6.LineThickness = 1
        Me.BunifuSeparator6.Location = New System.Drawing.Point(129, 260)
        Me.BunifuSeparator6.Name = "BunifuSeparator6"
        Me.BunifuSeparator6.Size = New System.Drawing.Size(462, 35)
        Me.BunifuSeparator6.TabIndex = 125
        Me.BunifuSeparator6.Transparency = 255
        Me.BunifuSeparator6.Vertical = False
        '
        'codigorecorte
        '
        Me.codigorecorte.Cursor = System.Windows.Forms.Cursors.Hand
        Me.codigorecorte.Location = New System.Drawing.Point(91, 54)
        Me.codigorecorte.Name = "codigorecorte"
        Me.codigorecorte.ReadOnly = True
        Me.codigorecorte.Size = New System.Drawing.Size(436, 20)
        Me.codigorecorte.TabIndex = 46
        '
        'BunifuSeparator4
        '
        Me.BunifuSeparator4.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator4.LineThickness = 1
        Me.BunifuSeparator4.Location = New System.Drawing.Point(49, 123)
        Me.BunifuSeparator4.Name = "BunifuSeparator4"
        Me.BunifuSeparator4.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator4.TabIndex = 43
        Me.BunifuSeparator4.Transparency = 255
        Me.BunifuSeparator4.Vertical = False
        '
        'Label76
        '
        Me.Label76.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label76.Location = New System.Drawing.Point(259, 26)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(124, 18)
        Me.Label76.TabIndex = 41
        Me.Label76.Text = "Código de corte *"
        '
        'Button27
        '
        Me.Button27.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button27.Location = New System.Drawing.Point(263, 84)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(120, 36)
        Me.Button27.TabIndex = 40
        Me.Button27.Text = "Consultar datos"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'TabPage13
        '
        Me.TabPage13.Controls.Add(Me.Panel4)
        Me.TabPage13.Controls.Add(Me.Panel5)
        Me.TabPage13.Controls.Add(Me.TextBox46)
        Me.TabPage13.Controls.Add(Me.BunifuSeparator9)
        Me.TabPage13.Controls.Add(Me.Label96)
        Me.TabPage13.Controls.Add(Me.Button28)
        Me.TabPage13.Location = New System.Drawing.Point(4, 22)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage13.Size = New System.Drawing.Size(637, 460)
        Me.TabPage13.TabIndex = 9
        Me.TabPage13.Text = "TabPage13"
        Me.TabPage13.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label82)
        Me.Panel4.Location = New System.Drawing.Point(4, 145)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(628, 216)
        Me.Panel4.TabIndex = 54
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.ForeColor = System.Drawing.Color.Red
        Me.Label82.Location = New System.Drawing.Point(248, 88)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(114, 13)
        Me.Label82.TabIndex = 0
        Me.Label82.Text = "Aún nada que mostrar."
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.TextBox33)
        Me.Panel5.Controls.Add(Me.Label83)
        Me.Panel5.Controls.Add(Me.Label84)
        Me.Panel5.Controls.Add(Me.RichTextBox2)
        Me.Panel5.Controls.Add(Me.Label91)
        Me.Panel5.Controls.Add(Me.TextBox27)
        Me.Panel5.Controls.Add(Me.TextBox32)
        Me.Panel5.Controls.Add(Me.TextBox28)
        Me.Panel5.Controls.Add(Me.Label85)
        Me.Panel5.Controls.Add(Me.Label90)
        Me.Panel5.Controls.Add(Me.TextBox29)
        Me.Panel5.Controls.Add(Me.Label89)
        Me.Panel5.Controls.Add(Me.Label86)
        Me.Panel5.Controls.Add(Me.Label88)
        Me.Panel5.Controls.Add(Me.TextBox30)
        Me.Panel5.Controls.Add(Me.TextBox31)
        Me.Panel5.Controls.Add(Me.Label87)
        Me.Panel5.Location = New System.Drawing.Point(4, 154)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(628, 207)
        Me.Panel5.TabIndex = 53
        '
        'TextBox33
        '
        Me.TextBox33.Location = New System.Drawing.Point(12, 33)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.ReadOnly = True
        Me.TextBox33.Size = New System.Drawing.Size(197, 20)
        Me.TextBox33.TabIndex = 121
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(226, 115)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(111, 13)
        Me.Label83.TabIndex = 70
        Me.Label83.Text = "Comentario (Opcional)"
        '
        'Label84
        '
        Me.Label84.Location = New System.Drawing.Point(579, 433)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(29, 13)
        Me.Label84.TabIndex = 120
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Location = New System.Drawing.Point(229, 132)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.ReadOnly = True
        Me.RichTextBox2.Size = New System.Drawing.Size(371, 60)
        Me.RichTextBox2.TabIndex = 60
        Me.RichTextBox2.Text = ""
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(14, 15)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(36, 13)
        Me.Label91.TabIndex = 56
        Me.Label91.Text = "Para *"
        '
        'TextBox27
        '
        Me.TextBox27.Location = New System.Drawing.Point(12, 132)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.ReadOnly = True
        Me.TextBox27.Size = New System.Drawing.Size(197, 20)
        Me.TextBox27.TabIndex = 58
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(12, 82)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.ReadOnly = True
        Me.TextBox32.Size = New System.Drawing.Size(197, 20)
        Me.TextBox32.TabIndex = 57
        '
        'TextBox28
        '
        Me.TextBox28.Location = New System.Drawing.Point(424, 84)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.ReadOnly = True
        Me.TextBox28.Size = New System.Drawing.Size(176, 20)
        Me.TextBox28.TabIndex = 69
        Me.TextBox28.TabStop = False
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(421, 66)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(31, 13)
        Me.Label85.TabIndex = 68
        Me.Label85.Text = "Total"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(9, 66)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(116, 13)
        Me.Label90.TabIndex = 59
        Me.Label90.Text = "Nombre del operando *"
        '
        'TextBox29
        '
        Me.TextBox29.Location = New System.Drawing.Point(424, 33)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.ReadOnly = True
        Me.TextBox29.Size = New System.Drawing.Size(176, 20)
        Me.TextBox29.TabIndex = 67
        Me.TextBox29.TabStop = False
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(9, 115)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(83, 13)
        Me.Label89.TabIndex = 61
        Me.Label89.Text = "Dinero en caja *"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(421, 15)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(35, 13)
        Me.Label86.TabIndex = 66
        Me.Label86.Text = "Retiro"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(226, 15)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(54, 13)
        Me.Label88.TabIndex = 62
        Me.Label88.Text = "Calculado"
        '
        'TextBox30
        '
        Me.TextBox30.Location = New System.Drawing.Point(229, 84)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.ReadOnly = True
        Me.TextBox30.Size = New System.Drawing.Size(176, 20)
        Me.TextBox30.TabIndex = 65
        Me.TextBox30.TabStop = False
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(229, 33)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.ReadOnly = True
        Me.TextBox31.Size = New System.Drawing.Size(176, 20)
        Me.TextBox31.TabIndex = 63
        Me.TextBox31.TabStop = False
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(226, 66)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(55, 13)
        Me.Label87.TabIndex = 64
        Me.Label87.Text = "Diferencia"
        '
        'TextBox46
        '
        Me.TextBox46.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TextBox46.Location = New System.Drawing.Point(92, 49)
        Me.TextBox46.Name = "TextBox46"
        Me.TextBox46.ReadOnly = True
        Me.TextBox46.Size = New System.Drawing.Size(436, 20)
        Me.TextBox46.TabIndex = 52
        '
        'BunifuSeparator9
        '
        Me.BunifuSeparator9.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator9.LineThickness = 1
        Me.BunifuSeparator9.Location = New System.Drawing.Point(50, 118)
        Me.BunifuSeparator9.Name = "BunifuSeparator9"
        Me.BunifuSeparator9.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator9.TabIndex = 51
        Me.BunifuSeparator9.Transparency = 255
        Me.BunifuSeparator9.Vertical = False
        '
        'Label96
        '
        Me.Label96.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label96.AutoSize = True
        Me.Label96.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label96.Location = New System.Drawing.Point(231, 21)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(187, 18)
        Me.Label96.TabIndex = 50
        Me.Label96.Text = "Código de corte individual *"
        '
        'Button28
        '
        Me.Button28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button28.Location = New System.Drawing.Point(264, 79)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(120, 36)
        Me.Button28.TabIndex = 49
        Me.Button28.Text = "Consultar datos"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'TabPage14
        '
        Me.TabPage14.Controls.Add(Me.Label167)
        Me.TabPage14.Controls.Add(Me.GroupBox26)
        Me.TabPage14.Controls.Add(Me.GroupBox25)
        Me.TabPage14.Controls.Add(Me.Label102)
        Me.TabPage14.Controls.Add(Me.GroupBox20)
        Me.TabPage14.Controls.Add(Me.BunifuSeparator7)
        Me.TabPage14.Controls.Add(Me.GroupBox18)
        Me.TabPage14.Controls.Add(Me.GroupBox17)
        Me.TabPage14.Controls.Add(Me.GroupBox16)
        Me.TabPage14.Controls.Add(Me.GroupBox14)
        Me.TabPage14.Controls.Add(Me.GroupBox15)
        Me.TabPage14.Controls.Add(Me.GroupBox13)
        Me.TabPage14.Controls.Add(Me.Chart1)
        Me.TabPage14.Location = New System.Drawing.Point(4, 22)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage14.Size = New System.Drawing.Size(637, 460)
        Me.TabPage14.TabIndex = 10
        Me.TabPage14.Text = "TabPage14"
        Me.TabPage14.UseVisualStyleBackColor = True
        '
        'Label167
        '
        Me.Label167.AutoSize = True
        Me.Label167.ForeColor = System.Drawing.Color.Red
        Me.Label167.Location = New System.Drawing.Point(517, 129)
        Me.Label167.Name = "Label167"
        Me.Label167.Size = New System.Drawing.Size(79, 13)
        Me.Label167.TabIndex = 10
        Me.Label167.Text = "¡Próximamente!"
        '
        'GroupBox26
        '
        Me.GroupBox26.Controls.Add(Me.LinkLabel14)
        Me.GroupBox26.Enabled = False
        Me.GroupBox26.Location = New System.Drawing.Point(510, 135)
        Me.GroupBox26.Name = "GroupBox26"
        Me.GroupBox26.Size = New System.Drawing.Size(98, 34)
        Me.GroupBox26.TabIndex = 8
        Me.GroupBox26.TabStop = False
        '
        'LinkLabel14
        '
        Me.LinkLabel14.AutoSize = True
        Me.LinkLabel14.Location = New System.Drawing.Point(8, 13)
        Me.LinkLabel14.Name = "LinkLabel14"
        Me.LinkLabel14.Size = New System.Drawing.Size(75, 13)
        Me.LinkLabel14.TabIndex = 0
        Me.LinkLabel14.TabStop = True
        Me.LinkLabel14.Text = "Ir a Ciber RNV"
        '
        'GroupBox25
        '
        Me.GroupBox25.Controls.Add(Me.Label165)
        Me.GroupBox25.Controls.Add(Me.tprod)
        Me.GroupBox25.Location = New System.Drawing.Point(482, 23)
        Me.GroupBox25.Name = "GroupBox25"
        Me.GroupBox25.Size = New System.Drawing.Size(144, 94)
        Me.GroupBox25.TabIndex = 7
        Me.GroupBox25.TabStop = False
        '
        'Label165
        '
        Me.Label165.AutoSize = True
        Me.Label165.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label165.Location = New System.Drawing.Point(9, 65)
        Me.Label165.Name = "Label165"
        Me.Label165.Size = New System.Drawing.Size(112, 18)
        Me.Label165.TabIndex = 6
        Me.Label165.Text = "Total productos"
        '
        'tprod
        '
        Me.tprod.AutoSize = True
        Me.tprod.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tprod.Location = New System.Drawing.Point(6, 13)
        Me.tprod.Name = "tprod"
        Me.tprod.Size = New System.Drawing.Size(42, 44)
        Me.tprod.TabIndex = 5
        Me.tprod.Text = "0"
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.ForeColor = System.Drawing.Color.Red
        Me.Label102.Location = New System.Drawing.Point(355, 137)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(45, 13)
        Me.Label102.TabIndex = 9
        Me.Label102.Text = "¡Nuevo!"
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.LinkLabel5)
        Me.GroupBox20.Location = New System.Drawing.Point(406, 135)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Size = New System.Drawing.Size(90, 34)
        Me.GroupBox20.TabIndex = 8
        Me.GroupBox20.TabStop = False
        '
        'LinkLabel5
        '
        Me.LinkLabel5.AutoSize = True
        Me.LinkLabel5.Location = New System.Drawing.Point(8, 13)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(35, 13)
        Me.LinkLabel5.TabIndex = 0
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "Notas"
        '
        'BunifuSeparator7
        '
        Me.BunifuSeparator7.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator7.LineThickness = 1
        Me.BunifuSeparator7.Location = New System.Drawing.Point(64, 175)
        Me.BunifuSeparator7.Name = "BunifuSeparator7"
        Me.BunifuSeparator7.Size = New System.Drawing.Size(510, 35)
        Me.BunifuSeparator7.TabIndex = 8
        Me.BunifuSeparator7.Transparency = 255
        Me.BunifuSeparator7.Vertical = False
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.LinkLabel4)
        Me.GroupBox18.Location = New System.Drawing.Point(280, 135)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(98, 34)
        Me.GroupBox18.TabIndex = 7
        Me.GroupBox18.TabStop = False
        '
        'LinkLabel4
        '
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.Location = New System.Drawing.Point(8, 13)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(71, 13)
        Me.LinkLabel4.TabIndex = 0
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Ir a inventario"
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.LinkLabel3)
        Me.GroupBox17.Location = New System.Drawing.Point(167, 135)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(103, 34)
        Me.GroupBox17.TabIndex = 6
        Me.GroupBox17.TabStop = False
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.Location = New System.Drawing.Point(8, 13)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(63, 13)
        Me.LinkLabel3.TabIndex = 0
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Ir a reportes"
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.LinkLabel2)
        Me.GroupBox16.Location = New System.Drawing.Point(28, 135)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(126, 34)
        Me.GroupBox16.TabIndex = 5
        Me.GroupBox16.TabStop = False
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(8, 13)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(108, 13)
        Me.LinkLabel2.TabIndex = 0
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Realizar nuevo cobro"
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.Label97)
        Me.GroupBox14.Controls.Add(Me.Label98)
        Me.GroupBox14.Location = New System.Drawing.Point(324, 23)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(144, 94)
        Me.GroupBox14.TabIndex = 4
        Me.GroupBox14.TabStop = False
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label97.Location = New System.Drawing.Point(9, 65)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(88, 18)
        Me.Label97.TabIndex = 6
        Me.Label97.Text = "Total ventas"
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label98.Location = New System.Drawing.Point(6, 13)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(42, 44)
        Me.Label98.TabIndex = 5
        Me.Label98.Text = "0"
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.Label94)
        Me.GroupBox15.Controls.Add(Me.Label95)
        Me.GroupBox15.Location = New System.Drawing.Point(167, 23)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(144, 94)
        Me.GroupBox15.TabIndex = 4
        Me.GroupBox15.TabStop = False
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label94.Location = New System.Drawing.Point(12, 65)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(74, 18)
        Me.Label94.TabIndex = 3
        Me.Label94.Text = "Cur / Serv"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label95.Location = New System.Drawing.Point(9, 13)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(42, 44)
        Me.Label95.TabIndex = 2
        Me.Label95.Text = "0"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.Label93)
        Me.GroupBox13.Controls.Add(Me.Label92)
        Me.GroupBox13.Location = New System.Drawing.Point(11, 23)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(144, 94)
        Me.GroupBox13.TabIndex = 3
        Me.GroupBox13.TabStop = False
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.Location = New System.Drawing.Point(11, 65)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(94, 18)
        Me.Label93.TabIndex = 1
        Me.Label93.Text = "Alumn / Clien"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label92.Location = New System.Drawing.Point(8, 13)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(42, 44)
        Me.Label92.TabIndex = 0
        Me.Label92.Text = "0"
        '
        'Chart1
        '
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(8, 208)
        Me.Chart1.Name = "Chart1"
        Me.Chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Academia NV"
        Series1.YValuesPerPoint = 2
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Vision NET"
        Series3.ChartArea = "ChartArea1"
        Series3.Legend = "Legend1"
        Series3.Name = "Inventario NV"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Series.Add(Series2)
        Me.Chart1.Series.Add(Series3)
        Me.Chart1.Size = New System.Drawing.Size(619, 239)
        Me.Chart1.TabIndex = 2
        Me.Chart1.Text = "Chart1"
        Title1.Name = "Title1"
        Title1.Text = "Ingresos hoy (Q)"
        Me.Chart1.Titles.Add(Title1)
        '
        'TabPage15
        '
        Me.TabPage15.Controls.Add(Me.Label99)
        Me.TabPage15.Controls.Add(Me.GroupBox19)
        Me.TabPage15.Location = New System.Drawing.Point(4, 22)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage15.Size = New System.Drawing.Size(637, 460)
        Me.TabPage15.TabIndex = 11
        Me.TabPage15.Text = "TabPage15"
        Me.TabPage15.UseVisualStyleBackColor = True
        '
        'Label99
        '
        Me.Label99.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label99.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label99.Location = New System.Drawing.Point(3, 167)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(631, 65)
        Me.Label99.TabIndex = 24
        Me.Label99.Text = "POR FAVOR, ESPERA..."
        Me.Label99.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label99.Visible = False
        '
        'GroupBox19
        '
        Me.GroupBox19.Controls.Add(Me.Label100)
        Me.GroupBox19.Controls.Add(Me.Button31)
        Me.GroupBox19.Controls.Add(Me.ComboBox3)
        Me.GroupBox19.Controls.Add(Me.ComboBox2)
        Me.GroupBox19.Controls.Add(Me.Label101)
        Me.GroupBox19.Location = New System.Drawing.Point(29, 40)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Size = New System.Drawing.Size(578, 390)
        Me.GroupBox19.TabIndex = 53
        Me.GroupBox19.TabStop = False
        '
        'Label100
        '
        Me.Label100.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label100.AutoSize = True
        Me.Label100.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label100.Location = New System.Drawing.Point(283, 28)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(37, 18)
        Me.Label100.TabIndex = 48
        Me.Label100.Text = "Giro"
        '
        'Button31
        '
        Me.Button31.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button31.Location = New System.Drawing.Point(447, 344)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(120, 36)
        Me.Button31.TabIndex = 47
        Me.Button31.Text = "Generar reporte"
        Me.Button31.UseVisualStyleBackColor = True
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Enabled = False
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(80, 113)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(427, 21)
        Me.ComboBox3.TabIndex = 52
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"-todos-", "Academia NV", "Vision NET"})
        Me.ComboBox2.Location = New System.Drawing.Point(80, 51)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(427, 21)
        Me.ComboBox2.TabIndex = 50
        '
        'Label101
        '
        Me.Label101.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label101.AutoSize = True
        Me.Label101.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label101.Location = New System.Drawing.Point(245, 90)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(114, 18)
        Me.Label101.TabIndex = 51
        Me.Label101.Text = "Curso / Servicio"
        '
        'TabPage16
        '
        Me.TabPage16.Controls.Add(Me.LinkLabel10)
        Me.TabPage16.Controls.Add(Me.Label115)
        Me.TabPage16.Controls.Add(Me.id_inventario)
        Me.TabPage16.Controls.Add(Me.Label106)
        Me.TabPage16.Controls.Add(Me.CheckBox3)
        Me.TabPage16.Controls.Add(Me.Label114)
        Me.TabPage16.Controls.Add(Me.TextBox39)
        Me.TabPage16.Controls.Add(Me.Label113)
        Me.TabPage16.Controls.Add(Me.TextBox38)
        Me.TabPage16.Controls.Add(Me.Label112)
        Me.TabPage16.Controls.Add(Me.TextBox37)
        Me.TabPage16.Controls.Add(Me.Button34)
        Me.TabPage16.Controls.Add(Me.RichTextBox3)
        Me.TabPage16.Controls.Add(Me.Label109)
        Me.TabPage16.Controls.Add(Me.cod_inventario)
        Me.TabPage16.Controls.Add(Me.Button35)
        Me.TabPage16.Controls.Add(Me.Label110)
        Me.TabPage16.Controls.Add(Me.Label111)
        Me.TabPage16.Controls.Add(Me.TextBox36)
        Me.TabPage16.Controls.Add(Me.Label108)
        Me.TabPage16.Controls.Add(Me.Button39)
        Me.TabPage16.Controls.Add(Me.Button38)
        Me.TabPage16.Controls.Add(Me.Button36)
        Me.TabPage16.Controls.Add(Me.Button37)
        Me.TabPage16.Location = New System.Drawing.Point(4, 22)
        Me.TabPage16.Name = "TabPage16"
        Me.TabPage16.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage16.Size = New System.Drawing.Size(637, 460)
        Me.TabPage16.TabIndex = 12
        Me.TabPage16.Text = "TabPage16"
        Me.TabPage16.UseVisualStyleBackColor = True
        '
        'LinkLabel10
        '
        Me.LinkLabel10.AutoSize = True
        Me.LinkLabel10.Location = New System.Drawing.Point(157, 433)
        Me.LinkLabel10.Name = "LinkLabel10"
        Me.LinkLabel10.Size = New System.Drawing.Size(127, 13)
        Me.LinkLabel10.TabIndex = 616
        Me.LinkLabel10.TabStop = True
        Me.LinkLabel10.Text = "Generar código de barras"
        '
        'Label115
        '
        Me.Label115.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label115.AutoSize = True
        Me.Label115.Location = New System.Drawing.Point(535, 203)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(72, 13)
        Me.Label115.TabIndex = 614
        Me.Label115.Text = "0% de útilidad"
        '
        'id_inventario
        '
        Me.id_inventario.AutoSize = True
        Me.id_inventario.Location = New System.Drawing.Point(677, 277)
        Me.id_inventario.Name = "id_inventario"
        Me.id_inventario.Size = New System.Drawing.Size(13, 13)
        Me.id_inventario.TabIndex = 610
        Me.id_inventario.Text = "0"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label106.ForeColor = System.Drawing.Color.Red
        Me.Label106.Location = New System.Drawing.Point(103, 19)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(13, 13)
        Me.Label106.TabIndex = 609
        Me.Label106.Text = "&?"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckBox3.Location = New System.Drawing.Point(16, 18)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(92, 17)
        Me.CheckBox3.TabIndex = 608
        Me.CheckBox3.Text = "Es un servicio"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'Label114
        '
        Me.Label114.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label114.AutoSize = True
        Me.Label114.Location = New System.Drawing.Point(73, 183)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(124, 13)
        Me.Label114.TabIndex = 81
        Me.Label114.Text = "Precio minimo de venta *"
        '
        'TextBox39
        '
        Me.TextBox39.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox39.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox39.Location = New System.Drawing.Point(76, 199)
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New System.Drawing.Size(457, 20)
        Me.TextBox39.TabIndex = 603
        '
        'Label113
        '
        Me.Label113.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label113.AutoSize = True
        Me.Label113.Location = New System.Drawing.Point(73, 140)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(89, 13)
        Me.Label113.TabIndex = 79
        Me.Label113.Text = "Precio de venta *"
        '
        'TextBox38
        '
        Me.TextBox38.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox38.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox38.Location = New System.Drawing.Point(76, 156)
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New System.Drawing.Size(457, 20)
        Me.TextBox38.TabIndex = 602
        '
        'Label112
        '
        Me.Label112.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label112.AutoSize = True
        Me.Label112.Location = New System.Drawing.Point(73, 100)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(97, 13)
        Me.Label112.TabIndex = 77
        Me.Label112.Text = "Precio de compra *"
        '
        'TextBox37
        '
        Me.TextBox37.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox37.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox37.Location = New System.Drawing.Point(76, 116)
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.Size = New System.Drawing.Size(457, 20)
        Me.TextBox37.TabIndex = 601
        '
        'Button34
        '
        Me.Button34.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button34.Location = New System.Drawing.Point(429, 420)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(92, 30)
        Me.Button34.TabIndex = 605
        Me.Button34.Text = "limpiar"
        Me.Button34.UseVisualStyleBackColor = True
        '
        'RichTextBox3
        '
        Me.RichTextBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RichTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RichTextBox3.Location = New System.Drawing.Point(76, 248)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.Size = New System.Drawing.Size(457, 84)
        Me.RichTextBox3.TabIndex = 604
        Me.RichTextBox3.Text = ""
        '
        'Label109
        '
        Me.Label109.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label109.AutoSize = True
        Me.Label109.Location = New System.Drawing.Point(17, 414)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(100, 13)
        Me.Label109.TabIndex = 69
        Me.Label109.Text = "Código en sistema *"
        '
        'cod_inventario
        '
        Me.cod_inventario.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cod_inventario.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.cod_inventario.Location = New System.Drawing.Point(20, 430)
        Me.cod_inventario.Name = "cod_inventario"
        Me.cod_inventario.Size = New System.Drawing.Size(132, 20)
        Me.cod_inventario.TabIndex = 68
        '
        'Button35
        '
        Me.Button35.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button35.Location = New System.Drawing.Point(527, 420)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(95, 30)
        Me.Button35.TabIndex = 607
        Me.Button35.Text = "Guardar"
        Me.Button35.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(73, 230)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(63, 13)
        Me.Label110.TabIndex = 67
        Me.Label110.Text = "Descripción"
        '
        'Label111
        '
        Me.Label111.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label111.AutoSize = True
        Me.Label111.Location = New System.Drawing.Point(73, 54)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(113, 13)
        Me.Label111.TabIndex = 66
        Me.Label111.Text = "Nombre del producto *"
        '
        'TextBox36
        '
        Me.TextBox36.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox36.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox36.Location = New System.Drawing.Point(76, 70)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New System.Drawing.Size(457, 20)
        Me.TextBox36.TabIndex = 600
        '
        'Label108
        '
        Me.Label108.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label108.AutoSize = True
        Me.Label108.Location = New System.Drawing.Point(535, 160)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(72, 13)
        Me.Label108.TabIndex = 613
        Me.Label108.Text = "0% de útilidad"
        '
        'Button39
        '
        Me.Button39.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button39.Image = Global.RNV.My.Resources.Resources._1_04_20
        Me.Button39.Location = New System.Drawing.Point(589, 54)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(34, 34)
        Me.Button39.TabIndex = 615
        Me.Button39.TabStop = False
        Me.Button39.UseVisualStyleBackColor = True
        Me.Button39.Visible = False
        '
        'Button38
        '
        Me.Button38.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button38.Image = Global.RNV.My.Resources.Resources.wooden_box__1_
        Me.Button38.Location = New System.Drawing.Point(589, 54)
        Me.Button38.Name = "Button38"
        Me.Button38.Size = New System.Drawing.Size(34, 34)
        Me.Button38.TabIndex = 612
        Me.Button38.TabStop = False
        Me.Button38.UseVisualStyleBackColor = True
        Me.Button38.Visible = False
        '
        'Button36
        '
        Me.Button36.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button36.Image = Global.RNV.My.Resources.Resources._1_04_20
        Me.Button36.Location = New System.Drawing.Point(589, 91)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(34, 34)
        Me.Button36.TabIndex = 72
        Me.Button36.TabStop = False
        Me.Button36.UseVisualStyleBackColor = True
        Me.Button36.Visible = False
        '
        'Button37
        '
        Me.Button37.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button37.Image = Global.RNV.My.Resources.Resources.if_pencil_173067
        Me.Button37.Location = New System.Drawing.Point(589, 17)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(34, 34)
        Me.Button37.TabIndex = 71
        Me.Button37.TabStop = False
        Me.Button37.UseVisualStyleBackColor = True
        '
        'TabPage17
        '
        Me.TabPage17.Controls.Add(Me.Label120)
        Me.TabPage17.Controls.Add(Me.gridTicket)
        Me.TabPage17.Controls.Add(Me.id_venta)
        Me.TabPage17.Controls.Add(Me.Panel6)
        Me.TabPage17.Controls.Add(Me.Panel7)
        Me.TabPage17.Controls.Add(Me.TextBox45)
        Me.TabPage17.Controls.Add(Me.BunifuSeparator10)
        Me.TabPage17.Controls.Add(Me.Label126)
        Me.TabPage17.Controls.Add(Me.Button42)
        Me.TabPage17.Location = New System.Drawing.Point(4, 22)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage17.Size = New System.Drawing.Size(637, 460)
        Me.TabPage17.TabIndex = 13
        Me.TabPage17.Text = "TabPage17"
        Me.TabPage17.UseVisualStyleBackColor = True
        '
        'Label120
        '
        Me.Label120.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label120.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label120.Location = New System.Drawing.Point(3, 171)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(631, 65)
        Me.Label120.TabIndex = 64
        Me.Label120.Text = "GENERANDO TICKET, POR FAVOR ESPERA..."
        Me.Label120.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label120.Visible = False
        '
        'gridTicket
        '
        Me.gridTicket.AllowUserToAddRows = False
        Me.gridTicket.AllowUserToOrderColumns = True
        Me.gridTicket.AllowUserToResizeColumns = False
        Me.gridTicket.AllowUserToResizeRows = False
        Me.gridTicket.BackgroundColor = System.Drawing.Color.White
        Me.gridTicket.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridTicket.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.descripcion, Me.DataGridViewTextBoxColumn1, Me.precio, Me.importe})
        Me.gridTicket.Location = New System.Drawing.Point(652, 32)
        Me.gridTicket.Name = "gridTicket"
        Me.gridTicket.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridTicket.Size = New System.Drawing.Size(10, 283)
        Me.gridTicket.TabIndex = 63
        '
        'descripcion
        '
        Me.descripcion.FillWeight = 120.0!
        Me.descripcion.HeaderText = "Descripción"
        Me.descripcion.Name = "descripcion"
        Me.descripcion.ReadOnly = True
        Me.descripcion.Width = 120
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'precio
        '
        Me.precio.HeaderText = "Precio"
        Me.precio.Name = "precio"
        '
        'importe
        '
        Me.importe.HeaderText = "Importe"
        Me.importe.Name = "importe"
        Me.importe.ReadOnly = True
        '
        'id_venta
        '
        Me.id_venta.AutoSize = True
        Me.id_venta.Location = New System.Drawing.Point(697, 440)
        Me.id_venta.Name = "id_venta"
        Me.id_venta.Size = New System.Drawing.Size(0, 13)
        Me.id_venta.TabIndex = 61
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label116)
        Me.Panel6.Location = New System.Drawing.Point(16, 156)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(618, 216)
        Me.Panel6.TabIndex = 60
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.ForeColor = System.Drawing.Color.Red
        Me.Label116.Location = New System.Drawing.Point(248, 88)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(114, 13)
        Me.Label116.TabIndex = 0
        Me.Label116.Text = "Aún nada que mostrar."
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.LinkLabel8)
        Me.Panel7.Controls.Add(Me.LinkLabel7)
        Me.Panel7.Controls.Add(Me.TextBox34)
        Me.Panel7.Controls.Add(Me.Label117)
        Me.Panel7.Controls.Add(Me.Label118)
        Me.Panel7.Controls.Add(Me.RichTextBox4)
        Me.Panel7.Controls.Add(Me.Label119)
        Me.Panel7.Controls.Add(Me.TextBox35)
        Me.Panel7.Controls.Add(Me.TextBox40)
        Me.Panel7.Controls.Add(Me.Label121)
        Me.Panel7.Controls.Add(Me.Label122)
        Me.Panel7.Location = New System.Drawing.Point(4, 165)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(628, 207)
        Me.Panel7.TabIndex = 59
        '
        'LinkLabel8
        '
        Me.LinkLabel8.AutoSize = True
        Me.LinkLabel8.Location = New System.Drawing.Point(504, 114)
        Me.LinkLabel8.Name = "LinkLabel8"
        Me.LinkLabel8.Size = New System.Drawing.Size(104, 13)
        Me.LinkLabel8.TabIndex = 123
        Me.LinkLabel8.TabStop = True
        Me.LinkLabel8.Text = "Volver a ver el ticket"
        '
        'LinkLabel7
        '
        Me.LinkLabel7.AutoSize = True
        Me.LinkLabel7.Location = New System.Drawing.Point(389, 114)
        Me.LinkLabel7.Name = "LinkLabel7"
        Me.LinkLabel7.Size = New System.Drawing.Size(111, 13)
        Me.LinkLabel7.TabIndex = 122
        Me.LinkLabel7.TabStop = True
        Me.LinkLabel7.Text = "Ver articulos vendidos"
        '
        'TextBox34
        '
        Me.TextBox34.Location = New System.Drawing.Point(12, 33)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.ReadOnly = True
        Me.TextBox34.Size = New System.Drawing.Size(197, 20)
        Me.TextBox34.TabIndex = 121
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(218, 15)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(60, 13)
        Me.Label117.TabIndex = 70
        Me.Label117.Text = "Comentario"
        '
        'Label118
        '
        Me.Label118.Location = New System.Drawing.Point(579, 433)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(29, 13)
        Me.Label118.TabIndex = 120
        '
        'RichTextBox4
        '
        Me.RichTextBox4.Location = New System.Drawing.Point(221, 32)
        Me.RichTextBox4.Name = "RichTextBox4"
        Me.RichTextBox4.ReadOnly = True
        Me.RichTextBox4.Size = New System.Drawing.Size(387, 79)
        Me.RichTextBox4.TabIndex = 60
        Me.RichTextBox4.Text = ""
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(14, 15)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(71, 13)
        Me.Label119.TabIndex = 56
        Me.Label119.Text = "No. De venta"
        '
        'TextBox35
        '
        Me.TextBox35.Location = New System.Drawing.Point(12, 132)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.ReadOnly = True
        Me.TextBox35.Size = New System.Drawing.Size(197, 20)
        Me.TextBox35.TabIndex = 58
        '
        'TextBox40
        '
        Me.TextBox40.Location = New System.Drawing.Point(12, 82)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.ReadOnly = True
        Me.TextBox40.Size = New System.Drawing.Size(197, 20)
        Me.TextBox40.TabIndex = 57
        '
        'Label121
        '
        Me.Label121.AutoSize = True
        Me.Label121.Location = New System.Drawing.Point(9, 66)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(82, 13)
        Me.Label121.TabIndex = 59
        Me.Label121.Text = "Fecha de venta"
        '
        'Label122
        '
        Me.Label122.AutoSize = True
        Me.Label122.Location = New System.Drawing.Point(9, 115)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(87, 13)
        Me.Label122.TabIndex = 61
        Me.Label122.Text = "Total de la venta"
        '
        'TextBox45
        '
        Me.TextBox45.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TextBox45.Location = New System.Drawing.Point(92, 60)
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.ReadOnly = True
        Me.TextBox45.Size = New System.Drawing.Size(436, 20)
        Me.TextBox45.TabIndex = 58
        '
        'BunifuSeparator10
        '
        Me.BunifuSeparator10.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator10.LineThickness = 1
        Me.BunifuSeparator10.Location = New System.Drawing.Point(50, 129)
        Me.BunifuSeparator10.Name = "BunifuSeparator10"
        Me.BunifuSeparator10.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator10.TabIndex = 57
        Me.BunifuSeparator10.Transparency = 255
        Me.BunifuSeparator10.Vertical = False
        '
        'Label126
        '
        Me.Label126.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label126.AutoSize = True
        Me.Label126.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label126.Location = New System.Drawing.Point(271, 32)
        Me.Label126.Name = "Label126"
        Me.Label126.Size = New System.Drawing.Size(104, 18)
        Me.Label126.TabIndex = 56
        Me.Label126.Text = "No. De venta *"
        '
        'Button42
        '
        Me.Button42.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button42.Location = New System.Drawing.Point(264, 90)
        Me.Button42.Name = "Button42"
        Me.Button42.Size = New System.Drawing.Size(120, 36)
        Me.Button42.TabIndex = 55
        Me.Button42.Text = "Consultar datos"
        Me.Button42.UseVisualStyleBackColor = True
        '
        'TabPage18
        '
        Me.TabPage18.Controls.Add(Me.Panel8)
        Me.TabPage18.Controls.Add(Me.Panel9)
        Me.TabPage18.Controls.Add(Me.TextBox44)
        Me.TabPage18.Controls.Add(Me.BunifuSeparator11)
        Me.TabPage18.Controls.Add(Me.Label134)
        Me.TabPage18.Controls.Add(Me.Button45)
        Me.TabPage18.Location = New System.Drawing.Point(4, 22)
        Me.TabPage18.Name = "TabPage18"
        Me.TabPage18.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage18.Size = New System.Drawing.Size(637, 460)
        Me.TabPage18.TabIndex = 14
        Me.TabPage18.Text = "TabPage18"
        Me.TabPage18.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label128)
        Me.Panel8.Location = New System.Drawing.Point(16, 170)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(613, 264)
        Me.Panel8.TabIndex = 70
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.ForeColor = System.Drawing.Color.Red
        Me.Label128.Location = New System.Drawing.Point(248, 88)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(114, 13)
        Me.Label128.TabIndex = 0
        Me.Label128.Text = "Aún nada que mostrar."
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.stock_actual)
        Me.Panel9.Controls.Add(Me.Label137)
        Me.Panel9.Controls.Add(Me.tipo_de_producto)
        Me.Panel9.Controls.Add(Me.Label136)
        Me.Panel9.Controls.Add(Me.precio_minimo)
        Me.Panel9.Controls.Add(Me.Label135)
        Me.Panel9.Controls.Add(Me.precio_venta)
        Me.Panel9.Controls.Add(Me.Label127)
        Me.Panel9.Controls.Add(Me.LinkLabel9)
        Me.Panel9.Controls.Add(Me.cod_producto)
        Me.Panel9.Controls.Add(Me.Label129)
        Me.Panel9.Controls.Add(Me.Label130)
        Me.Panel9.Controls.Add(Me.descripcion_producto)
        Me.Panel9.Controls.Add(Me.Label131)
        Me.Panel9.Controls.Add(Me.precio_compra)
        Me.Panel9.Controls.Add(Me.nombre_producto)
        Me.Panel9.Controls.Add(Me.Label132)
        Me.Panel9.Controls.Add(Me.Label133)
        Me.Panel9.Location = New System.Drawing.Point(4, 171)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(628, 276)
        Me.Panel9.TabIndex = 69
        '
        'stock_actual
        '
        Me.stock_actual.Location = New System.Drawing.Point(221, 176)
        Me.stock_actual.Name = "stock_actual"
        Me.stock_actual.ReadOnly = True
        Me.stock_actual.Size = New System.Drawing.Size(387, 20)
        Me.stock_actual.TabIndex = 130
        '
        'Label137
        '
        Me.Label137.AutoSize = True
        Me.Label137.Location = New System.Drawing.Point(218, 159)
        Me.Label137.Name = "Label137"
        Me.Label137.Size = New System.Drawing.Size(67, 13)
        Me.Label137.TabIndex = 131
        Me.Label137.Text = "Stock actual"
        '
        'tipo_de_producto
        '
        Me.tipo_de_producto.Location = New System.Drawing.Point(221, 129)
        Me.tipo_de_producto.Name = "tipo_de_producto"
        Me.tipo_de_producto.ReadOnly = True
        Me.tipo_de_producto.Size = New System.Drawing.Size(387, 20)
        Me.tipo_de_producto.TabIndex = 128
        '
        'Label136
        '
        Me.Label136.AutoSize = True
        Me.Label136.Location = New System.Drawing.Point(218, 112)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(88, 13)
        Me.Label136.TabIndex = 129
        Me.Label136.Text = "Tipo de producto"
        '
        'precio_minimo
        '
        Me.precio_minimo.Location = New System.Drawing.Point(12, 224)
        Me.precio_minimo.Name = "precio_minimo"
        Me.precio_minimo.ReadOnly = True
        Me.precio_minimo.Size = New System.Drawing.Size(197, 20)
        Me.precio_minimo.TabIndex = 126
        '
        'Label135
        '
        Me.Label135.AutoSize = True
        Me.Label135.Location = New System.Drawing.Point(9, 207)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(117, 13)
        Me.Label135.TabIndex = 127
        Me.Label135.Text = "Precio minimo de venta"
        '
        'precio_venta
        '
        Me.precio_venta.Location = New System.Drawing.Point(12, 176)
        Me.precio_venta.Name = "precio_venta"
        Me.precio_venta.ReadOnly = True
        Me.precio_venta.Size = New System.Drawing.Size(197, 20)
        Me.precio_venta.TabIndex = 124
        '
        'Label127
        '
        Me.Label127.AutoSize = True
        Me.Label127.Location = New System.Drawing.Point(9, 159)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(67, 13)
        Me.Label127.TabIndex = 125
        Me.Label127.Text = "Precio venta"
        '
        'LinkLabel9
        '
        Me.LinkLabel9.AutoSize = True
        Me.LinkLabel9.Location = New System.Drawing.Point(480, 202)
        Me.LinkLabel9.Name = "LinkLabel9"
        Me.LinkLabel9.Size = New System.Drawing.Size(128, 13)
        Me.LinkLabel9.TabIndex = 123
        Me.LinkLabel9.TabStop = True
        Me.LinkLabel9.Text = "Ver movimientos de stock"
        '
        'cod_producto
        '
        Me.cod_producto.Location = New System.Drawing.Point(12, 33)
        Me.cod_producto.Name = "cod_producto"
        Me.cod_producto.ReadOnly = True
        Me.cod_producto.Size = New System.Drawing.Size(197, 20)
        Me.cod_producto.TabIndex = 121
        '
        'Label129
        '
        Me.Label129.AutoSize = True
        Me.Label129.Location = New System.Drawing.Point(218, 15)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(125, 13)
        Me.Label129.TabIndex = 70
        Me.Label129.Text = "Descripción del producto"
        '
        'Label130
        '
        Me.Label130.Location = New System.Drawing.Point(579, 433)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(29, 13)
        Me.Label130.TabIndex = 120
        '
        'descripcion_producto
        '
        Me.descripcion_producto.Location = New System.Drawing.Point(221, 32)
        Me.descripcion_producto.Name = "descripcion_producto"
        Me.descripcion_producto.ReadOnly = True
        Me.descripcion_producto.Size = New System.Drawing.Size(387, 70)
        Me.descripcion_producto.TabIndex = 60
        Me.descripcion_producto.Text = ""
        '
        'Label131
        '
        Me.Label131.AutoSize = True
        Me.Label131.Location = New System.Drawing.Point(14, 15)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(85, 13)
        Me.Label131.TabIndex = 56
        Me.Label131.Text = "Código producto"
        '
        'precio_compra
        '
        Me.precio_compra.Location = New System.Drawing.Point(12, 129)
        Me.precio_compra.Name = "precio_compra"
        Me.precio_compra.ReadOnly = True
        Me.precio_compra.Size = New System.Drawing.Size(197, 20)
        Me.precio_compra.TabIndex = 58
        '
        'nombre_producto
        '
        Me.nombre_producto.Location = New System.Drawing.Point(12, 82)
        Me.nombre_producto.Name = "nombre_producto"
        Me.nombre_producto.ReadOnly = True
        Me.nombre_producto.Size = New System.Drawing.Size(197, 20)
        Me.nombre_producto.TabIndex = 57
        '
        'Label132
        '
        Me.Label132.AutoSize = True
        Me.Label132.Location = New System.Drawing.Point(9, 66)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(106, 13)
        Me.Label132.TabIndex = 59
        Me.Label132.Text = "Nombre del producto"
        '
        'Label133
        '
        Me.Label133.AutoSize = True
        Me.Label133.Location = New System.Drawing.Point(9, 112)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(75, 13)
        Me.Label133.TabIndex = 61
        Me.Label133.Text = "Precio compra"
        '
        'TextBox44
        '
        Me.TextBox44.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TextBox44.Location = New System.Drawing.Point(92, 66)
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.ReadOnly = True
        Me.TextBox44.Size = New System.Drawing.Size(436, 20)
        Me.TextBox44.TabIndex = 68
        '
        'BunifuSeparator11
        '
        Me.BunifuSeparator11.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator11.LineThickness = 1
        Me.BunifuSeparator11.Location = New System.Drawing.Point(50, 135)
        Me.BunifuSeparator11.Name = "BunifuSeparator11"
        Me.BunifuSeparator11.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator11.TabIndex = 67
        Me.BunifuSeparator11.Transparency = 255
        Me.BunifuSeparator11.Vertical = False
        '
        'Label134
        '
        Me.Label134.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label134.AutoSize = True
        Me.Label134.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label134.Location = New System.Drawing.Point(252, 38)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(149, 18)
        Me.Label134.TabIndex = 66
        Me.Label134.Text = "Código de producto *"
        '
        'Button45
        '
        Me.Button45.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button45.Location = New System.Drawing.Point(264, 96)
        Me.Button45.Name = "Button45"
        Me.Button45.Size = New System.Drawing.Size(120, 36)
        Me.Button45.TabIndex = 65
        Me.Button45.Text = "Consultar datos"
        Me.Button45.UseVisualStyleBackColor = True
        '
        'TabPage19
        '
        Me.TabPage19.Controls.Add(Me.GroupBox5)
        Me.TabPage19.Controls.Add(Me.Button46)
        Me.TabPage19.Controls.Add(Me.Panel10)
        Me.TabPage19.Controls.Add(Me.Panel11)
        Me.TabPage19.Controls.Add(Me.BunifuSeparator12)
        Me.TabPage19.Location = New System.Drawing.Point(4, 22)
        Me.TabPage19.Name = "TabPage19"
        Me.TabPage19.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage19.Size = New System.Drawing.Size(637, 460)
        Me.TabPage19.TabIndex = 16
        Me.TabPage19.Text = "TabPage19"
        Me.TabPage19.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox51)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox5.Controls.Add(Me.CheckBox5)
        Me.GroupBox5.Controls.Add(Me.CheckBox4)
        Me.GroupBox5.Location = New System.Drawing.Point(19, 25)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(596, 84)
        Me.GroupBox5.TabIndex = 77
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Filtros"
        '
        'TextBox51
        '
        Me.TextBox51.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TextBox51.Location = New System.Drawing.Point(11, 51)
        Me.TextBox51.Name = "TextBox51"
        Me.TextBox51.ReadOnly = True
        Me.TextBox51.Size = New System.Drawing.Size(292, 20)
        Me.TextBox51.TabIndex = 48
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(452, 51)
        Me.DateTimePicker1.MinDate = New Date(2015, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(129, 20)
        Me.DateTimePicker1.TabIndex = 17
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(315, 51)
        Me.DateTimePicker2.MinDate = New Date(2015, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(125, 20)
        Me.DateTimePicker2.TabIndex = 16
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(297, 17)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(122, 17)
        Me.CheckBox5.TabIndex = 1
        Me.CheckBox5.Text = "Todos los productos"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Checked = True
        Me.CheckBox4.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox4.Location = New System.Drawing.Point(171, 17)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(116, 17)
        Me.CheckBox4.TabIndex = 0
        Me.CheckBox4.Text = "Producto individual"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'Button46
        '
        Me.Button46.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button46.Location = New System.Drawing.Point(272, 116)
        Me.Button46.Name = "Button46"
        Me.Button46.Size = New System.Drawing.Size(99, 30)
        Me.Button46.TabIndex = 47
        Me.Button46.Text = "Consultar datos"
        Me.Button46.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.Label139)
        Me.Panel10.Location = New System.Drawing.Point(7, 167)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(622, 264)
        Me.Panel10.TabIndex = 76
        '
        'Label139
        '
        Me.Label139.AutoSize = True
        Me.Label139.ForeColor = System.Drawing.Color.Red
        Me.Label139.Location = New System.Drawing.Point(262, 88)
        Me.Label139.Name = "Label139"
        Me.Label139.Size = New System.Drawing.Size(114, 13)
        Me.Label139.TabIndex = 0
        Me.Label139.Text = "Aún nada que mostrar."
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.gridProductos)
        Me.Panel11.Controls.Add(Me.Label145)
        Me.Panel11.Location = New System.Drawing.Point(4, 168)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(628, 276)
        Me.Panel11.TabIndex = 75
        '
        'gridProductos
        '
        Me.gridProductos.AllowUserToAddRows = False
        Me.gridProductos.AllowUserToDeleteRows = False
        Me.gridProductos.AllowUserToOrderColumns = True
        Me.gridProductos.AllowUserToResizeRows = False
        Me.gridProductos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.gridProductos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders
        Me.gridProductos.BackgroundColor = System.Drawing.Color.White
        Me.gridProductos.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridProductos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridProductos.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridProductos.Location = New System.Drawing.Point(2, -1)
        Me.gridProductos.MultiSelect = False
        Me.gridProductos.Name = "gridProductos"
        Me.gridProductos.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridProductos.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridProductos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader
        Me.gridProductos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.gridProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridProductos.ShowCellErrors = False
        Me.gridProductos.ShowCellToolTips = False
        Me.gridProductos.ShowEditingIcon = False
        Me.gridProductos.ShowRowErrors = False
        Me.gridProductos.Size = New System.Drawing.Size(619, 274)
        Me.gridProductos.TabIndex = 121
        '
        'Label145
        '
        Me.Label145.Location = New System.Drawing.Point(579, 433)
        Me.Label145.Name = "Label145"
        Me.Label145.Size = New System.Drawing.Size(29, 13)
        Me.Label145.TabIndex = 120
        '
        'BunifuSeparator12
        '
        Me.BunifuSeparator12.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator12.LineThickness = 1
        Me.BunifuSeparator12.Location = New System.Drawing.Point(50, 136)
        Me.BunifuSeparator12.Name = "BunifuSeparator12"
        Me.BunifuSeparator12.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator12.TabIndex = 73
        Me.BunifuSeparator12.Transparency = 255
        Me.BunifuSeparator12.Vertical = False
        '
        'TabPage20
        '
        Me.TabPage20.AutoScroll = True
        Me.TabPage20.Controls.Add(Me.Button57)
        Me.TabPage20.Controls.Add(Me.Button54)
        Me.TabPage20.Controls.Add(Me.Label153)
        Me.TabPage20.Controls.Add(Me.Label152)
        Me.TabPage20.Controls.Add(Me.Label150)
        Me.TabPage20.Controls.Add(Me.Button51)
        Me.TabPage20.Controls.Add(Me.pc_rnv)
        Me.TabPage20.Controls.Add(Me.Button49)
        Me.TabPage20.Controls.Add(Me.Button50)
        Me.TabPage20.Controls.Add(Me.Label148)
        Me.TabPage20.Controls.Add(Me.Label149)
        Me.TabPage20.Controls.Add(Me.Button53)
        Me.TabPage20.Controls.Add(Me.PictureBox3)
        Me.TabPage20.Controls.Add(Me.BunifuSeparator13)
        Me.TabPage20.Location = New System.Drawing.Point(4, 22)
        Me.TabPage20.Name = "TabPage20"
        Me.TabPage20.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage20.Size = New System.Drawing.Size(637, 460)
        Me.TabPage20.TabIndex = 17
        Me.TabPage20.Text = "TabPage20"
        Me.TabPage20.UseVisualStyleBackColor = True
        '
        'Button57
        '
        Me.Button57.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button57.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button57.Location = New System.Drawing.Point(373, 82)
        Me.Button57.Name = "Button57"
        Me.Button57.Size = New System.Drawing.Size(100, 27)
        Me.Button57.TabIndex = 121
        Me.Button57.TabStop = False
        Me.Button57.Text = "Asignar tiempo"
        Me.Button57.UseMnemonic = False
        Me.Button57.UseVisualStyleBackColor = False
        '
        'Button54
        '
        Me.Button54.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button54.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button54.Location = New System.Drawing.Point(291, 82)
        Me.Button54.Name = "Button54"
        Me.Button54.Size = New System.Drawing.Size(76, 27)
        Me.Button54.TabIndex = 120
        Me.Button54.TabStop = False
        Me.Button54.Text = "Planes"
        Me.Button54.UseMnemonic = False
        Me.Button54.UseVisualStyleBackColor = False
        '
        'Label153
        '
        Me.Label153.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label153.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label153.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label153.ForeColor = System.Drawing.Color.Black
        Me.Label153.Location = New System.Drawing.Point(388, 437)
        Me.Label153.Name = "Label153"
        Me.Label153.Size = New System.Drawing.Size(241, 13)
        Me.Label153.TabIndex = 119
        '
        'Label152
        '
        Me.Label152.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label152.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label152.Location = New System.Drawing.Point(4, 236)
        Me.Label152.Name = "Label152"
        Me.Label152.Size = New System.Drawing.Size(628, 65)
        Me.Label152.TabIndex = 118
        Me.Label152.Text = "CARGANDO DATOS. POR FAVOR, ESPERA..."
        Me.Label152.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label150
        '
        Me.Label150.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label150.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Label150.ForeColor = System.Drawing.Color.Red
        Me.Label150.Location = New System.Drawing.Point(8, 437)
        Me.Label150.Name = "Label150"
        Me.Label150.Size = New System.Drawing.Size(404, 13)
        Me.Label150.TabIndex = 17
        Me.Label150.Text = "Para ver más opciones de la terminal, haz doble click sobre la fila deseada."
        '
        'Button51
        '
        Me.Button51.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button51.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button51.Location = New System.Drawing.Point(479, 82)
        Me.Button51.Name = "Button51"
        Me.Button51.Size = New System.Drawing.Size(76, 27)
        Me.Button51.TabIndex = 117
        Me.Button51.TabStop = False
        Me.Button51.Text = "Ajustes"
        Me.Button51.UseMnemonic = False
        Me.Button51.UseVisualStyleBackColor = False
        '
        'pc_rnv
        '
        Me.pc_rnv.AllowUserToAddRows = False
        Me.pc_rnv.AllowUserToDeleteRows = False
        Me.pc_rnv.AllowUserToResizeColumns = False
        Me.pc_rnv.AllowUserToResizeRows = False
        Me.pc_rnv.BackgroundColor = System.Drawing.Color.White
        Me.pc_rnv.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.pc_rnv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.pc_rnv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.codigo, Me.DataGridViewTextBoxColumn2, Me.ip, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn3})
        Me.pc_rnv.Location = New System.Drawing.Point(9, 128)
        Me.pc_rnv.MultiSelect = False
        Me.pc_rnv.Name = "pc_rnv"
        Me.pc_rnv.RowHeadersVisible = False
        Me.pc_rnv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.pc_rnv.Size = New System.Drawing.Size(618, 300)
        Me.pc_rnv.TabIndex = 116
        '
        'codigo
        '
        Me.codigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.NullValue = Nothing
        Me.codigo.DefaultCellStyle = DataGridViewCellStyle4
        Me.codigo.FillWeight = 70.0!
        Me.codigo.HeaderText = "PC"
        Me.codigo.Name = "codigo"
        Me.codigo.ReadOnly = True
        Me.codigo.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.FillWeight = 130.0!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Tiempo restante"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'ip
        '
        Me.ip.FillWeight = 140.0!
        Me.ip.HeaderText = "Dirección IP"
        Me.ip.Name = "ip"
        Me.ip.ReadOnly = True
        Me.ip.Width = 140
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.FillWeight = 125.0!
        Me.DataGridViewTextBoxColumn4.HeaderText = "Usuario"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 125
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn3.FillWeight = 150.0!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Estado"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'Button49
        '
        Me.Button49.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button49.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button49.Location = New System.Drawing.Point(185, 82)
        Me.Button49.Name = "Button49"
        Me.Button49.Size = New System.Drawing.Size(100, 27)
        Me.Button49.TabIndex = 115
        Me.Button49.TabStop = False
        Me.Button49.Text = "PC / Terminales"
        Me.Button49.UseMnemonic = False
        Me.Button49.UseVisualStyleBackColor = False
        '
        'Button50
        '
        Me.Button50.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button50.Location = New System.Drawing.Point(84, 82)
        Me.Button50.Name = "Button50"
        Me.Button50.Size = New System.Drawing.Size(94, 27)
        Me.Button50.TabIndex = 114
        Me.Button50.TabStop = False
        Me.Button50.Text = "Usuarios"
        Me.Button50.UseMnemonic = False
        Me.Button50.UseVisualStyleBackColor = False
        '
        'Label148
        '
        Me.Label148.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label148.AutoSize = True
        Me.Label148.Location = New System.Drawing.Point(411, 22)
        Me.Label148.Name = "Label148"
        Me.Label148.Size = New System.Drawing.Size(31, 13)
        Me.Label148.TabIndex = 112
        Me.Label148.Text = "1.0.0"
        '
        'Label149
        '
        Me.Label149.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label149.AutoSize = True
        Me.Label149.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label149.Location = New System.Drawing.Point(269, 31)
        Me.Label149.Name = "Label149"
        Me.Label149.Size = New System.Drawing.Size(173, 31)
        Me.Label149.TabIndex = 110
        Me.Label149.Text = "CYBER RNV"
        '
        'Button53
        '
        Me.Button53.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button53.Image = Global.RNV.My.Resources.Resources.view_refresh_24
        Me.Button53.Location = New System.Drawing.Point(589, 75)
        Me.Button53.Name = "Button53"
        Me.Button53.Size = New System.Drawing.Size(36, 34)
        Me.Button53.TabIndex = 18
        Me.Button53.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button53.UseCompatibleTextRendering = True
        Me.Button53.UseMnemonic = False
        Me.Button53.UseVisualStyleBackColor = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.RNV.My.Resources.Resources.Streamline_08_64
        Me.PictureBox3.Location = New System.Drawing.Point(208, 20)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(62, 51)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 111
        Me.PictureBox3.TabStop = False
        '
        'BunifuSeparator13
        '
        Me.BunifuSeparator13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BunifuSeparator13.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator13.LineThickness = 1
        Me.BunifuSeparator13.Location = New System.Drawing.Point(41, 101)
        Me.BunifuSeparator13.Name = "BunifuSeparator13"
        Me.BunifuSeparator13.Size = New System.Drawing.Size(563, 35)
        Me.BunifuSeparator13.TabIndex = 113
        Me.BunifuSeparator13.Transparency = 255
        Me.BunifuSeparator13.Vertical = False
        '
        'TabPage21
        '
        Me.TabPage21.Controls.Add(Me.Panel12)
        Me.TabPage21.Controls.Add(Me.Panel13)
        Me.TabPage21.Controls.Add(Me.TextBox53)
        Me.TabPage21.Controls.Add(Me.BunifuSeparator14)
        Me.TabPage21.Controls.Add(Me.Label164)
        Me.TabPage21.Controls.Add(Me.Button55)
        Me.TabPage21.Location = New System.Drawing.Point(4, 22)
        Me.TabPage21.Name = "TabPage21"
        Me.TabPage21.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage21.Size = New System.Drawing.Size(637, 460)
        Me.TabPage21.TabIndex = 18
        Me.TabPage21.Text = "TabPage21"
        Me.TabPage21.UseVisualStyleBackColor = True
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Label154)
        Me.Panel12.Location = New System.Drawing.Point(619, 158)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(10, 264)
        Me.Panel12.TabIndex = 76
        '
        'Label154
        '
        Me.Label154.AutoSize = True
        Me.Label154.ForeColor = System.Drawing.Color.Red
        Me.Label154.Location = New System.Drawing.Point(248, 88)
        Me.Label154.Name = "Label154"
        Me.Label154.Size = New System.Drawing.Size(114, 13)
        Me.Label154.TabIndex = 0
        Me.Label154.Text = "Aún nada que mostrar."
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Label155)
        Me.Panel13.Controls.Add(Me.Label156)
        Me.Panel13.Controls.Add(Me.TextBox48)
        Me.Panel13.Controls.Add(Me.Label158)
        Me.Panel13.Controls.Add(Me.TextBox49)
        Me.Panel13.Controls.Add(Me.Label161)
        Me.Panel13.Controls.Add(Me.TextBox50)
        Me.Panel13.Controls.Add(Me.TextBox52)
        Me.Panel13.Controls.Add(Me.Label162)
        Me.Panel13.Controls.Add(Me.Label163)
        Me.Panel13.Controls.Add(Me.BunifuSeparator15)
        Me.Panel13.Controls.Add(Me.Label160)
        Me.Panel13.Location = New System.Drawing.Point(4, 159)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(628, 276)
        Me.Panel13.TabIndex = 75
        '
        'Label155
        '
        Me.Label155.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label155.ForeColor = System.Drawing.Color.Red
        Me.Label155.Location = New System.Drawing.Point(81, 207)
        Me.Label155.Name = "Label155"
        Me.Label155.Size = New System.Drawing.Size(432, 22)
        Me.Label155.TabIndex = 131
        Me.Label155.Text = "@mora"
        Me.Label155.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label156
        '
        Me.Label156.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label156.ForeColor = System.Drawing.Color.Red
        Me.Label156.Location = New System.Drawing.Point(81, 86)
        Me.Label156.Name = "Label156"
        Me.Label156.Size = New System.Drawing.Size(432, 22)
        Me.Label156.TabIndex = 130
        Me.Label156.Text = "@mora"
        Me.Label156.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextBox48
        '
        Me.TextBox48.Location = New System.Drawing.Point(316, 173)
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.ReadOnly = True
        Me.TextBox48.Size = New System.Drawing.Size(197, 20)
        Me.TextBox48.TabIndex = 124
        '
        'Label158
        '
        Me.Label158.AutoSize = True
        Me.Label158.Location = New System.Drawing.Point(313, 156)
        Me.Label158.Name = "Label158"
        Me.Label158.Size = New System.Drawing.Size(140, 13)
        Me.Label158.TabIndex = 125
        Me.Label158.Text = "Próximo mes de pago VNET"
        '
        'TextBox49
        '
        Me.TextBox49.Location = New System.Drawing.Point(81, 53)
        Me.TextBox49.Name = "TextBox49"
        Me.TextBox49.ReadOnly = True
        Me.TextBox49.Size = New System.Drawing.Size(197, 20)
        Me.TextBox49.TabIndex = 121
        '
        'Label161
        '
        Me.Label161.AutoSize = True
        Me.Label161.Location = New System.Drawing.Point(83, 35)
        Me.Label161.Name = "Label161"
        Me.Label161.Size = New System.Drawing.Size(115, 13)
        Me.Label161.TabIndex = 56
        Me.Label161.Text = "Último mes pagado NV"
        '
        'TextBox50
        '
        Me.TextBox50.Location = New System.Drawing.Point(81, 173)
        Me.TextBox50.Name = "TextBox50"
        Me.TextBox50.ReadOnly = True
        Me.TextBox50.Size = New System.Drawing.Size(197, 20)
        Me.TextBox50.TabIndex = 58
        '
        'TextBox52
        '
        Me.TextBox52.Location = New System.Drawing.Point(316, 53)
        Me.TextBox52.Name = "TextBox52"
        Me.TextBox52.ReadOnly = True
        Me.TextBox52.Size = New System.Drawing.Size(197, 20)
        Me.TextBox52.TabIndex = 57
        '
        'Label162
        '
        Me.Label162.AutoSize = True
        Me.Label162.Location = New System.Drawing.Point(313, 37)
        Me.Label162.Name = "Label162"
        Me.Label162.Size = New System.Drawing.Size(126, 13)
        Me.Label162.TabIndex = 59
        Me.Label162.Text = "Próximo mes de pago NV"
        '
        'Label163
        '
        Me.Label163.AutoSize = True
        Me.Label163.Location = New System.Drawing.Point(78, 156)
        Me.Label163.Name = "Label163"
        Me.Label163.Size = New System.Drawing.Size(129, 13)
        Me.Label163.TabIndex = 61
        Me.Label163.Text = "Último mes pagado VNET"
        '
        'BunifuSeparator15
        '
        Me.BunifuSeparator15.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator15.LineThickness = 1
        Me.BunifuSeparator15.Location = New System.Drawing.Point(27, 112)
        Me.BunifuSeparator15.Name = "BunifuSeparator15"
        Me.BunifuSeparator15.Size = New System.Drawing.Size(563, 35)
        Me.BunifuSeparator15.TabIndex = 77
        Me.BunifuSeparator15.Transparency = 255
        Me.BunifuSeparator15.Vertical = False
        '
        'Label160
        '
        Me.Label160.Location = New System.Drawing.Point(579, 433)
        Me.Label160.Name = "Label160"
        Me.Label160.Size = New System.Drawing.Size(29, 13)
        Me.Label160.TabIndex = 120
        '
        'TextBox53
        '
        Me.TextBox53.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TextBox53.Location = New System.Drawing.Point(97, 54)
        Me.TextBox53.Name = "TextBox53"
        Me.TextBox53.ReadOnly = True
        Me.TextBox53.Size = New System.Drawing.Size(436, 20)
        Me.TextBox53.TabIndex = 74
        '
        'BunifuSeparator14
        '
        Me.BunifuSeparator14.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BunifuSeparator14.LineThickness = 1
        Me.BunifuSeparator14.Location = New System.Drawing.Point(50, 123)
        Me.BunifuSeparator14.Name = "BunifuSeparator14"
        Me.BunifuSeparator14.Size = New System.Drawing.Size(539, 35)
        Me.BunifuSeparator14.TabIndex = 73
        Me.BunifuSeparator14.Transparency = 255
        Me.BunifuSeparator14.Vertical = False
        '
        'Label164
        '
        Me.Label164.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label164.AutoSize = True
        Me.Label164.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.Label164.Location = New System.Drawing.Point(266, 28)
        Me.Label164.Name = "Label164"
        Me.Label164.Size = New System.Drawing.Size(125, 18)
        Me.Label164.TabIndex = 72
        Me.Label164.Text = "Alumno / Cliente *"
        '
        'Button55
        '
        Me.Button55.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button55.Location = New System.Drawing.Point(264, 84)
        Me.Button55.Name = "Button55"
        Me.Button55.Size = New System.Drawing.Size(120, 36)
        Me.Button55.TabIndex = 71
        Me.Button55.Text = "Consultar datos"
        Me.Button55.UseVisualStyleBackColor = True
        '
        'TabPage22
        '
        Me.TabPage22.Location = New System.Drawing.Point(4, 22)
        Me.TabPage22.Name = "TabPage22"
        Me.TabPage22.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage22.Size = New System.Drawing.Size(637, 460)
        Me.TabPage22.TabIndex = 19
        Me.TabPage22.Text = "TabPage22"
        Me.TabPage22.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.Label6.Location = New System.Drawing.Point(232, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(533, 391)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Bienvenido a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Proyectos Nueva Visión."
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'openFD
        '
        Me.openFD.FileName = "OpenFileDialog1"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Timer2
        '
        Me.Timer2.Interval = 1
        '
        'Articulos1
        '
        Me.Articulos1.DataSetName = "articulos"
        Me.Articulos1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Tag = "RNV  v1.7.0"
        Me.NotifyIcon1.Text = "RNV  v1.7.0"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbrirRNVToolStripMenuItem, Me.ToolStripMenuItem2, Me.SincronizarYCerrarToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(175, 54)
        '
        'AbrirRNVToolStripMenuItem
        '
        Me.AbrirRNVToolStripMenuItem.Name = "AbrirRNVToolStripMenuItem"
        Me.AbrirRNVToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.AbrirRNVToolStripMenuItem.Text = "Abrir RNV"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(171, 6)
        '
        'SincronizarYCerrarToolStripMenuItem
        '
        Me.SincronizarYCerrarToolStripMenuItem.Name = "SincronizarYCerrarToolStripMenuItem"
        Me.SincronizarYCerrarToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.SincronizarYCerrarToolStripMenuItem.Text = "Sincronizar y cerrar"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.RNV.My.Resources.Resources.small_1111_59ca7c7f9f2d1
        Me.PictureBox2.Location = New System.Drawing.Point(448, 38)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(85, 105)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 4
        Me.PictureBox2.TabStop = False
        '
        'ComboBox5
        '
        Me.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.ComboBox5.Location = New System.Drawing.Point(16, 392)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(257, 21)
        Me.ComboBox5.TabIndex = 78
        '
        'Label166
        '
        Me.Label166.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label166.AutoSize = True
        Me.Label166.Location = New System.Drawing.Point(13, 376)
        Me.Label166.Name = "Label166"
        Me.Label166.Size = New System.Drawing.Size(85, 13)
        Me.Label166.TabIndex = 79
        Me.Label166.Text = "Último mes pago"
        '
        'EpersonascobroBindingSource
        '
        Me.EpersonascobroBindingSource.DataSource = GetType(RNV.Epersonas_cobro)
        '
        'view_principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(813, 451)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.PictureBox2)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "view_principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RNV | Proyectos Nueva Visión"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.groupCobro.ResumeLayout(False)
        Me.groupCobro.PerformLayout()
        CType(Me.cantidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.tabpage2.ResumeLayout(False)
        Me.tabpage2.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.GroupBox24.ResumeLayout(False)
        Me.GroupBox24.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox22.ResumeLayout(False)
        Me.GroupBox22.PerformLayout()
        Me.GroupBox21.ResumeLayout(False)
        Me.GroupBox21.PerformLayout()
        CType(Me.maxdias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage8.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.BunifuImageButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.TabPage11.ResumeLayout(False)
        Me.TabPage11.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage12.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TabPage13.ResumeLayout(False)
        Me.TabPage13.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.TabPage14.ResumeLayout(False)
        Me.TabPage14.PerformLayout()
        Me.GroupBox26.ResumeLayout(False)
        Me.GroupBox26.PerformLayout()
        Me.GroupBox25.ResumeLayout(False)
        Me.GroupBox25.PerformLayout()
        Me.GroupBox20.ResumeLayout(False)
        Me.GroupBox20.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage15.ResumeLayout(False)
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        Me.TabPage16.ResumeLayout(False)
        Me.TabPage16.PerformLayout()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        CType(Me.gridTicket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.TabPage18.ResumeLayout(False)
        Me.TabPage18.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.TabPage19.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        CType(Me.gridProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage20.ResumeLayout(False)
        Me.TabPage20.PerformLayout()
        CType(Me.pc_rnv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage21.ResumeLayout(False)
        Me.TabPage21.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        CType(Me.Articulos1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EpersonascobroBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents cbox_mes As ComboBox
    Friend WithEvents cbox_cobro_curso As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_client As TextBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Button5 As Button
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents btnGuardar As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents txt_tel3 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txt_cod As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txt_direccion As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_tel2 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txt_tel1 As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_madres As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_padres As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_nombres As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Button6 As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txt_id As TextBox
    Friend WithEvents tabpage2 As TabPage
    Friend WithEvents txt_desc As RichTextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txt_precio As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txt_cod_curso As TextBox
    Friend WithEvents btnGuardar_curso As Button
    Friend WithEvents Label24 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents txt_curso As TextBox
    Friend WithEvents groupCobro As GroupBox
    Friend WithEvents cantidad As NumericUpDown
    Friend WithEvents txtpagar As TextBox
    Friend WithEvents cobro_precio As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents viewcar As LinkLabel
    Friend WithEvents btnAdd As Button
    Friend WithEvents txt_descripcion As Label
    Friend WithEvents txt_codigo As Label
    Friend WithEvents txtdireccion As Label
    Friend WithEvents idx As Label
    Friend WithEvents openFD As OpenFileDialog
    Friend WithEvents btnEdit_curso As Button
    Friend WithEvents btn_delete_curso As Button
    Friend WithEvents btnDelete_al As Button
    Friend WithEvents btn_editar_alum As Button
    Friend WithEvents idMaster As Label
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents reasig As LinkLabel
    Friend WithEvents txt_realID As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents cbox_servi As ComboBox
    Friend WithEvents Button11 As Button
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Button12 As Button
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents Button14 As Button
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents cañoe As ComboBox
    Friend WithEvents cmese As ComboBox
    Friend WithEvents lblañomes As Label
    Friend WithEvents c_año As ComboBox
    Friend WithEvents lblaño As Label
    Friend WithEvents c_mes As ComboBox
    Friend WithEvents lblmes As Label
    Friend WithEvents lblfechafecha As Label
    Friend WithEvents lblfecha As Label
    Friend WithEvents datafecha2 As DateTimePicker
    Friend WithEvents datafecha1 As DateTimePicker
    Friend WithEvents datafecha As DateTimePicker
    Friend WithEvents c_user As ComboBox
    Friend WithEvents Label41 As Label
    Friend WithEvents cbox_fast As ComboBox
    Friend WithEvents Label42 As Label
    Friend WithEvents reporte_para As ComboBox
    Friend WithEvents Label40 As Label
    Friend WithEvents Button13 As Button
    Friend WithEvents Label43 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents texto_consulta As Label
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label45 As Label
    Friend WithEvents Button15 As Button
    Friend WithEvents Label46 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Button16 As Button
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents Label50 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents BunifuSeparator1 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label51 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Button19 As Button
    Friend WithEvents Label25 As Label
    Friend WithEvents codiestu As Label
    Friend WithEvents comentario As RichTextBox
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents Timer1 As Timer
    Friend WithEvents lupdate As Label
    Friend WithEvents Label55 As Label
    Friend WithEvents Button18 As Button
    Friend WithEvents Button17 As Button
    Friend WithEvents BunifuImageButton1 As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents Button21 As Button
    Friend WithEvents BunifuSeparator2 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents txt_insc As MaskedTextBox
    Friend WithEvents txt_nac As MaskedTextBox
    Friend WithEvents Button22 As Button
    Friend WithEvents TabPage9 As TabPage
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents Button23 As Button
    Friend WithEvents TabPage10 As TabPage
    Friend WithEvents TabPage11 As TabPage
    Friend WithEvents GroupBox12 As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label57 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Label64 As Label
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Label62 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents Label59 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Label60 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label58 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents BunifuSeparator3 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label56 As Label
    Friend WithEvents Button24 As Button
    Friend WithEvents MaskedTextBox1 As MaskedTextBox
    Friend WithEvents Label65 As Label
    Friend WithEvents Button26 As Button
    Friend WithEvents Button25 As Button
    Friend WithEvents Label66 As Label
    Friend WithEvents mostrarpagos As LinkLabel
    Friend WithEvents jtid As TextBox
    Friend WithEvents TabPage12 As TabPage
    Friend WithEvents codigorecorte As TextBox
    Friend WithEvents BunifuSeparator4 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label76 As Label
    Friend WithEvents Button27 As Button
    Friend WithEvents TextBox14 As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label67 As Label
    Friend WithEvents Label68 As Label
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents TextBox12 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents TextBox15 As TextBox
    Friend WithEvents TextBox16 As TextBox
    Friend WithEvents Label79 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents BunifuSeparator5 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox24 As TextBox
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents Label78 As Label
    Friend WithEvents TextBox11 As TextBox
    Friend WithEvents Label77 As Label
    Friend WithEvents TextBox17 As TextBox
    Friend WithEvents Label75 As Label
    Friend WithEvents TextBox18 As TextBox
    Friend WithEvents Label74 As Label
    Friend WithEvents Label70 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents TextBox19 As TextBox
    Friend WithEvents Label72 As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents TextBox23 As TextBox
    Friend WithEvents TextBox20 As TextBox
    Friend WithEvents TextBox22 As TextBox
    Friend WithEvents TextBox21 As TextBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label80 As Label
    Friend WithEvents TextBox13 As TextBox
    Friend WithEvents TextBox25 As TextBox
    Friend WithEvents TextBox26 As TextBox
    Friend WithEvents Label81 As Label
    Friend WithEvents BunifuSeparator6 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents TabPage13 As TabPage
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label82 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label84 As Label
    Friend WithEvents TextBox46 As TextBox
    Friend WithEvents BunifuSeparator9 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label96 As Label
    Friend WithEvents Button28 As Button
    Friend WithEvents TextBox33 As TextBox
    Friend WithEvents Label83 As Label
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents Label91 As Label
    Friend WithEvents TextBox27 As TextBox
    Friend WithEvents TextBox32 As TextBox
    Friend WithEvents TextBox28 As TextBox
    Friend WithEvents Label85 As Label
    Friend WithEvents Label90 As Label
    Friend WithEvents TextBox29 As TextBox
    Friend WithEvents Label89 As Label
    Friend WithEvents Label86 As Label
    Friend WithEvents Label88 As Label
    Friend WithEvents TextBox30 As TextBox
    Friend WithEvents TextBox31 As TextBox
    Friend WithEvents Label87 As Label
    Friend WithEvents TabPage14 As TabPage
    Friend WithEvents EpersonascobroBindingSource As BindingSource
    Friend WithEvents Chart1 As DataVisualization.Charting.Chart
    Friend WithEvents BunifuSeparator7 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents GroupBox18 As GroupBox
    Friend WithEvents LinkLabel4 As LinkLabel
    Friend WithEvents GroupBox17 As GroupBox
    Friend WithEvents LinkLabel3 As LinkLabel
    Friend WithEvents GroupBox16 As GroupBox
    Friend WithEvents LinkLabel2 As LinkLabel
    Friend WithEvents GroupBox14 As GroupBox
    Friend WithEvents Label97 As Label
    Friend WithEvents Label98 As Label
    Friend WithEvents GroupBox15 As GroupBox
    Friend WithEvents Label94 As Label
    Friend WithEvents Label95 As Label
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents Label93 As Label
    Friend WithEvents Label92 As Label
    Friend WithEvents Button29 As Button
    Friend WithEvents Button30 As Button
    Friend WithEvents lcode As Label
    Friend WithEvents TabPage15 As TabPage
    Friend WithEvents ComboBox3 As ComboBox
    Friend WithEvents Label101 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents Label100 As Label
    Friend WithEvents Button31 As Button
    Friend WithEvents Label99 As Label
    Friend WithEvents GroupBox19 As GroupBox
    Friend WithEvents GroupBox20 As GroupBox
    Friend WithEvents LinkLabel5 As LinkLabel
    Friend WithEvents Timer2 As Timer
    Friend WithEvents Button33 As Button
    Friend WithEvents BunifuSeparator8 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents TabPage16 As TabPage
    Friend WithEvents Label114 As Label
    Friend WithEvents TextBox39 As TextBox
    Friend WithEvents Label113 As Label
    Friend WithEvents TextBox38 As TextBox
    Friend WithEvents Label112 As Label
    Friend WithEvents TextBox37 As TextBox
    Friend WithEvents Button34 As Button
    Friend WithEvents RichTextBox3 As RichTextBox
    Friend WithEvents Label109 As Label
    Friend WithEvents cod_inventario As TextBox
    Friend WithEvents Button35 As Button
    Friend WithEvents Label110 As Label
    Friend WithEvents Label111 As Label
    Friend WithEvents TextBox36 As TextBox
    Friend WithEvents Button36 As Button
    Friend WithEvents Button37 As Button
    Friend WithEvents Label36 As Label
    Friend WithEvents cbox_select As ComboBox
    Friend WithEvents Label106 As Label
    Friend WithEvents CheckBox3 As CheckBox
    Friend WithEvents id_inventario As Label
    Friend WithEvents Button38 As Button
    Friend WithEvents Label115 As Label
    Friend WithEvents Label108 As Label
    Friend WithEvents Label102 As Label
    Friend WithEvents Button39 As Button
    Friend WithEvents LinkLabel6 As LinkLabel
    Friend WithEvents preminimo As Label
    Friend WithEvents prenormal As Label
    Friend WithEvents tipo_producto As Label
    Friend WithEvents Button40 As Button
    Friend WithEvents Button41 As Button
    Friend WithEvents TabPage17 As TabPage
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label116 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents LinkLabel7 As LinkLabel
    Friend WithEvents TextBox34 As TextBox
    Friend WithEvents Label117 As Label
    Friend WithEvents Label118 As Label
    Friend WithEvents RichTextBox4 As RichTextBox
    Friend WithEvents Label119 As Label
    Friend WithEvents TextBox35 As TextBox
    Friend WithEvents TextBox40 As TextBox
    Friend WithEvents Label121 As Label
    Friend WithEvents Label122 As Label
    Friend WithEvents TextBox45 As TextBox
    Friend WithEvents BunifuSeparator10 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label126 As Label
    Friend WithEvents Button42 As Button
    Friend WithEvents id_venta As Label
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents LinkLabel8 As LinkLabel
    Friend WithEvents gridTicket As DataGridView
    Friend WithEvents descripcion As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents precio As DataGridViewTextBoxColumn
    Friend WithEvents importe As DataGridViewTextBoxColumn
    Friend WithEvents Label120 As Label
    Friend WithEvents Button43 As Button
    Friend WithEvents Label123 As Label
    Friend WithEvents txt_correo As TextBox
    Friend WithEvents Label125 As Label
    Friend WithEvents micorreo As Label
    Friend WithEvents TabPage18 As TabPage
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Label128 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents LinkLabel9 As LinkLabel
    Friend WithEvents cod_producto As TextBox
    Friend WithEvents Label129 As Label
    Friend WithEvents Label130 As Label
    Friend WithEvents descripcion_producto As RichTextBox
    Friend WithEvents Label131 As Label
    Friend WithEvents precio_compra As TextBox
    Friend WithEvents nombre_producto As TextBox
    Friend WithEvents Label132 As Label
    Friend WithEvents Label133 As Label
    Friend WithEvents TextBox44 As TextBox
    Friend WithEvents BunifuSeparator11 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label134 As Label
    Friend WithEvents Button45 As Button
    Friend WithEvents stock_actual As TextBox
    Friend WithEvents Label137 As Label
    Friend WithEvents tipo_de_producto As TextBox
    Friend WithEvents Label136 As Label
    Friend WithEvents precio_minimo As TextBox
    Friend WithEvents Label135 As Label
    Friend WithEvents precio_venta As TextBox
    Friend WithEvents Label127 As Label
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents GroupBox22 As GroupBox
    Friend WithEvents Label124 As Label
    Friend WithEvents Button44 As Button
    Friend WithEvents GroupBox21 As GroupBox
    Friend WithEvents frontal As CheckBox
    Friend WithEvents Label104 As Label
    Friend WithEvents maxdias As NumericUpDown
    Friend WithEvents Label103 As Label
    Friend WithEvents Button32 As Button
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents btnGuardarProtected As Button
    Friend WithEvents Label52 As Label
    Friend WithEvents txt_protected2 As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents txt_protected As TextBox
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents Button20 As Button
    Friend WithEvents NumericUpDown1 As NumericUpDown
    Friend WithEvents Label54 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents btnGuardar_pass As Button
    Friend WithEvents Label37 As Label
    Friend WithEvents txtpass2 As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents txtpass As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnGuardar_anv As Button
    Friend WithEvents Label31 As Label
    Friend WithEvents txt_facebook_vnet As TextBox
    Friend WithEvents Button8 As Button
    Friend WithEvents Label32 As Label
    Friend WithEvents txt_logo_vnet As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents txt_slogan_vnet As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents txt_titulo_vnet As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btn_Guardar_vnet As Button
    Friend WithEvents Label29 As Label
    Friend WithEvents txt_facebook_nv As TextBox
    Friend WithEvents Button7 As Button
    Friend WithEvents Label28 As Label
    Friend WithEvents txt_logo_nv As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents txt_slogan_nv As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents txt_titulo_nv As TextBox
    Friend WithEvents txtsafe1 As Label
    Friend WithEvents txtsafe2 As Label
    Friend WithEvents Label138 As Label
    Friend WithEvents txt_cuerpo_email As RichTextBox
    Friend WithEvents TabPage19 As TabPage
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents CheckBox5 As CheckBox
    Friend WithEvents CheckBox4 As CheckBox
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Label139 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Label145 As Label
    Friend WithEvents BunifuSeparator12 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents TextBox51 As TextBox
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents DateTimePicker2 As DateTimePicker
    Friend WithEvents Button46 As Button
    Friend WithEvents gridProductos As DataGridView
    Friend WithEvents GroupBox23 As GroupBox
    Friend WithEvents Label146 As Label
    Friend WithEvents TextBox41 As TextBox
    Friend WithEvents NumericUpDown3 As NumericUpDown
    Friend WithEvents Label144 As Label
    Friend WithEvents NumericUpDown2 As NumericUpDown
    Friend WithEvents Label143 As Label
    Friend WithEvents CheckBox8 As CheckBox
    Friend WithEvents CheckBox7 As CheckBox
    Friend WithEvents Label141 As Label
    Friend WithEvents CheckBox6 As CheckBox
    Friend WithEvents Label140 As Label
    Friend WithEvents Button47 As Button
    Friend WithEvents Label147 As Label
    Friend WithEvents LinkLabel10 As LinkLabel
    Friend WithEvents Timer3 As Timer
    Friend WithEvents refreshPC As Timer
    Friend WithEvents Button48 As Button
    Friend WithEvents TabPage20 As TabPage
    Friend WithEvents Button49 As Button
    Friend WithEvents Button50 As Button
    Friend WithEvents BunifuSeparator13 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label148 As Label
    Friend WithEvents Label149 As Label
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents pc_rnv As DataGridView
    Friend WithEvents Button51 As Button
    Friend WithEvents Button52 As Button
    Friend WithEvents Label151 As Label
    Friend WithEvents Label150 As Label
    Friend WithEvents Label152 As Label
    Friend WithEvents Button53 As Button
    Friend WithEvents Label153 As Label
    Friend WithEvents Button54 As Button
    Friend WithEvents codigo As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents ip As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents TabPage21 As TabPage
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Label154 As Label
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Label155 As Label
    Friend WithEvents Label156 As Label
    Friend WithEvents TextBox48 As TextBox
    Friend WithEvents Label158 As Label
    Friend WithEvents TextBox49 As TextBox
    Friend WithEvents Label161 As Label
    Friend WithEvents TextBox50 As TextBox
    Friend WithEvents TextBox52 As TextBox
    Friend WithEvents Label162 As Label
    Friend WithEvents Label163 As Label
    Friend WithEvents BunifuSeparator15 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label160 As Label
    Friend WithEvents TextBox53 As TextBox
    Friend WithEvents BunifuSeparator14 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents Label164 As Label
    Friend WithEvents Button55 As Button
    Friend WithEvents Articulos1 As articulos
    Friend WithEvents GroupBox24 As GroupBox
    Friend WithEvents Label105 As Label
    Friend WithEvents Button56 As Button
    Friend WithEvents TextBox42 As TextBox
    Friend WithEvents Label107 As Label
    Friend WithEvents Label142 As Label
    Friend WithEvents LinkLabel12 As LinkLabel
    Friend WithEvents ciclo_alumnos As Label
    Friend WithEvents LinkLabel13 As LinkLabel
    Friend WithEvents ciclo_cursos As Label
    Friend WithEvents Label157 As Label
    Friend WithEvents txt_ciclo_curso As TextBox
    Friend WithEvents Label159 As Label
    Friend WithEvents txt_ciclo_alumno As TextBox
    Friend WithEvents LinkLabel11 As LinkLabel
    Friend WithEvents ComboBox4 As ComboBox
    Friend WithEvents GroupBox25 As GroupBox
    Friend WithEvents Label165 As Label
    Friend WithEvents tprod As Label
    Friend WithEvents Label167 As Label
    Friend WithEvents GroupBox26 As GroupBox
    Friend WithEvents LinkLabel14 As LinkLabel
    Friend WithEvents Button57 As Button
    Friend WithEvents TabPage22 As TabPage
    Friend WithEvents NotifyIcon1 As NotifyIcon
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents AbrirRNVToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents SincronizarYCerrarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label166 As Label
    Friend WithEvents ComboBox5 As ComboBox
End Class
