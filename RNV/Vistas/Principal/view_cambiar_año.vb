﻿Imports System.ComponentModel
Imports System.Text.RegularExpressions

Public Class view_cambiar_año
    Private Sub view_cambiar_año_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        main.Enabled = True
    End Sub

    Private Sub view_cambiar_año_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Text = main.actual_year
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text <> "" Then
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                main.actual_year = TextBox1.Text
                Me.Close()
            End If
        Else
            MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object,
                            ByVal e As KeyPressEventArgs) Handles TextBox1.KeyPress

        Dim re As New Regex("[^0-9_\-\b]", RegexOptions.IgnoreCase)
        e.Handled = re.IsMatch(e.KeyChar)

    End Sub
End Class