﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Text.RegularExpressions
Imports coreRNV
Imports ZXing
Imports System.Net.Sockets

Public Class view_principal
    '* Año actual -> el usuario puede cambiarlo.
    Public actual_year = Today.Year()
    '* Instancia clase de conexión
    Dim s As New cConexion

    Public Sub ShakeControl(obj As Control, time As Integer, radius As Integer, Optional interval As Integer = 100)
        Dim st As New ShakeControlST
        st.obj = obj
        st.radius = radius
        st.originalpos = obj.Location
        st.tooriginpos = False
        st.shaketime = time
        st.t = New Timer
        st.t.Interval = interval

        st.t.Tag = st
        AddHandler st.t.Tick, AddressOf sTheShake
        st.t.Start()
    End Sub

    Public Class ShakeControlST
        Public t As Timer
        Public obj As Control
        Public shaketime As Integer
        Public shaketimeelapsed As Integer
        Public radius As Integer
        Public originalpos As Point
        Public tooriginpos As Boolean
    End Class

    Public Sub sTheShake(sender As Object, e As System.EventArgs)
        Dim struc As ShakeControlST = CType(CType(sender, Timer).Tag, ShakeControlST)
        If struc.shaketimeelapsed < struc.shaketime Then
            struc.shaketimeelapsed = struc.shaketimeelapsed + 1
            Dim rnd As New Random
            If struc.tooriginpos = False Then
                struc.tooriginpos = True
                struc.obj.Location = New Point(struc.obj.Location.X + rnd.Next(-struc.radius, struc.radius), struc.obj.Location.Y + rnd.Next(-struc.radius, struc.radius))
            Else
                struc.tooriginpos = False
                struc.obj.Location = struc.originalpos
            End If
        Else
            struc.obj.Location = struc.originalpos
            struc.t.Stop()
            struc = Nothing
        End If
    End Sub

    Function leerRNV()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "current.rnv")
        Return fileReader
    End Function

    Function leerSync()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "ifsync.rnv")
        Return fileReader
    End Function

    Function leerupdate()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(s.rutaSistema & "lastupdate.rnv")
        Return fileReader
    End Function

    Public Sub verificarTimer()
        If Timer1.Enabled <> True Then
            Timer1.Start()
            File.WriteAllText(s.rutaSistema & "ifsync.rnv", "true")
        End If
    End Sub

    Public Sub inicializarCobro()
        Label5.Text = functions_cobros.numero_de_comprobante
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 0
        cbox_cobro_curso.Items.Clear()
        cbox_mes.Items.Clear()
        cbox_select.SelectedIndex = 0
        cbox_select.Enabled = False
        comentario.Text = ""
        comentario.Visible = False
        cbox_cobro_curso.Enabled = False
        cbox_mes.Enabled = False
        groupCobro.Enabled = False
        txt_precio.Text = "0"
        txt_client.Text = ""
        txtpagar.Text = "0"
        cantidad.Value = 1
        view_carrito.dt_productos.Rows.Clear()
        view_carrito.Close()
        viewcar.Text = "Ver carrito (0)"
    End Sub

    Private Sub view_principal_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Set optimized drawing settings
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.SetStyle(ControlStyles.UserPaint, True)
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        Me.BringToFront()
        Timer3.Start()
        verificarNotas()
        refreshPC.Start()
        Timer2.Enabled = True
        cargarAjustes()
        verificarNotas()
        If frontal.Checked = True Then
            view_notas.Show()
        End If

        If leerupdate() = "" Then
            lupdate.Text = "n/a"
        Else
            lupdate.Text = leerupdate()
        End If
        ''****
        GetPagos()
        total_ventas()
        total_alumnos()
        total_cursos()
        total_inventario()
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 10
    End Sub

    Public Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        inicializarCobro()
    End Sub

    Private Sub view_principal_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If Timer1.Enabled = True Then
            func_backupBDD()
            If leerSync() = "true" Then
                MsgBox("Cambios detectados: Aún no has realizado la Sincronización. RNV intentará comenzar con el proceso.", vbInformation)
                Call Button13_Click(sender, e)
                Call Button16_Click(sender, e)
            End If
        End If
        Try
            My.Computer.FileSystem.DeleteFile(s.rutaArchivos & "\helper_ticket_NV")
        Catch ex As Exception
        End Try
        view_notas.Close()
        frmLoginNODB.Show()
        frmLoginNODB.BringToFront()
        e.Cancel = True
        Me.Hide()
        'liberar()
    End Sub

    Private Sub TextBox1_Click(sender As Object, e As EventArgs) Handles txt_client.Click
        Me.Enabled = False
        'Me.WindowState = FormWindowState.Maximized
        view_buscar.Show()
        'view_buscar.BringToFront()

    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        limpiarAlumno()
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 1
        '' setear codigo
        txt_id.Text = cadena_aleatoria(10)
        FlowLayoutPanel1.Controls.Clear()
        view_carrito.Close()
        viewcar.Text = "Ver carrito (0)"
        '' cargar // cursos
        'carga_cursos_manage()
    End Sub

    Private Sub limpiarAlumno()
        txt_nombres.Text = ""
        txt_cod.Text = ""
        txt_padres.Text = ""
        txt_madres.Text = ""
        txt_tel1.Text = ""
        txt_tel2.Text = ""
        txt_nac.Text = ""
        ComboBox5.SelectedIndex = 0
        txt_insc.Text = ""
        txt_tel3.Text = ""
        txt_correo.Text = ""
        txt_direccion.Text = ""
        txt_ciclo_alumno.Text = actual_year
        ComboBox4.SelectedIndex = 0
        FlowLayoutPanel1.Controls.Clear()
        ComboBox4.Enabled = True
        '' cargar // cursos
        'carga_cursos_manage()
        txt_id.Text = cadena_aleatoria(10)
        btnDelete_al.Visible = False
        reasig.Visible = False
    End Sub

    Private Sub limpiarCurso()
        txt_curso.Text = ""
        txt_desc.Text = ""
        txt_precio.Text = ""
        txt_cod_curso.Text = cadena_aleatoria(10)
        txt_ciclo_curso.Text = actual_year
        btn_delete_curso.Visible = False
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim mChck As Boolean = False
            For Each c As Control In FlowLayoutPanel1.Controls
                Dim chkbox As CheckBox = TryCast(c, CheckBox)
                If chkbox IsNot Nothing AndAlso chkbox.Checked = True Then
                    mChck = True
                    Exit For
                End If
            Next
            If mChck = False Then
                MsgBox("Debes seleccionar al menos 1 curso / servicio.", vbExclamation, "RNV")
            Else
                If (txt_nombres.Text <> "" Or txt_tel1.Text <> "") Then
                    If (salvar_alumno(txt_cod.Text, txt_nombres.Text, txt_tel1.Text, txt_tel2.Text, txt_tel3.Text, txt_padres.Text, txt_madres.Text, txt_nac.Text, txt_insc.Text, txt_id.Text, txt_direccion.Text, txt_correo.Text, txt_ciclo_alumno.Text) = True) Then
                        MsgBox("Registro completado.", vbInformation, "RNV")
                        limpiarAlumno()
                    Else
                        MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                    End If
                Else
                    MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
                End If
            End If

        End If
    End Sub

    Private Sub btnGuardar_curso_Click(sender As Object, e As EventArgs) Handles btnGuardar_curso.Click

        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If cbox_servi.Text <> "-Selecciona cobro para-" Then
                If (txt_curso.Text <> "" Or txt_precio.Text <> "") Then
                    If (salvar_curso(txt_curso.Text, txt_desc.Text, txt_precio.Text, txt_cod_curso.Text, cbox_servi.Text, txt_ciclo_curso.Text) = True) Then
                        MsgBox("Registro completado.", vbInformation, "RNV")
                        limpiarCurso()
                    Else
                        MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                    End If
                Else
                    MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
                End If
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If

        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        limpiarCurso()
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 2
        cbox_servi.SelectedIndex = 0
        view_carrito.Close()
        viewcar.Text = "Ver carrito (0)"
        '' setear codigo
        txt_cod_curso.Text = cadena_aleatoria(10)
    End Sub

    Private Sub cbox_cobro_curso_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbox_cobro_curso.SelectedIndexChanged
        If cbox_cobro_curso.Text <> "-ninguno-" Then
            Dim ff As String = cbox_cobro_curso.Text
            Dim id_ben As String
            id_ben = Microsoft.VisualBasic.Right(ff, 9)
            Dim id_curso = Convert.ToInt32(Microsoft.VisualBasic.Right(ff, 9))
            cargarCursoData(id_curso)
            groupCobro.Enabled = True
            Try
                If (CheckBox6.Checked = True) Then
                    Dim tipo = ""
                    If CheckBox7.Checked = True Then
                        tipo = "fijo"
                    Else
                        tipo = "diario"
                    End If
                    detectarMora(idMaster.Text, cbox_select.Text, NumericUpDown2.Value, NumericUpDown3.Value, TextBox41.Text, tipo)

                    Dim ifAgregado = False
                    For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                        If (row.Cells(0).Value = "SISTEMA") Then
                            ifAgregado = True
                        End If
                    Next
                    If ifAgregado = False Then
                        If tipo = "fijo" Then
                            view_carrito.dt_productos.Rows.Add("SISTEMA", "COBRO POR MORA " & cantidadJ, cantidadT, mcobro, cantidadT * mcobro)
                        Else
                            view_carrito.dt_productos.Rows.Add("SISTEMA", "COBRO POR MORA (" & dias_atraso & " día/s)", dias_atraso, mcobro, dias_atraso * mcobro)
                        End If
                        viewcar.Text = "Ver carrito (" & view_carrito.dt_productos.Rows.Count.ToString & ")"
                    End If
                    cantidad.Value = cantidadT
                    cbox_mes.SelectedIndex = cantidadT - 1
                End If
            Catch ex As Exception
            End Try
        Else
            groupCobro.Enabled = False
        End If
    End Sub

    Private Sub cantidad_ValueChanged(sender As Object, e As EventArgs) Handles cantidad.ValueChanged
        Try
            If cbox_select.Text = "Inventario NV" Then
                If tipo_producto.Text = "producto" Then
                    If verificarStockProductos(idMaster.Text, cantidad.Value) = True Then
                        txtpagar.Text = (FormatNumber(cobro_precio.Text) * cantidad.Value)
                        btnAdd.Enabled = True
                        Button4.Enabled = True
                    Else
                        MsgBox("La cantidad solicitada es mayor a la que se encuentra en Stock.", vbExclamation, "RNV")
                        btnAdd.Enabled = False
                        Button4.Enabled = False
                    End If
                Else
                    txtpagar.Text = (FormatNumber(cobro_precio.Text) * cantidad.Value)
                End If
            Else
                txtpagar.Text = (FormatNumber(cobro_precio.Text) * cantidad.Value)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cobro_precio_TextChanged(sender As Object, e As EventArgs) Handles cobro_precio.TextChanged
        Call cantidad_ValueChanged(sender, e)
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim carrito As New view_carrito
        If cbox_select.Text = "-Selecciona cobro para-" Then
            MsgBox("Debes seleccionar giro.", vbExclamation, "RNV")
        Else
            If cbox_cobro_curso.Text <> "-ninguno-" Then
                If view_carrito.dt_productos.Rows.Count.ToString = 0 Then
                    MsgBox("El carrito no puede quedar vacío.", vbExclamation, "RNV")
                Else
                    If txt_client.Text <> "" Or cobro_precio.Text <> "" Then
                        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas generar este cobro?", "RNV", MessageBoxButtons.YesNo)
                        If result = DialogResult.Yes Then
                            Me.Enabled = False
                            If cbox_select.Text <> "Inventario NV" Then
                                If guardarRecibo(Label5.Text, idx.Text) = True Then
                                    Label25.Visible = True
                                    MsgBox("Cobro archivado correctamente.", vbInformation, "RNV")
                                    Me.Enabled = True
                                    view_recibo.Show()
                                Else
                                    Me.Enabled = True
                                    MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                                End If
                            Else
                                ListBox1.Items.Add("Proyectos NV")
                                ListBox1.Items.Add("Jerez, Jutiapa")
                                ListBox1.Items.Add("--------------------------------------")
                                ListBox1.Items.Add("Fecha y hora: " & DateTime.Now.ToString("dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString() & "")
                                ListBox1.Items.Add("Cliente: Consumidor Final")
                                ListBox1.Items.Add("Ticket: " & Label5.Text & "")
                                ListBox1.Items.Add("Articulo - Cantidad - Precio - Importe")
                                ListBox1.Items.Add("--------------------------------------")
                                For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                                    ListBox1.Items.Add("" & row.Cells(1).Value & " (x" & row.Cells(2).Value & ") (Q" & row.Cells(3).Value & ") (Q" & row.Cells(4).Value & ")")
                                Next
                                Dim total As Double
                                For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
                                    total += Val(row.Cells(4).Value)
                                Next
                                ListBox1.Items.Add("--------------------------------------")
                                ListBox1.Items.Add("Total pagar: Q" & total & "")

                                If guardarVenta(Label5.Text, idMaster.Text) = True Then
                                    Label25.Visible = True
                                    MsgBox("Venta realizada correctamente.", vbInformation, "RNV")
                                    Comenzar()
                                Else
                                    Me.Enabled = True
                                    MsgBox("Ocurrió un Error.", vbExclamation, "RNV")
                                End If
                            End If
                            Call Button1_Click(sender, e)
                            carrito.Close()
                        End If
                    Else
                        MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
                    End If
                End If
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub


    Private Sub viewcar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles viewcar.LinkClicked
        Me.Enabled = False
        'Me.WindowState = FormWindowState.Maximized
        Dim tx As Double
        For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
            tx += Val(row.Cells(4).Value)
        Next
        view_carrito.total.Text = "Q" & FormatNumber(tx, 2)
        view_carrito.Show()
        view_carrito.BringToFront()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim isadd As Boolean = False
        For Each row As DataGridViewRow In view_carrito.dt_productos.Rows
            If cbox_select.Text = "Inventario NV" Then
                If idMaster.Text = row.Cells(0).Value Then
                    isadd = True
                Else
                    isadd = False
                End If
            Else
                If txt_codigo.Text = row.Cells(0).Value Then
                    isadd = True
                Else
                    isadd = False
                End If
            End If
        Next
        If isadd = False Then
            If cbox_select.Text = "Inventario NV" Then
                view_carrito.dt_productos.Rows.Add(idMaster.Text, txt_descripcion.Text, cantidad.Text, cobro_precio.Text, txtpagar.Text)
            Else
                view_carrito.dt_productos.Rows.Add(txt_codigo.Text, txt_descripcion.Text, cantidad.Text, cobro_precio.Text, txtpagar.Text)
            End If
            viewcar.Text = "Ver carrito (" & view_carrito.dt_productos.Rows.Count.ToString & ")"
            MsgBox("Agregado al carrito correctamente!", vbInformation, "RNV")
        Else
            MsgBox("Esto ya fue agregado al carrito.", vbExclamation, "RNV")
        End If


    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        view_protected.Show()
        view_protected.ventana = "ajustes"
        view_protected.BringToFront()
        Me.Enabled = False
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        openFD.Title = "Selecciona una imagen | PNG"
        openFD.Filter = "Image Files|*.png"
        openFD.ShowDialog()
        txt_logo_nv.Text = openFD.SafeFileName
        txtsafe1.Text = openFD.FileName
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        openFD.Title = "Selecciona una imagen | PNG"
        openFD.Filter = "Image Files|*.png"
        openFD.ShowDialog()
        txt_logo_vnet.Text = openFD.SafeFileName
        txtsafe2.Text = openFD.FileName
    End Sub

    Private Sub btn_Guardar_vnet_Click(sender As Object, e As EventArgs) Handles btn_Guardar_vnet.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If txt_titulo_nv.Text <> "" Or txt_slogan_nv.Text <> "" Or txt_facebook_nv.Text <> "" Or txt_logo_nv.Text <> "" Then
                Dim nuevoNombre = cadena_aleatoria(11)
                If actualizarNV(txt_titulo_nv.Text, txt_slogan_nv.Text, txt_facebook_nv.Text, nuevoNombre + ".png") = True Then
                    Try
                        File.Copy(txtsafe1.Text, My.Application.Info.DirectoryPath & "\images\" + nuevoNombre + ".png", True)
                    Catch ex As Exception
                    End Try
                    MsgBox("Datos actualizados!", vbInformation, "RNV")
                    txt_logo_nv.Text = My.Application.Info.DirectoryPath & "\images\" + nuevoNombre + ".png"
                    'Call Button6_Click(sender, e)
                Else
                    MsgBox("Ocurrió un Error.", vbExclamation, "RNV")
                End If
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub btnGuardar_anv_Click(sender As Object, e As EventArgs) Handles btnGuardar_anv.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If txt_titulo_vnet.Text <> "" Or txt_slogan_vnet.Text <> "" Or txt_facebook_vnet.Text <> "" Or txt_logo_vnet.Text <> "" Then
                Dim nuevoNombre = cadena_aleatoria(11)
                If actualizarVNET(txt_titulo_vnet.Text, txt_slogan_vnet.Text, txt_facebook_vnet.Text, nuevoNombre + ".png") = True Then
                    Try
                        File.Copy(txtsafe2.Text, My.Application.Info.DirectoryPath & "\images\" + nuevoNombre + ".png", True)
                    Catch ex As Exception
                    End Try
                    MsgBox("Datos actualizados!", vbInformation, "RNV")
                    txt_logo_vnet.Text = My.Application.Info.DirectoryPath & "\images\" + nuevoNombre + ".png"
                    'Call Button6_Click(sender, e)
                Else
                    MsgBox("Ocurrió un Error.", vbExclamation, "RNV")
                End If
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub btnGuardar_pass_Click(sender As Object, e As EventArgs) Handles btnGuardar_pass.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If txtpass.Text <> "" Or txtpass2.Text <> "" Then
                If txtpass.Text = txtpass2.Text Then
                    If actualizarPASS(txtpass.Text) = True Then
                        MsgBox("Datos actualizados!", vbInformation, "RNV")
                        txtpass.Text = ""
                        txtpass2.Text = ""
                    Else
                        MsgBox("Ocurrió un Error.", vbExclamation, "RNV")
                    End If
                Else
                    MsgBox("Las contraseñas no coinciden.", vbExclamation, "RNV")
                End If

            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub btnEdit_curso_Click(sender As Object, e As EventArgs) Handles btnEdit_curso.Click
        Me.Enabled = False
        'Me.WindowState = FormWindowState.Maximized
        view_buscar_cliente.Show()
        view_buscar_cliente.BringToFront()
    End Sub

    Private Sub btn_delete_curso_Click(sender As Object, e As EventArgs) Handles btn_delete_curso.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas eliminar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If borrar_curso(txt_cod_curso.Text) = True Then
                MsgBox("Registro eliminado.", vbInformation, "RNV")
                limpiarCurso()
            Else
                MsgBox("Ocurrió un Error.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub btn_editar_alum_Click(sender As Object, e As EventArgs) Handles btn_editar_alum.Click
        Me.Enabled = False
        'Me.WindowState = FormWindowState.Maximized
        view_buscar_alum.Show()
        view_buscar_alum.BringToFront()
    End Sub

    Private Sub btnDelete_al_Click(sender As Object, e As EventArgs) Handles btnDelete_al.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas eliminar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If borrar_alumno(txt_id.Text) = True Then
                MsgBox("Registro eliminado.", vbInformation, "RNV")
                limpiarAlumno()
            Else
                MsgBox("Ocurrió un Error.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas limpiar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            limpiarCurso()
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas limpiar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            limpiarAlumno()
        End If
    End Sub

    Private Sub txtpagar_TextChanged(sender As Object, e As EventArgs) Handles txtpagar.TextChanged
        Call cantidad_ValueChanged(sender, e)
    End Sub

    Private Sub cbox_select_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbox_select.SelectedIndexChanged

        If cbox_select.Text = "Academia NV" Or cbox_select.Text = "Vision NET" Then
            '' cargar cursos asociados al tipo de bro y verificar si el usuario los tiene y cargarlos
            Try
                '' cargar // cursos
                cbox_cobro_curso.Items.Clear()
                Using cn As New OleDbConnection(s.sQuery)
                    cn.Open()
                    Dim objAdapter As New OleDbDataAdapter("Select rnv_cursos.id, rnv_cursos.nombre_curso FROM rnv_cursos 
                                                        INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_curso = rnv_cursos.id 
                                                        WHERE rnv_alumnos_cursos.id_alumno = " & idMaster.Text & " AND rnv_cursos.cobro_para = '" & cbox_select.Text & "'", cn)
                    Dim objDataSet As New DataSet
                    Dim nombre_nivel As String = ""
                    objAdapter.Fill(objDataSet, "rnv_cursos.nombre_curso")
                    Dim i As Integer
                    cbox_cobro_curso.Items.Add("-ninguno-")
                    For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                        nombre_nivel = objDataSet.Tables(0).Rows(i).Item(1)
                        If nombre_nivel = "-ninguno-" Then
                        Else
                            cbox_cobro_curso.Items.Add(objDataSet.Tables(0).Rows(i).Item(1) & "                                                                                                                                                                                                                                                                                                                                                                                                 " _
                                                                                                                     & "                                                                                           " _
                        & objDataSet.Tables(0).Rows(i).Item(0))
                        End If
                    Next
                    cn.Close()
                End Using
                cbox_cobro_curso.Enabled = True
                cbox_mes.Enabled = True
                cbox_cobro_curso.SelectedIndex = 0
            Catch ex As Exception

            End Try

            Try
                ''cargar mes que toca
                cbox_mes.Items.Clear()
                Using cn As New OleDbConnection(s.sQuery)
                    cn.Open()
                    Dim consulta As String = ""
                    consulta = "SELECT mes_pago_nv, mes_pago_vnet FROM rnv_alumnos WHERE id = " & idMaster.Text & ""
                    Dim cmd As New OleDbCommand(consulta, cn)
                    Dim reader As OleDbDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        Dim mes_siguiente = Convert.ToString(reader(("mes_pago_nv")))
                        Dim mes_sigiuente_vnet = Convert.ToString(reader(("mes_pago_vnet")))
                        Dim arrMeses() As String
                        If cbox_select.Text = "Academia NV" Then
                            If mes_siguiente = 1 Then
                                arrMeses = {"FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 2 Then
                                arrMeses = {"MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 3 Then
                                arrMeses = {"ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 4 Then
                                arrMeses = {"MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 5 Then
                                arrMeses = {"JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 6 Then
                                arrMeses = {"JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 7 Then
                                arrMeses = {"AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 8 Then
                                arrMeses = {"SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 9 Then
                                arrMeses = {"OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 10 Then
                                arrMeses = {"NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_siguiente = 11 Then
                                arrMeses = {"DICIEMBRE", "ENERO", "FEBRERO", "MARZO"}
                            ElseIf mes_siguiente > 11 Then
                                arrMeses = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            Else
                                arrMeses = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            End If
                        Else
                            If mes_sigiuente_vnet = 1 Then
                                arrMeses = {"FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 2 Then
                                arrMeses = {"MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 3 Then
                                arrMeses = {"ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 4 Then
                                arrMeses = {"MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 5 Then
                                arrMeses = {"JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 6 Then
                                arrMeses = {"JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 7 Then
                                arrMeses = {"AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 8 Then
                                arrMeses = {"SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 9 Then
                                arrMeses = {"OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 10 Then
                                arrMeses = {"NOVIEMBRE", "DICIEMBRE"}
                            ElseIf mes_sigiuente_vnet = 11 Then
                                arrMeses = {"DICIEMBRE", "ENERO", "FEBRERO", "MARZO"}
                            ElseIf mes_sigiuente_vnet > 11 Then
                                arrMeses = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            Else
                                arrMeses = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
                            End If
                        End If
                        For Each meses As String In arrMeses
                            cbox_mes.Items.Add(meses)
                        Next
                        Try
                            cbox_mes.SelectedIndex = 0

                        Catch ex As Exception
                            cbox_mes.SelectedIndex = 0
                        End Try
                    End If
                    cn.Close()
                End Using
                LinkLabel1.Text = "Agregar comentario al recibo"
                cbox_cobro_curso.Enabled = True
                cbox_select.Enabled = True
                LinkLabel1.Enabled = True
                cbox_mes.Enabled = True
            Catch ex As Exception
            End Try

        ElseIf cbox_select.Text = "Inventario NV" Then
            cbox_select.Enabled = True
            LinkLabel1.Enabled = True
            LinkLabel1.Text = "Agregar comentario"
            cbox_cobro_curso.Enabled = False
            cbox_mes.Enabled = False
            groupCobro.Enabled = True
            Label21.Text = "Precio de venta *"
            LinkLabel6.Visible = True
        Else
            txt_client.Enabled = True
            txt_client.Text = ""
            cbox_select.Enabled = False
            LinkLabel1.Enabled = False
            cbox_cobro_curso.Enabled = False
            cbox_mes.Enabled = False
        End If

    End Sub

    Private Sub reasig_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles reasig.LinkClicked
        Me.Enabled = False
        'Me.WindowState = FormWindowState.Maximized
        reasignar_cursos.Show()
        reasignar_cursos.BringToFront()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Me.Enabled = False
        view_buscar_recibos.Show()
        view_buscar_recibos.BringToFront()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        view_protected.Show()
        view_protected.ventana = "reportes"
        view_protected.BringToFront()
        Me.Enabled = False
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        TabControl2.SelectedIndex = 1
    End Sub

    Private Sub cbox_fast_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbox_fast.SelectedIndexChanged
        If cbox_fast.SelectedIndex = 0 Then
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = False
            datafecha.Enabled = False
            lblfechafecha.Enabled = False
            datafecha1.Enabled = False
            datafecha2.Enabled = False
            lblmes.Enabled = False
            c_mes.Enabled = False
        ElseIf cbox_fast.SelectedIndex = 1 Then
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = False
            datafecha.Enabled = False
            lblfechafecha.Enabled = False
            datafecha1.Enabled = False
            datafecha2.Enabled = False
            lblmes.Enabled = False
            c_mes.Enabled = False
        ElseIf cbox_fast.SelectedIndex = 2 Then
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = False
            datafecha.Enabled = False
            lblfechafecha.Enabled = False
            datafecha1.Enabled = False
            datafecha2.Enabled = False
            lblmes.Enabled = False
            c_mes.Enabled = False
        ElseIf cbox_fast.SelectedIndex = 3 Then
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = False
            datafecha.Enabled = False
            lblfechafecha.Enabled = False
            datafecha1.Enabled = False
            datafecha2.Enabled = False
            lblmes.Enabled = False
            c_mes.Enabled = False
        ElseIf cbox_fast.SelectedIndex = 4 Then
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = False
            datafecha.Enabled = False
            lblfechafecha.Enabled = False
            datafecha1.Enabled = False
            datafecha2.Enabled = False
            lblmes.Enabled = False
            c_mes.Enabled = False
        ElseIf cbox_fast.SelectedIndex = 5 Then
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = True
            datafecha.Enabled = True
            lblfechafecha.Enabled = False
            datafecha1.Enabled = False
            datafecha2.Enabled = False
            lblmes.Enabled = False
            c_mes.Enabled = False
        ElseIf cbox_fast.SelectedIndex = 6 Then
            lblmes.Enabled = False
            c_mes.Enabled = False
            lblaño.Enabled = False
            c_año.Enabled = False
            lblañomes.Enabled = False
            cmese.Enabled = False
            cañoe.Enabled = False
            lblfecha.Enabled = False
            datafecha.Enabled = False
            lblfechafecha.Enabled = True
            datafecha1.Enabled = True
            datafecha2.Enabled = True
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            Try
                '' cargar // clientes alumnos
                c_user.Items.Clear()
                Using cn As New OleDbConnection(s.sQuery)
                    cn.Open()
                    Dim objAdapter As New OleDbDataAdapter("SELECT rnv_alumnos.id, rnv_alumnos.nombres_alumno FROM (rnv_alumnos INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id) INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso WHERE rnv_cursos.cobro_para = '" & reporte_para.Text & "'", cn)
                    Dim objDataSet As New DataSet
                    Dim nombre_nivel As String = ""
                    objAdapter.Fill(objDataSet, "rnv_alumnos.nombres_alumno")
                    Dim i As Integer
                    c_user.Items.Add("-ninguno-")

                    For i = 0 To objDataSet.Tables(0).Rows.Count - 1

                        nombre_nivel = objDataSet.Tables(0).Rows(i).Item(1)
                        If nombre_nivel = "-ninguno-" Then
                        Else

                            If c_user.FindString(objDataSet.Tables(0).Rows(i).Item(1) & "                                                                                                                                                                                                                                                                                                                                                                                                 " _
                                                                                                                 & "                                                                                           " _
                    & objDataSet.Tables(0).Rows(i).Item(0)) <> "-1" Then
                            Else
                                c_user.Items.Add(objDataSet.Tables(0).Rows(i).Item(1) & "                                                                                                                                                                                                                                                                                                                                                                                                 " _
                                                                                     & "                                                                                           " _
& objDataSet.Tables(0).Rows(i).Item(0))
                            End If
                        End If
                    Next
                    cn.Close()
                End Using
                c_user.SelectedIndex = 0
                CheckBox2.Checked = True
                CheckBox2.Enabled = False
                CheckBox1.Enabled = True
                CheckBox1.Checked = False
                c_user.Enabled = True
                c_user.Visible = True
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            CheckBox1.Checked = True
            CheckBox1.Enabled = False
            CheckBox2.Enabled = True
            CheckBox2.Checked = False
            c_user.Enabled = False
            c_user.Visible = False

        End If
    End Sub

    Private Sub reporte_para_SelectedIndexChanged(sender As Object, e As EventArgs) Handles reporte_para.SelectedIndexChanged
        If reporte_para.Text = "-Selecciona reporte para-" Or reporte_para.Text = "Inventario NV" Then
            CheckBox1.Enabled = False
            CheckBox1.Checked = False
            CheckBox2.Enabled = False
            CheckBox2.Checked = False
            c_user.Visible = False
        Else
            CheckBox1.Checked = True
        End If
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click

        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas generar este reporte?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If reporte_para.Text = "-Selecciona reporte para-" Then
                If CheckBox2.Checked = True Then
                    If c_user.Text = "-ninguno-" Then
                        MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")

                    End If
                Else
                    MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")

                End If

            Else
                Dim id_user_marcado = 0
                Dim nombresel = ""
                Try
                    Dim ff As String = c_user.Text
                    Dim id_ben As String
                    id_ben = Microsoft.VisualBasic.Right(ff, 9)
                    id_user_marcado = Convert.ToInt32(Microsoft.VisualBasic.Right(ff, 9))
                    nombresel = Convert.ToInt32(Microsoft.VisualBasic.Left(ff, 18))
                Catch ex As Exception

                End Try

                Dim mesint, mesint2 As Integer
                If c_mes.SelectedIndex = 0 Then
                    mesint = "01"
                ElseIf c_mes.SelectedIndex = 1 Then
                    mesint = "02"
                ElseIf c_mes.SelectedIndex = 2 Then
                    mesint = "03"
                ElseIf c_mes.SelectedIndex = 3 Then
                    mesint = "04"
                ElseIf c_mes.SelectedIndex = 4 Then
                    mesint = "05"
                ElseIf c_mes.SelectedIndex = 5 Then
                    mesint = "06"
                ElseIf c_mes.SelectedIndex = 6 Then
                    mesint = "07"
                ElseIf c_mes.SelectedIndex = 7 Then
                    mesint = "08"
                ElseIf c_mes.SelectedIndex = 8 Then
                    mesint = "09"
                ElseIf c_mes.SelectedIndex = 9 Then
                    mesint = "10"
                ElseIf c_mes.SelectedIndex = 10 Then
                    mesint = "11"
                ElseIf c_mes.SelectedIndex = 11 Then
                    mesint = "12"
                End If
                If cmese.SelectedIndex = 0 Then
                    mesint2 = "01"
                ElseIf cmese.SelectedIndex = 1 Then
                    mesint2 = "02"
                ElseIf cmese.SelectedIndex = 2 Then
                    mesint2 = "03"
                ElseIf cmese.SelectedIndex = 3 Then
                    mesint2 = "04"
                ElseIf cmese.SelectedIndex = 4 Then
                    mesint2 = "05"
                ElseIf cmese.SelectedIndex = 5 Then
                    mesint2 = "06"
                ElseIf cmese.SelectedIndex = 6 Then
                    mesint2 = "07"
                ElseIf cmese.SelectedIndex = 7 Then
                    mesint2 = "08"
                ElseIf cmese.SelectedIndex = 8 Then
                    mesint2 = "09"
                ElseIf cmese.SelectedIndex = 9 Then
                    mesint2 = "10"
                ElseIf cmese.SelectedIndex = 10 Then
                    mesint2 = "11"
                ElseIf cmese.SelectedIndex = 11 Then
                    mesint2 = "12"
                End If
                grid_datos_cobros.Show()
                grid_datos_cobros.BringToFront()
                Dim codigoReporte = cadena_aleatoria(10)
                Label44.Text = codigoReporte
                Dim paraus = ""
                If CheckBox1.Checked = True Then
                    paraus = "Todos"
                Else
                    paraus = nombresel
                End If
                If salvar_reporte(codigoReporte, frmLoginNODB.txtuser.Text, paraus, Date.Today, reporte_para.Text) = True Then
                    If reporte_para.Text <> "Inventario NV" Then
                        informe_rapido(cbox_fast.SelectedIndex, mesint, mesint2, id_user_marcado)
                        Label43.Visible = True
                        MsgBox("Reporte archivado correctamente.", vbInformation, "RNV")
                        view_reportes.Show()
                        view_reportes.BringToFront()
                    Else
                        informe_ventas_inventario(cbox_fast.SelectedIndex, mesint, mesint2)
                        Label43.Visible = True
                        MsgBox("Reporte archivado correctamente.", vbInformation, "RNV")
                        view_reportes_inventario.Show()
                        view_reportes_inventario.BringToFront()
                    End If


                    grid_datos_cobros.Close()
                    Me.Enabled = False
                Else
                    MsgBox("Ocurrió une error.", vbExclamation, "RNV")
                End If
            End If
        End If
    End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As Object,
                                ByVal e As KeyPressEventArgs) Handles txt_tel1.KeyPress, TextBox42.KeyPress

        Dim re As New Regex("[^0-9_\-\b]", RegexOptions.IgnoreCase)
        e.Handled = re.IsMatch(e.KeyChar)

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object,
                                ByVal e As KeyPressEventArgs) Handles txt_tel2.KeyPress

        Dim re As New Regex("[^0-9_\-\b]", RegexOptions.IgnoreCase)
        e.Handled = re.IsMatch(e.KeyChar)

    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object,
                                ByVal e As KeyPressEventArgs) Handles txt_tel3.KeyPress

        Dim re As New Regex("[^0-9_\-\b]", RegexOptions.IgnoreCase)
        e.Handled = re.IsMatch(e.KeyChar)

    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 5
        Label49.Text = leerRNV()

    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        If Application.OpenForms().OfType(Of webchat).Any Then
        Else
            Dim f2 As New webchat
            f2.Text = "ChatRNV"
            f2.Show()
            f2.BringToFront()
        End If
    End Sub



    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas empezar con el proceso?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then

            If verificarSiEstaOcupado() = True Then
                MsgBox("Esta base de datos esta siendo sincronizada en otro sistema. Por favor intenta más tarde.")
            Else
                ocupar()
                ' se descarga la base de datos
                Label51.Visible = True
                Me.Refresh()
                Me.Update()
                DownloadFile(leerRNV, leerRNV)
                If getAlumnos() = True Then

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                        UploadFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb", "ESME_rnv_bdd.mdb")
                    End If

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                        UploadFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb", "JEREZ_rnv_bdd.mdb")
                    End If

                    MsgBox("La sincronización se realizó con exito.", vbInformation)
                    'verificarTimer()
                    File.WriteAllText(s.rutaSistema & "ifsync.rnv", "false")
                    File.WriteAllText(s.rutaSistema & "lastupdate.rnv", DateTime.Now.ToString("dd/MM/yyy") & " " & DateTime.Now.ToShortTimeString())
                    If leerupdate() = "" Then
                        lupdate.Text = "n/a"
                    Else
                        lupdate.Text = leerupdate()
                    End If
                    Label51.Visible = False
                    liberar()

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb")
                    End If

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb")
                    End If
                Else

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb")
                    End If

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb")
                    End If
                    MsgBox("ocurrio un error")
                    Label51.Visible = False
                    liberar()
                End If
            End If
        End If
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        Dim result As Integer = MessageBox.Show("Antes de utilizar esta función por favor verificar si esta BDD realmente no esta siendo Sincronizada. ¿continuar?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            liberar()
            MsgBox("SYNC liberado correctamente.", vbInformation, "RNV")
        End If

    End Sub
    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        If Timer1.Enabled = True Then
            func_backupBDD()
            If leerSync() = "true" Then
                MsgBox("Cambios detectados: Aún no has realizado la Sincronización. RNV intentará comenzar con el proceso.", vbInformation)
                Call Button13_Click(sender, e)
                Call Button16_Click(sender, e)
                File.WriteAllText(s.rutaSistema & "ifsync.rnv", "false")
            End If
        End If
        Application.Restart()
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Me.Enabled = False
        view_buscar_reporte.Show()
        view_buscar_reporte.BringToFront()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        If cbox_select.Text <> "Inventario NV" Then
            If comentario.Visible = True Then
                comentario.Visible = False
                LinkLabel1.Text = "Agregar comentario al recibo"

            Else
                comentario.Visible = True
                LinkLabel1.Text = "Cerrar caja de comentario"
            End If
        Else
            If comentario.Visible = True Then
                comentario.Visible = False
                LinkLabel1.Text = "Agregar comentario"

            Else
                comentario.Visible = True
                LinkLabel1.Text = "Cerrar caja de comentario"
            End If
        End If
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If actualizarbackup(NumericUpDown1.Value) = True Then
                MsgBox("Datos actualizados!", vbInformation, "RNV")
                txtpass.Text = ""
                txtpass2.Text = ""
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub
    Dim countdown = 0
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        countdown = countdown - 1
        If countdown = 0 Then
            Dim worker As New BackgroundWorker
            worker.WorkerSupportsCancellation = True
            worker.WorkerReportsProgress = True
            If worker.IsBusy Then
                worker.CancelAsync()
            Else
                worker.RunWorkerAsync(func_backupBDD())
            End If
            Timer1.Stop()
            countdown = NumericUpDown1.Value
            Timer1.Start()
        End If

    End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles BunifuImageButton1.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas empezar con el proceso?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then

            If verificarSiEstaOcupado() = True Then
                MsgBox("Esta base de datos esta siendo sincronizada en otro sistema. Por favor intenta más tarde.")
            Else
                ocupar()
                ' se descarga la base de datos
                Label51.Visible = True
                Me.Refresh()
                Me.Update()
                DownloadFile(leerRNV, leerRNV)
                If getAlumnos() = True Then
                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                        UploadFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb", "ESME_rnv_bdd.mdb")
                    End If
                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                        UploadFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb", "JEREZ_rnv_bdd.mdb")
                    End If
                    MsgBox("La sincronización se realizó con exito.", vbInformation, "RNV SYNC")
                    'verificarTimer()
                    File.WriteAllText(s.rutaSistema & "ifsync.rnv", "false")
                    File.WriteAllText(s.rutaSistema & "lastupdate.rnv", DateTime.Now.ToString("dd/MM/yyy") & " " & DateTime.Now.ToShortTimeString())
                    If leerupdate() = "" Then
                        lupdate.Text = "n/a"
                    Else
                        lupdate.Text = leerupdate()
                    End If
                    Label51.Visible = False
                    liberar()

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb")
                    End If

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb")
                    End If
                Else

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\ESME_rnv_bdd.mdb")
                    End If

                    If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb") Then
                        My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\sys\JEREZ_rnv_bdd.mdb")
                    End If
                    MsgBox("ocurrio un error")
                    Label51.Visible = False
                    liberar()
                End If
            End If
        End If
    End Sub

    Private Sub all_string_fields_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txt_curso.KeyPress, txt_desc.KeyPress, txt_nombres.KeyPress, txt_cod.KeyPress, txt_padres.KeyPress, txt_madres.KeyPress, txt_direccion.KeyPress, TextBox36.KeyPress, RichTextBox3.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789 .,+/áéíóú´" 'Permite unicamente esta cadena de caracteres
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub txt_precio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txt_precio.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And txt_precio.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub cobro_precio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cobro_precio.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And cobro_precio.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub btnGuardarProtected_Click(sender As Object, e As EventArgs) Handles btnGuardarProtected.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If txt_protected.Text <> "" Or txt_protected2.Text <> "" Then
                If txt_protected.Text = txt_protected2.Text Then
                    If actualizarPROTECTED(txt_protected.Text) = True Then
                        MsgBox("Datos actualizados!", vbInformation, "RNV")
                        txt_protected.Text = ""
                        txt_protected2.Text = ""
                    Else
                        MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                    End If
                Else
                    MsgBox("Las contraseñas no coinciden.", vbExclamation, "RNV")
                End If

            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Me.Enabled = False
        view_corte.Show()
        view_corte.BringToFront()
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        TextBox46.Text = ""
        Panel4.Visible = True
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 6
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        ComboBox1.Items.Clear()
        TextBox14.Text = ""
        mostrarpagos.Visible = False
        Panel1.Visible = True
        consultarUsuarios()
        TabControl1.SelectedIndex = 7
    End Sub


    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        If TextBox14.Text <> "" Then
            consultarDatosUsuario(jtid.Text)
            consultarCursos(jtid.Text)
            Panel1.Visible = False
            mostrarpagos.Visible = True
        Else
            Panel1.Visible = True
            mostrarpagos.Visible = False
        End If

    End Sub

    Private Sub mostrarpagos_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles mostrarpagos.LinkClicked
        'view_protected.Show()
        'view_protected.ventana = "pagos_realizados"
        'view_protected.BringToFront()
        view_pagos_realizados.Show()
        Me.Enabled = False
    End Sub

    Private Sub Label35_Click(sender As Object, e As EventArgs) Handles Label35.Click
        'Batír el formulario principal. ME
        ShakeControl(Me, 20, 20, 20)
    End Sub

    Private Sub TextBox13_Click(sender As Object, e As EventArgs) Handles codigorecorte.Click
        Me.Enabled = False
        view_buscar_corte_general.Show()
        view_buscar_corte_general.consulta = True
    End Sub

    Private Sub TextBox14_Click(sender As Object, e As EventArgs) Handles TextBox14.Click
        Me.Enabled = False
        view_buscar.is_consulta = True
        view_buscar.Show()
        view_buscar.consulta = True
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click
        view_protected.Show()
        view_protected.ventana = "recorte"
        view_protected.BringToFront()
        Me.Enabled = False
    End Sub


    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click
        If codigorecorte.Text <> "" Then
            consultarMegaCortes(codigorecorte.Text)
            Panel3.Visible = False
        Else
            Panel3.Visible = True
            mostrarpagos.Visible = False
        End If
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        view_protected.Show()
        view_protected.ventana = "recortegeneral"
        view_protected.BringToFront()
        Me.Enabled = False
    End Sub

    Private Sub TextBox46_Click(sender As Object, e As EventArgs) Handles TextBox46.Click
        Me.Enabled = False
        view_buscar_corte.Show()
        view_buscar_corte.consulta = True
    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        If TextBox46.Text <> "" Then
            consultarCortes(TextBox46.Text)
            Panel4.Visible = False
        Else
            Panel4.Visible = True
        End If
    End Sub

    Public Sub total_ventas()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "Select count(*) from rnv_cobros"
            Dim cmd As New OleDbCommand(consulta, cn)
            Label98.Text = CStr(cmd.ExecuteScalar())
            Label98.Update()
            Label98.Refresh()
            cn.Close()
        End Using
    End Sub

    Public Sub total_inventario()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "Select count(*) from rnv_inventario"
            Dim cmd As New OleDbCommand(consulta, cn)
            tprod.Text = CStr(cmd.ExecuteScalar())
            tprod.Update()
            tprod.Refresh()
            cn.Close()
        End Using
    End Sub

    Public Sub total_alumnos()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "Select count(*) from rnv_alumnos"
            Dim cmd As New OleDbCommand(consulta, cn)
            Label92.Text = CStr(cmd.ExecuteScalar())
            Label92.Update()
            Label92.Refresh()
            cn.Close()
        End Using
    End Sub

    Public Sub total_cursos()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = "Select count(*) from rnv_cursos"
            Dim cmd As New OleDbCommand(consulta, cn)
            Label95.Text = CStr(cmd.ExecuteScalar())
            Label95.Update()
            Label95.Refresh()
            cn.Close()
        End Using
    End Sub
    Dim nv, vnet, inventario As Double
    Public Sub GetPagos()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim str As Date = Date.Today
            Dim str2 As String
            Dim convertidor As DateTime
            Dim hoy As String
            Try
                str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                convertidor = str2
                hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
            Catch ex As Exception
                Dim hoySinConvert As String = Date.Today.ToString("MM/dd/yyyy")
                hoy = DateTime.ParseExact(hoySinConvert, "M/d/yyyy",
                                 Globalization.CultureInfo.InstalledUICulture)
            End Try
            Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_cobros WHERE fecha = #" & hoy & "#", cn)
            Dim ds As New DataSet
            dt.Fill(ds)
            For Each dtr As DataRow In ds.Tables(0).Rows
                If dtr("giro") = "Academia NV" Then
                    nv += FormatNumber(dtr("total_venta"), 2)
                ElseIf dtr("giro") = "Vision NET" Then
                    vnet += FormatNumber(dtr("total_venta"), 2)
                Else
                    inventario += FormatNumber(dtr("total_venta"), 2)
                End If

            Next
            Chart1.Series("Academia NV").Points.AddY("Q " & nv)
            Chart1.Series("Vision NET").Points.AddY("Q " & vnet)
            Chart1.Series("Inventario NV").Points.AddY("Q" & inventario)

            cn.Close()
        End Using
    End Sub

    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click
        'view_protected.Show()
        'view_protected.ventana = "escritorio"
        'view_protected.BringToFront()
        'Me.Enabled = False
        nv = 0
        vnet = 0
        inventario = 0
        Chart1.Series(0).Points.Clear()
        Chart1.Series(1).Points.Clear()
        Chart1.Series(2).Points.Clear()
        Chart1.Series.Clear()
        Chart1.Series.Add("Academia NV")
        Chart1.Series.Add("Vision NET")
        Chart1.Series.Add("Inventario NV")
        GetPagos()
        total_ventas()
        total_alumnos()
        total_cursos()
        total_inventario()
        TabControl1.Visible = True
        TabControl1.SelectedIndex = 10
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Call Button1_Click(sender, e)
    End Sub

    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        Call Button5_Click(sender, e)
    End Sub

    Sub informe_usuarios()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim whereconsulta As String = ""
            If ComboBox2.Text = "-todos-" Then
                whereconsulta = "SELECT rnv_alumnos.nombres_alumno, rnv_cursos.nombre_curso, rnv_alumnos.fecha_nacimiento_alumno, rnv_alumnos.direccion, rnv_cursos.cobro_para
                         FROM (rnv_alumnos
                         INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id)
                         INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso"

            ElseIf ComboBox2.Text <> "-todos-" And ComboBox3.Text = "-todos-" Then
                whereconsulta = "SELECT rnv_alumnos.nombres_alumno, rnv_cursos.nombre_curso, rnv_alumnos.fecha_nacimiento_alumno, rnv_alumnos.direccion, rnv_cursos.cobro_para
                         FROM (rnv_alumnos
                         INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id)
                         INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso
                         WHERE rnv_cursos.cobro_para = '" & ComboBox2.Text & "' "
            ElseIf ComboBox2.Text <> "-todos-" And ComboBox3.Text <> "-todos-" Then
                Dim removeCiclo = ComboBox3.Text
                Dim squirt = removeCiclo.Replace("(2017) ", "")
                Dim requirt = squirt.Replace("(2018) ", "")
                Dim lastQuiert = requirt.Replace("(2019) ", "")
                Dim rekimit = lastQuiert.Replace("(2020)", "")
                whereconsulta = "SELECT rnv_alumnos.nombres_alumno, rnv_cursos.nombre_curso, rnv_alumnos.fecha_nacimiento_alumno, rnv_alumnos.direccion, rnv_cursos.cobro_para
                         FROM (rnv_alumnos
                         INNER JOIN rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id)
                         INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso
                         WHERE rnv_cursos.cobro_para = '" & ComboBox2.Text & "' AND rnv_cursos.nombre_curso = '" & rekimit & "' "
            End If


            Dim cmd As New OleDbCommand(whereconsulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Dim dt As New DataTable
            adaptador.Fill(dt)
            Dim dv As DataView = dt.DefaultView
            grid_listado.gridinforme.DataSource = dv
            cn.Close()
        End Using
    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click
        TabControl1.SelectedIndex = 11
        ComboBox2.SelectedIndex = 0
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        If ComboBox2.Text <> "-todos-" Then
            If ComboBox2.Text = "Academia NV" Then
                Try
                    '' cargar // cursos
                    ComboBox3.Items.Clear()
                    Using cn As New OleDbConnection(s.sQuery)
                        cn.Open()
                        Dim objAdapter As New OleDbDataAdapter("Select id, nombre_curso, ciclo FROM rnv_cursos
                                                        WHERE rnv_cursos.cobro_para = '" & ComboBox2.Text & "'", cn)
                        Dim objDataSet As New DataSet
                        Dim nombre_nivel As String = ""
                        objAdapter.Fill(objDataSet, "nombre_curso")
                        Dim i As Integer
                        ComboBox3.Items.Add("-todos-")
                        For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                            nombre_nivel = objDataSet.Tables(0).Rows(i).Item(1)
                            If nombre_nivel = "-todos-" Then
                            Else
                                ComboBox3.Items.Add("(" & objDataSet.Tables(0).Rows(i).Item(2) & ") " & objDataSet.Tables(0).Rows(i).Item(1))
                            End If
                        Next
                        cn.Close()
                    End Using
                    ComboBox3.SelectedIndex = 0
                    ComboBox3.Enabled = True
                Catch ex As Exception

                End Try
            Else
                Try
                    '' cargar // cursos
                    ComboBox3.Items.Clear()
                    Using cn As New OleDbConnection(s.sQuery)
                        cn.Open()
                        Dim objAdapter As New OleDbDataAdapter("Select id, nombre_curso, ciclo FROM rnv_cursos
                                                        WHERE rnv_cursos.cobro_para = '" & ComboBox2.Text & "'", cn)
                        Dim objDataSet As New DataSet
                        Dim nombre_nivel As String = ""
                        objAdapter.Fill(objDataSet, "nombre_curso")
                        Dim i As Integer
                        ComboBox3.Items.Add("-todos-")
                        For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                            nombre_nivel = objDataSet.Tables(0).Rows(i).Item(1)
                            If nombre_nivel = "-todos-" Then
                            Else
                                ComboBox3.Items.Add(objDataSet.Tables(0).Rows(i).Item(1))
                            End If
                        Next
                        cn.Close()
                    End Using
                    ComboBox3.SelectedIndex = 0
                    ComboBox3.Enabled = True
                Catch ex As Exception

                End Try
            End If

        Else
            ComboBox3.Items.Clear()
            ComboBox3.Enabled = False
        End If
    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas generar este reporte?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Label99.Visible = True
            Me.Refresh()
            Me.Update()
            If salvar_reporte(lcode.Text, frmLoginNODB.txtuser.Text, "", Date.Today, "") = True Then
                grid_listado.Show()
                informe_usuarios()
                MsgBox("Reporte archivado correctamente.", vbInformation, "RNV")
                view_listado_users.Show()
                view_listado_users.BringToFront()
                grid_listado.Close()
                Label99.Visible = False
            Else
                MsgBox("Ocurrió une error.", vbExclamation, "RNV")
                Label99.Visible = False
            End If
        End If
    End Sub

    Private Sub LinkLabel5_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
        'Me.Enabled = False
        view_notas.Show()
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        view_notas.Top = Me.Top
        view_notas.Left = Me.Left + Me.Width
    End Sub

    Private Sub TabPage3_Click(sender As Object, e As EventArgs) Handles TabPage3.Click

    End Sub

    Private Sub Button32_Click(sender As Object, e As EventArgs) Handles Button32.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then

            Dim nuevoNombre = cadena_aleatoria(11)
            Dim bool As Integer = 0
            If frontal.Checked = True Then
                bool = 1
            Else
                bool = 0
            End If
            If actualizarNotas(maxdias.Value, bool) = True Then
                MsgBox("Datos actualizados!", vbInformation, "RNV")
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Public Sub limpiar_inventario()
        TextBox36.Text = ""
        CheckBox3.Checked = False
        TextBox37.Text = ""
        TextBox38.Text = ""
        view_stock.Close()
        cod_inventario.Text = cadena_aleatoria(10)
        TextBox39.Text = ""
        RichTextBox3.Text = ""
        Button36.Visible = False
        Button38.Visible = False
        Button39.Visible = False
    End Sub

    Private Sub Button33_Click(sender As Object, e As EventArgs) Handles Button33.Click
        limpiar_inventario()
        TabControl1.Visible = True
        cod_inventario.Text = cadena_aleatoria(10)
        TabControl1.SelectedIndex = 12
    End Sub

    Private Sub LinkLabel4_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        Call Button33_Click(sender, e)
    End Sub

    Private Sub textbox37_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox37.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox37.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub textbox38_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox38.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox38.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub textbox41_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox41.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox41.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub Label106_Click(sender As Object, e As EventArgs) Handles Label106.Click
        MsgBox("Al marcar como servicio no se aplicará Stock.", vbInformation, "RNV INVENTARIO")
    End Sub

    Public tipo

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click
        If String.IsNullOrEmpty(TextBox36.Text) And String.IsNullOrEmpty(TextBox37.Text) And String.IsNullOrEmpty(TextBox38.Text) And String.IsNullOrEmpty(TextBox39.Text) And String.IsNullOrEmpty(cod_inventario.Text) Then
            MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
        Else
            If Button38.Visible = True Then
                Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then

                    If salvarInventario(TextBox36.Text, TextBox37.Text, TextBox38.Text, TextBox39.Text, RichTextBox3.Text, id_inventario.Text, tipo, 0, cod_inventario.Text) = True Then
                        MsgBox("Registro actualizado.", vbInformation, "RNV")
                        limpiar_inventario()
                    Else
                        MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                    End If
                End If
            Else
                If CheckBox3.Checked = False Then
                    Me.Enabled = False
                    view_stock.Label1.Text = TextBox36.Text
                    view_stock.Show()
                Else
                    Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
                    If result = DialogResult.Yes Then
                        If salvarInventario(TextBox36.Text, TextBox37.Text, TextBox38.Text, TextBox39.Text, RichTextBox3.Text, id_inventario.Text, tipo, 0, cod_inventario.Text) = True Then
                            MsgBox("Registro completado.", vbInformation, "RNV")
                            limpiar_inventario()
                        Else
                            MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                        End If
                    End If
                End If
            End If

        End If
    End Sub


    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            tipo = "servicio"
        Else
            tipo = "producto"
        End If
    End Sub

    Private Sub Button34_Click(sender As Object, e As EventArgs) Handles Button34.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas limpiar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            limpiar_inventario()
        End If
    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        Me.Enabled = False
        'Me.WindowState = FormWindowState.Maximized
        view_buscar_inventario.Show()
        view_buscar_inventario.BringToFront()
    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs) Handles Button36.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas eliminar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If borrar_inventario(cod_inventario.Text) = True Then
                MsgBox("Registro eliminado.", vbInformation, "RNV")
                limpiarCurso()
                limpiar_inventario()
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click
        Me.Enabled = False
        view_update_stock.Label1.Text = TextBox36.Text
        view_update_stock.Show()
    End Sub

    Private Sub TextBox38_TextChanged(sender As Object, e As EventArgs) Handles TextBox38.TextChanged
        Try
            ''---------- PRECIO VENTA
            Dim num(2) As Double
            num(0) = Val(TextBox37.Text)
            num(1) = Val(TextBox38.Text)
            num(2) = FormatNumber(((num(1) * 100)) / num(0) - 100, 2)
            Label108.Text = num(2) & "% de útilidad"
            ''--------------------
        Catch ex As Exception
        End Try
    End Sub

    Private Sub TextBox39_TextChanged(sender As Object, e As EventArgs) Handles TextBox39.TextChanged
        Try
            ''---------- PRECIO MINIMO VENTA
            Dim num(2) As Double
            num(0) = Val(TextBox37.Text)
            num(1) = Val(TextBox39.Text)
            num(2) = FormatNumber(((num(1) * 100)) / num(0) - 100, 2)
            Label115.Text = num(2) & "% de útilidad"
            ''--------------------
        Catch ex As Exception
        End Try
    End Sub

    Private Sub TextBox37_TextChanged(sender As Object, e As EventArgs) Handles TextBox37.TextChanged
        Try
            ''---------- PRECIO VENTA
            Dim num(2) As Double
            num(0) = Val(TextBox37.Text)
            num(1) = Val(TextBox38.Text)
            num(2) = FormatNumber(((num(1) * 100)) / num(0) - 100, 2)
            Label108.Text = num(2) & "% de útilidad"
            ''--------------------
        Catch ex As Exception
        End Try
        Try
            ''---------- PRECIO MINIMO VENTA
            Dim num(2) As Double
            num(0) = Val(TextBox37.Text)
            num(1) = Val(TextBox39.Text)
            num(2) = FormatNumber(((num(1) * 100)) / num(0) - 100, 2)
            Label115.Text = num(2) & "% de útilidad"
            ''--------------------
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button39_Click(sender As Object, e As EventArgs) Handles Button39.Click
        Call Button36_Click(sender, e)
    End Sub

    Private Sub LinkLabel6_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel6.LinkClicked
        If LinkLabel6.Text = "Usar precio mínimo" Then
            LinkLabel6.Text = "Usar precio normal"
            cobro_precio.Text = preminimo.Text
        Else
            LinkLabel6.Text = "Usar precio mínimo"
            cobro_precio.Text = prenormal.Text
        End If
    End Sub

    Private Sub Button40_Click(sender As Object, e As EventArgs) Handles Button40.Click
        TabControl1.SelectedIndex = 13
    End Sub

    Private Sub Button42_Click(sender As Object, e As EventArgs) Handles Button42.Click
        consultarVentas(TextBox45.Text)
        cargarProductosVendidosTicket()
    End Sub

    Private Sub textbox39_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox39.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox39.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub TextBox45_Click(sender As Object, e As EventArgs) Handles TextBox45.Click
        Me.Enabled = False
        buscar_venta.Show()
    End Sub


    Private Sub LinkLabel7_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel7.LinkClicked
        Me.Enabled = False
        view_articulos_vendidos.Show()
        view_articulos_vendidos.Label2.Text = TextBox34.Text
    End Sub

    Private Sub LinkLabel8_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel8.LinkClicked
        If My.Computer.FileSystem.FileExists(s.rutaArchivos & "\recibos\ticket_" & TextBox45.Text & ".pdf") Then
            System.Diagnostics.Process.Start(s.rutaArchivos & "\recibos\ticket_" & TextBox45.Text & ".pdf")
        Else
            Label120.Visible = True
            Me.Refresh()
            ''//Re generar el ticket
            ''TODO: Tomar en cuenta que el correlativo puede ser igual a uno ya impreso
            ListBox1.Items.Add("Proyectos NV")
            ListBox1.Items.Add("Jerez, Jutiapa")
            ListBox1.Items.Add("--------------------------------------")
            ListBox1.Items.Add("Fecha y hora: " & DateTime.Now.ToString("dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString() & "")
            ListBox1.Items.Add("Cliente: Consumidor Final")
            ListBox1.Items.Add("Ticket: " & TextBox45.Text & "")
            ListBox1.Items.Add("Articulo - Cantidad - Precio - Importe")
            ListBox1.Items.Add("--------------------------------------")
            For Each row As DataGridViewRow In gridTicket.Rows
                ListBox1.Items.Add("" & row.Cells(0).Value & " (x" & row.Cells(1).Value & ") (Q" & row.Cells(2).Value & ") (Q" & row.Cells(3).Value & ")")
            Next
            Dim total As Double
            For Each row As DataGridViewRow In gridTicket.Rows
                total += Val(row.Cells(3).Value)
            Next
            ListBox1.Items.Add("--------------------------------------")
            ListBox1.Items.Add("Total pagar: Q" & total & "")
            reCrearTicket()
        End If
    End Sub

    Public Sub reCrearTicket()
        Dim aleatString = cadena_aleatoria(10)
        Dim pgSize As New iTextSharp.text.Rectangle(219, 500)
        Dim doc As New iTextSharp.text.Document(pgSize, 5, 5, 4, 5)
        Dim nombrearchivo As String = ""
        Dim filename As String = ""
        nombrearchivo = "recibo_inventario"
        view_server.nombreTxt = nombrearchivo
        view_server.fichero = My.Application.Info.DirectoryPath & "\recibos\ticket_" & TextBox45.Text & ".txt"
        filename = My.Application.Info.DirectoryPath & "\recibos\ticket_" & TextBox45.Text & ".pdf"
        Dim file As New FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)
        Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(doc, file)
        doc.Open()
        ExportarDatosPDF(doc)
        doc.Close()
        ListBox1.Items.Clear()
        System.Diagnostics.Process.Start(My.Application.Info.DirectoryPath & "\recibos\ticket_" & TextBox45.Text & ".pdf")
        Label120.Visible = False
    End Sub

    Private Sub Button44_Click(sender As Object, e As EventArgs) Handles Button44.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If actualizarEmail(txt_cuerpo_email.Text) = True Then
                MsgBox("Datos actualizados!", vbInformation, "RNV")
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub Label125_Click(sender As Object, e As EventArgs) Handles Label125.Click
        MsgBox("Se utilizará para enviar el recibo de pago via correo electrónico", vbInformation, "RNV")
    End Sub

    Private Sub Button41_Click(sender As Object, e As EventArgs) Handles Button41.Click
        TabControl1.SelectedIndex = 14
    End Sub

    Public Sub Comenzar()
        Dim aleatString = cadena_aleatoria(10)
        Dim pgSize As New iTextSharp.text.Rectangle(219, 500)
        Dim doc As New iTextSharp.text.Document(pgSize, 5, 5, 4, 5)
        Dim nombrearchivo As String = ""
        Dim filename As String = ""
        nombrearchivo = "recibo_inventario"
        view_server.nombreTxt = nombrearchivo
        view_server.fichero = My.Application.Info.DirectoryPath & "\recibos\ticket_" & Label5.Text & ".txt"
        filename = My.Application.Info.DirectoryPath & "\recibos\ticket_" & Label5.Text & ".pdf"
        Dim file As New FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)
        Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(doc, file)
        doc.Open()
        ExportarDatosPDF(doc)
        doc.Close()
        ListBox1.Items.Clear()
        Dim resultg As Integer = MessageBox.Show("¿Deseas imprimir el ticket de venta?", "RNV", MessageBoxButtons.YesNo)
        If resultg = DialogResult.Yes Then
            System.Diagnostics.Process.Start(My.Application.Info.DirectoryPath & "\recibos\ticket_" & Label5.Text & ".pdf")
            Label25.Visible = False
            Me.Enabled = True
        Else
            Label25.Visible = False
            Me.Enabled = True
        End If
    End Sub

    Private Sub Button45_Click(sender As Object, e As EventArgs) Handles Button45.Click
        consultarProductosInventario(TextBox44.Text)
    End Sub

    Private Sub LinkLabel9_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel9.LinkClicked
        view_mov_small_stock.Show()
        Me.Enabled = False
    End Sub

    Private Sub Label138_Click(sender As Object, e As EventArgs) Handles Label138.Click
        MsgBox("Parámetros que puedes incluir en el cuerpo del mensaje:" & vbCrLf & vbCrLf & "{CLIENTE} = Imprime el nombre del alumno / cliente." & vbCrLf & "{MES_PAGO} = Imprime el mes que pagó el alumno / cliente.", vbInformation, "Parámetro de correo")
    End Sub

    Private Sub CheckBox4_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            CheckBox5.Checked = False
            TextBox51.Enabled = True
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
        Else
            CheckBox5.Checked = True
            TextBox51.Enabled = False
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox5.CheckedChanged
        If CheckBox5.Checked = True Then
            CheckBox4.Checked = False
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
        Else
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            CheckBox4.Checked = True
        End If
    End Sub

    Private Sub Button43_Click(sender As Object, e As EventArgs) Handles Button43.Click
        TabControl1.SelectedIndex = 15
    End Sub

    Private Sub TextBox44_TextChanged(sender As Object, e As EventArgs) Handles TextBox44.TextChanged

    End Sub

    Private Sub TextBox51_TextChanged(sender As Object, e As EventArgs) Handles TextBox51.TextChanged

    End Sub

    Public Sub ExportarDatosPDF(ByVal document As Document)
        Using a As StreamWriter =
              New StreamWriter(view_server.fichero, True)
            Dim Informacion As Paragraph
            Dim PedazoDeInformacion As String = ""
            Dim texto As String = " "
            For i As Integer = 0 To ListBox1.Items.Count - 1
                PedazoDeInformacion = ListBox1.Items.Item(i)
                a.WriteLine(PedazoDeInformacion)
                Informacion = New Paragraph(PedazoDeInformacion, New Font(Font.Name = "Lucida Console", 9, Font.Bold))
                Informacion.Alignment = Element.ALIGN_CENTER
                document.Add(Informacion)
            Next
        End Using

    End Sub

    Private Sub Button46_Click(sender As Object, e As EventArgs) Handles Button46.Click
        If CheckBox4.Checked = True Then
            consultarStockProducto(TextBox51.Text)
            Panel10.Visible = False
        Else
            consultarStockProducto("")
            Panel10.Visible = False
        End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox7.CheckedChanged
        If CheckBox7.Checked = True Then
            CheckBox8.Checked = False
        Else
            CheckBox8.Checked = True
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox8.CheckedChanged
        If CheckBox8.Checked = True Then
            CheckBox7.Checked = False
        Else
            CheckBox7.Checked = True
        End If
    End Sub

    Private Sub Button47_Click(sender As Object, e As EventArgs) Handles Button47.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If TextBox41.Text <> "" Then
                Dim tipo, inicio
                If CheckBox7.Checked = True Then
                    tipo = "fijo"
                Else
                    tipo = "diario"
                End If
                If CheckBox6.Checked = True Then
                    inicio = 1
                Else
                    inicio = 2
                End If
                If actualizarMora(NumericUpDown2.Value, NumericUpDown3.Value, TextBox41.Text, tipo, inicio) = True Then
                    MsgBox("Datos actualizados!", vbInformation, "RNV")
                Else
                    MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                End If
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub LinkLabel10_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel10.LinkClicked
        Dim GENERADOR As BarcodeWriter = New BarcodeWriter 'INICIALIZA EL GENERADOR
        GENERADOR.Format = BarcodeFormat.CODE_128
        Try 'GENERA UN BITMAP Y LO PRESENTA EN EL PICTUREBOX
            Dim IMAGEN As Bitmap = New Bitmap(GENERADOR.Write(cod_inventario.Text), view_codigo_barras.PictureBox1.Width, view_codigo_barras.PictureBox1.Height)
            view_codigo_barras.Show()
            view_codigo_barras.PictureBox1.Image = IMAGEN
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button52_Click(sender As Object, e As EventArgs) Handles Button52.Click
        Label152.Visible = True
        Me.Refresh()
        verificarOrdenadores()
        TabControl1.SelectedIndex = 16
    End Sub

    Private Sub Button49_Click(sender As Object, e As EventArgs) Handles Button49.Click
        view_terminales_.Show()
        Me.Enabled = False
    End Sub

    Private Sub TextBox44_Click(sender As Object, e As EventArgs) Handles TextBox44.Click
        Me.Enabled = False
        view_buscar_inventario.concepto = "consulta"
        view_buscar_inventario.Show()
    End Sub

    Private Sub TextBox51_Click(sender As Object, e As EventArgs) Handles TextBox51.Click
        Me.Enabled = False
        view_buscar_inventario.concepto = "consulta_stock"
        view_buscar_inventario.Show()
    End Sub

    Private Sub Button53_Click(sender As Object, e As EventArgs) Handles Button53.Click
        Label152.Visible = True
        Me.Refresh()
        verificarOrdenadores()
    End Sub

    Private Sub Button48_Click(sender As Object, e As EventArgs) Handles Button48.Click
        TabControl1.SelectedIndex = 17
    End Sub

    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function

    Private Sub Button55_Click(sender As Object, e As EventArgs) Handles Button55.Click
        'consultarMora(TextBox53.Text, NumericUpDown2.Value, NumericUpDown3.Value, TextBox41.Text, tipo)
    End Sub

    Private Sub Button56_Click(sender As Object, e As EventArgs) Handles Button56.Click
        If TextBox42.Text <> "" Then
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas actualizar esto?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                If actualizarCiclo(TextBox42.Text) = True Then
                    MsgBox("Datos actualizados!", vbInformation, "RNV")
                Else
                    MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                End If
            End If
        Else
            MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
        End If
    End Sub



    Private Sub LinkLabel11_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        view_cambiar_año.Show()
        Me.Enabled = False
    End Sub

    Private Sub LinkLabel13_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel13.LinkClicked
        If txt_ciclo_curso.ReadOnly = True Then
            txt_ciclo_curso.ReadOnly = False
        Else
            txt_ciclo_curso.ReadOnly = False
        End If
    End Sub

    Private Sub cursoalumno(ByVal sender As Object,
                            ByVal e As KeyPressEventArgs) Handles txt_ciclo_curso.KeyPress, txt_ciclo_alumno.KeyPress

        Dim re As New Regex("[^0-9_\-\b]", RegexOptions.IgnoreCase)
        e.Handled = re.IsMatch(e.KeyChar)

    End Sub

    Private Sub LinkLabel11_LinkClicked_1(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel11.LinkClicked
        If txt_ciclo_alumno.ReadOnly = True Then
            txt_ciclo_alumno.ReadOnly = False
        Else
            txt_ciclo_alumno.ReadOnly = False
        End If
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox4.SelectedIndexChanged
        FlowLayoutPanel1.Controls.Clear()
        If ComboBox4.Text = "Academia NV" Then
            carga_cursos_manageNV(ComboBox4.Text)
        Else
            carga_cursos_manageVNET(ComboBox4.Text)
        End If
    End Sub

    Private Sub Button57_Click(sender As Object, e As EventArgs) Handles Button57.Click
        Me.Enabled = False
        view_asignar_tiempo.Show()
    End Sub

    Private Sub Button50_Click(sender As Object, e As EventArgs) Handles Button50.Click
        Me.Enabled = False
        view_clientes_ciber.Show()
    End Sub

    Private Sub cbox_mes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbox_mes.SelectedIndexChanged
        cantidad.Value = cbox_mes.SelectedIndex + 1
    End Sub

    Private Sub pc_rnv_DoubleClick(sender As Object, e As EventArgs) Handles pc_rnv.DoubleClick
        If IsDataGridViewEmpty(pc_rnv) = False Then
            Dim row As DataGridViewRow = pc_rnv.CurrentRow
            view_opciones_terminal.Text = CStr(row.Cells(2).Value)
            If (CStr(row.Cells(4).Value) <> "DESCONECTADO") Then
                view_opciones_terminal.Label36.ForeColor = Color.Blue
                view_opciones_terminal.Button1.Enabled = True
                view_opciones_terminal.Button3.Enabled = True
                view_opciones_terminal.Button4.Enabled = True
            Else
                view_opciones_terminal.Button1.Enabled = False
                view_opciones_terminal.Button3.Enabled = False
                view_opciones_terminal.Button4.Enabled = False
            End If
            view_opciones_terminal.Label36.Text = CStr(row.Cells(4).Value)
            view_opciones_terminal.Label3.Text = CStr(row.Cells(3).Value)
            view_opciones_terminal.Show()
            Me.Enabled = False
        End If
    End Sub

    Private Sub TextBox53_Click(sender As Object, e As EventArgs) Handles TextBox53.Click
        view_buscar_alum.Show()
        view_buscar_alum.tipo = "mora"
        Me.Enabled = False
    End Sub

    Private Sub AbrirRNVToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirRNVToolStripMenuItem.Click
        ShowInTaskbar = True
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False
        Me.BringToFront()
    End Sub

    Private Sub SincronizarYCerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SincronizarYCerrarToolStripMenuItem.Click
        ShowInTaskbar = True
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False
        Me.Close()
    End Sub

    Private Sub Button54_Click(sender As Object, e As EventArgs) Handles Button54.Click
        Me.Enabled = False
        view_planes.Show()
    End Sub

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            NotifyIcon1.Visible = True
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info
            NotifyIcon1.BalloonTipTitle = "RNV v1.7.0"
            NotifyIcon1.BalloonTipText = "Seguiré por acá"
            NotifyIcon1.ShowBalloonTip(50000)
            'Me.Hide()
            ShowInTaskbar = False
        End If
    End Sub

    Private Sub NotifyIcon1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotifyIcon1.DoubleClick
        'Me.Show()
        ShowInTaskbar = True
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False
    End Sub
End Class