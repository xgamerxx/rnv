﻿Imports System.ComponentModel

Public Class view_new_note
    Private Sub view_new_note_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
        view_notas.Enabled = True
        view_filter_notas.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text <> "" Or RichTextBox1.Text <> "" Then
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esta nota?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Dim send As Boolean = False
                If CheckBox1.Checked = True Then
                    send = True
                Else
                    send = False
                End If
                If salvarNota(TextBox1.Text, RichTextBox1.Text, send, Date.Today.AddDays(+view_principal.maxdias.Value), id.Text) = True Then
                    MsgBox("Nota registada correctamente.", vbInformation, "RNV")

                    If CheckBox1.Checked = True Then
                        view_notas.RichTextBox1.Text = RichTextBox1.Text
                        view_notas.Button1.Enabled = True
                    End If
                    Me.Close()
                Else
                    MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                End If

            End If
        Else
            MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")

        End If
    End Sub

    Private Sub view_new_note_Load(sender As Object, e As EventArgs) Handles Me.Load
        view_notas.Enabled = False
       main.Enabled = False
    End Sub

    Private Sub btn_delete_curso_Click(sender As Object, e As EventArgs) Handles btn_delete_curso.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas eliminar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If borrar_nota(id.Text) = True Then
                MsgBox("Registro eliminado.", vbInformation, "RNV")
                If view_notas.RichTextBox1.Text = RichTextBox2.Text Then
                    view_notas.RichTextBox1.Text = ""
                    view_notas.Button1.Enabled = False
                End If
                Me.Close()
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub
End Class