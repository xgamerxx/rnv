﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV


Public Class view_notas

    Dim s As New cConexion

    Protected Overrides Sub WndProc(ByRef m As Message)
        If (m.Msg = &H112) AndAlso (m.WParam.ToInt32() = &HF010) Then
            Return
        End If
        If (m.Msg = &HA1) AndAlso (m.WParam.ToInt32() = &H2) Then
            Return
        End If
        MyBase.WndProc(m)
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub view_notas_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
       main.Enabled = False
        Me.Enabled = False
        view_edit_nota.Show()

    End Sub

    Private Sub cargarNota()
        '' cargar última nota no vencida.
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta = "SELECT * FROM rnv_notas WHERE show=1"
            Dim cmdx As New OleDbCommand(consulta, cn)
            Dim reader As OleDbDataReader = cmdx.ExecuteReader()
            If reader.Read() Then
                Label1.Text = Convert.ToInt32(reader("id"))
                RichTextBox1.Text = Convert.ToString(reader("contenido"))
            End If
            cn.Close()
        End Using
    End Sub

    Private Sub verificarNotas()
        '' verificar notas vencidas.
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim dt As New OleDbDataAdapter("SELECT * FROM rnv_notas", cn)
            Dim ds As New DataSet
            dt.Fill(ds)
            For Each dtr As DataRow In ds.Tables(0).Rows
                Dim id = Convert.ToString(dtr(("id")))
                Dim expira = Convert.ToString(dtr(("expira")))
                If expira = Date.Today.ToString("dd/MM/yyyy") Then
                    Dim cAdapter As New OleDbDataAdapter
                    cAdapter.InsertCommand = New OleDbCommand("UPDATE rnv_notas SET show=0 WHERE id = " & id & " ", cn)
                    cAdapter.InsertCommand.Connection = cn
                    cAdapter.InsertCommand.ExecuteNonQuery()
                End If
            Next
        End Using
    End Sub

    Private Sub view_notas_Load(sender As Object, e As EventArgs) Handles Me.Load
        verificarNotas()
        cargarNota()
        If RichTextBox1.Text <> "" Then
            Button1.Enabled = True
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
       main.Enabled = False
        Me.Enabled = False
        view_new_note.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
       main.Enabled = False
        Me.Enabled = False
        view_filter_notas.Show()
    End Sub
End Class