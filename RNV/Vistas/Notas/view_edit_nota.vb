﻿Imports System.ComponentModel

Public Class view_edit_nota
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If RichTextBox1.Text <> "" Then
            Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas editar esta nota?", "RNV", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                If editarNota(view_notas.Label1.Text, RichTextBox1.Text) = True Then
                    MsgBox("Nota editada correctamente.", vbInformation, "RNV")
                    view_notas.RichTextBox1.Text = RichTextBox1.Text
                    Me.Close()
                Else
                    MsgBox("Ocurrió un error.")
                End If
            End If
        Else
            MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
        End If
    End Sub

    Private Sub view_edit_nota_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
        view_notas.Enabled = True
    End Sub

    Private Sub view_edit_nota_Load(sender As Object, e As EventArgs) Handles Me.Load
        RichTextBox1.Text = view_notas.RichTextBox1.Text
    End Sub
End Class