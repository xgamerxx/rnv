﻿Imports coreRNV
Imports System.Data.OleDb
Imports System.ComponentModel

Public Class view_filter_notas

    Dim s As New cConexion

    Sub filterAll(palabra As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "SELECT id, titulo, contenido, show, expira FROM rnv_notas WHERE titulo LIKE '%" & palabra & "%'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(1).HeaderText = "Titulo"
                gridProductos.Columns(2).HeaderText = "Contenido"
                gridProductos.Columns(2).Width = 200
                gridProductos.Columns(4).HeaderText = "Expira en principal"
                gridProductos.Columns(3).HeaderText = "Mostrado en principal"
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Sub showAll()
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "SELECT id, titulo, contenido, show, expira FROM rnv_notas"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(1).HeaderText = "Titulo"
                gridProductos.Columns(2).HeaderText = "Contenido"
                gridProductos.Columns(2).Width = 200
                gridProductos.Columns(4).HeaderText = "Expira en principal"
                gridProductos.Columns(3).HeaderText = "Mostrado en principal"
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function

    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick
        If IsDataGridViewEmpty(gridProductos) = False Then

            Dim row As DataGridViewRow = gridProductos.CurrentRow
            view_new_note.id.Text = CStr(row.Cells(0).Value)
            view_new_note.TextBox1.Text = CStr(row.Cells(1).Value)
            If CStr(row.Cells(3).Value) = 0 Then
                view_new_note.CheckBox1.Checked = False
            Else
                view_new_note.CheckBox1.Checked = True
            End If
            view_new_note.RichTextBox1.Text = CStr(row.Cells(2).Value)
            view_new_note.RichTextBox2.Text = CStr(row.Cells(2).Value)
            view_new_note.btn_delete_curso.Visible = True
            view_new_note.Show()
            Me.Close()
        End If
    End Sub

    Private Sub view_filter_notas_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        view_notas.Enabled = True
       main.Enabled = True
    End Sub

    Private Sub view_filter_notas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        showAll()
    End Sub

    Private Sub txtbus_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        Try
            filterAll(txtbus.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class