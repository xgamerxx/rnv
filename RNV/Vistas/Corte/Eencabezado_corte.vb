﻿Public Class Eencabezado_corte
    'Clase propiedades de encabezado de la factura
    Public Property codigo_reporte() As String
    Public Property total() As String
    Public Property total_2() As String
    Public Property realizado_por() As String
    Public Property calculado() As String
    Public property diferencia() As String
    Public Property logo() As String
    Public Property comentario() As String
    Public Property fecha() As String
    'Creamos una lista con una nueva Instancia de la clase personas cobro
    'esta lista contendra el detalle del reporte de cobro
    Public Detalle As New List(Of Epersonas_cobro)()
End Class
