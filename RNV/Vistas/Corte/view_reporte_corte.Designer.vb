﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class view_reporte_corte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(view_reporte_corte))
        Me.rv_factura = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.SuspendLayout()
        '
        'rv_factura
        '
        Me.rv_factura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rv_factura.Location = New System.Drawing.Point(0, 0)
        Me.rv_factura.Name = "rv_factura"
        Me.rv_factura.ShowExportButton = False
        Me.rv_factura.ShowStopButton = False
        Me.rv_factura.Size = New System.Drawing.Size(776, 529)
        Me.rv_factura.TabIndex = 2
        '
        'view_reporte_corte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 529)
        Me.Controls.Add(Me.rv_factura)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "view_reporte_corte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Corte | RNV"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents rv_factura As Microsoft.Reporting.WinForms.ReportViewer
End Class
