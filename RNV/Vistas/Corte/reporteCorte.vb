﻿Imports Microsoft.Reporting.WinForms
Imports System.Drawing.Printing
Imports System.IO
Imports coreRNV
Imports System.ComponentModel

Public Class reporteCorte
    'Encabezado factura y detalle (articulos)
    Public Invoice As New List(Of Eencabezado_corteGeneral)()
    'Cree las propiedades publicas Titulo y Empresa

    Public calculado, caja, diferencia As Double
    Private Sub InvoiceGenerate()
        Dim invoice As New Eencabezado_corteGeneral()

        ' **** TODA LA DATA ****************************************************

        invoice.codigo_corte = view_corte_general.codigo.Text
        invoice.emitido_por = view_corte_general.TextBox1.Text

        invoice.total_venta_nv = view_corte_general.TextBox2.Text
        invoice.total_calculado_nv = view_corte_general.TextBox16.Text
        invoice.total_caja_nv = view_corte_general.TextBox11.Text

        invoice.total_venta_vnet = view_corte_general.TextBox3.Text
        invoice.total_calculado_vnet = view_corte_general.TextBox15.Text
        invoice.total_caja_vnet = view_corte_general.TextBox10.Text

        invoice.total_venta_accesorios = "Q " & FormatNumber(view_corte_general.TextBox4.Text)
        invoice.total_calculado_accesorios = "Q " & FormatNumber(view_corte_general.TextBox14.Text)
        invoice.total_caja_accesorios = "Q " & FormatNumber(view_corte_general.TextBox9.Text)

        invoice.total_venta_pelis = "Q " & FormatNumber(view_corte_general.TextBox5.Text)
        invoice.total_calculado_pelis = "Q " & FormatNumber(view_corte_general.TextBox13.Text)
        invoice.total_caja_pelis = "Q " & FormatNumber(view_corte_general.TextBox8.Text)

        invoice.total_venta_internet = "Q " & FormatNumber(view_corte_general.TextBox6.Text)
        invoice.total_calculado_internet = "Q " & FormatNumber(view_corte_general.TextBox12.Text)
        invoice.total_caja_internet = "Q " & FormatNumber(view_corte_general.TextBox7.Text)

        invoice.total_venta_extras = "Q " & FormatNumber(view_corte_general.TextBox19.Text)
        invoice.total_calculado_extras = "Q " & FormatNumber(view_corte_general.TextBox17.Text)
        invoice.total_caja_extras = "Q " & FormatNumber(view_corte_general.TextBox18.Text)

        '''''''' ********* OPERAR
        Dim c2, c3, c4, c5, c6, c17 As Double
        Dim c11, c10, c9, c8, c7, c18 As Double
        Dim c16, c15, c14, c13, c12, c19 As Double

        c2 = FormatNumber(view_corte_general.TextBox2.Text, 2)
        c3 = FormatNumber(view_corte_general.TextBox3.Text, 2)
        c4 = FormatNumber(view_corte_general.TextBox4.Text, 2)
        c5 = FormatNumber(view_corte_general.TextBox5.Text, 2)
        c6 = FormatNumber(view_corte_general.TextBox6.Text, 2)
        c17 = FormatNumber(view_corte_general.TextBox17.Text, 2)


        c11 = FormatNumber(view_corte_general.TextBox11.Text, 2)
        c10 = FormatNumber(view_corte_general.TextBox10.Text, 2)
        c9 = FormatNumber(view_corte_general.TextBox9.Text, 2)
        c8 = FormatNumber(view_corte_general.TextBox8.Text, 2)
        c7 = FormatNumber(view_corte_general.TextBox7.Text, 2)
        c18 = FormatNumber(view_corte_general.TextBox18.Text, 2)

        c16 = FormatNumber(view_corte_general.TextBox16.Text, 2)
        c15 = FormatNumber(view_corte_general.TextBox15.Text, 2)
        c14 = FormatNumber(view_corte_general.TextBox14.Text, 2)
        c13 = FormatNumber(view_corte_general.TextBox13.Text, 2)
        c12 = FormatNumber(view_corte_general.TextBox12.Text, 2)
        c19 = FormatNumber(view_corte_general.TextBox19.Text, 2)

        calculado = c2 + c3 + c4 + c5 + c6 + c17
        caja = c11 + c10 + c9 + c8 + c7 + c18
        diferencia = c16 + c15 + c14 + c13 + c12 + c19

        invoice.total_calculado = "Q " & FormatNumber(diferencia, 2)
        invoice.total_caja = "Q " & FormatNumber(caja, 2)
        invoice.total_venta = "Q " & FormatNumber(calculado, 2)


        '**********************************************************************

        If view_corte_general.RichTextBox1.Text <> "" Then
            invoice.comentario = "***>>> " & view_corte_general.RichTextBox1.Text & " <<<***"
        End If
        invoice.fecha = Format(DateTime.Now, "dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString()

        Me.Invoice.Add(invoice)
    End Sub

    Private Function guardar_pdf()
        Try
            Dim ps As PageSettings = rv_factura.GetPageSettings
            Dim ruta_exacta As String = System.Environment.CurrentDirectory & "\recibos\corte_general_" & view_corte.codigoCorte & ".pdf"
            Dim byteViewer As Byte() = rv_factura.LocalReport.Render("PDF")
            Dim pdf_dialogo As New SaveFileDialog()
            pdf_dialogo.Filter = "*PDF files (*.pdf)|*.pdf"
            pdf_dialogo.FilterIndex = 2
            pdf_dialogo.RestoreDirectory = True
            Dim factura_ As New FileStream(ruta_exacta, FileMode.Create)
            factura_.Write(byteViewer, 0, byteViewer.Length)
            factura_.Close()
        Catch ex As Exception
        End Try
        Return True
    End Function

    Private Sub view_recibo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            InvoiceGenerate()
            rv_factura.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
            rv_factura.LocalReport.EnableExternalImages = True
            rv_factura.DocumentMapCollapsed = True
            rv_factura.LocalReport.ReportPath = System.Environment.CurrentDirectory & "\reporte_corte_general.rdlc"
            rv_factura.LocalReport.DataSources.Clear()
            'Establecemos los margenes de la factura
            Dim instance As New PageSettings()
            Dim value As New Margins(31, 0, 15, 5)
            instance.Margins = value
            rv_factura.SetPageSettings(instance)
            'Limpiemos el DataSource del informe
            rv_factura.LocalReport.DataSources.Clear()
            'Establezcamos la lista como Datasource del informe
            rv_factura.LocalReport.DataSources.Add(New ReportDataSource("Encabezado", Invoice))
            'rv_factura.LocalReport.DataSources.Add(New ReportDataSource("Detalle", Detail))
            'Hagamos un refresh al reportViewer
            rv_factura.RefreshReport()
           main.Label43.Visible = False
            Dim worker As New BackgroundWorker
            worker.WorkerSupportsCancellation = True
            worker.WorkerReportsProgress = True
            If worker.IsBusy Then
                worker.CancelAsync()
            Else
                worker.RunWorkerAsync(guardar_pdf())
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.rv_factura.RefreshReport()
    End Sub

    Private Sub view_recibo_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Me.Hide()
       main.Enabled = True
    End Sub
End Class