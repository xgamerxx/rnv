﻿
Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV

Public Class view_corte_general
    Dim s As New cConexion
    Public codigoCorte As String


    Private Sub gardarGeneral()
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = ""
            Dim cAdapter As New OleDbDataAdapter
            Dim today As String = Format(DateTime.Now, "dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString()
            codigoCorte = cadena_aleatoria(9)
            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_corte_general (codigo_corte, emitido_por, calculado_vnet, caja_vnet, diferencia_vnet, calculado_nv, caja_nv, diferencia_nv, calculado_internet, caja_internet, diferencia_internet, calculado_pelis, caja_pelis, diferencia_pelis, calculado_accesorios, caja_accesorios, diferencia_accesorios, fecha, fecha_2, comentario, calculado, caja, diferencia) VALUES ('" & codigoCorte & "', '" & TextBox1.Text & "', '" & TextBox3.Text & "', '" & TextBox10.Text & "', '" & TextBox15.Text & "',  '" & TextBox2.Text & "','" & TextBox11.Text & "','" & TextBox16.Text & "', '" & TextBox6.Text & "','" & TextBox7.Text & "','" & TextBox12.Text & "', '" & TextBox5.Text & "', '" & TextBox8.Text & "', '" & TextBox13.Text & "', '" & TextBox4.Text & "','" & TextBox9.Text & "','" & TextBox14.Text & "', '" & Date.Today.ToString("dd/MM/yyy") & "', '" & Date.Today.ToString("dd/MM/yyyy") & "', '" & RichTextBox1.Text & "', '" & reporteCorte.calculado & "', '" & reporteCorte.caja & "', '" & reporteCorte.diferencia & "' )")
            cAdapter.InsertCommand.Connection = cn
            cAdapter.InsertCommand.ExecuteNonQuery()
           main.verificarTimer()
            cn.Close()
        End Using
    End Sub



    Dim today = DateTime.Now.ToString("dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString()
    Public Sub getPagosNV()

        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()

            Dim dt As New OleDbDataAdapter("SELECT calculado, caja, diferencia, fecha_2 FROM rnv_corte WHERE giro='Academia NV' AND cut=0", cn)
            Dim ds As New DataSet
            dt.Fill(ds)
            Dim calculado, caja, diferencia As Double
            For Each dtr As DataRow In ds.Tables(0).Rows
                Dim fhoy As String = Date.Today.ToString("dd/MM/yyy")
                Try
                    If (Convert.ToString(dtr(("fecha_2"))) = fhoy) Then
                        calculado += (dtr(("calculado")))
                        caja += (dtr(("caja")))
                        diferencia += (dtr(("diferencia")))
                    End If
                Catch ex As Exception
                End Try
            Next
            TextBox2.Text = "Q " & FormatNumber(calculado)
            TextBox11.Text = "Q " & FormatNumber(caja)
            TextBox16.Text = "Q " & FormatNumber(diferencia)
            cn.Close()
        End Using
    End Sub


    Public Sub getPagosVNET()

        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()

            Dim dt As New OleDbDataAdapter("SELECT calculado, caja, diferencia, fecha_2 FROM rnv_corte WHERE giro='Vision NET' AND cut=0", cn)
            Dim ds As New DataSet
            dt.Fill(ds)
            Dim calculado, caja, diferencia As Double
            For Each dtr As DataRow In ds.Tables(0).Rows
                Dim fhoy As String = Date.Today.ToString("dd/MM/yyy")
                Try
                    If (Convert.ToString(dtr(("fecha_2"))) = fhoy) Then
                        calculado += (dtr(("calculado")))
                        caja += (dtr(("caja")))
                        diferencia += (dtr(("diferencia")))
                    End If
                Catch ex As Exception
                End Try
            Next
            TextBox3.Text = "Q " & FormatNumber(calculado)
            TextBox10.Text = "Q " & FormatNumber(caja)
            TextBox15.Text = "Q " & FormatNumber(diferencia)
            cn.Close()
        End Using
    End Sub

    Public Sub getPagosINVENTARIO()

        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()

            Dim dt As New OleDbDataAdapter("SELECT calculado, caja, diferencia, fecha_2 FROM rnv_corte WHERE giro='Inventario NV' AND cut=0", cn)
            Dim ds As New DataSet
            dt.Fill(ds)
            Dim calculado, caja, diferencia As Double
            For Each dtr As DataRow In ds.Tables(0).Rows
                Dim fhoy As String = Date.Today.ToString("dd/MM/yyy")
                Try
                    If (Convert.ToString(dtr(("fecha_2"))) = fhoy) Then
                        calculado += (dtr(("calculado")))
                        caja += (dtr(("caja")))
                        diferencia += (dtr(("diferencia")))
                    End If
                Catch ex As Exception
                End Try
            Next
            TextBox4.Text = "Q " & FormatNumber(calculado)
            TextBox9.Text = "Q " & FormatNumber(caja)
            TextBox14.Text = "Q " & FormatNumber(diferencia)
            cn.Close()
        End Using
    End Sub



    Private Sub view_corte_general_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        view_corte.Enabled = True
    End Sub

    Private Sub limpiar()
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        RichTextBox1.Text = ""
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas generar este reporte?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If TextBox1.Text <> "" And TextBox4.Text <> "" And TextBox5.Text <> "" And TextBox6.Text <> "" And TextBox7.Text <> "" And TextBox8.Text <> "" And TextBox9.Text <> "" And TextBox19.Text <> "" And TextBox18.Text <> "" Then
                Label25.Visible = True
                MsgBox("Reporte archivado correctamente.", vbInformation, "RNV")
                Me.Refresh()
                Me.Update()
                reporteCorte.Show()
                gardarGeneral()
                actualizar2()
                limpiar()
                Label25.Visible = False
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub view_corte_general_Load(sender As Object, e As EventArgs) Handles Me.Load
        codigo.Text = cadena_aleatoria(9)
        getPagosVNET()
        getPagosNV()
        getPagosINVENTARIO()
    End Sub

    Private Sub TextBox9_TextChanged(sender As Object, e As EventArgs) Handles TextBox9.TextChanged
        Try
            Dim calculado, ingresado As Double
            calculado = TextBox4.Text
            ingresado = TextBox9.Text
            If ingresado < calculado Then
                TextBox14.ForeColor = Color.Red
                TextBox14.Text = "Q" & FormatNumber(TextBox9.Text - TextBox4.Text)
            Else
                TextBox14.ForeColor = Color.Black
                TextBox14.Text = "Q" & FormatNumber(TextBox9.Text - TextBox4.Text)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox4_TextChanged(sender As Object, e As EventArgs) Handles TextBox4.TextChanged
        TextBox9_TextChanged(sender, e)
    End Sub

    Private Sub TextBox8_TextChanged(sender As Object, e As EventArgs) Handles TextBox8.TextChanged
        Try
            Dim calculado, ingresado As Double
            calculado = TextBox5.Text
            ingresado = TextBox8.Text
            If ingresado < calculado Then
                TextBox13.ForeColor = Color.Red
                TextBox13.Text = "Q" & FormatNumber(TextBox8.Text - TextBox5.Text)
            Else
                TextBox13.ForeColor = Color.Black
                TextBox13.Text = "Q" & FormatNumber(TextBox8.Text - TextBox5.Text)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.TextChanged
        TextBox8_TextChanged(sender, e)
    End Sub

    Private Sub TextBox7_TextChanged(sender As Object, e As EventArgs) Handles TextBox7.TextChanged
        Try
            Dim calculado, ingresado As Double
            calculado = TextBox6.Text
            ingresado = TextBox7.Text
            If ingresado < calculado Then
                TextBox12.ForeColor = Color.Red
                TextBox12.Text = "Q" & FormatNumber(TextBox7.Text - TextBox6.Text)
            Else
                TextBox12.ForeColor = Color.Black
                TextBox12.Text = "Q" & FormatNumber(TextBox7.Text - TextBox6.Text)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Me.Enabled = False
        view_protected.Show()
        view_protected.ventana = "viewcortegeneral"
        view_protected.BringToFront()
    End Sub



    Private Sub txt_precio4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox4.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox4.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub txt_precio5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox5.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox5.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub txt_precio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox6.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox6.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub txt_precio7_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox7.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox7.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub txt_precio8_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox8.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox8.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub txt_precio9_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox9.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox9.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub TextBox18_TextChanged(sender As Object, e As EventArgs) Handles TextBox18.TextChanged
        Try
            Dim calculado, ingresado As Double
            calculado = TextBox19.Text
            ingresado = TextBox18.Text
            If ingresado < calculado Then
                TextBox17.ForeColor = Color.Red
                TextBox17.Text = "Q" & FormatNumber(TextBox18.Text - TextBox19.Text)
            Else
                TextBox17.ForeColor = Color.Black
                TextBox17.Text = "Q" & FormatNumber(TextBox18.Text - TextBox19.Text)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub TextBox19_TextChanged(sender As Object, e As EventArgs) Handles TextBox19.TextChanged
        TextBox18_TextChanged(sender, e)
    End Sub

    Private Sub TextBox6_TextChanged(sender As Object, e As EventArgs) Handles TextBox6.TextChanged
        TextBox7_TextChanged(sender, e)
    End Sub
End Class