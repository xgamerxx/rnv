﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV

Public Class view_corte
    Dim s As New cConexion
    Public codigoCorte As String

    Function guardarCorte(giro As String, operador As String, caja As String, calculado As String, diferencia As String, retiro As String, total As String, comentario As String)
        Using cn As New OleDbConnection(s.sQuery)
            cn.Open()
            Dim consulta As String = ""
            Dim cAdapter As New OleDbDataAdapter
            Dim today As String = Format(DateTime.Now, "dd/MM/yyyy") & " " & DateTime.Now.ToShortTimeString()
            codigoCorte = cadena_aleatoria(9)
            cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_corte (codigo_corte, emitido_por, calculado, diferencia, caja, retiro, fecha, giro, comentario, fecha_2) VALUES ('" & codigoCorte & "', '" & operador & "', '" & calculado & "', '" & diferencia & "', '" & caja & "', '" & retiro & "', '" & today & "', '" & giro & "', '" & comentario & "', '" & Date.Today.ToString("dd/MM/yyy") & "')")
            cAdapter.InsertCommand.Connection = cn
            cAdapter.InsertCommand.ExecuteNonQuery()
           main.verificarTimer()
            cn.Close()
            Return True
        End Using
        Return False
    End Function

    Private Sub view_corte_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub view_corte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        giro.SelectedIndex = 0
    End Sub

    Private Sub giro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles giro.SelectedIndexChanged
        If giro.Text <> "-Selecciona para-" Then
            Dim total As Double = 0
            Dim totalretiro As Double = 0
            '' cargar el total de dinero que hay en caja para el giro seleccionado
            dt_productos.Rows.Clear()
            Try
                Using cn As New OleDbConnection(s.sQuery)
                    cn.Open()
                    Dim dt2 As New OleDbDataAdapter("SELECT * FROM rnv_corte WHERE giro = '" & giro.Text & "' AND fecha_2 = '" & Date.Today.ToString("dd/MM/yyyy") & "'", cn)
                    Dim ds2 As New DataSet
                    dt2.Fill(ds2)
                    For Each dtr As DataRow In ds2.Tables(0).Rows
                        totalretiro += Convert.ToDouble(FormatNumber(dtr(("calculado"))))
                    Next
                    Dim str As Date = Date.Today.ToString("dd/MM/yyyy")
                    Dim str2 As String
                    Dim convertidor As DateTime
                    Dim hoy As String
                    Try
                        str2 = DateTime.Parse(str).ToString("yyyy/MM/dd")
                        convertidor = str2
                        hoy = DateTime.ParseExact(convertidor, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    Catch ex As Exception
                        str2 = DateTime.Parse(str).ToString("MM/dd/yyyy")
                        hoy = DateTime.ParseExact(str2, "M/d/yyyy",
                        Globalization.CultureInfo.InstalledUICulture)
                    End Try
                    Dim dt As New OleDbDataAdapter("SELECT * 
                                                 FROM rnv_cobros
                                                 INNER JOIN rnv_recibos_cobros ON rnv_recibos_cobros.id_recibo = rnv_cobros.id
                                                 WHERE rnv_cobros.giro = '" & giro.Text & "' AND rnv_cobros.fecha = #" & hoy & "#", cn)
                    Dim ds As New DataSet
                    dt.Fill(ds)
                    For Each dtr As DataRow In ds.Tables(0).Rows
                        dt_productos.Rows.Add(dtr(("importe")))
                    Next
                    cn.Close()
                End Using
                For Each row As DataGridViewRow In dt_productos.Rows
                    total = total + Val(row.Cells(0).Value)
                Next
                TextBox2.Text = "Q" & FormatNumber(total - totalretiro)
                TextBox4.Text = "Q" & FormatNumber(0)
                TextBox5.Text = "Q" & FormatNumber(0)
                TextBox3.ForeColor = Color.Red
                TextBox3.Text = "-Q" & FormatNumber(total - totalretiro)
                total = 0
                totalretiro = 0
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            limpiarData()
        End If

    End Sub

    Private Sub txt_precio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox6.KeyPress
        Dim FullStop As Char
        FullStop = "."

        ' if the '.' key was pressed see if there already is a '.' in the string
        ' if so, dont handle the keypress
        If e.KeyChar = FullStop And TextBox6.Text.IndexOf(FullStop) <> -1 Then
            e.Handled = True
            Return
        End If

        ' If the key aint a digit
        If Not Char.IsDigit(e.KeyChar) Then
            ' verify whether special keys were pressed
            ' (i.e. all allowed non digit keys - in this example
            ' only space and the '.' are validated)
            If (e.KeyChar <> FullStop) And
               (e.KeyChar <> Convert.ToChar(Keys.Back)) Then
                ' if its a non-allowed key, dont handle the keypress
                e.Handled = True
                Return
            End If
        End If
    End Sub

    Private Sub TextBox6_TextChanged(sender As Object, e As EventArgs) Handles TextBox6.TextChanged, TextBox3.TextChanged
        Try
            Dim calculado, ingresado As Double
            calculado = TextBox2.Text
            ingresado = TextBox6.Text
            If ingresado < calculado Then
                TextBox3.ForeColor = Color.Red
                TextBox3.Text = "Q" & FormatNumber(TextBox6.Text - TextBox2.Text)
            Else
                TextBox3.ForeColor = Color.Black
                TextBox3.Text = "Q" & FormatNumber(TextBox6.Text - TextBox2.Text)
            End If
            TextBox4.Text = "Q" & FormatNumber(TextBox6.Text)
            TextBox5.Text = "Q" & FormatNumber(TextBox6.Text)
        Catch ex As Exception

        End Try
    End Sub

    Sub limpiarData()
        giro.SelectedIndex = 0
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        grid_datos_cobros.Close()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas generar este reporte?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If giro.Text <> "-Selecciona corte para-" Then
                If TextBox1.Text <> "" Or TextBox6.Text <> "" Then
                    If giro.Text <> "Inventario NV" Then
                        If guardarCorte(giro.Text, TextBox1.Text, TextBox6.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, RichTextBox1.Text) = True Then
                            Label25.Visible = True
                            MsgBox("Reporte archivado correctamente.", vbInformation, "RNV")

                            grid_datos_cobros.Show()
                            consulta_corte()
                            view_reporte_corte.Show()
                            actualizar()
                            Label25.Visible = False
                            limpiarData()
                        Else
                            MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                        End If
                    Else
                        If guardarCorte(giro.Text, TextBox1.Text, TextBox6.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, RichTextBox1.Text) = True Then
                            Label25.Visible = True
                            MsgBox("Reporte archivado correctamente.", vbInformation, "RNV")

                            grid_datos_cobros.Show()
                            consulta_corte_inventario()
                            view_reporte_corte_inventario.Show()
                            actualizar()
                            Label25.Visible = False
                            limpiarData()
                        Else
                            MsgBox("Ocurrió un error.", vbExclamation, "RNV")
                        End If
                    End If
                Else
                    MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
                End If
            Else
                MsgBox("Los campos marcados con (*) son obligatorios.", vbExclamation, "RNV")
            End If
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Enabled = False
        view_corte_general.Show()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Me.Enabled = False
        view_protected.Show()
        view_protected.ventana = "viewcorte"
        view_protected.BringToFront()
    End Sub
End Class