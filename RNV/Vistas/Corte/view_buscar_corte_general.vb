﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class view_buscar_corte_general
    Dim s As New cConexion
    Public consulta As Boolean = False

    Sub filtrar(palabra As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "SELECT * FROM rnv_corte_general WHERE ciclo = " & main.TextBox42.Text & " AND codigo_corte LIKE '%" & palabra & "%' OR comentario LIKE '% " & palabra & " %'"
                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(1).HeaderText = "Código de corte"
                gridProductos.Columns(1).Width = 200
                gridProductos.Columns(2).HeaderText = "Emitido por"
                gridProductos.Columns(3).HeaderText = "Para"
                gridProductos.Columns(4).HeaderText = "Fecha"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Giro"
                gridProductos.Columns(5).Width = 170
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        view_corte_general.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        filtrar(txtbus.Text)
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick
        Try
            If IsDataGridViewEmpty(gridProductos) = False Then
                If consulta = True Then
                    Dim row As DataGridViewRow = gridProductos.CurrentRow
                   main.codigorecorte.Text = CStr(row.Cells(1).Value)
                    Me.Close()
                Else
                    Dim row As DataGridViewRow = gridProductos.CurrentRow
                    System.Diagnostics.Process.Start(My.Application.Info.DirectoryPath & "\recibos\corte_general_" & CStr(row.Cells(1).Value) & ".pdf")

                End If
            Else
                If consulta = False Then
                    MsgBox("PDF inexistente.", vbExclamation)
                End If
            End If
        Catch ex As Exception
            '' es poco probable que tire error
        End Try
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        view_corte.Enabled = True
       main.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub
End Class