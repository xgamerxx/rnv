﻿


Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class view_buscar_alum
    Dim s As New cConexion
    Public tipo

    Sub filtrar_vnet(palabra As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "select t_id, codigo_alumno, nombres_alumno, direccion, telefono_1, telefono_2, telefono_3, nombres_padre, nombres_madre, fecha_nacimiento_alumno, fecha_inscripcion, rnv_alumnos.id, rnv_alumnos.correo, rnv_alumnos.ciclo FROM rnv_alumnos WHERE mes_pago_vnet > 0 AND nombres_alumno LIKE '%" & palabra & "%' OR codigo_alumno LIKE '%" & palabra & "%'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)

                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "Código sistema"
                gridProductos.Columns(1).HeaderText = "Código personal"
                gridProductos.Columns(2).HeaderText = "Nombre completo"
                gridProductos.Columns(3).HeaderText = "Dirección"
                gridProductos.Columns(4).HeaderText = "Telefono 1"
                gridProductos.Columns(5).HeaderText = "Telefono 2"
                gridProductos.Columns(6).HeaderText = "Telefono 3"
                gridProductos.Columns(7).HeaderText = "Nombres (Padre)"
                gridProductos.Columns(8).HeaderText = "Nombres (Madre)"
                gridProductos.Columns(9).HeaderText = "Fecha nacimiento"
                gridProductos.Columns(10).HeaderText = "Fecha inscripción"
                gridProductos.Columns(11).HeaderText = "ID"
                gridProductos.Columns(12).HeaderText = "Correo"
                gridProductos.Columns(13).HeaderText = "Ciclo"

            End Using
        Catch ex As Exception
        End Try
    End Sub

    Sub filtrar(palabra As String)
        Try
            Using cn As New OleDbConnection(s.sQuery)
                Dim consulta As String = "select t_id, codigo_alumno, nombres_alumno, direccion, telefono_1, telefono_2, telefono_3, nombres_padre, nombres_madre, fecha_nacimiento_alumno, fecha_inscripcion, rnv_alumnos.id, rnv_alumnos.correo, rnv_alumnos.ciclo FROM rnv_alumnos WHERE  mes_pago_nv > 0 AND ciclo = " & main.TextBox42.Text & " AND nombres_alumno LIKE '%" & palabra & "%' OR codigo_alumno LIKE '%" & palabra & "%'"

                Dim cmd As New OleDbCommand(consulta, cn)
                Dim adaptador As New OleDbDataAdapter(cmd)

                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "Código sistema"
                gridProductos.Columns(1).HeaderText = "Código personal"
                gridProductos.Columns(2).HeaderText = "Nombre completo"
                gridProductos.Columns(3).HeaderText = "Dirección"
                gridProductos.Columns(4).HeaderText = "Telefono 1"
                gridProductos.Columns(5).HeaderText = "Telefono 2"
                gridProductos.Columns(6).HeaderText = "Telefono 3"
                gridProductos.Columns(7).HeaderText = "Nombres (Padre)"
                gridProductos.Columns(8).HeaderText = "Nombres (Madre)"
                gridProductos.Columns(9).HeaderText = "Fecha nacimiento"
                gridProductos.Columns(10).HeaderText = "Fecha inscripción"
                gridProductos.Columns(11).HeaderText = "ID"
                gridProductos.Columns(12).HeaderText = "Correo"
                gridProductos.Columns(13).HeaderText = "Ciclo"

            End Using
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
       main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        If txtbus.Text <> "" Then
            If ComboBox1.SelectedIndex = 0 Then
                filtrar(txtbus.Text)
            Else
                filtrar_vnet(txtbus.Text)
            End If
        End If
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick

        If IsDataGridViewEmpty(gridProductos) = False Then
            Dim row As DataGridViewRow = gridProductos.CurrentRow
            If tipo = "mora" Then
                main.TextBox53.Text = CStr(row.Cells(0).Value)
                Me.Close()
            Else
                main.txt_id.Text = CStr(row.Cells(0).Value)
                main.txt_cod.Text = CStr(row.Cells(1).Value)
                main.txt_nombres.Text = CStr(row.Cells(2).Value)
                main.txt_direccion.Text = CStr(row.Cells(3).Value)
                main.txt_tel1.Text = CStr(row.Cells(4).Value)
                main.txt_tel2.Text = CStr(row.Cells(5).Value)
                main.txt_tel3.Text = CStr(row.Cells(6).Value)
                main.txt_padres.Text = CStr(row.Cells(7).Value)
                main.txt_madres.Text = CStr(row.Cells(8).Value)
                main.txt_nac.Text = CStr(row.Cells(9).Value)
                main.txt_insc.Text = CStr(row.Cells(10).Value)
                main.txt_realID.Text = CInt(row.Cells(11).Value)
                Try
                    main.txt_correo.Text = IIf(CStr(row.Cells(12).Value) Is DBNull.Value, "", CStr(row.Cells(12).Value))
                    main.txt_ciclo_alumno.Text = CInt(row.Cells(12).Value)
                Catch ex As Exception
                End Try
                main.FlowLayoutPanel1.Controls.Clear()
                Using cn As New OleDbConnection(s.sQuery)
                    cn.Open()
                    Dim objAdapter As New OleDbDataAdapter("
                 select rnv_cursos.id, rnv_cursos.nombre_curso  from ( rnv_alumnos INNER JOIN  rnv_alumnos_cursos ON rnv_alumnos_cursos.id_alumno = rnv_alumnos.id) INNER JOIN rnv_cursos ON rnv_cursos.id = rnv_alumnos_cursos.id_curso WHERE rnv_alumnos.id = " & CInt(row.Cells(11).Value) & "", cn)
                    Dim objDataSet As New DataSet
                    Dim tipo As String = "", nombre_beneficio As String = ""
                    objAdapter.Fill(objDataSet, "flippy_niveles.nombre_nivel")
                    Dim i As Integer
                    For i = 0 To objDataSet.Tables(0).Rows.Count - 1
                        Dim maquinaCheck As New CheckBox
                        With maquinaCheck
                            .Name = objDataSet.Tables(0).Rows(i).Item(0)
                            .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                            .Checked = True
                            .Enabled = False
                            .Text = objDataSet.Tables(0).Rows(i).Item(1)
                        End With
                        main.FlowLayoutPanel1.Controls.Add(maquinaCheck)
                    Next
                End Using
                main.Enabled = True
                main.ComboBox4.Enabled = False
                main.btnDelete_al.Visible = True
                main.reasig.Visible = True
                Me.Close()

            End If
        End If

    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub

    Private Sub view_buscar_alum_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.SelectedIndex = 0
    End Sub
End Class