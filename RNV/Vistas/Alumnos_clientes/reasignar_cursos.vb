﻿Imports RNV
Imports coreRNV
Imports System.Data.OleDb
Imports System.ComponentModel

Public Class reasignar_cursos
    Dim s As New cConexion

    Sub reasignarNV()
        Try
            Using cn As New OleDbConnection(s.sQuery)

                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT id, nombre_curso from rnv_cursos WHERE ciclo = " & main.TextBox42.Text & " AND cobro_para = '" & ComboBox4.Text & "'", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                Dim i As Integer
                i = 0
                For Each dtr As DataRow In ds.Tables(0).Rows
                        Dim maquinaCheck As New CheckBox
                        With maquinaCheck
                            .Name = CType(dtr("id"), Integer)
                            .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                            .Text = CType(dtr("nombre_curso"), String)
                        End With
                        FlowLayoutPanel1.Controls.Add(maquinaCheck)
                        i = i + 1
                    Next
                    cn.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Sub reasignarVNET()
        Try
            Using cn As New OleDbConnection(s.sQuery)
                cn.Open()
                Dim dt As New OleDbDataAdapter("SELECT id, nombre_curso from rnv_cursos WHERE cobro_para = '" & ComboBox4.Text & "'", cn)
                Dim ds As New DataSet
                dt.Fill(ds)
                Dim i As Integer
                i = 0
                For Each dtr As DataRow In ds.Tables(0).Rows
                    Dim maquinaCheck As New CheckBox
                    With maquinaCheck
                        .Name = CType(dtr("id"), Integer)
                        .Location = New Point(35 + ((.Left + .Height + 100)) * i, 20)
                        .Text = CType(dtr("nombre_curso"), String)
                    End With
                    FlowLayoutPanel1.Controls.Add(maquinaCheck)
                    i = i + 1
                Next
                cn.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub


    Private Sub reasignar_cursos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox4.SelectedIndex = 0
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim mChck As Boolean = False
        For Each c As Control In FlowLayoutPanel1.Controls
            Dim chkbox As CheckBox = TryCast(c, CheckBox)
            If chkbox IsNot Nothing AndAlso chkbox.Checked = True Then
                mChck = True
                Exit For
            End If
        Next
        If mChck = False Then
            MsgBox("Debes seleccionar al menos 1 curso / servicio.", vbExclamation, "RNV")
        Else
            Try
                Using cn As New OleDbConnection(s.sQuery)
                    cn.Open()
                    Dim cAdapter As New OleDbDataAdapter
                    Dim idAlum =main.txt_realID.Text
                    Dim cmd2 As New OleDbCommand("DELETE * FROM rnv_alumnos_cursos WHERE id_alumno = " & idAlum & "", cn)
                    cmd2.ExecuteNonQuery()
                    For Each c As Control In FlowLayoutPanel1.Controls
                        If TypeOf c Is CheckBox Then
                            Dim chk As CheckBox
                            Dim count As Integer
                            count += 1
                            chk = CType(c, CheckBox)
                            If chk.Checked Then
                                Dim idcurso = chk.Name
                                cAdapter.InsertCommand = New OleDbCommand("INSERT INTO rnv_alumnos_cursos (id_alumno, id_curso) VALUES (" & idAlum & ", " & idcurso & ")")
                                cAdapter.InsertCommand.Connection = cn
                                cAdapter.InsertCommand.ExecuteNonQuery()
                            End If
                        End If
                    Next
                    Threading.Thread.Sleep(1000)
                    MsgBox("Cursos / Servicios reasignados correctamente.", vbInformation)
                    recargarCursos()
                    Me.Close()
                    cn.Close()
                End Using
            Catch ex As Exception
                MsgBox("Ocurrió un error", vbExclamation, "RNV")
            End Try


        End If
    End Sub

    Private Sub reasignar_cursos_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        main.Enabled = True
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox4.SelectedIndexChanged
        Try
            FlowLayoutPanel1.Controls.Clear()

            If ComboBox4.Text = "Academia NV" Then
                reasignarNV()
            Else
                reasignarVNET()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class