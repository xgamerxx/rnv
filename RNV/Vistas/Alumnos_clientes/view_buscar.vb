﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class view_buscar
    Dim s As New cConexion
    Public consulta As Boolean = False
    Sub filtrar_nv(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select id, t_id, nombres_alumno, direccion, fecha_registro, codigo_alumno, correo from rnv_alumnos WHERE mes_pago_nv > 0 AND ciclo = " & main.TextBox42.Text & " AND nombres_alumno LIKE '%" & palabra & "%' OR codigo_alumno LIKE '%" & palabra & "%'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(0).Width = 170
                gridProductos.Columns(1).HeaderText = "Código"
                gridProductos.Columns(1).Width = 170
                gridProductos.Columns(2).HeaderText = "Nombre completo"
                gridProductos.Columns(2).Width = 170
                gridProductos.Columns(3).HeaderText = "Dirección"
                gridProductos.Columns(3).Width = 170
                gridProductos.Columns(4).HeaderText = "Fecha de registro"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Codigo alumno"
                gridProductos.Columns(6).HeaderText = "Correo"
                gridProductos.Enabled = True
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Using
    End Sub

    Sub filtrar_vnet(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select id, t_id, nombres_alumno, direccion, fecha_registro, codigo_alumno, correo from rnv_alumnos WHERE mes_pago_vnet > 0 AND nombres_alumno LIKE '%" & palabra & "%' OR codigo_alumno LIKE '%" & palabra & "%'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(0).Width = 170
                gridProductos.Columns(1).HeaderText = "Código"
                gridProductos.Columns(1).Width = 170
                gridProductos.Columns(2).HeaderText = "Nombre completo"
                gridProductos.Columns(2).Width = 170
                gridProductos.Columns(3).HeaderText = "Dirección"
                gridProductos.Columns(3).Width = 170
                gridProductos.Columns(4).HeaderText = "Fecha de registro"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Codigo alumno"
                gridProductos.Columns(6).HeaderText = "Correo"
                gridProductos.Enabled = True
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Using
    End Sub

    Sub filtrar_inventario(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select id, codigo, nombre_producto, precio_compra, precio_venta, precio_minimo, descripcion, tipo from rnv_inventario WHERE nombre_producto LIKE '%" & palabra & "%' OR codigo LIKE '%" & palabra & "%'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(0).Width = 170
                gridProductos.Columns(1).HeaderText = "Código "
                gridProductos.Columns(1).Width = 170
                gridProductos.Columns(2).HeaderText = "Nombre"
                gridProductos.Columns(2).Width = 170
                gridProductos.Columns(3).HeaderText = "Precio compra"
                gridProductos.Columns(3).Width = 170
                gridProductos.Columns(4).HeaderText = "Precio venta"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Precio mínimo"
                gridProductos.Columns(5).Width = 170
                gridProductos.Columns(6).HeaderText = "Descripción"
                gridProductos.Columns(6).Width = 170
                gridProductos.Columns(7).HeaderText = "Tipo"
                gridProductos.Enabled = True
            Catch ex As Exception
            End Try
        End Using
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
       main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        If txtbus.Text <> "" Then
            If ComboBox1.SelectedIndex = 2 Then
                filtrar_inventario(txtbus.Text)
            ElseIf ComboBox1.SelectedIndex = 0 Then
                filtrar_nv(txtbus.Text)
            Else
                filtrar_vnet(txtbus.Text)
            End If
        End If
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick
        If IsDataGridViewEmpty(gridProductos) = False Then
            If consulta = False Then
                If ComboBox1.SelectedIndex = 2 Then
                    main.cbox_select.Items.Clear()
                    main.cbox_select.Items.Add("Inventario NV")
                    main.cbox_select.SelectedIndex = 0
                    Dim row As DataGridViewRow = gridProductos.CurrentRow
                    main.txt_client.Text = CStr(row.Cells(2).Value)
                    main.txt_descripcion.Text = CStr(row.Cells(2).Value)
                    main.txt_codigo.Text = CStr(row.Cells(1).Value)
                    main.idMaster.Text = CStr(row.Cells(0).Value)
                    'MsgBox(CStr(row.Cells(4).Value))
                    main.cobro_precio.Text = CStr(row.Cells(4).Value)
                    main.prenormal.Text = CStr(row.Cells(4).Value)
                    main.preminimo.Text = CStr(row.Cells(5).Value)
                    main.tipo_producto.Text = CStr(row.Cells(7).Value)
                    main.groupCobro.Enabled = True
                Else
                    main.inicializarCobro()
                   main.cbox_select.Items.Clear()
                   main.cbox_select.Items.Add("Academia NV")
                   main.cbox_select.Items.Add("Vision NET")
                   main.cbox_select.SelectedIndex = 0
                   main.Label21.Text = "Precio *"
                   main.LinkLabel6.Visible = False
                    Dim row As DataGridViewRow = gridProductos.CurrentRow
                   main.txt_client.Text = CStr(row.Cells(2).Value)
                   main.txtdireccion.Text = CStr(row.Cells(3).Value)
                   main.idx.Text = CStr(row.Cells(1).Value)
                   main.idMaster.Text = CStr(row.Cells(0).Value)
                   main.codiestu.Text = CStr(row.Cells(5).Value)
                    Try
                       main.micorreo.Text = CStr(row.Cells(6).Value)
                    Catch ex As Exception
                    End Try
                End If
               main.cbox_select.Enabled = True
               main.Enabled = True
                Me.Close()
            Else
                Dim row As DataGridViewRow = gridProductos.CurrentRow
               main.jtid.Text = CStr(row.Cells(0).Value)
               main.TextBox14.Text = CStr(row.Cells(2).Value)
               main.Enabled = True
                Me.Close()
            End If
        End If
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub


    Public is_consulta As Boolean = False
    Private Sub view_buscar_Load(sender As Object, e As EventArgs) Handles Me.Load
        ComboBox1.SelectedIndex = 0
        If is_consulta = True Then
            ComboBox1.SelectedIndex = 2
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            Label1.Text = "Buscar por Nombre y apellido ó código"
            txtbus.Text = ""
        ElseIf ComboBox1.SelectedIndex = 1 Then
            Label1.Text = "Buscar por Nombre y apellido ó código"
            txtbus.Text = ""
        Else
            Label1.Text = "Buscar por nombre del producto ó código"
            txtbus.Text = ""
        End If
    End Sub
End Class