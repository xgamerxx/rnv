﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV


Public Class buscar_venta
    Dim s As New cConexion
    Public consulta As Boolean = False
    Sub filtrar(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select id, id_recibo, por, total_venta, fecha, comentario FROM rnv_cobros WHERE ciclo = " & main.TextBox42.Text & " AND giro = 'Inventario NV' AND ( id_recibo LIKE '%" & palabra & "%' OR comentario LIKE '%" & palabra & "%')"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridProductos.DataSource = dv
                gridProductos.Columns(0).HeaderText = "ID"
                gridProductos.Columns(0).Width = 170
                gridProductos.Columns(1).HeaderText = "No. Venta"
                gridProductos.Columns(1).Width = 170
                gridProductos.Columns(2).HeaderText = "Realizada por"
                gridProductos.Columns(2).Width = 170
                gridProductos.Columns(3).HeaderText = "Total de la venta"
                gridProductos.Columns(3).Width = 170
                gridProductos.Columns(4).HeaderText = "Fecha de venta"
                gridProductos.Columns(4).Width = 170
                gridProductos.Columns(5).HeaderText = "Comentario"
                gridProductos.Enabled = True
            Catch ex As Exception
            End Try
        End Using
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
       main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        filtrar(txtbus.Text)
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridProductos.DoubleClick
        If IsDataGridViewEmpty(gridProductos) = False Then
            Dim row As DataGridViewRow = gridProductos.CurrentRow
           main.TextBox45.Text = CStr(row.Cells(1).Value)
            Me.Close()
        End If
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub

End Class