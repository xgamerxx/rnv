﻿Imports coreRNV
Imports System.Data.OleDb
Imports System.ComponentModel

Public Class view_buscar_inventario
    Dim s As New cConexion

    Public concepto As String

    Sub filtrar(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select * FROM rnv_inventario WHERE nombre_producto LIKE '%" & palabra & "%' OR codigo LIKE '%" & palabra & "%'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridCursos.DataSource = dv
            Catch ex As Exception
            End Try
        End Using
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
       main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        filtrar(txtbus.Text)
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function

    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridCursos.DoubleClick
        If IsDataGridViewEmpty(gridCursos) = False Then
            Dim row As DataGridViewRow = gridCursos.CurrentRow
            If concepto = "consulta" Then
               main.TextBox44.Text = CStr(row.Cells(1).Value)
            ElseIf concepto = "consulta_stock" Then
               main.TextBox51.Text = CStr(row.Cells(1).Value)
            Else
               main.cod_inventario.Text = CStr(row.Cells(1).Value)
               main.TextBox36.Text = CStr(row.Cells(2).Value)
               main.TextBox37.Text = CStr(row.Cells(3).Value)
               main.TextBox38.Text = CStr(row.Cells(4).Value)
               main.TextBox39.Text = CStr(row.Cells(5).Value)
               main.RichTextBox3.Text = CStr(row.Cells(6).Value)
                Dim tipo = CStr(row.Cells(7).Value)
                If (tipo = "servicio") Then
                   main.CheckBox3.Checked = True
                   main.Button38.Visible = False
                   main.Button39.Visible = True
                   main.Button36.Visible = False
                Else
                   main.CheckBox3.Checked = False
                   main.Button38.Visible = True
                   main.Button39.Visible = False
                   main.Button36.Visible = True
                End If
            End If
            Me.Close()
        End If
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        filtrar(txtbus.Text)
    End Sub

    Private Sub view_buscar_inventario_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub
End Class