﻿Imports System.ComponentModel

Public Class view_update_stock
    Private Sub view_update_stock_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub view_update_stock_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label2.Text = cargarStock(view_principal.cod_inventario.Text) & " en Stock actual"
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            RadioButton2.Checked = False
        Else
            RadioButton1.Checked = False
            RadioButton2.Checked = True
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            RadioButton1.Checked = False
        Else
            RadioButton2.Checked = False
            RadioButton1.Checked = True
        End If
    End Sub

    Dim a = True

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas generar este movimiento?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim stock, tipo
            If RadioButton1.Checked = True Then
                stock = "+ " & NumericUpDown1.Value
                tipo = "positivo"
            Else
                stock = "- " & NumericUpDown1.Value
                tipo = "negativo"
            End If
            If actualizar_stock(stock, RichTextBox1.Text, tipo, NumericUpDown1.Value) = True Then
                MsgBox("Movimiento realizado.", vbInformation, "RNV")
                Me.Close()
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub
End Class