﻿Imports System.ComponentModel

Public Class view_stock
    Private Sub view_stock_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub view_stock_Load(sender As Object, e As EventArgs) Handles Me.Load
        main.tipo = "producto"
    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click
        Dim result As Integer = MessageBox.Show("¿Estás seguro que deseas guardar esto?", "RNV", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            If salvarInventario(main.TextBox36.Text, main.TextBox37.Text, main.TextBox38.Text, main.TextBox39.Text, main.RichTextBox3.Text, main.id_inventario.Text, main.tipo, NumericUpDown1.Value, main.cod_inventario.Text) = True Then
                MsgBox("Registro completado.", vbInformation, "RNV")
                main.limpiar_inventario()
            Else
                MsgBox("Ocurrió un error.", vbExclamation, "RNV")
            End If
        End If
    End Sub
End Class