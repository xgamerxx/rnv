﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports coreRNV
Imports RNV
Public Class view_buscar_cliente
    Dim s As New cConexion

    Sub filtrar_vnet(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select codigo_curso, nombre_curso, desc_curso, mensualidad_curso, cobro_para, ciclo from rnv_cursos WHERE cobro_para = 'Vision NET' AND nombre_curso LIKE '%" & palabra & "%' OR codigo_curso LIKE '%" & palabra & "%'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridCursos.DataSource = dv
                gridCursos.Columns(0).HeaderText = "Código"
                gridCursos.Columns(1).HeaderText = "Nombre"
                gridCursos.Columns(2).HeaderText = "Descripción"
                gridCursos.Columns(3).HeaderText = "Mensualidad"
                gridCursos.Columns(4).HeaderText = "Cobro para"
                gridCursos.Columns(5).HeaderText = "Ciclo"
            Catch ex As Exception
            End Try
        End Using
    End Sub
    Sub filtrar(palabra As String)
        Using cn As New OleDbConnection(s.sQuery)
            Dim consulta As String = "select codigo_curso, nombre_curso, desc_curso, mensualidad_curso, cobro_para, ciclo from rnv_cursos WHERE cobro_para = 'Academia NV' AND ciclo = " & main.TextBox42.Text & " AND nombre_curso LIKE '%" & palabra & "%' OR codigo_curso LIKE '%" & palabra & "%'"
            Dim cmd As New OleDbCommand(consulta, cn)
            Dim adaptador As New OleDbDataAdapter(cmd)
            Try
                Dim dt As New DataTable
                adaptador.Fill(dt)
                Dim dv As DataView = dt.DefaultView
                gridCursos.DataSource = dv
                gridCursos.Columns(0).HeaderText = "Código"
                gridCursos.Columns(1).HeaderText = "Nombre"
                gridCursos.Columns(2).HeaderText = "Descripción"
                gridCursos.Columns(3).HeaderText = "Mensualidad"
                gridCursos.Columns(4).HeaderText = "Cobro para"
                gridCursos.Columns(5).HeaderText = "Ciclo"
            Catch ex As Exception
            End Try
        End Using
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
       main.Enabled = True
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtbus.TextChanged
        If txtbus.Text <> "" Then
            filtrar(txtbus.Text)
        Else
            Try
                gridCursos.Rows.Clear()
            Catch ex As Exception
            End Try
        End If
    End Sub
    '' -- valida datagrid vació
    Public Function IsDataGridViewEmpty(ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = True
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If Not String.IsNullOrEmpty(cell.Value) Then
                    If Not String.IsNullOrEmpty(Trim(cell.Value.ToString())) Then
                        isEmpty = False
                        Exit For
                    End If
                End If
            Next
        Next
        Return isEmpty
    End Function
    Private Sub drpro_DoubleClick(sender As Object, e As EventArgs) Handles gridCursos.DoubleClick
        If IsDataGridViewEmpty(gridCursos) = False Then
            Dim row As DataGridViewRow = gridCursos.CurrentRow
            main.txt_cod_curso.Text = CStr(row.Cells(0).Value)
            main.txt_curso.Text = CStr(row.Cells(1).Value)
            main.txt_desc.Text = CStr(row.Cells(2).Value)
            main.txt_precio.Text = CStr(row.Cells(3).Value)
            main.txt_ciclo_curso.Text = CStr(row.Cells(4).Value)
            Dim servi As Integer =main.cbox_servi.FindString(CStr(row.Cells(4).Value))
            main.cbox_servi.SelectedIndex = servi
            main.Enabled = True
            main.btn_delete_curso.Visible = True
            Me.Close()

        End If
    End Sub

    Private Sub view_buscar_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
       main.Enabled = True
    End Sub

    Private Sub BunifuTextbox1_OnTextChange(sender As Object, e As EventArgs)
        If ComboBox1.SelectedIndex = 0 Then
            filtrar(txtbus.Text)
        Else
            filtrar_vnet(txtbus.Text)
        End If

    End Sub

    Private Sub view_buscar_cliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.SelectedIndex = 0
    End Sub
End Class