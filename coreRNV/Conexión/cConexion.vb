﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data.OleDb
Imports System.Net

''   Core de RNV   ''

''' <summary>
''' Desarrollador: Luis Leiva
''' Clase: Core
''' Version: 1.0.0
''' </summary>
''' <remarks></remarks>
''' 
Public Class cConexion

    ''--- ARCHIVOS DEL SISTEMA ''
    Public confBD As String = My.Application.Info.DirectoryPath & "\sys\rnv_bdd.md"
    Public rutaSistema As String = My.Application.Info.DirectoryPath & "\sys\"
    Public rutaArchivos As String = My.Application.Info.DirectoryPath & "\"
    '/ se obtiene la ruta del manejador de ruta de archivos
    Public strRuta As String = File.ReadAllText(confBD)

    '/ se localiza el indice donde se encuentra ubicado el archivo mdb
    Public bdd As String = strRuta

    '/se hace un string de conexion a la base de datos
    Public sQuery As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & bdd & "; Jet OLEDB:Database Password=soylapolla12"
    Public strAccess As New OleDbConnection(sQuery)
    '///////

    '/ se abre la conexión local para posteriomente probar su estado
    Sub abrir_conexion_local()
        Try
            strAccess.Open()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    '/ se cierra la conexión local luego de haber probado su estado
    Sub cerrar_conexion_local()
        Try
            strAccess.Close()
        Catch ex As Exception
        End Try
    End Sub

    '/ se prueba la conexion con la base de datos local
    Function probar_conexion_local() As Boolean
        abrir_conexion_local()
        If strAccess.State = ConnectionState.Open Then
            strAccess.Close()
            ' start_conexion()
            Return True
        Else
            Return False
        End If
    End Function

    '/ se prueba la conexion a internet
    Function probar_conexion_internet() As Boolean
        Try
            Using client = New WebClient()
                Using stream = client.OpenRead("http://google.com/")
                    Return True
                End Using
            End Using
        Catch
            Return False
        End Try
    End Function

    Function constructorBDD(tbd)
        Return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & tbd & "; Jet OLEDB:Database Password=soylapolla12"
    End Function

End Class
