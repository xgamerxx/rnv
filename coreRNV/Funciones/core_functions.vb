﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions

Public Module funciones_core
    '' instancia a clase de conexión
    Private s As New cConexion

    '' ------------- GUARDAR NUEVA RUTA BDD --------------------

    Function guardar_ruta_DBB(ruta As String) As Boolean
        Dim objWriter As New System.IO.StreamWriter(s.confBD.ToString)
        objWriter.Write(ruta)
        objWriter.Close()
        Return True
    End Function


    '' ------------- FUNCIONES GENERALES DEL CORE ------------------

    '///////////// Cadena de de textos y numeros aleatorios
    Function cadena_aleatoria(logitud As Integer) As String
        Dim o As New Random()
        Dim posibles As String = "1A2B3C4E5F6G7H8I9J"
        Dim longitud As Integer = posibles.Length
        Dim letra As Char
        Dim longitudnuevacadena As Integer = logitud
        Dim nuevacadena As String = ""
        For i As Integer = 0 To longitudnuevacadena - 1
            letra = posibles(o.[Next](longitud))
            nuevacadena += letra.ToString()
        Next
        Return nuevacadena
    End Function

    '' calcula el porcentaje de ganancia de un producto
    Function calcularPorcentaje(ByVal cantidad As Decimal, ByVal tipoPorcentaje As Decimal) As Decimal
        Return (FormatNumber(cantidad * tipoPorcentaje, 2))
    End Function

    '' validar decimales en editar
    Function validarDecimal(cadena As String) As Boolean
        If Regex.IsMatch(cadena, "^[0-9.]*$") Then
            Return True
        Else
            Return False
        End If
        Return False
    End Function

    '/////////////////// Pasar Mes a string

    Function mess(ByVal este As String)
        Try
            Dim mes As String = ""
            Select Case este
                Case 1
                    mes = "Enero"
                Case 2
                    mes = "Febrero"
                Case 3
                    mes = "Marzo"
                Case 4
                    mes = "Abril"
                Case 5
                    mes = "Mayo"
                Case 6
                    mes = "Junio"
                Case 7
                    mes = "Julio"
                Case 8
                    mes = "Agosto"
                Case 9
                    mes = "septiembre"
                Case 10
                    mes = "Octubre"
                Case 11
                    mes = "Noviembre"
                Case 12
                    mes = "Diciembre"
            End Select
            Return mes
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical, "Excepción")
        End Try
        Return ""
    End Function


    Function mes_string(ByVal fecha As String)
        Dim mes As String
        Dim meses As Integer = Month(fecha)
        mes = Nothing
        Try
            Select Case meses
                Case 1
                    mes = "Enero"
                Case 2
                    mes = "Febrero"
                Case 3
                    mes = "Marzo"
                Case 4
                    mes = "Abril"
                Case 5
                    mes = "Mayo"
                Case 6
                    mes = "Junio"
                Case 7
                    mes = "Julio"
                Case 8
                    mes = "Agosto"
                Case 9
                    mes = "septiembre"
                Case 10
                    mes = "Octubre"
                Case 11
                    mes = "Noviembre"
                Case 12
                    mes = "Diciembre"
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical, "Excepción")
        End Try
        Return meses
    End Function

    '/////////////// Funcion para leer cantidades
    Function Num2Text(ByVal value As Long) As String
        Select Case value
            Case 0 : Num2Text = "CERO"
            Case 1 : Num2Text = "UN"
            Case 2 : Num2Text = "DOS"
            Case 3 : Num2Text = "TRES"
            Case 4 : Num2Text = "CUATRO"
            Case 5 : Num2Text = "CINCO"
            Case 6 : Num2Text = "SEIS"
            Case 7 : Num2Text = "SIETE"
            Case 8 : Num2Text = "OCHO"
            Case 9 : Num2Text = "NUEVE"
            Case 10 : Num2Text = "DIEZ"
            Case 11 : Num2Text = "ONCE"
            Case 12 : Num2Text = "DOCE"
            Case 13 : Num2Text = "TRECE"
            Case 14 : Num2Text = "CATORCE"
            Case 15 : Num2Text = "QUINCE"
            Case Is < 20 : Num2Text = "DIECI" & Num2Text(value - 10)
            Case 20 : Num2Text = "VEINTE"
            Case Is < 30 : Num2Text = "VEINTI" & Num2Text(value - 20)
            Case 30 : Num2Text = "TREINTA"
            Case 40 : Num2Text = "CUARENTA"
            Case 50 : Num2Text = "CINCUENTA"
            Case 60 : Num2Text = "SESENTA"
            Case 70 : Num2Text = "SETENTA"
            Case 80 : Num2Text = "OCHENTA"
            Case 90 : Num2Text = "NOVENTA"
            Case Is < 100 : Num2Text = Num2Text(Int(value \ 10) * 10) & " Y " & Num2Text(value Mod 10)
            Case 100 : Num2Text = "CIEN"
            Case Is < 200 : Num2Text = "CIENTO " & Num2Text(value - 100)
            Case 200, 300, 400, 600, 800 : Num2Text = Num2Text(Int(value \ 100)) & "CIENTOS"
            Case 500 : Num2Text = "QUINIENTOS"
            Case 700 : Num2Text = "SETECIENTOS"
            Case 900 : Num2Text = "NOVECIENTOS"
            Case Is < 1000 : Num2Text = Num2Text(Int(value \ 100) * 100) & " " & Num2Text(value Mod 100)
            Case 1000 : Num2Text = "MIL"
            Case Is < 2000 : Num2Text = "MIL " & Num2Text(value Mod 1000)
            Case Is < 1000000 : Num2Text = Num2Text(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then Num2Text = Num2Text & " " & Num2Text(value Mod 1000)
            Case 1000000 : Num2Text = "UN MILLON"
            Case Is < 2000000 : Num2Text = "UN MILLON " & Num2Text(value Mod 1000000)
            Case Is < 1000000000000 : Num2Text = Num2Text(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000) * 1000000)
            Case 1000000000000 : Num2Text = "UN BILLON"
            Case Is < 2000000000000 : Num2Text = "UN BILLON " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            Case Else : Num2Text = Num2Text(Int(value / 1000000000000.0#)) & " BILLONES"
        End Select
    End Function

    '' -------- FTP ''''''''''''''''''''''''''''''''''
    ''Credenciales FTPS

    '/// Para DEVELOPMENT
    'Public rutaFTP As String = "ftp://flippyescolar.com/RNV/", userFTP As String = "u896497622.outfilesflippy", passFTP As String = "4GoziVqS8EwB"
    '/// para PRODUCTION
    Public rutaFTP As String = "ftp://10.0.10.1/disk1/PNV_BDD/", userFTP As String = "admin", passFTP As String = "Proyectosanv"

    ''Ruta guardar FTPS
    Public rutaSAVE As String = My.Application.Info.DirectoryPath & "\sys\"

    '/// Esto es lo que la mosi puede aportar a tu proyecto de RNV 
    '/// Un comentario porque es bien inutil programando
    '/// PD TE AMO

    '** DESCARGAR FTP
    Function DownloadFile(fileName As String, destination As String)

        Try

            Dim destinationFile, ftpURI As String

            ftpURI = rutaFTP & fileName
            destinationFile = rutaSAVE & destination

            Dim FTPRequest As FtpWebRequest = FtpWebRequest.Create(ftpURI)
            With FTPRequest
                .EnableSsl = False
                .Credentials = New NetworkCredential(userFTP, passFTP)
                .KeepAlive = False
                .UseBinary = True
                .UsePassive = True
                .Method = System.Net.WebRequestMethods.Ftp.DownloadFile
            End With

            Using FTPResponse As System.Net.FtpWebResponse = CType(FTPRequest.GetResponse, System.Net.FtpWebResponse)
                Using responseStream As IO.Stream = FTPResponse.GetResponseStream
                    Using fs As New IO.FileStream(destinationFile, IO.FileMode.Create)
                        Dim buffer(2047) As Byte
                        Dim read As Integer = 0
                        Do
                            read = responseStream.Read(buffer, 0, buffer.Length)
                            fs.Write(buffer, 0, read)
                        Loop Until read = 0

                        responseStream.Close()
                        fs.Flush()
                        fs.Close()
                    End Using
                    responseStream.Close()
                End Using
                FTPResponse.Close()
            End Using
            Return "true"
        Catch ex As Exception
            Return "false"
        End Try
    End Function

    '** Subir al FTP
    Function UploadFile(ByVal _FileName As String, ByVal _Folder As String) As Boolean
        Try
            Dim _FileInfo As New System.IO.FileInfo(_FileName)
            Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(rutaFTP & _Folder)), System.Net.FtpWebRequest)
            _FtpWebRequest.Credentials = New System.Net.NetworkCredential(userFTP, passFTP)
            _FtpWebRequest.KeepAlive = False
            _FtpWebRequest.Timeout = 20000
            _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile
            _FtpWebRequest.UseBinary = True
            _FtpWebRequest.ContentLength = _FileInfo.Length
            Dim buffLength As Integer = 5048
            Dim buff(buffLength - 1) As Byte
            Dim _FileStream As System.IO.FileStream = _FileInfo.OpenRead()

            Try
                Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()
                Dim contentLen As Integer = _FileStream.Read(buff, 0, buffLength)
                Do While contentLen <> 0
                    _Stream.Write(buff, 0, contentLen)
                    contentLen = _FileStream.Read(buff, 0, buffLength)
                Loop
                _Stream.Close()
                _Stream.Dispose()
                _FileStream.Close()
                _FileStream.Dispose()
                Return True
            Catch ex As Exception
                Return False
            End Try
        Catch ex As Exception
            Return False
        End Try

    End Function

    Sub ocupar()
        Try
            'Dim file As System.IO.File

            File.WriteAllText(s.rutaSistema & "isbusy.rnv", "true")

            ' Get the object used to communicate with the server.
            Dim request = CType(WebRequest.Create(rutaFTP & "isbusy.rnv"), FtpWebRequest)
            request.Method = WebRequestMethods.Ftp.UploadFile

            ' This example assumes the FTP site uses anonymous logon.
            request.Credentials = New NetworkCredential(userFTP, passFTP)

            ' Copy the contents of the file to the request stream.
            Dim sourceStream = New StreamReader(s.rutaSistema & "isbusy.rnv")
            Dim fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd())
            sourceStream.Close()
            request.ContentLength = fileContents.Length

            Dim requestStream = request.GetRequestStream()
            requestStream.Write(fileContents, 0, fileContents.Length)
            requestStream.Close()

            Dim response = CType(request.GetResponse(), FtpWebResponse)


            response.Close()
        Catch ex As Exception
        End Try
    End Sub

    Sub liberar()
        Try
            'Dim file As System.IO.File
            File.WriteAllText(s.rutaSistema & "isbusy.rnv", "false")

            ' Get the object used to communicate with the server.
            Dim request = CType(WebRequest.Create(rutaFTP & "isbusy.rnv"), FtpWebRequest)
            request.Method = WebRequestMethods.Ftp.UploadFile

            ' This example assumes the FTP site uses anonymous logon.
            request.Credentials = New NetworkCredential(userFTP, passFTP)

            ' Copy the contents of the file to the request stream.
            Dim sourceStream = New StreamReader(s.rutaSistema & "isbusy.rnv")
            Dim fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd())
            sourceStream.Close()
            request.ContentLength = fileContents.Length

            Dim requestStream = request.GetRequestStream()
            requestStream.Write(fileContents, 0, fileContents.Length)
            requestStream.Close()

            Dim response = CType(request.GetResponse(), FtpWebResponse)


            response.Close()
        Catch ex As Exception

        End Try
    End Sub



    '' Auto Backup Local BDD
    Function func_backupBDD()
        Try
            If My.Computer.FileSystem.DirectoryExists(My.Application.Info.DirectoryPath & "\BK") = False Then
                My.Computer.FileSystem.CreateDirectory(My.Application.Info.DirectoryPath & "\BK")
            End If
            If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\BK\rnv_bdd.mdb") = True Then
                My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\BK\rnv_bdd.mdb")
            End If
            If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\BK\current.rnv") = True Then
                My.Computer.FileSystem.DeleteFile(My.Application.Info.DirectoryPath & "\BK\current.rnv")
            End If
            My.Computer.FileSystem.CopyFile(s.rutaSistema & "\rnv_bdd.mdb", My.Application.Info.DirectoryPath & "\BK\rnv_bdd.mdb")
            My.Computer.FileSystem.CopyFile(s.rutaSistema & "\current.rnv", My.Application.Info.DirectoryPath & "\BK\current.rnv")

            Return True
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    '' restaura la base de datos local
    Function func_restoreBDD()
        Try
            If My.Computer.FileSystem.DirectoryExists(My.Application.Info.DirectoryPath & "\BK") = False Then
                My.Computer.FileSystem.CreateDirectory(My.Application.Info.DirectoryPath & "\BK")
            End If
            If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\BK\rnv_bdd.mdb") = True Then
                If My.Computer.FileSystem.FileExists(s.rutaSistema & "\rnv_bdd.mdb") = True Then
                    My.Computer.FileSystem.DeleteFile(s.rutaSistema & "\rnv_bdd.mdb")
                End If
                If My.Computer.FileSystem.FileExists(s.rutaSistema & "\current.rnv") = True Then
                    My.Computer.FileSystem.DeleteFile(s.rutaSistema & "\current.rnv")
                End If
                My.Computer.FileSystem.CopyFile(My.Application.Info.DirectoryPath & "\BK\rnv_bdd.mdb", s.rutaSistema & "\rnv_bdd.mdb")
                My.Computer.FileSystem.CopyFile(My.Application.Info.DirectoryPath & "\BK\current.rnv", s.rutaSistema & "\current.rnv")
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function


End Module
